#include "aux/run_aux.h"
#include "aux/aux_card.h"

int main(int argc, char **argv) {

  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("tag"   ,value< std::string >()->default_value("Aux1"), "AUX Name in OKS Configuration")
    ("slot"  ,value< std::string >()->default_value("-1"), "The card slot.")
    ("config",bool_switch()->default_value(false), "Run the CONFIG step.")
    ("run"   ,bool_switch()->default_value(false), "Run the START step.")
    ;

  
  positional_options_description p;
  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  } catch ( ... ) { std::cerr << desc << endl; return 1; }
  notify(vm);

  if (vm.count("help") ) { cout << endl <<  desc << endl ; return 0; }

  string aux_name = vm["tag"].as<std::string>();
  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );

  bool doCONFIG = vm["config"].as<bool>();
  bool doRUN    = vm["run"]   .as<bool>();
  bool doAll    = !doCONFIG && !doRUN;

  cout << "Loading OKS Configuration." << endl;
  std::unique_ptr<::Configuration> db;
  try {
    db.reset(  new ::Configuration("oksconfig:daq/partitions/FTK.data.xml"));
  } catch(daq::config::Generic e) {
    std::cerr << "ERROR: Cannot load OKS partition." << std::endl;
    return -1;
  }

  const daq::ftk::dal::ReadoutModule_Aux* settings = db->get<daq::ftk::dal::ReadoutModule_Aux>(aux_name.c_str());
  if(settings==0)
    {
      std::cerr << "Invalid tag " << aux_name << "! exiting..." << std::endl;
      return -1;
    }

  if (slot < 0) slot = settings->get_Slot();
  cout << "Slot         = " << slot << endl;

  daq::ftk::aux a(slot, ""); // create aux 
  a.setup(settings);
  if(doCONFIG || doAll)
    {
      sleep(2);
      a.configure();
      sleep(2);
      a.connect();
    }
  if(doRUN || doAll)
    {
      sleep(2);
      a.prepareForRun();
    }

  // delete db; // this deletion is fucked up for some reason.

  return 0;

}


/*           Program to issue a routine for read the AUX RAM                   */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */

#include <string>
#include <iostream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "ftkvme/VMEManager.h"

#include "aux/aux.h"
#include "aux/aux_spy_regs.h"
#include "aux/aux_vme_regs.h"

namespace daq 
{
  namespace ftk 
  {

    bool check_dataflow(int slot)
    {
      //I think the rate should be common to all fpgas 
      VMEInterface *proc1 =VMEManager::global().aux(slot,3);



      aux_linkstatus_t link_status=aux_load_linkstatus(proc1,0x56,0,LINK_STATUS,12);

      if(link_status.ratePackets == 0)
	      return false;
      else
	      return true;
    }
    
    bool check_freeze (int slot)
    {

      VMEInterface *input1=VMEManager::global().aux(slot,1);
      VMEInterface *input2=VMEManager::global().aux(slot,2);
      VMEInterface *proc1 =VMEManager::global().aux(slot,3);
      VMEInterface *proc2 =VMEManager::global().aux(slot,4);
      VMEInterface *proc3 =VMEManager::global().aux(slot,5);
      VMEInterface *proc4 =VMEManager::global().aux(slot,6);
      VMEInterface *fpgas[6]={input1,input2,proc1,proc2,proc3,proc4};

      u_int register_content;
      bool freeze_high = false;
      
      for(unsigned int fpgaidx=0;fpgaidx<6;fpgaidx++)
	{
	  register_content=fpgas[fpgaidx]->read_word(FREEZE_STATUS);
	  if(register_content != 0) freeze_high = true;
	}
      register_content=fpgas[1]->read_word(HW_FREEZE_STATUS);
      if(register_content != 0) freeze_high = true;

      return freeze_high;
    }


    void freeze_inputs(int slot)
    {
      // Pulse freeze request
      VMEManager::global().aux(slot,1)->write_word(TMODE,0x1);
      VMEManager::global().aux(slot,2)->write_word(TMODE,0x1);
      
      VMEManager::global().aux(slot,1)->write_bit(FREEZE_REQ, BIT_FREEZE_REQ, true);
      VMEManager::global().aux(slot,2)->write_bit(FREEZE_REQ, BIT_FREEZE_REQ, true);
      VMEManager::global().aux(slot,1)->write_bit(FREEZE_REQ, BIT_FREEZE_REQ, false);
      VMEManager::global().aux(slot,2)->write_bit(FREEZE_REQ, BIT_FREEZE_REQ, false);

      VMEManager::global().aux(slot,1)->write_word(TMODE,0x0);
      VMEManager::global().aux(slot,2)->write_word(TMODE,0x0);

      std::cout << "Waiting on freeze to propogate  ..." << std::endl;
      // Kept for compability with old firmware
      // TODO remove!
      u_int register_content=0;
      VMEInterface *vme=0;
      for (int fpga = 1; fpga <= 6; fpga++)
	{
	  while (!register_content)
	    {
	      vme=VMEManager::global().aux(slot,fpga);
	      register_content=vme->read_word(FREEZE_STATUS);
	      usleep(1000);
	    }
	}
    }


    void vme_acknowledge(int slot)
    {
      for (int fpga = 1; fpga <= 6; fpga++)
	{
	  // toggle VME read ack
	  VMEManager::global().aux(slot,fpga)->write_word(TMODE,0x1);
	  VMEManager::global().aux(slot,fpga)->write_bit(FREEZE_ACK, BIT_FREEZE_ACK, true);
	  VMEManager::global().aux(slot,fpga)->write_bit(FREEZE_ACK, BIT_FREEZE_ACK, false);
	  VMEManager::global().aux(slot,fpga)->write_word(TMODE,0x0);
	}
    }

//    void aux_wait_for_spybuffer_freeze(int slot, int fpga,const std::vector<u_int>& buffer)
//    {
//      VMEInterface *vme=VMEManager::global().aux(slot,fpga);
//      for(auto buff : buffer)
//	{
//	  
//	  u_int buffinfo=0;
//	      buffinfo=vme->read_word(buff<<16);
//	      usleep(1000);
//	    }
//	  while((buffinfo&0x8000)==0);
//	}
//    }
    

    void aux_dump_tf_spybuffers(int slot, const std::string& source)
    {
      //daq::ftk::aux_wait_for_spybuffer_freeze(slot, 2, aux_i2_SB_TF_out);
      daq::ftk::aux_read_buffer(slot, 2, 0, aux_i2_SB_TF_out,       "spy_output/tf_i2.dat",    source);  
      daq::ftk::aux_read_buffer(slot, 3, 0, aux_proc_SB_TF_inputs,  "spy_output/tf_in_p1.dat", source);  
      daq::ftk::aux_read_buffer(slot, 4, 0, aux_proc_SB_TF_inputs,  "spy_output/tf_in_p2.dat", source);  
      daq::ftk::aux_read_buffer(slot, 5, 0, aux_proc_SB_TF_inputs,  "spy_output/tf_in_p3.dat", source);  
      daq::ftk::aux_read_buffer(slot, 6, 0, aux_proc_SB_TF_inputs,  "spy_output/tf_in_p4.dat", source);  

      daq::ftk::aux_read_buffer(slot, 3, 0, aux_proc_SB_TF_out,     "spy_output/tf_out_p1.dat", source);  
      daq::ftk::aux_read_buffer(slot, 4, 0, aux_proc_SB_TF_out,     "spy_output/tf_out_p2.dat", source);  
      daq::ftk::aux_read_buffer(slot, 5, 0, aux_proc_SB_TF_out,     "spy_output/tf_out_p3.dat", source);  
      daq::ftk::aux_read_buffer(slot, 6, 0, aux_proc_SB_TF_out,     "spy_output/tf_out_p4.dat", source);  

    }

    void aux_dump_do_spybuffers(int slot, const std::string& source) {

      daq::ftk::aux_read_buffer(slot, 1, 0, aux_i1_SB_hits_to_proc, "spy_output/do_input_i1.dat", source);  
      daq::ftk::aux_read_buffer(slot, 2, 0, aux_i2_SB_hits_to_proc, "spy_output/do_input_i2.dat", source);  

      daq::ftk::aux_read_buffer(slot, 1, 0, aux_i1_SB_ssids_to_amb, "spy_output/amb_input_i1.dat", source);  
      daq::ftk::aux_read_buffer(slot, 2, 0, aux_i2_SB_ssids_to_amb, "spy_output/amb_input_i2.dat", source);  

      daq::ftk::aux_read_buffer(slot, 3, 0, aux_proc_SB_DO_output,  "spy_output/do_output_p1.dat", source);  
      daq::ftk::aux_read_buffer(slot, 4, 0, aux_proc_SB_DO_output,  "spy_output/do_output_p2.dat", source);  
      daq::ftk::aux_read_buffer(slot, 5, 0, aux_proc_SB_DO_output,  "spy_output/do_output_p3.dat", source);  
      daq::ftk::aux_read_buffer(slot, 6, 0, aux_proc_SB_DO_output,  "spy_output/do_output_p4.dat", source);  

      daq::ftk::aux_read_buffer(slot, 3, 0, aux_proc_SB_DO_roads,   "spy_output/do_roads_p1.dat", source);  
      daq::ftk::aux_read_buffer(slot, 4, 0, aux_proc_SB_DO_roads,   "spy_output/do_roads_p2.dat", source);  
      daq::ftk::aux_read_buffer(slot, 5, 0, aux_proc_SB_DO_roads,   "spy_output/do_roads_p3.dat", source);  
      daq::ftk::aux_read_buffer(slot, 6, 0, aux_proc_SB_DO_roads,   "spy_output/do_roads_p4.dat", source);  


      daq::ftk::aux_read_buffer(slot, 3, 0, aux_proc_SB_hits_in,    "spy_output/hits_in_p1.dat", source);  
      daq::ftk::aux_read_buffer(slot, 4, 0, aux_proc_SB_hits_in,    "spy_output/hits_in_p2.dat", source);  
      daq::ftk::aux_read_buffer(slot, 5, 0, aux_proc_SB_hits_in,    "spy_output/hits_in_p3.dat", source);  
      daq::ftk::aux_read_buffer(slot, 6, 0, aux_proc_SB_hits_in,    "spy_output/hits_in_p4.dat", source);  

    }

    void aux_dump_ss_spybuffers(int slot, const std::string& source) {

      daq::ftk::aux_read_buffer(slot, 1, 0, aux_i1_SB_ssmap_hits,   "spy_output/ss_i1.dat", source);  
      daq::ftk::aux_read_buffer(slot, 2, 0, aux_i2_SB_ssmap_hits,   "spy_output/ss_i2.dat", source);  
      
    }

    void aux_dump_hw_spybuffers(int slot, const std::string& source) {

      daq::ftk::aux_read_buffer(slot, 2, 0, aux_i2_SB_HW_in_out,    "spy_output/hw_i2.dat", source);  

    }
    
    void aux_dump_latency_spybuffers(int slot, const std::string& source) {

      daq::ftk::aux_read_buffer(slot, 3, 0, aux_proc_latency,    "spy_output/latency_p1.dat", source);  
      daq::ftk::aux_read_buffer(slot, 4, 0, aux_proc_latency,    "spy_output/latency_p2.dat", source);  
      daq::ftk::aux_read_buffer(slot, 5, 0, aux_proc_latency,    "spy_output/latency_p3.dat", source);  
      daq::ftk::aux_read_buffer(slot, 6, 0, aux_proc_latency,    "spy_output/latency_p4.dat", source);  
      daq::ftk::aux_read_buffer(slot, 1, 0, aux_i1_latency,      "spy_output/latency_i1.dat", source);  
      daq::ftk::aux_read_buffer(slot, 2, 0, aux_i2_latency,      "spy_output/latency_i2.dat", source);  

    }

    // On each proc, read #packets and L1ID from same reg used by aux_linkstatus.
    // L1IDOffset = L1ID-#packets+1
    // Print 1 per processor, 4 words in 1 file
    void get_DO_L1ID_offset(int slot) {
      //daq::ftk::aux_read(slot, 3, 
    }
    
    void aux_dump_event_spybuffers(int slot, const std::string& source) {

      daq::ftk::aux_read_buffer(slot, 2, 0, aux_i2_per_event,    "spy_output/hw_perEvent.dat", source);  

    }
    
    
  } //namespcae daq
} //namespcae ftk


#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


int main(int argc, char **argv) 
{ 

  using namespace  boost::program_options ;

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot",  value<int>()->default_value(15), "The card slot")
    ("source",value<std::string>()->default_value("vme"), "Where to get the buffers from (vme, emon, txt)")
    ("tf",    bool_switch()->default_value(false),  "Do Track Fitter")
    ("hw",    bool_switch()->default_value(false),  "Do Hit Warrior")
    ("do",    bool_switch()->default_value(false),  "Do Data Organizer")
    ("event",    bool_switch()->default_value(false),  "Do per event")
    ("latency",    bool_switch()->default_value(false),  "Do latency")
    ("ss",    bool_switch()->default_value(false),  "Do SSMap")
    ("all",   bool_switch()->default_value(true),  "Do All")
    ("over_ride_freeze",    bool_switch()->default_value(false),  "Override waiting on the freeze status")
    ;

  positional_options_description p;
  p.add("buffer", -1);

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  } catch( ... ) { // In case of errors during the parsing process, desc is printed for help 
    std::cerr << desc << std::endl; 
    return 1;
  }

  notify(vm);

  if( vm.count("help") ) { // if help is required, then desc is printed to output
    std::cout << std::endl <<  desc << std::endl ; 
    return 0;	
  }

  int slot  = vm["slot"].as<int>();
  std::string source  = vm["source"].as<std::string>();

  bool doTF = vm["tf"].as<bool>();
  bool doHW = vm["hw"].as<bool>();
  bool doDO = vm["do"].as<bool>();
  bool doSS = vm["ss"].as<bool>();
  bool doEB = vm["event"].as<bool>();
  bool doLatency = vm["latency"].as<bool>();

  bool over_ride_freeze = vm["over_ride_freeze"].as<bool>();

  if (vm["all"].as<bool>() and not(doTF or doHW or doDO or doSS or doEB or doLatency)) doTF = doHW = doDO = doSS = doEB = doLatency = true;

    
  if( source == "vme" ){
      bool hasFreeze = daq::ftk::check_freeze(slot);
      bool hasDataflow = daq::ftk::check_dataflow(slot);
      
      if(over_ride_freeze){
        if(hasFreeze){
            std::cout << "Warning!! Board has a freeze!" << std::endl;
        }
        if(!hasDataflow){
            std::cout << "Warning!! Board has no dataflow!" << std::endl;
        }
        std::cout << "Over-riding freeze request" << std::endl;
      }
      else{
        std::cout << "Request freeze" << std::endl;
        if(hasFreeze){
          std:: cout << "BOARD HAS A FREEZE! Adding in freeze override" << std::endl;
          over_ride_freeze = true;
        }
        if(!hasDataflow){
          std::cout << "BOARD HAS NO DATAFLOW! Running dump with freeze override!" << std::endl;
          over_ride_freeze = true;
        }
        if(!over_ride_freeze){
          daq::ftk::freeze_inputs(slot);
        }
      }
  }

  if (doHW) daq::ftk::aux_dump_hw_spybuffers(slot, source);
  if (doTF) daq::ftk::aux_dump_tf_spybuffers(slot, source);
  if (doDO) daq::ftk::aux_dump_do_spybuffers(slot, source);
  if (doSS) daq::ftk::aux_dump_ss_spybuffers(slot, source);
  if (doEB) daq::ftk::aux_dump_event_spybuffers(slot, source);
  if (doLatency) daq::ftk::aux_dump_latency_spybuffers(slot, source);

  if(!over_ride_freeze && source == "vme"){
    std::cout << "Acknowledge freeze" << std::endl;  
    daq::ftk::vme_acknowledge(slot);
  }

  return 0;

}

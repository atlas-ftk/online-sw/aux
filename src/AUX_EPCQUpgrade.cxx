#include "aux/AUX_EPCQUpgrade.h"

#include "ftkcommon/core.h"
#include "ftkvme/VMEManager.h"
#include "aux/aux_vme_regs.h"
#include "aux/aux_vme.h"

#include <iostream>
#include <fstream>
#include <sstream>
//#include <iomanip>

namespace daq 
{
  namespace ftk 
  {
    AUX_EPCQUpgrade::AUX_EPCQUpgrade(const AUX_EPCQUpgrade& o)
      : _vme(o._vme), _fpga(o._fpga), _map(o._map)
    { }

    AUX_EPCQUpgrade::AUX_EPCQUpgrade(uint slot, uint fpga, AUX_FWMap map) 
      : _vme(0), _fpga(fpga), _map(map)
    { 
      _vme=VMEManager::global().aux(slot,fpga);
    }

    bool AUX_EPCQUpgrade::isDone() const
    { return _done; }

    void AUX_EPCQUpgrade::wait_for_done()  
    {      
      if(_firstOp)
	{
	  _firstOp=false;
	  return;
	}

      u_int register_content=0;
      do
	{
	  register_content=_vme->read_word(RU_STATUS);
	  //std::cout << "AUX_EPCQUpgrade::wait_for_done() = " << std::dec << register_content << std::endl;
	}
      while((register_content&2)!=2);

      reset_control();
    }

    void AUX_EPCQUpgrade::reset_control()
    {
      //std::cout << "AUX_EPCQUpgrade::reset_control()" << std::endl;

      _vme->write_word(TMODE,0x1);

      _vme->write_word(RU_EPCQ, 0x0);
      _vme->write_word(RU_ADDR, 0x0);

      _vme->write_word(TMODE,0x0);
    }

    void AUX_EPCQUpgrade::clear_buffer()
    {
      //std::cout << "AUX_EPCQUpgrade::clear_buffer()" << std::endl;

      _vme->write_word(TMODE, 0x1);
      _vme->write_word(RU_EPCQ, 0x10);
      _vme->write_word(TMODE, 0x0);

      _vme->write_word(TMODE, 0x1);
      _vme->write_word(RU_EPCQ, 0x0);
      _vme->write_word(TMODE, 0x0);
    }

    void AUX_EPCQUpgrade::write_init(uint epcqaddress)
    {
      //std::cout << "AUX_EPCQUpgrade::write_init()" << std::endl;
      _fh.open(_map.RPDPath().c_str(), std::ios::binary);
      _fh.seekg(_map.block(0).start_address);

      _done=false;

      _firstOp=true;
      _nextaddr=epcqaddress;
      _bytesread=0;
      //nPercent=0;
      _epcqaddress=epcqaddress;
      
      // Need to read without freeze
      _vme->write_word(TMODE, 0x1);
      _vme->write_bit(FREEZE_ACK, BIT_FREEZE_ACK, 1);
      _vme->write_word(TMODE, 0x0);
    }

    void AUX_EPCQUpgrade::sector_erase()
    {
      //std::cout << "AUX_EPCQUpgrade::sector_erase(_nextaddr=0x" << std::hex << std::setw(8) << std::setfill('0') << _nextaddr << ")" << std::endl;
      if(_done)
	{
	  std::cerr << "AUX_EPCQUpgrade: sector_erase on a done operation!" << std::endl;
	  return;
	}
      wait_for_done();

      _vme->write_word(TMODE,   0x1);
      _vme->write_word(RU_EPCQ, 0x4);
      _vme->write_word(RU_ADDR, _nextaddr);
      _vme->write_word(TMODE,   0x0);
    }

    void AUX_EPCQUpgrade::write_page()
    {
      //std::cout << "AUX_EPCQUpgrade::write_page(_nextaddr=0x" << std::hex << std::setw(8) << std::setfill('0') << _nextaddr << ")" << std::endl;
      if(_done)
	{
	  std::cerr << "AUX_EPCQUpgrade: write on a done operation!" << std::endl;
	  return;
	}
      wait_for_done();

      // Load packet
      _vme->write_word(TMODE, 0x1);

      char c[4];
      uint data;
      uint nBytes=_map.block(0).end_address-_map.block(0).start_address;
      std::vector<uint> block;
      for(uint wi=0;wi<64;wi++)  // wi = word index
	{
	  // Read the next data word
	  _fh.read(c,sizeof(c));
	  data=0;
	  data|=(((unsigned char)c[3])<<0);
	  data|=(((unsigned char)c[2])<<8);
	  data|=(((unsigned char)c[1])<<16);
	  data|=(((unsigned char)c[0])<<24);
	  //PRINT_LOG("DATA " << std::setfill('0') << std::setw(8) << std::hex << data);

	  // Load the data...
	  block.push_back(data);

	  uint nPercent = _bytesread*100/nBytes + 1;
	  uint fpga     = _fpga;
	  //Check progress and report using ASCI control characters. "\033[L"=begining of previous line "\033[K" = clear line "\033[E" = next line
	  if (_bytesread%10000 == 0){
	    for (uint line = 0;line < fpga;line++){std::cout<<"\033[F";}
	    std::cout<<"\033[K"<<std::setw(51)<<std::string(nPercent/2+1,'-')<<std::setw(8)<<"Loaded "<<std::dec<<nPercent<<"% FPGA "<<fpga<<std::flush;
	    for (uint line = 0;line < fpga;line++){std::cout<<"\033[E";}
	  }

	  _bytesread+=4;
	  if(_bytesread>nBytes)
	    {
	      _done=true;
	      break;
	    }
	}

      _vme->write_block(RU_DATA, block);
      _vme->write_word(TMODE, 0x0);

      // Write the contents as 256 byte buffer is full
      _vme->write_word(TMODE, 0x1);
	      
      _vme->write_word(RU_EPCQ, 0x2);
      _vme->write_word(RU_ADDR, _nextaddr);

      _vme->write_word(TMODE, 0x0);
      //std::cout << "BEGIN WRITE AT " << std::setfill('0') << std::setw(8) << std::hex << _nextaddr << std::endl;

      _nextaddr+=256;
    }

    void AUX_EPCQUpgrade::write_deinit()
    {
      // Disable remote update again
      _vme->write_word(RU_ENABLE, 0);

      _vme->write_word(TMODE, 0x1);
      _vme->write_bit(FREEZE_ACK, BIT_FREEZE_ACK, 0);
      _vme->write_word(TMODE, 0x0);
    }

  }
}

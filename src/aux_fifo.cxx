/*           Program to read the status of a fifo
             Written for AMBFTK board v1.0   FTK collaboration                 */

#include <string>
#include <iostream>
#include <fstream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "aux/aux.h"
#include "aux/aux_vme_regs.h"


namespace daq 
{
  namespace ftk 
  {
    aux_fifostatus_t aux_fifostatus(VMEInterface* vme,u_int addr, u_int period)
    {
      u_int register_content,sel;//sel is bits of vme addr 7 downto 4, hence the bitshifts by 4 below
      aux_fifostatus_t fifo_status;

      sel=2<<4;
      register_content = vme->read_word(addr+sel);

      fifo_status.size     =(register_content>>16)&0xffff;
      fifo_status.usedw    =(register_content)    &0x1fff;
      fifo_status.ovrflw   =(register_content>>13)&0x1;
      fifo_status.empty    =(register_content>>14)&0x1;
      fifo_status.hold     =(register_content>>15)&0x1;

      sel=0<<4;
      register_content = vme->read_word(addr+sel);
      fifo_status.holdRate=((float)register_content)/((float)period);//Should be named hold fraction...

      sel=1<<4;
      register_content = vme->read_word(addr+sel);
      fifo_status.usedw_avg=((float)register_content)/(((float)period)/100.0);//firmware can't count beyond 32 bits, average over period/100

      return fifo_status;
    }

    aux_fifostatus_t aux_fifostatus_hitsort(VMEInterface* vme,u_int addr, u_int period)
    {
      u_int register_content,sel;//sel is bits of vme addr 7 downto 4, hence the bitshifts by 4 below
      aux_fifostatus_t fifo_status;

      sel=2<<4;
      register_content = vme->read_word(addr+sel);

      fifo_status.size     =(register_content>>16)&0xffff;
      fifo_status.usedw    =(register_content)    &0x1fff;
      fifo_status.ovrflw   =(register_content>>13)&0x1;
      fifo_status.empty    =(register_content>>14)&0x1;
      fifo_status.hold     =(register_content>>15)&0x1;

      sel=0<<4;
      register_content = vme->read_word(addr+sel);
      fifo_status.holdRate=((float)register_content)/((float)period);//Should be named hold fraction...

      sel=1<<4;
      register_content = vme->read_word(addr+sel);
      fifo_status.usedw_avg=((float)register_content)/(((float)period)/100.0);//firmware can't count beyond 32 bits, average over period/100

      return fifo_status;
    }

  }
}

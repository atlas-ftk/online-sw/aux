#include "aux/aux_check.h"
#include <stdlib.h>

#include "aux/aux_spybuffer_parsers.h"

#include <iostream>

#include "aux/TF/TFEvent.h"
#include "aux/TF/TFEventList.h"
#include "aux/TF/TFConstants.h"
#include "aux/TF/TFFitter.h"

#include "aux/SpyBuffer/EventFragmentDOOutput.h"
#include "aux/SpyBuffer/EventFragmentTrackFTKPacket.h"

#include <ftkcommon/EventFragmentCollection.h>

namespace daq {
  namespace ftk {

    bool aux_tf_compare(const std::string& tf_consts, std::map<u_int, std::vector<u_int> >& tf_in, const std::vector<u_int>& tf_out, bool dumpBitstream, std::string proc, bool debug, bool allSectors, bool is_loop, bool split_sectors)
    {
      std::cout << "TF bit-check ..." ;

      //
      // Parse the data
      //
      if(debug) cout << endl << "Parse DO output" << endl;
      std::vector<EventFragment*> pixhits0=DOOutput_splitFragments(tf_in[0x33]);
      std::vector<EventFragment*> pixhits1=DOOutput_splitFragments(tf_in[0x34]);
      std::vector<EventFragment*> pixhits2=DOOutput_splitFragments(tf_in[0x35]);
      std::vector<EventFragment*> pixhits3=DOOutput_splitFragments(tf_in[0x36]);
      std::vector<EventFragment*> pixhits4=DOOutput_splitFragments(tf_in[0x37]);
      std::vector<EventFragment*> pixhits5=DOOutput_splitFragments(tf_in[0x38]);

      std::vector<EventFragment*> scthits0=DOOutput_splitFragments(tf_in[0x39]);
      std::vector<EventFragment*> scthits1=DOOutput_splitFragments(tf_in[0x3A]);
      std::vector<EventFragment*> scthits2=DOOutput_splitFragments(tf_in[0x3B]);
      std::vector<EventFragment*> scthits3=DOOutput_splitFragments(tf_in[0x3C]);
      std::vector<EventFragment*> scthits4=DOOutput_splitFragments(tf_in[0x3D]);

      std::vector<EventFragment*> sectors =DOOutput_splitFragments(tf_in[0x3E]);
      std::vector<EventFragment*> roads   =DOOutput_splitFragments(tf_in[0x3F]);

      if(debug) cout << "Parse TF output" << endl;
      std::vector<EventFragment*> tracks  =FTKPacket_splitFragments<EventFragmentTrackFTKPacket>(tf_out,0);

      if(debug) cout << "Store event fragments" << endl;
      std::vector<std::vector<EventFragment*> > fragmentss;
      fragmentss.push_back(pixhits0);
      fragmentss.push_back(pixhits1);
      fragmentss.push_back(pixhits2);
      fragmentss.push_back(pixhits3);
      fragmentss.push_back(pixhits4);
      fragmentss.push_back(pixhits5);
      fragmentss.push_back(scthits0);
      fragmentss.push_back(scthits1);
      fragmentss.push_back(scthits2);
      fragmentss.push_back(scthits3);
      fragmentss.push_back(scthits4);

      fragmentss.push_back(sectors );
      fragmentss.push_back(roads   );

      fragmentss.push_back(tracks  );

      if(debug) cout << "Build event fragment collections\n";
      //is_loop allows for less strict event overlap when running same event on a loop
      std::vector<EventFragmentCollection*> eventCollections     =build_eventFragmentCollection(fragmentss, false, 0xFFFFFFFF, is_loop);
      if(eventCollections.size()==0)
	{
	  std::cout << "no overlap" << std::endl;
	  // if(dumpBitstream)
	  //   {
	      
	  //   };
	  return false;
	}

      if(debug) cout << "Split fragment collections by input and output\n";
      std::vector<EventFragmentCollection*> doEventCollections   =subset_eventFragmentCollection(eventCollections,{0,1,2,3,4,5,6,7,8,9,10,11,12});
      std::vector<EventFragmentCollection*> trackEventCollections=subset_eventFragmentCollection(eventCollections,{13});

      if(debug) cout << "Build do_events\n";
      TFEventList do_events("DO Output");
      bool build_error = do_events.buildFromDOOutputEventCollections(eventCollections,debug, stoi(proc));

      if(build_error){
	std::cout << "FAILED" << std::endl;
	return !build_error;
      }

      // Load constants
      if(debug) cout << "Load Constants\n";
      TFConstants *consts = new TFConstants();
      //consts->buildFromFile(tf_consts);
      consts->buildFromGCON(tf_consts);

      // Use the constants to calculate
      // what we think the answer should be.
      if(debug) cout << "Get Chi2\n";
      TFFitter fitter(consts, &do_events);
      
      //      fitter.calculateChiSquares(allSectors, atoi(proc.c_str()));
      fitter.calculateChiSquares(true, atoi(proc.c_str()));

      if(debug) cout << "Build TF Output\n";
      TFEventList tf_events("TF Output");
      tf_events.buildFromTrackEventCollections(trackEventCollections);

      if(debug) cout << "Compare Chip to Sim\n";
      bool allGood=do_events.compareAllEvents(&tf_events, split_sectors);

      if(!allGood) std::cout << "FAILED" << std::endl;
      else         std::cout << "passed" << std::endl;

      //
      // Debug bit stream
      //
      if(dumpBitstream)
	{
	  std::vector<EventFragment*> emuTrackEvents=do_events.dumpToEventFragments();
	  std::vector<EventFragment*> outTrackEvents=tf_events.dumpToEventFragments();

	  dump_eventFragments(emuTrackEvents,"tracks_proc"+proc+"_emu.txt");
	  dump_eventFragments(outTrackEvents,"tracks_proc"+proc+"_out.txt");
	}
      
      return allGood;
    }
  } // namespace ftk
} // namespace daq


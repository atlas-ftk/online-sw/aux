/*           Program to issue a routine for read the AUX RAM                   */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */

#include <string>
#include <iostream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "ftkvme/VMEManager.h"
#include "aux/aux_vme_regs.h"

namespace daq 
{
  namespace ftk 
  {
    /*! \brief Write to an AUX VME register
     *
     * The register is read after write and the result is written
     * to std::out.
     *
     * \param slot The slot with the AUX card
     * \param fpga The target FPGA
     * \param regaddr Address of the register in the FPGA
     * \param value The value to write into
     */
    void aux_write( int slot, int fpga, int regaddr, int value  )
    {
      VMEInterface *vme=VMEManager::global().aux(slot,fpga);

      //
      // WRITE
      //
      vme->write_word(TMODE  , 0x1);
      vme->write_word(regaddr, value);
      vme->write_word(TMODE  , 0x0);

      //
      // READ
      //
      u_int register_content=vme->read_word(regaddr);
      std::cout << "Address: 0x" << std::hex << regaddr << "\tValue: 0x" << std::hex << register_content << std::endl;
    }
    
  } //namespcae daq
} //namespcae ftk


/////////////////////////////////////////////////////////////////////////////
// Stand alone application
/////////////////////////////////////////////////////////////////////////////

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("fpga", value< std::string >()->default_value("1"), "The FPGA ID")
    ("nocheck", "Do not check the output value.")
    ("regaddr", value< std::string >()->default_value("0"), "Register address")
    ("value", value< std::string >()->default_value("0"), "Value")
    ;

  positional_options_description p;
  p.add("regaddr", 1);
  p.add("value", 1);
  
  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  int slot    = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int fpga    = daq::ftk::string_to_int( vm["fpga"].as<std::string>() );
  int regaddr = daq::ftk::string_to_int( vm["regaddr"].as<std::string>() );
  int value   = daq::ftk::string_to_int( vm["value"].as<std::string>() );

  daq::ftk::aux_write(slot, fpga, regaddr, value);
  return 0;
}


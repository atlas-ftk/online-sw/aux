/*           Program to print out the status of the AUX card
             Written for AMBFTK board v1.0   FTK collaboration                 */
#include "aux/aux.h"
#include "aux/aux_status.h"
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


/** Parsing input parmaeters and calling the am_init function
 *
 */
int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  bool IH = true;
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("details", "Print DO Details")
    ("fifos",   "Print Fifo Details")
    ("IH", bool_switch(&IH), "Whether using I2 InputHandler firmware")
    ;
  
  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  int  slot          = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  if (IH == true)
    return daq::ftk::aux_i2_fifo_status_IH(slot);    
  else
    return  daq::ftk::aux_i2_fifo_status(slot) ;
}

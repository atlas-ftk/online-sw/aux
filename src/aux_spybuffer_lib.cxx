#include "aux/aux_spy_regs.h"
#include "aux/aux_spybuffer_lib.h"

#include <iostream>
#include <sstream>
#include <fstream>

namespace daq 
{
  namespace ftk 
  {

    void aux_get_from_file(std::map<u_int, std::vector<u_int> > &data, std::vector<u_int> buffs, const std::string& file_name, bool debug)
    {

      u_int curr_buff = 0;
      u_int nbuffs = buffs.size();

      std::string stra, strb;
      u_int val;

      std::vector<u_int> last(nbuffs,0xffffffff);
      std::vector<bool> overflow(nbuffs,false);
      std::vector<u_int> wordcount(nbuffs,0);
      std::ifstream input (file_name.c_str());
      while (input >> stra >> strb) 
	{
	  wordcount[curr_buff] += 1;
	  if (stra == "[ovfl]:") {
	    if (strb == "NO" &&  // if the buffer did not overlow, and it is in the DO output.
		file_name.find("do_output_p") != std::string::npos ) 
	      {
		// then add the eoe at the beginning, so the first event will be counted.
		data[buffs[curr_buff]].insert(data[buffs[curr_buff]].begin(), 0xb0b00d01);
	      }
	    if (strb == "YES") overflow[curr_buff] = true;
	  } 
	  else if (stra != "[last]:") 
	    {
	      val = strtol(strb.c_str(), NULL,16);
	      data[buffs[curr_buff]].push_back(val);
	    }
	  //else if(!overflow[curr_buff] && file_name.find("do_roads_p") != std::string::npos)

	  if(last[curr_buff]!=0xffffffff && !overflow[curr_buff])
	    {
	      // if not overflow[curr_buff] and parsing road stream, clear data up to first word written to buffer
	      if(debug){
		std::cout << "did not find overflow[curr_buff], clear data up to first word written to buffer: " << file_name << " " << curr_buff << std::endl;
	      }
	      data[buffs[curr_buff]].erase( data[buffs[curr_buff]].begin(), data[buffs[curr_buff]].end() - last[curr_buff]+1 );
	      if(debug) std::cout << "first word: " << std::hex << data[buffs[curr_buff]][0] << std::endl;
	    }

	  if(stra == "[last]:") {
	    last[curr_buff] = strtol(strb.c_str(),NULL,16);
	  }

	  curr_buff = (curr_buff+1) % nbuffs;

	}

      input.close();

    }

    void aux_buffers_build_data_structure(std::map<u_int, std::map<u_int, std::vector<u_int> > > &data)
    {
      for (int f = 1; f <= 6; f++)
	{
	  data[f] = std::map<u_int, std::vector<u_int> >();
	}

      for (auto b : aux_i1_SB_ssmap_hits)   data[1][b] = std::vector<u_int>();
      for (auto b : aux_i1_SB_hits_to_proc) data[1][b] = std::vector<u_int>();

      for (auto b : aux_i2_SB_hits_to_proc) data[2][b] = std::vector<u_int>(); 
      for (auto b : aux_i2_SB_ssmap_hits)   data[2][b] = std::vector<u_int>();  
      for (auto b : aux_i2_SB_HW_in_out)    data[2][b] = std::vector<u_int>();

      for (int f = 3; f <= 6; f++)
	{
	  for (auto b : aux_proc_SB_TF_inputs) data[f][b] = std::vector<u_int>();
	  for (auto b : aux_proc_SB_DO_output) data[f][b] = std::vector<u_int>();
	  for (auto b : aux_proc_SB_hits_in)   data[f][b] = std::vector<u_int>();
	  for (auto b : aux_proc_SB_DO_roads)  data[f][b] = std::vector<u_int>();
	}

    }

    void aux_buffers_download_data(std::map<u_int, std::map<u_int, std::vector<u_int> > > &data, const std::string& inDirName, bool debug)
    {
      aux_get_from_file(data[1], aux_i1_SB_ssmap_hits,   inDirName+"/ss_i1.dat", debug);  
      aux_get_from_file(data[2], aux_i2_SB_ssmap_hits,   inDirName+"/ss_i2.dat", debug);  

      aux_get_from_file(data[2], aux_i2_SB_HW_in_out,    inDirName+"/hw_i2.dat", debug);

      aux_get_from_file(data[3], aux_proc_SB_TF_inputs,  inDirName+"/tf_in_p1.dat", debug);
      aux_get_from_file(data[4], aux_proc_SB_TF_inputs,  inDirName+"/tf_in_p2.dat", debug);
      aux_get_from_file(data[5], aux_proc_SB_TF_inputs,  inDirName+"/tf_in_p3.dat", debug);
      aux_get_from_file(data[6], aux_proc_SB_TF_inputs,  inDirName+"/tf_in_p4.dat", debug);

      aux_get_from_file(data[3], aux_proc_SB_DO_output,  inDirName+"/do_output_p1.dat", debug);
      aux_get_from_file(data[4], aux_proc_SB_DO_output,  inDirName+"/do_output_p2.dat", debug);
      aux_get_from_file(data[5], aux_proc_SB_DO_output,  inDirName+"/do_output_p3.dat", debug);
      aux_get_from_file(data[6], aux_proc_SB_DO_output,  inDirName+"/do_output_p4.dat", debug);

      aux_get_from_file(data[3], aux_proc_SB_DO_roads,  inDirName+"/do_roads_p1.dat", debug);
      aux_get_from_file(data[4], aux_proc_SB_DO_roads,  inDirName+"/do_roads_p2.dat", debug);
      aux_get_from_file(data[5], aux_proc_SB_DO_roads,  inDirName+"/do_roads_p3.dat", debug);
      aux_get_from_file(data[6], aux_proc_SB_DO_roads,  inDirName+"/do_roads_p4.dat", debug);

      aux_get_from_file(data[1], aux_i1_SB_hits_to_proc, inDirName+"/do_input_i1.dat", debug);  
      aux_get_from_file(data[2], aux_i2_SB_hits_to_proc, inDirName+"/do_input_i2.dat", debug);  
      // aux_get_from_file(data[3], aux_proc_SB_DO_output,  inDirName+"/do_p1.dat", debug);  
      // aux_get_from_file(data[4], aux_proc_SB_DO_output,  inDirName+"/do_p2.dat", debug);  
      // aux_get_from_file(data[5], aux_proc_SB_DO_output,  inDirName+"/do_p3.dat", debug);  
      // aux_get_from_file(data[6], aux_proc_SB_DO_output,  inDirName+"/do_p4.dat", debug);  

      // aux_get_from_file(data[3], aux_proc_SB_hits_in,    inDirName+"/hits_in_p1.dat", debug);  
      // aux_get_from_file(data[4], aux_proc_SB_hits_in,    inDirName+"/hits_in_p2.dat", debug);  
      // aux_get_from_file(data[5], aux_proc_SB_hits_in,    inDirName+"/hits_in_p3.dat", debug);  
      // aux_get_from_file(data[6], aux_proc_SB_hits_in,    inDirName+"/hits_in_p4.dat", debug);  
    }

  } //namespcae daq
} //namespcae ftk

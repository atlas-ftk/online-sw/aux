#include "aux/AUX_FWMap.h"

#include "ftkcommon/core.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

namespace daq 
{
  namespace ftk 
  {
    AUX_FWMap::AUX_FWMap()
    { }

    AUX_FWMap::AUX_FWMap(const std::string& RPDPath, const std::string& MAPPath)
      : _RPDPath(RPDPath)
    {
      std::cout << "AUX_FWMap: Start reading " << MAPPath << std::endl;
      
      std::ifstream fh(MAPPath);
      if(!fh.is_open())
	{
	  std::cerr << "AUX_FWMap: Error opening map file " << MAPPath << std::endl;
	  return;
	}

      std::string line;

      std::getline(fh, line); // header
      std::getline(fh, line); // empty

      std::string name;
      std::string start_address_hex;
      std::string end_address_hex;
      aux_fwmap_block_t block;
      while (std::getline(fh, line))
	{
	  if(line.empty()) break;

	  std::istringstream iss(line);
	  if (!(iss >> name >> start_address_hex >> end_address_hex)) { break; } // error

	  block.name=name;
	  block.start_address=daq::ftk::string_to_int(start_address_hex);
	  block.end_address=daq::ftk::string_to_int(end_address_hex);

	  _blocks.push_back(block);
	}

      fh.close();

      std::cout << "AUX_FWMap: Found " << _blocks.size() << " blocks  (end address=0x" << std::hex << block.end_address << ")" << std::endl;

    }

    std::string AUX_FWMap::RPDPath() const
    { return _RPDPath; }

    aux_fwmap_block_t AUX_FWMap::block(uint idx) const
    { return _blocks[idx]; }
  }
}

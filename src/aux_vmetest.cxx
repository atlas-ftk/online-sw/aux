/*           Program to issue a routine for read the AUX RAM                   */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */

#include <string>
#include <iostream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "aux/aux_vme_regs.h"
#include "aux/aux_vmetest.h"



namespace daq 
{
  namespace ftk 
  {
    bool aux_vmetest( int slot, int fpga, u_int addr, u_int value )
    { 
      int status;
      u_int ret, register_content;
      u_int vmeaddr, slotaddr, fpgaaddr;
      int handle;
      
      
      static VME_MasterMap_t master_map = {0x0, 0x10000000, VME_A32, 0};
      
      slotaddr = ((slot&0x1f) << 27);
      fpgaaddr = ((fpga&0x1f) << 24);
      vmeaddr = slotaddr + fpgaaddr;
      //
      // Write random word
      //

      // Open connection
      ret = VME_Open();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Open' in function" );
	}

      master_map.vmebus_address   = vmeaddr;
      master_map.window_size      = 0x1000000;
      master_map.address_modifier = VME_A32;
      master_map.options          = 0;
      ret = VME_MasterMap(&master_map, &handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterMap' in function"  );
	}

      //set tmode rx stratix
      status = VME_WriteSafeUInt(handle, TMODE, 0x1);
      CHECK_ERROR(status);

      // Write some random word
      status = VME_WriteSafeUInt(handle, addr, value);
      CHECK_ERROR(status);
      
      //set tmode rx stratix
      status = VME_WriteSafeUInt(handle, TMODE, 0x0);
      CHECK_ERROR(status);
      
      // Close connection
      ret = VME_MasterUnmap(handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterUnMap' in function"  );
	}
      
      ret = VME_Close();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Close' in function"  );	  
	}

      //
      // Read back!
      //

      // Open connection
      ret = VME_Open();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Open' in function" );
	}

      master_map.vmebus_address   = vmeaddr;
      master_map.window_size      = 0x1000000;
      master_map.address_modifier = VME_A32;
      master_map.options          = 0;
      ret = VME_MasterMap(&master_map, &handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterMap' in function"  );
	}
      
      //set tmode rx stratix
      status = VME_WriteSafeUInt(handle, TMODE, 0x1);
      CHECK_ERROR(status);

      // Read back the random word
      status = VME_ReadSafeUInt(handle, addr, &register_content);
      CHECK_ERROR(status);
      
      
      //set tmode rx stratix
      status = VME_WriteSafeUInt(handle, TMODE, 0x0);
      CHECK_ERROR(status);
     
      // Close connection
      ret = VME_MasterUnmap(handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterUnMap' in function"  );	  
	}
      
      ret = VME_Close();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Close' in function"  );	 
	}

      //
      // Error checking
      //
      if(value==register_content) return 0;

      u_int error=value^register_content;
      PRINT_LOG("ERROR FPGA: " << fpga << "\tADDR: " << std::hex << addr << " (0x" << vmeaddr+addr <<")" << "\tWROTE: " << std::hex << value << "\tREAD: " << std::hex << register_content << "\tERROR: " << std::hex << error);

      return -1;
      
    }
    
  } //namespcae daq
} //namespcae ftk


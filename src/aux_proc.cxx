#include "aux/aux_proc.h"

#include "ftkcommon/exceptions.h"

#include "aux/aux.h"
#include "aux/aux_vme_regs.h"
#include <string>
#include <libgen.h>

namespace daq
{
    namespace ftk
    {

        proc::proc(uint _slot, uint _fpga, std::string _name) : 
            aux_interface(_slot, _fpga, _name),m_EMIFready(false) { m_name = _name; }

        // Run Control //
        void proc::configure()
        {
            aux_interface::configure();
            enable_rslb();
        }

        bool proc::check_link_status(int i)
        {
            uint link_status = m_vme->read_word(LINK_STATUS);
            if( ((link_status>>12)&0xFFF) == 0xFFF){
                ERS_LOG("Proc " << m_fpga-2 << ": All hit links are up");
                return true;
            }
            else{
                ERS_LOG("Slot "<< m_slot<<" Proc "<<m_fpga-2<<": Not all links are up, reset sync again! Link status: " << std::hex<<std::setfill('0')<<std::setw(8) << link_status );
                if(i>=9) {

                    // daq::ftk::FTKIssue ex(ERS_HERE, "Slot " + std::to_string(m_slot) + " Proc " + std::to_string(m_fpga-2) + ": Not all links are up. Link status: "  + std::to_string(link_status) );
                    // ers::error(ex);
                    daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(), "Proc " + std::to_string(m_fpga-2) +" Hit", std::to_string(link_status) );
                    throw ex;

                }
                else if (i==-1) {
                    daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(), "Proc " + std::to_string(m_fpga-2) +" Hit", std::to_string(link_status) );
                    throw ex ;
                }
                return false;
            }
        }

        // Functionality //
        void proc::UnlockProcsDriveAUXbus() 
        {
            ERS_LOG("Proc " << m_fpga-2 << ": Unlock InternalAUXbus(7 downto 4) for use as FREEZE");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_word(0xe0, 0x0FEDC0DE);
            m_vme->write_word(TMODE, 0x0);
        }

        void proc::set_checksumWarning(bool warningOnly)
        {
            m_checksumWarning = warningOnly;
        }

        void proc::load_rams(bool isLoadMode)
        {
            ERS_LOG("Loading Processsor " << m_fpga-2);

            reset_logic();

            //TF Constants 
            if(!tf_constants.empty())
            {
                uint tf_check_val = calc_tf_check_sum();

                if(tf_check_val == tf_checksum){
                    ERS_LOG("TF Checksum passes. Expected: " << tf_checksum << " Got: " << tf_check_val);
                }
                else{
                    if(!isLoadMode){
                        daq::ftk::FTKWrongChecksum ex(ERS_HERE, name_ftk(), tf_checksum, tf_check_val, std::to_string(m_fpga), "TF Constants");
                        if(isFatalMode()) throw ex;
                        else ers::error(ex);
                    }
                    else{
                        daq::ftk::FTKWrongChecksum ex(ERS_HERE, name_ftk(), tf_checksum, tf_check_val, std::to_string(m_fpga), "TF Constants. In isLoadMode, reloading");
                        ers::warning(ex);


                        try{
                            ERS_LOG("Loading TF Constants from " << tf_constants);
                            aux_tfconst_load(m_slot, m_fpga, tf_constants, -1, false, false, loadAllSectors);
                        }
                        catch(ers::Issue &ex){
                            daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Problem loading TF Constants", ex);
                            if(isFatalMode()) throw e;
                            else ers::error(e);
                        }


                        uint tf_check_val_2 = calc_tf_check_sum();
                        ERS_LOG("TF checksum = " << std::hex << std::setw(8) << std::setfill('0') << tf_check_val_2 << std::dec << ((tf_check_val_2 == tf_checksum) ? " (matches)" : " (fails)"));
                        if(tf_check_val_2 != tf_checksum){
                            daq::ftk::FTKWrongChecksum ex(ERS_HERE, name_ftk(), tf_checksum, tf_check_val_2, std::to_string(m_fpga), "TF Constants");
                            if(isFatalMode()) throw ex;
                            ers::error(ex);
                        }
                    }
                }
            }

            //Patternbanks
            if(!am_constants.empty())
            {
                try{
                    calibrate_emif();
                }
                catch(ers::Issue &ex){
                    daq::ftk::FTKOperationTimedOut e(ERS_HERE, name_ftk(), "EMIF Calibration failed", ex);
                    throw e;
                }
                uint am_check_val = get_am_checksum();
                
                if( am_check_val == m_bankChecksum ){
                    ERS_LOG("AMMap Checksum passes. Expected: " << am_check_val << " Got: " << m_bankChecksum);
                }
                else{
                    if(!isLoadMode){
                        daq::ftk::FTKWrongChecksum ex(ERS_HERE, name_ftk(), m_bankChecksum, am_check_val, std::to_string(m_fpga), "AMMap");
                        if(isFatalMode()) throw ex;
                        else ers::error(ex);
                    }
                    else{
                        daq::ftk::FTKWrongChecksum ex(ERS_HERE, name_ftk(), m_bankChecksum, am_check_val, std::to_string(m_fpga), "AMMap. In isLoadMode, reloading");
                        ers::warning(ex);

                        std::ifstream amconstfile(am_constants);

                        if(amconstfile){
                            try{
                                ERS_LOG("Loading AMMap from " << am_constants);
                                aux_ammap_load_rc(m_slot, m_fpga, am_constants,8388608, NPattPerChip, banktype, ssoffset, InChipOff, loadAllSectors, false, m_FTKPatternBank, m_bankChecksum);

                            }
                            catch(ers::Issue &ex){
                                daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Problem loading AMMap", ex);
                                if(isFatalMode()) throw e;
                                else ers::error(e);
                            }
                        }
                        else {
                            daq::ftk::ftkException e(ERS_HERE, name_ftk(), "AMMap File Does Not Exist", ex);
                            if(isFatalMode()) throw e;
                            else ers::error(e);
                        }

                        amconstfile.close();                        

                        uint am_check_val_2 = get_am_checksum();
                        ERS_LOG("AMMap checksum = " << std::hex << std::setw(8) << std::setfill('0') << am_check_val_2 << std::dec << ((am_check_val == m_bankChecksum) ? " (matches)" : " (fails)"));
                        if(am_check_val_2 != m_bankChecksum){
                            daq::ftk::FTKWrongChecksum ex(ERS_HERE, name_ftk(), m_bankChecksum, am_check_val_2, std::to_string(m_fpga), "AMMap");
                            if(isFatalMode()) throw ex;
                            else ers::error(ex);
                        }
                    }
                }

            }

        }

        void proc::set_ignore_freeze(bool ignore) {
            if (ignore) ERS_LOG("Ignore FREEZE in Proc " << m_fpga-2);
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(0x10, 16, ignore);
            m_vme->write_word(TMODE, 0x0);
        }

        void proc::set_freeze_mask(u_int mask) {
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_word(FREEZE_MASK_REG, mask);
            m_vme->write_word(TMODE, 0x0);
        }

        void proc::set_MaxFitsPerRoad(u_int MaxFitsPerRoad) {
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_mask(MAX_FITS_PER_ROAD, (0b10<<30) + (MaxFitsPerRoad<<1), 0XFFFFFFFE);
            m_vme->write_word(TMODE, 0x0);
        }

        void proc::set_MaxRoadsPerEvent(u_int MaxRoadsPerEvent) {
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_mask(MAX_FITS_PER_ROAD, (0b11<<30) + (MaxRoadsPerEvent<<1), 0XFFFFFFFE);
            m_vme->write_word(TMODE, 0x0);
        }

        void proc::set_source_roads(bool amb){
            m_roads_from_amb = amb;
        }

        void proc::enable_upstream_rx(bool enable)
        {
            aux_interface::enable_upstream_rx(enable);
            m_vme->write_word(TMODE, 0x1);
            // Hits
            m_vme->write_mask(HIT_DISABLE_DATA,(0xFFFF*!enable)<<BIT_PROC_HIT_DISABLE_DATA, 0xFFFF<<BIT_PROC_HIT_DISABLE_DATA);
            // Roads
            m_vme->write_mask(DISABLE_DATA,(0xF*!(enable & m_roads_from_amb))<<BIT_PROC_ROAD_DISABLE_DATA, 0xF<<BIT_PROC_ROAD_DISABLE_DATA);
            m_vme->write_word(TMODE, 0x0);
        }

        void proc::set_upstream_flow(bool enable)
        {
            aux_interface::set_upstream_flow(enable);

            m_vme->write_word(TMODE, 0x1);
            //
            // Hits
            //m_vme->write_mask(HIT_DISABLE_DATA,(0xFFFF*!enable)<<BIT_PROC_HIT_DISABLE_DATA, 0xFFFF<<BIT_PROC_HIT_DISABLE_DATA);
            m_vme->write_mask(HIT_HOLD_CONTROL,(0xFFFF*!enable)<<BIT_PROC_HIT_FORCE_HOLD,   0xFFFF<<BIT_PROC_HIT_FORCE_HOLD);
            //
            // Roads
            //m_vme->write_mask(DISABLE_DATA,(0xF*!enable)<<BIT_PROC_ROAD_DISABLE_DATA, 0xF<<BIT_PROC_ROAD_DISABLE_DATA);
            m_vme->write_mask(HOLD_CONTROL,(0xF*!enable)<<BIT_PROC_ROAD_FORCE_HOLD,   0xF<<BIT_PROC_ROAD_FORCE_HOLD);
            m_vme->write_word(TMODE, 0x0);
        }

        void proc::set_downstream_flow(bool enable)
        {
            aux_interface::set_downstream_flow(enable);
            //Hit Warrior
            proc::hold_ignore_hw();
        }

        void proc::set_hold_ignore_hw(bool ignore) 
        {
            m_holdIgnoreHW = ignore;
        }

        void proc::hold_ignore_hw() 
        {
            ERS_LOG("Proc " << m_fpga-2 << ": Ignore HOLD from HW: "<<m_holdIgnoreHW);
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(AUX_DISABLE_HOLDS, BIT_PROC_HW_IGNORE_HOLD, m_holdIgnoreHW);
            m_vme->write_word(TMODE, 0x0);
        }

        void proc::enable_rslb() 
        {
            ERS_LOG("Proc " << m_fpga-2 << ": Enable RSLB");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(PROC_ENABLE_RSLB_REG, BIT_PROC_ENABLE_RSLB, false);//toggled on rising edge
            m_vme->write_bit(PROC_ENABLE_RSLB_REG, BIT_PROC_ENABLE_RSLB, true);
            m_vme->write_word(TMODE, 0x0);
        }

        void proc::set_hits_from_tvs(bool v)
        {
            m_vme->write_word(TMODE, 1);
            m_vme->write_bit (PROC_FROM_FIFO_REG, PROC_HITS_FROM_FIFO_BIT, v);
            m_vme->write_word(TMODE, 0);
        }

        void proc::set_roads_from_tvs(bool v)
        {
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit (PROC_FROM_FIFO_REG, PROC_ROAD_FROM_FIFO_BIT, v);
            m_vme->write_word(TMODE, 0x0);
        }

        void proc::reset_dfsynch()
        {
            ERS_LOG("resetting dfsynch, proc " << m_fpga-2);
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(RESETS, BIT_PROC_RESET_DFSYNCH, false);
            m_vme->write_bit(RESETS, BIT_PROC_RESET_DFSYNCH, true);
            m_vme->write_word(TMODE, 0x0);
        }

        void proc::reset_SimpleSimplexLDC()
        {
            ERS_LOG("resetting simple simplex LDC, proc " << m_fpga-2);
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(RESETS, 29, true);//roads
            m_vme->write_bit(RESETS, 30, true);//hits
            m_vme->write_bit(RESETS, 29, false);//roads
            m_vme->write_bit(RESETS, 30, false);//hits
            m_vme->write_word(TMODE, 0x0);
        }

        void proc::reset_do()
        {
            ERS_LOG("resetting do, proc " << m_fpga-2);
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit (PROC_DO_RESET_REG, PROC_DO_RESET_BIT, true);
            // m_vme->write_word(TMODE, 0x0);
            // ERS_LOG("val == " << m_vme->read_word(0x70));
            // m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit (PROC_DO_RESET_REG, PROC_DO_RESET_BIT, false);
            m_vme->write_word(TMODE, 0x0);
        }

        void proc::reset_tf() {

            ERS_LOG("resetting tf, proc " << m_fpga-2);
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit (PROC_TF_RESET_REG, PROC_TF_RESET_BIT, true);
            // m_vme->write_word(TMODE, 0x0);
            // ERS_LOG("val == " << m_vme->read_word(0x30));
            // m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit (PROC_TF_RESET_REG, PROC_TF_RESET_BIT, false);
            m_vme->write_word(TMODE, 0x0);

        }

        void proc::set_towerID(uint towerID)
        {
            ERS_LOG("Setting TowerID proc" << m_fpga-2<<" slot "<<m_slot<<": "<<towerID);
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_mask(PROC_TF_TOWERID_REG, (0x3F&towerID)<<PROC_TF_TOWERID_BIT, 0x3F<<PROC_TF_TOWERID_BIT);
            m_vme->write_word(TMODE, 0x0);
        }

        void proc::reset_logic()
        {
            aux_interface::reset_logic();
            reset_dfsynch();
            reset_SimpleSimplexLDC();
            reset_do();
            reset_tf();
        }


        uint proc::calc_tf_check_sum()
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(), "Calculate TF checksums Processor " + std::to_string(m_fpga-2));
            
            m_vme->write_word(TMODE, 1);
            m_vme->write_word(PROC_TF_RUN_CHECKSUM_REG, PROC_TF_RUN_CHECKSUM_WORD);
            m_vme->write_word(TMODE, 0);

            sleep(2);

            tLog.end(ERS_HERE);  // End log conter
            return m_vme->read_word(PROC_TF_READ_CHECKSUM_WORD);
        }

        void proc::calibrate_emif(){
            
            u_int do_status;
            static u_int vme_ctrl_20 = 0x20;
            u_int calibration_status = 0x0;
            int calibration_attempts = 1;
            // Read DO_Status to get calibration success

            while (calibration_status == 0x0 and calibration_attempts < 101) { 
                do_status= m_vme->read_word(DO_STATUS);
                calibration_status=(do_status>>22)&0x1;
                if (calibration_status == 0x1) {
                    ERS_LOG("EMIF calibration success. Attempt: "+to_string(calibration_attempts-1));
                } else {
                    ERS_INFO("EMIF calibration failure, trying soft reset. Attempt " << calibration_attempts);
                    m_vme->write_word(TMODE, 0x1);
                    //vme->write_word(vme_ctrl_40,0x2);
                    m_vme->write_word(vme_ctrl_20,0x1);
                    usleep(2e5);
                    //vme->write_word(vme_ctrl_40,0x0);
                    //usleep(1);
                    m_vme->write_word(vme_ctrl_20,0x0);
                    m_vme->write_word(TMODE, 0x0);
                    usleep(2e5);
                };
                calibration_attempts += 1;
            };
            if (calibration_attempts == 101 and calibration_status == 0x0){
                daq::ftk::FTKOperationTimedOut ex(ERS_HERE, name_ftk(), "EMIF Calibration failed");
                throw ex;
            }
        }


        uint proc::get_am_checksum(){
            ftkTimeLog tLog(ERS_HERE, name_ftk(), "Calculate AMMap checksum " + std::to_string(m_fpga-2));

            m_vme->write_word(TMODE, 0x1);

            m_vme->write_bit(PROC_DO_VMECONTROL, BIT_AMMAP_CHECKSUM_RESET, true);
            m_vme->write_bit(PROC_DO_VMECONTROL, BIT_AMMAP_CHECKSUM_RESET, false);

            m_vme->write_bit(PROC_DO_VMECONTROL, BIT_AMMAP_MUX_CHECKSUM,   true);
            m_vme->write_bit(PROC_DO_VMECONTROL, BIT_AMMAP_CHECKSUM_START, true);

            m_vme->write_word(TMODE, 0x0);

            // Wait till it is ready
            bool done=false;
            do
                done=m_vme->read_bit(DO_STATUS,BIT_AMMAP_CHECKSUM_DONE);
            while(!done);

            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(PROC_DO_VMECONTROL, BIT_AMMAP_CHECKSUM_START, false);      
            m_vme->write_bit(PROC_DO_VMECONTROL, BIT_AMMAP_MUX_CHECKSUM,   false);
            m_vme->write_word(TMODE, 0x0);

            tLog.end(ERS_HERE);  // End log conter
            return m_vme->read_word(PROC_AM_READ_CHECKSUM_WORD);
        }

        void proc::load_road_fifos(string path)
        {
            ERS_LOG("loading proc " << m_fpga-2 << " road fifos: " << path);
            try{
                if(path.find("proc")){
                    string proc_str = std::to_string(m_fpga-2);
                    aux_load_tx_fifo(m_slot, m_fpga, PROC_ROAD_FIFO_REG0, path + proc_str + "_TXInput_0.txt");
                    aux_load_tx_fifo(m_slot, m_fpga, PROC_ROAD_FIFO_REG1, path + proc_str + "_TXInput_1.txt");
                    aux_load_tx_fifo(m_slot, m_fpga, PROC_ROAD_FIFO_REG2, path + proc_str + "_TXInput_2.txt");
                    aux_load_tx_fifo(m_slot, m_fpga, PROC_ROAD_FIFO_REG3, path + proc_str + "_TXInput_3.txt");
                }else{
                    aux_load_tx_fifo(m_slot, m_fpga, PROC_ROAD_FIFO_REG0, path + "_TXInput_0.txt");
                    aux_load_tx_fifo(m_slot, m_fpga, PROC_ROAD_FIFO_REG1, path + "_TXInput_1.txt");
                    aux_load_tx_fifo(m_slot, m_fpga, PROC_ROAD_FIFO_REG2, path + "_TXInput_2.txt");
                    aux_load_tx_fifo(m_slot, m_fpga, PROC_ROAD_FIFO_REG3, path + "_TXInput_3.txt");
                }
            }
            catch(ers::Issue &ex){
                daq::ftk::IOError e(ERS_HERE, name_ftk(), "Unable to open TV File", ex);
                throw e;
            }
            
        }

        void proc::load_hit_and_ssid_fifos(string path)
        {

            // 4 SCT Links
            aux_load_tx_fifo(m_slot, m_fpga, PROC_HIT_SSID_FIFO_REG0,  path + "_0.txt");
            aux_load_tx_fifo(m_slot, m_fpga, PROC_HIT_SSID_FIFO_REG1,  path + "_1.txt");
            aux_load_tx_fifo(m_slot, m_fpga, PROC_HIT_SSID_FIFO_REG2,  path + "_2.txt");
            aux_load_tx_fifo(m_slot, m_fpga, PROC_HIT_SSID_FIFO_REG3,  path + "_3.txt");

            // Input 1 (1-SCT link + 6 Pixel Links)            
            aux_load_tx_fifo(m_slot, m_fpga, PROC_HIT_SSID_FIFO_REG4,  path + "_4.txt");
            aux_load_tx_fifo(m_slot, m_fpga, PROC_HIT_SSID_FIFO_REG5,  path + "_5.txt");
            aux_load_tx_fifo(m_slot, m_fpga, PROC_HIT_SSID_FIFO_REG6,  path + "_6.txt");
            aux_load_tx_fifo(m_slot, m_fpga, PROC_HIT_SSID_FIFO_REG7,  path + "_7.txt");
            aux_load_tx_fifo(m_slot, m_fpga, PROC_HIT_SSID_FIFO_REG8,  path + "_8.txt");
            aux_load_tx_fifo(m_slot, m_fpga, PROC_HIT_SSID_FIFO_REG9,  path + "_9.txt");
            aux_load_tx_fifo(m_slot, m_fpga, PROC_HIT_SSID_FIFO_REG10, path + "_10.txt");

        }

        void proc::reset_hit_and_road_fifos()
        {
            ERS_LOG("reset_hit_and_road_fifos");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(PROC_RESET_HIT_ROAD_FIFO_REG, 12, true);
            m_vme->write_bit(PROC_RESET_HIT_ROAD_FIFO_REG, 12, false);
            m_vme->write_word(TMODE, 0x0);
        }

        void proc::send_road_fifos(bool loop)
        {
            set_roads_from_tvs(true);

            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(PROC_SEND_FIFO_DATA_REG, PROC_SEND_FIFO_ROAD_BIT, true);
            m_vme->write_bit(PROC_SEND_FIFO_DATA_REG, PROC_SEND_FIFO_LOOP_BIT, loop);
            m_vme->write_word(TMODE, 0x0);

        }

        void proc::send_hit_and_ssid_fifos(bool loop)
        {
            set_hits_from_tvs(true);

            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(PROC_SEND_FIFO_DATA_REG, PROC_SEND_FIFO_HITS_BIT, true);
            m_vme->write_bit(PROC_SEND_FIFO_DATA_REG, PROC_SEND_FIFO_LOOP_BIT, loop);
            m_vme->write_word(TMODE, 0x0);

        }

    }
}



/*           Performe remote update of an FPGA on the AUX card                 */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */
#include "aux/aux.h"
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  positional_options_description p;
  p.add("file", 1);
  p.add("startaddress", 1);
  p.add("endaddress", 1);

  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("fpga", value< std::string >()->default_value("1"), "The FPGA ID")
    ("file", value< std::string >(), "The RPD file containing the new firmware")
    ("startaddress", value< std::string >(), "The start address of the firmware in the RPD file")
    ("endaddress", value< std::string >(), "The end (inclusive) address of the firmware in the RPD file")
    ("epcqaddress", value< std::string >()->default_value("0"), "The start address in the EPCQ")
    ;
  
  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  int slot    = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int fpga    = daq::ftk::string_to_int( vm["fpga"].as<std::string>() );
  
  std::string file = vm["file"].as<std::string>();
  uint startaddress = daq::ftk::string_to_int( vm["startaddress"].as<std::string>() );
  uint endaddress   = daq::ftk::string_to_int( vm["endaddress"]  .as<std::string>() );
  uint epcqaddress  = daq::ftk::string_to_int( vm["epcqaddress"] .as<std::string>() );
  
  return  daq::ftk::aux_upgrade(slot, fpga, epcqaddress, file, startaddress, endaddress) ;
}

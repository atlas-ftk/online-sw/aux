/*           Program to print out the status of the AUX card FIFOs
             Written for AMBFTK board v1.0   FTK collaboration                 */
#include "aux/aux_status.h"
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

int main(int argc, char **argv) 
{ 
  // Which module to print errors from. Default do all
  bool DO_errors;
  bool TFBL_errors;
  bool TFComb_errors;
  bool InputFreeze;
  bool ProcFreeze;
  bool Truncations;
  int slot;
  std::vector<int> fpgas; // Which fpga's to print

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  using namespace  boost::program_options ;
  options_description desc("usage: aux_error_count__main --slot X --fpga 1 2 3 4 5 6");
  desc.add_options()
    ("help,h",                                               "Usage")
    ("slot,s", value< std::string >()->default_value("15"),  "The card slot. Default is 15.")
    ("fpga,f", value< std::vector<int> >()->multitoken(),    "List of fpgas to show status for. Default is all.")
    ("DO,do",  bool_switch(&DO_errors),                      "Print out the DO error counts. Must display at least one proc.")  // Is set to false if not present
    ("TFBL,tfbl",  bool_switch(&TFBL_errors),                      "Print out the TFBL error counts. Must display at least one proc.")
    ("TFComb,TFCombiner,tfcomb,tfcombiner,tf_combiner", bool_switch(&TFComb_errors), "Print out TFCombiner error counts. Must display at least one proc.")
    ("InputFreeze,if", bool_switch(&InputFreeze),            "Print the input freezes. Must also be displaying input fpgas.")
    ("ProcFreeze,pf",  bool_switch(&ProcFreeze),             "Print the proc  freezes. Must display at least one proc.")
    ("Truncations,tr",  bool_switch(&Truncations),           "Print data truncations")
    ;
  
  // Load parameters into varaibles map
  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  notify(vm);

  // if help is required, then desc is printed to output
  if( vm.count("help") ) 
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }

  // Setup the slot and which fpgas being used
  slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  if(vm.count("fpga")==0)
    { // Default is all info
        fpgas.assign({1,2,3,4,5,6});
    }
  else {
    fpgas = vm["fpga"].as<std::vector<int> >();
    // Check the the fpga nums are either 1,2,3,4,5, or 6
    for (std::vector<int>::iterator it = fpgas.begin(); it < fpgas.end(); it++) {
        if(!((*it == 1) ||
             (*it == 2) ||
             (*it == 3) ||
             (*it == 4) ||
             (*it == 5) ||
             (*it == 6) )){
            std::cout << "Invalid FPGA Number: " << *it << "." << std::endl;
            return 1;
            }
    }
  }
  
  // If not given what to display, will do all by default
  if ((! DO_errors) && (! TFBL_errors) && (! TFComb_errors) && (! InputFreeze) && (! ProcFreeze) && (! Truncations)) {
    DO_errors     = true;
    TFBL_errors   = true;
    TFComb_errors = true;
    InputFreeze   = true;
    ProcFreeze    = true;
    Truncations   = true;
  }

  return  daq::ftk::aux_error_count(slot, fpgas, DO_errors, TFBL_errors, TFComb_errors, InputFreeze, ProcFreeze, Truncations) ;
}

#include "aux/TF/TFGen.h"

#include "aux/AuxUtils.h"

#include <string>

namespace tf
{
  unsigned int getMissingLayer(unsigned int layermap)
  {
    for(unsigned int ilayer = 0 ; ilayer < NLayers ; ilayer++ )
      {
	if( aux::getBit(layermap,ilayer)==0 ) return ilayer;
      }
    return NLayers;
  }

  unsigned int getLayerFromCoord(unsigned int icoord)
  {
    if( icoord < NPixCoords ) return auxmath::FloorNint(icoord/2);
    return icoord - (NPixCoords/2);
  }    

  unsigned int getLayerFromStream(unsigned int istream )
  {  
    if( istream < NPixStreams ) return auxmath::FloorNint(istream/2);
    return istream - (NPixStreams/2);
  }

  unsigned int getLayermapFromStreammap(unsigned int streammap)
  {
    unsigned int layermap = 0;

    for(unsigned int istream = 0 ; istream < NStreams ; istream++ )
      {
	int ilayer = getLayerFromStream(istream);
	layermap |= (aux::getBit(streammap,istream) << ilayer);
      }

    return layermap;

  }
  
  std::string getLayermapStr(unsigned int layermap)
  { return aux::getBinStr(layermap,NLayers); }

  std::string getHitWordStr(const HitWord &hitword)
  {
    std::string result = aux::getHexStr(hitword.hit,0);
    result += (hitword.eor ? ",1" : ",0");
    return result;
  }
  
  std::string getHitStr(Int_tf hit)
  { return aux::getHexStr(hit,0); }  
}


namespace doparse
{
  unsigned int getValidBit(unsigned int iword)
  { return END-iword-3; }
}

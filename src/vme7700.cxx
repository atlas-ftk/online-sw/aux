/*           Program to issue a routine for read the AUX RAM                   */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */

#include <string>
#include <iostream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "aux/aux_vme_regs.h"



namespace daq 
{
  namespace ftk 
  {
    //
    //  to write
    //
    bool vme7700write( int slot, int fpga, int regaddr, int value)
    { 
      // For error checking 
      int status;
      u_int ret;
      u_int vmeaddr, slotaddr, fpgaaddr;
      int handle;
      
      static VME_MasterMap_t master_map = {0x0, 0x10000000, VME_A32, 0};
      
      slotaddr = ((slot&0x1f) << 27);
      fpgaaddr = ((fpga&0x1f) << 24);
      vmeaddr = slotaddr + fpgaaddr;

      ret = VME_Open();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Open' in function" );
	}
      
      master_map.vmebus_address   = vmeaddr;
      master_map.window_size      = 0x1000000;
      master_map.address_modifier = VME_A32;
      master_map.options          = 0;
      ret = VME_MasterMap(&master_map, &handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterMap' in function"  );
	}

      //
      // WRITE 
      //
      status = VME_WriteSafeUInt(handle, regaddr, value);
      CHECK_ERROR(status);

      ret = VME_MasterUnmap(handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterUnMap' in function"  );
	  
	}
      
      ret = VME_Close();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Close' in function"  );
	  
	}
      
      return 0;
      
    }

    //
    // to read 
    //
    bool vme7700read( int slot, int fpga, int regaddr)
    { 
      int status;
      u_int ret, register_content;
      u_int vmeaddr, slotaddr, fpgaaddr;
      int handle;
      
      static VME_MasterMap_t master_map = {0x0, 0x10000000, VME_A32, 0};
      
      slotaddr = ((slot&0x1f) << 27);
      fpgaaddr = ((fpga&0x1f) << 24);
      vmeaddr = slotaddr + fpgaaddr;

      ret = VME_Open();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Open' in function" );
	}
      
      master_map.vmebus_address   = vmeaddr;
      master_map.window_size      = 0x1000000;
      master_map.address_modifier = VME_A32;
      master_map.options          = 0;
      ret = VME_MasterMap(&master_map, &handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterMap' in function"  );
	}


      //
      // READ
      //
      
      //write the data
      status = VME_ReadSafeUInt(handle, regaddr, &register_content);
      CHECK_ERROR(status);

      //PRINT_LOG("Address: 0x" << std::hex << vmeaddr+regaddr << "\tValue: 0x" << std::hex << register_content);

      printf("%08x\n ", register_content);      

      ret = VME_MasterUnmap(handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterUnMap' in function"  );
	  
	}
      
      ret = VME_Close();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Close' in function"  );
	  
	}
      
      return 0;
      
    }

    
  } //namespcae daq
} //namespcae ftk



#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


/** Parsing input parmaeters and calling the am_init function
 *
 */
int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("mode", value< std::string >()->default_value("read"), "read or write")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    //("fpga", value< std::string >()->default_value("1"), "The FPGA ID")
    ("gaddr", value< std::string >()->default_value("0"), "Global address  (fpga+Register address)")
    ("value", value< std::string >()->default_value(""), "Value")
    ;

  //./vme7700 write %i %i00003c 0"

  positional_options_description p;
  p.add("mode", 1);
  p.add("slot", 1);
  p.add("gaddr", 1);
  p.add("value", 1);
  
  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }


  int slot    = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int gaddr = daq::ftk::string_to_int( "0x"+vm["gaddr"].as<std::string>() );

  //./vme7700 write %i %i00003c 0"
  int fpga = (gaddr >> (6*4));
  int regaddr = (gaddr & 0xffffff);

  if(vm["mode"].as<std::string>() == "read"){
    return  daq::ftk::vme7700read(slot, fpga, regaddr) ;  
  }else if(vm["mode"].as<std::string>() == "write"){

    if(vm["value"].as<std::string>() == ""){
      std::cout << "ERROR: Please give value to write" << std::endl;
    }

    int value   = daq::ftk::string_to_int( "0x"+vm["value"].as<std::string>() );
    return  daq::ftk::vme7700write(slot, fpga, regaddr, value) ;  
  }else{
    std::cout << "ERROR: Please set mode to either read or write" << std::endl;
  }
  
  return 0;
}

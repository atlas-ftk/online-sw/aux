/*           Program to issue a routine for read the AUX RAM                   */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */

#include "aux/aux.h"
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ("debug,d", "produce debug messages")
    ("slot,s", value< std::string >()->default_value("15"), "The card slot.")
    ("fpga,f", value< std::string >()->default_value("1"), "The FPGA ID.")
    ("checksum", "compute checksum but do not load")
    ("convert", "converting root file into hex file for simulation. Format for direct memory loading")
    ("convert_tradition", "converting root file into hex file for simulation. Format for simulated VME loading")
    ("npatts,n", value< std::string >()->default_value("512"), "Number of patterns in the root file.")
    ("ammappath,a", value< std::string >(), "Location of input AMMAP.")
    ("loadAllSectors,l", "don't bitshift sector ID. Used when loading all TF sectors to each FPGA instead of just the ones that belong")
    ("InChipOff", value< std::string >()->default_value("1"), "Constant RoadID offset.")
    ;
  
  positional_options_description p;
  p.add("ammappath", 1);

  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  u_int slot    = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  u_int fpga    = daq::ftk::string_to_int( vm["fpga"].as<std::string>() );

  std::string ammappath  = vm["ammappath"].as<std::string>();
  unsigned int npatterns = daq::ftk::string_to_int( vm["npatts"].as<std::string>() );

  bool debug = false;
  if( vm.count("debug") ) // if help is required, then desc is printed to output
    {
      debug =true;
    }

  bool loadAllSectors;
  if( vm.count("loadAllSectors") )
    loadAllSectors = true;
  else
    loadAllSectors = false;

  bool checksum;
  if( vm.count("checksum") )
    checksum = true;
  else
    checksum = false;


  if( vm.count("convert") ) // This is for generating input files for simulation!
    {
      u_int InChipOff = daq::ftk::string_to_int( vm["InChipOff"].as<std::string>() );      
      return daq::ftk::aux_ammap_convert(ammappath, npatterns, debug, InChipOff, loadAllSectors);
    }
  else if(vm.count("convert_tradition"))
    {
      return daq::ftk::aux_ammap_convert_old(ammappath, npatterns, debug, loadAllSectors);
    }


  if (ammappath.substr(ammappath.length()-3,ammappath.length())=="hex") 
    return daq::ftk::aux_ammap_load_hex(slot, fpga, ammappath, debug) ;
  else{
    FTKPatternBank b;
    return  daq::ftk::aux_ammap_load(slot, fpga, ammappath, npatterns, 131072, 0, 0, 0, loadAllSectors, debug, &b, 0, checksum);
  }
}

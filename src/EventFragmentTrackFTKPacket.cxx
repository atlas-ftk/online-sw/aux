#include "aux/SpyBuffer/EventFragmentTrackFTKPacket.h"

#include <iostream>
#include <iomanip>

#include "aux/AuxUtils.h"
#include "aux/Report.h"
#include "aux/TF/TFCombination.h"
#include "aux/TF/TFGen.h"


namespace daq {
  namespace ftk {

    EventFragmentTrackFTKPacket::EventFragmentTrackFTKPacket()
      : EventFragmentFTKPacket(), _dumpChi2(false)
    { }

    EventFragmentTrackFTKPacket::~EventFragmentTrackFTKPacket()
    {
      // Delete tracks
      for(auto track : _tracks)
	delete track;
      _tracks.clear();
    }

    unsigned int EventFragmentTrackFTKPacket::getNTracks() const
    { return _tracks.size(); }

    TFCombination* EventFragmentTrackFTKPacket::getTrack(unsigned int idx) const
    { return _tracks[idx]; }

    void EventFragmentTrackFTKPacket::addTrack(TFCombination *track)
    {
      _tracks.push_back(track);
    }

    bool EventFragmentTrackFTKPacket::getDumpChi2() const
    { return _dumpChi2; }

    void EventFragmentTrackFTKPacket::setDumpChi2(bool dumpChi2)
    { _dumpChi2=dumpChi2; }

    void EventFragmentTrackFTKPacket::parsePayload(const std::vector<unsigned int> &data)
    {
      _dumpChi2=false;

      TFCombination *tmp_comb=0;

      unsigned int parseStep=0;
      unsigned int layerIdx=0;
      Int_tf hit=0;
      Int_tf chi2=0;
      for(auto word : data)
	{
	  if(aux::getBit(word,31))
	    {
	      tmp_comb=new TFCombination();

	      // Set the info stored in the first word
	      tmp_comb->setLayermap(aux::getBitRange(word,16,23));
	      tmp_comb->setID(aux::getBitRange(word,13,15));
	      tmp_comb->setSectorID(aux::getBitRange(word,0,13));
	      tmp_comb->setPasses(true);

	      _tracks.push_back(tmp_comb);

	      // Process road next
	      parseStep=1;
	    }
	  else
	    {
	      switch(parseStep)
		{
		case 1: // Road word
		  tmp_comb->setRoadID(aux::getBitRange(word,0,23));

		  // Process hit next
		  parseStep=2;
		  layerIdx=0;
		  break;
		case 2: // Parse pixel hit
		  hit=aux::getBitRange(word,0,30);
		  tmp_comb->setHit(layerIdx,hit);
		  layerIdx++;
		  if(layerIdx==3)
		    parseStep=3;
		  break;
		case 3: // Parse sct hit
		  hit=aux::getBitRange(word,0,14);
		  tmp_comb->setHit(layerIdx,hit);
		  layerIdx++;
		  if(layerIdx==8)
		    parseStep=4;
		  break;
		case 4: // Parse MSB of chi2
		  chi2=(aux::getBitRange(word,0,27)<<28);
		  parseStep=5;
		  _dumpChi2=true;
		  break;	
		case 5: // Parse LSB of chi2
		  chi2|=aux::getBitRange(word,0,27);
		  tmp_comb->setChiSquare(chi2);
		  parseStep=6;
		  _dumpChi2=true;
		  break;	
		default:
		  report::error("Unknown track parsing step: %d",parseStep);
		  break;
		}
	    }
	}

      // Finalize all of the coordinates
      for(auto track : _tracks)
	track->extractCoordinates();

    }

    std::vector<unsigned int> EventFragmentTrackFTKPacket::bitstreamPayload() const
    {
      std::vector<unsigned int> result;
      for(auto track : _tracks)
	{
	  result.push_back(0xa8000000|track->getLayermap()<<16|track->getSectorID()); // Begin
	  result.push_back(0x40000000|track->getRoadID()); // Road

	  // Hits
	  for(unsigned int ilayer = 0 ; ilayer < tf::NLayers ; ilayer++ )
	      result.push_back(track->getHit(ilayer));

	  // Chi2
	  if(_dumpChi2)
	    {
	      result.push_back(aux::getBitRange(track->getChiSquare(),32,63));
	      result.push_back(aux::getBitRange(track->getChiSquare(), 0,31));
	    }
	}
      return result;
    }
  } // namespace ftk
} // namespace daq

#include "aux/AuxMath.h"

namespace auxmath
{
  int FloorNint(double x)
  { return Nint( floor(x) ); }
}


#include "aux/aux_status.h"
#include "aux/aux.h"

// These are used only in aux_error_counter
#include <sstream>
#include <numeric>

namespace daq 
{
  namespace ftk 
  {

const std::string FPGATitles[6]={"Input 1",
                 "Input 2",
                 "Processor 1",
                 "Processor 2",
                 "Processor 3",
                 "Processor 4"};

/********************************************************************/
/********************************************************************
 * aux_freeze_status
 * Used in aux_status. No standalone tool.
 * *****************************************************************/
/********************************************************************/

void aux_freeze_status( int slot ) {
  VMEInterface *input1=VMEManager::global().aux(slot,1);
  VMEInterface *input2=VMEManager::global().aux(slot,2);
  VMEInterface *proc1 =VMEManager::global().aux(slot,3);
  VMEInterface *proc2 =VMEManager::global().aux(slot,4);
  VMEInterface *proc3 =VMEManager::global().aux(slot,5);
  VMEInterface *proc4 =VMEManager::global().aux(slot,6);

  VMEInterface *fpgas[6]={input1,input2,proc1,proc2,proc3,proc4};
  u_int register_content;
  //bool freeze_high = false;

  std::map<u_int,std::string> freeze_code_i1;
  freeze_code_i1[ 0]="VME freeze";
  freeze_code_i1[ 1]="Synchronized freeze";
  freeze_code_i1[ 2]="QSFP L1ID Sync Error";
  freeze_code_i1[ 3]="Hit Rx L1ID Sync Error";
  freeze_code_i1[ 4]="QSFP Rx L1ID Skip";
  freeze_code_i1[ 5]="Hit Rx L1ID Skip";
  freeze_code_i1[ 6]="SSID L1ID Sync Error";
  freeze_code_i1[ 8]="QSFP Packet Error";
  freeze_code_i1[ 9]="Hit packet Error";
  freeze_code_i1[13]="SSMap Input PacketHandler Error";
  freeze_code_i1[14]="SSMap FIFO Overflow";
  freeze_code_i1[15]="SSMap AM L1ID FIFO Underflow";
  freeze_code_i1[16]="freeze from AMB";
  freeze_code_i1[26]="[depricated]";
  freeze_code_i1[28]="freeze from proc 1";
  freeze_code_i1[29]="freeze from proc 2";
  freeze_code_i1[30]="freeze from proc 3";
  freeze_code_i1[31]="freeze from proc 4";

  std::map<u_int,std::string> freeze_code_i2;
  freeze_code_i2[ 0]="VME freeze";
  freeze_code_i2[ 1]="Synchronized freeze";
  freeze_code_i2[ 2]="QSFP L1ID Sync Error";
  freeze_code_i2[ 3]="Hit Rx L1ID Sync Error";
  freeze_code_i2[ 4]="Gen Data Rx L1ID Sync Error";
  freeze_code_i2[ 5]="Gen Data Tx L1ID Sync Error";
  freeze_code_i2[ 6]="SSID L1ID Sync Error";
  freeze_code_i2[ 8]="QSFP Packet Error";
  freeze_code_i2[ 9]="Hit packet Error";
  freeze_code_i2[10]="Gen Data Rx Packet Error";
  freeze_code_i2[11]="Gen Data Tx Packet Error";
  freeze_code_i2[12]="SFP Data Packet Error";
  freeze_code_i2[13]="SSMap Input PacketHandler Error";
  freeze_code_i2[14]="SSMap FIFO Overflow";
  freeze_code_i1[15]="SSMap AM L1ID FIFO Underflow";
  freeze_code_i2[16]="freeze from AMB";
  freeze_code_i1[22]="QSFP L1ID Skip";
  freeze_code_i1[23]="Hit L1ID Skip";
  freeze_code_i1[24]="Gen Data Rx L1ID Skip";
  freeze_code_i1[25]="Gen Data Tx L1ID Skip";
  freeze_code_i1[26]="SFP L1ID Skip";
  freeze_code_i2[28]="freeze from proc 1";
  freeze_code_i2[29]="freeze from proc 2";
  freeze_code_i2[30]="freeze from proc 3";
  freeze_code_i2[31]="freeze from proc 4";

  std::map<u_int,std::string> freeze_code_hw;
  freeze_code_hw[ 0]="VME freeze";
  freeze_code_hw[ 1]="Synchronized freeze";
  freeze_code_hw[ 2]="Gen Data Rx L1ID Sync Error";
  freeze_code_hw[ 3]="Gen Data Rx Packet Error";
  freeze_code_hw[ 4]="SFP Data Packet Error";
  freeze_code_hw[ 5]="Freeze from SSB";
  freeze_code_hw[ 6]="HW PacketHandler Error";
  freeze_code_hw[ 7]="HW TrackHandler Error";
  freeze_code_hw[16]="freeze from AMB";
  freeze_code_hw[17]="AUX2ROS Packet Error";
  freeze_code_hw[18]="AUX2ROS L1ID Skip";
  freeze_code_hw[19]="Gen Data Rx L1ID Skip";
  freeze_code_hw[20]="SFP L1ID Skip";

  std::map<u_int,std::string> freeze_code_pr;
  freeze_code_pr[ 0]="VME freeze";
  freeze_code_pr[ 1]="Synchronized freeze";
  freeze_code_pr[ 2]="Hit Tx L1ID Sync Error";
  freeze_code_pr[ 3]="Hit Rx L1ID Sync Error";
  freeze_code_pr[ 4]="Road Rx L1ID Sync Error";
  freeze_code_pr[ 5]="Hit Rx Packet Error";
  freeze_code_pr[ 6]="Hit Tx Packet Error";
  freeze_code_pr[ 7]="Tracks Out Packet Error";
  freeze_code_pr[ 8]="Gendata Rx Packet Error";
  freeze_code_pr[ 9]="DO Error";
  freeze_code_pr[10]="Freeze from HW";
  freeze_code_pr[11]="Road Event Rate Monitor";
  freeze_code_pr[12]="Hit Rx L1ID Skip";
  freeze_code_pr[13]="Hit Tx L1ID Skip";
  freeze_code_pr[14]="Tracks Out L1ID Skip";
  freeze_code_pr[15]="Gendata Rx L1ID Skip";
  freeze_code_pr[16]="freeze from AMB";
  freeze_code_pr[17]="RxBlock fifo overflow";
  freeze_code_pr[18]="Combiner Overflow (Expected)";
  freeze_code_pr[20]="Repeated word from AMB 0";
  freeze_code_pr[21]="Repeated word from AMB 1";
  freeze_code_pr[22]="Repeated word from AMB 2";
  freeze_code_pr[23]="Repeated word from AMB 3";
  freeze_code_pr[24]="TFBL Error";
  freeze_code_pr[25]="TFBL FIFO OVFL";
  freeze_code_pr[26]="TF Combiner Error";
  freeze_code_i2[28]="freeze from proc 1";
  freeze_code_i2[29]="freeze from proc 2";
  freeze_code_i2[30]="freeze from proc 3";
  freeze_code_i2[31]="freeze from proc 4";
  
  std::map<u_int,std::string> freeze_code[7]={freeze_code_i1,freeze_code_i2,freeze_code_pr,freeze_code_pr,freeze_code_pr,freeze_code_pr};

  //
  // Freeze status
  //
  cout << "Freeze Info: "<<endl;
  for(unsigned int fpgaidx=0;fpgaidx<6;fpgaidx++)
    {
      register_content=fpgas[fpgaidx]->read_word(FREEZE_STATUS);
      //if(register_content) freeze_high = true;

      cout << std::left << setw(20) << FPGATitles[fpgaidx] << ": 0x" << std::hex << register_content <<endl;

      for(u_int i = 0; i<32; i++) {
        u_int bit = (register_content>>i)&1;
        if(bit) cout << setw(16) << "" << "["<<std::dec<<setw(2)<<std::right<<i<<"]: " << freeze_code[fpgaidx][i] << endl;
      }
      if(register_content) cout << endl;

      // If Input2, also read the HW freeze bits
      if (fpgaidx==1) {
        register_content=fpgas[fpgaidx]->read_word(HW_FREEZE_STATUS);
        cout << std::left << setw(20) << "HitWarrior" << ": 0x" << std::hex << register_content <<endl;
        for(u_int i = 0; i<32; i++) {
          u_int bit = (register_content>>i)&1;
          if(bit) cout << setw(16) << "" << "["<<std::dec<<setw(2)<<std::right<<i<<"]: " << freeze_code_hw[i] << endl;
        }
        if(register_content) cout << endl;
      }
    }
}

/********************************************************************/
/********************************************************************
 * aux_status
 * *****************************************************************/
/********************************************************************/
bool aux_status( int slot ) { 
      VMEInterface *input1=VMEManager::global().aux(slot,1);
      VMEInterface *input2=VMEManager::global().aux(slot,2);
      VMEInterface *proc1 =VMEManager::global().aux(slot,3);
      VMEInterface *proc2 =VMEManager::global().aux(slot,4);
      VMEInterface *proc3 =VMEManager::global().aux(slot,5);
      VMEInterface *proc4 =VMEManager::global().aux(slot,6);

      VMEInterface *fpgas[6]={input1,input2,proc1,proc2,proc3,proc4};

      u_int register_content;
      bool freeze_high = false;

      ////
      cout << "--- FIRMWARE VERSIONS ---" << endl;
      for(unsigned int fpgaidx=0;fpgaidx<6;fpgaidx++)
        {
          register_content=fpgas[fpgaidx]->read_word(INFO);

          cout << FPGATitles[fpgaidx] << ":\t";
          cout << "0x" << std::hex << std::setfill('0') << setw(8) << register_content;

          register_content=fpgas[fpgaidx]->read_word(CHIPID2);
          cout << "\t(" << std::hex << std::setfill('0') << setw(8) << register_content;

          register_content=fpgas[fpgaidx]->read_word(CHIPID1);
          cout << std::hex << std::setfill('0') << setw(8) << register_content << ")";

          register_content=fpgas[fpgaidx]->read_word(GIT_FPGA);
          cout << " Git (FPGA): " << std::hex << std::setfill('0') << setw(8) << register_content;

          register_content=fpgas[fpgaidx]->read_word(GIT_AUXCOMMON);
          cout << " Git (AUXCommon): " << std::hex << std::setfill('0') << setw(8) << register_content;

          register_content=fpgas[fpgaidx]->read_word(CLOCK_FREQUENCY);
          cout << " Clock: " << std::dec << std::setfill('0') << setw(3) << register_content << " (MHz)";

          cout << endl;
        }
      cout << endl;
      ////

      ////
      cout << "--- INTERNAL LINES ---" << endl;
      for(unsigned int fpgaidx=0;fpgaidx<6;fpgaidx++)
        {
          register_content=fpgas[fpgaidx]->read_word(INTERNAL_LINES);

          cout << FPGATitles[fpgaidx] << ":\t";
          cout << "0x" << std::hex << std::setfill('0') << setw(2) << ((register_content>>16)&0xff) << std::dec << endl;
        }
      cout << endl;
      ////

      ////
      cout << "--- TEMPERATURE ---" << endl;
      cout << std::setfill(' ');
      for(unsigned int fpgaidx=0;fpgaidx<6;fpgaidx++)
        {
          cout << std::right << setw(15) << FPGATitles[fpgaidx];
        }
      cout << std::right << endl;

      
      int temperature;
      for(unsigned int fpgaidx=0;fpgaidx<6;fpgaidx++)
        {
          register_content=fpgas[fpgaidx]->read_word(TEMPERATURE);
          temperature=127-0xFF+((register_content>>8)&0xff);

          cout << std::right << setw(14) << temperature << "C";
        }
      cout << endl;
      ////
      
      //
      // Freeze status
      //
      cout << "-- FREEZE STATUS ---" << endl;
      
      cout << std::setfill(' ');
      cout << setw(17) << "";
      for(unsigned int fpgaidx=0;fpgaidx<6;fpgaidx++)
        {
          cout << std::left << setw(15) << FPGATitles[fpgaidx];
        }
      cout << std::right << endl;

      cout << setw(17) << "Freeze Status: ";
      for(unsigned int fpgaidx=0;fpgaidx<6;fpgaidx++)
        {
          register_content=fpgas[fpgaidx]->read_word(FREEZE_STATUS);
          if(register_content != 0) freeze_high = true;
          cout << "0x" << std::hex << std::left << setw(13) << register_content;
        }
      cout << std::right << endl;

      //HitWarrior freeze
      register_content=fpgas[1]->read_word(HW_FREEZE_STATUS);
      if(register_content != 0) freeze_high = true;
      cout << "    Hit Warrior:                0x"<<std::hex<<register_content << endl;


      cout << setw(17) << "FREEZEin/out: ";
      for(unsigned int fpgaidx=0;fpgaidx<6;fpgaidx++)
        {
          register_content=fpgas[fpgaidx]->read_word(FREEZE_LINES);

          cout << ((register_content>>25)&1) << "/" 
                << std::left << setw(13) << ((register_content>>24)&1);
        }
      cout << endl;

      cout << endl;
      ///

      cout << "--- SLINK STATUS ---" << endl;
      //
      // Input 1 SLINK Status
      //
      register_content=input1->read_word(SLINK_STATUS);

      cout << "Input 1 QSFP LDOWN =";
      cout << " " << !((register_content>>0)&0x1);
      cout << " " << !((register_content>>1)&0x1);
      cout << " " << !((register_content>>2)&0x1);
      cout << " " << !((register_content>>3)&0x1) << endl;

      //
      // Input 2 SLINK Status
      //
      register_content=input2->read_word(SLINK_STATUS);

      cout << "Input 2 QSFP LDOWN =";
      cout << " " << !((register_content>>0)&0x1);
      cout << " " << !((register_content>>1)&0x1);
      cout << " " << !((register_content>>2)&0x1);
      cout << " " << !((register_content>>3)&0x1) << endl;

      cout << endl;
      cout << "Input 2 SFP";
      cout << "  LDOWN = " << !((register_content>>4)&0x1)
		<< "\tLFF = " << !((register_content>>5)&0x1)
		<< "\tLRL = 0x" << std::hex << ((register_content>>16)&0xF) 
		<< endl;



      ////
      cout << endl;
      cout << "--- LINK STATUS ---" << endl;
      //
      // Input 2 Gen data
      //
      register_content=input2->read_word(LINK_STATUS);

      cout << "Input 2 Gendata LDOWN =";
      cout << " " << !((register_content>>20)&0x1);
      cout << " " << !((register_content>>21)&0x1);
      cout << " " << !((register_content>>22)&0x1);
      cout << " " << !((register_content>>23)&0x1) << endl;

      //
      // Processor link statuses
      //
      for(unsigned int fpgaidx=2;fpgaidx<6;fpgaidx++)
        {
          register_content=fpgas[fpgaidx]->read_word(LINK_STATUS);

          cout << "Processor " << (fpgaidx-1) << " Hit LDOWN =";
          cout << " " << !((register_content>>12)&0x1);
          cout << " " << !((register_content>>13)&0x1);
          cout << " " << !((register_content>>14)&0x1);
          cout << " " << !((register_content>>15)&0x1);
          cout << " " << !((register_content>>16)&0x1);
          cout << " " << !((register_content>>17)&0x1);
          cout << " " << !((register_content>>18)&0x1);
          cout << " " << !((register_content>>19)&0x1);
          cout << " " << !((register_content>>20)&0x1);
          cout << " " << !((register_content>>21)&0x1);
          cout << " " << !((register_content>>22)&0x1);
          cout << " " << !((register_content>>23)&0x1) << endl;
        }

      cout << endl;
      for(unsigned int fpgaidx=2;fpgaidx<6;fpgaidx++)
        {
          register_content=fpgas[fpgaidx]->read_word(LINK_STATUS);

          cout << "Processor " << (fpgaidx-1) << " AMB LDOWN =";
          cout << " " << !((register_content>>24)&0x1);
          cout << " " << !((register_content>>25)&0x1);
          cout << " " << !((register_content>>26)&0x1);
          cout << " " << !((register_content>>27)&0x1) << endl;
        }

      cout << endl;
      for(unsigned int fpgaidx=2;fpgaidx<6;fpgaidx++)
        {
          register_content=fpgas[fpgaidx]->read_word(LINK_STATUS);
          cout << "Processor " << (fpgaidx-1) << " Merge LDOWN =";
          cout << " " << !((register_content>>28)&0x1) << endl;

          cout << "Processor " << (fpgaidx-1) << "  Gen LDOWN =";
          cout << " " << !((register_content>>29)&0x1) << endl;
        }


      ////

      ////
      cout << endl;
      cout << "--- HOLD STATUS ---" << endl;
      //
      register_content=input2->read_word(HOLD_STATUS);

      cout << "Input 2 HOLDtoSFP:\t";
      cout << ((register_content>>4 )&0x1) << endl;

      cout << "Input 2 HOLDtoProcessor:";
      cout << ((register_content>>8 )&0x1) << " ";
      cout << ((register_content>>9 )&0x1) << " ";
      cout << ((register_content>>10)&0x1) << " ";
      cout << ((register_content>>11)&0x1) << " ";
      cout << endl;

      //
      register_content=input1->read_word(HOLD_STATUS);

      cout << "Input 1 HOLDfromAMB:\t";
      cout << ((register_content>>0 )&0x1) << " ";
      cout << ((register_content>>1 )&0x1) << " ";
      cout << ((register_content>>2 )&0x1) << " ";
      cout << ((register_content>>3 )&0x1) << " ";
      cout << ((register_content>>4 )&0x1) << " ";
      cout << ((register_content>>5 )&0x1) << " ";
      cout << ((register_content>>6 )&0x1) << " ";
      cout << ((register_content>>7 )&0x1) << endl;

      //
      register_content=input2->read_word(HOLD_STATUS);

      cout << "Input 2 HOLDfromAMB:\t";
      cout << ((register_content>>0 )&0x1) << " ";
      cout << ((register_content>>1 )&0x1) << " ";
      cout << ((register_content>>2 )&0x1) << " ";
      cout << ((register_content>>3 )&0x1) << endl;

      //
      register_content=input1->read_word(HITHOLD1_STATUS);

      cout << "Input 1 HOLDfromP1:\t";
      cout << ((register_content>>0 )&0x1) << " ";
      cout << ((register_content>>1 )&0x1) << " ";
      cout << ((register_content>>2 )&0x1) << " ";
      cout << ((register_content>>3 )&0x1) << " ";
      cout << ((register_content>>4 )&0x1) << " ";
      cout << ((register_content>>5 )&0x1) << " ";
      cout << ((register_content>>6 )&0x1) << " ";
      cout << ((register_content>>7 )&0x1) << endl;
      cout << "Input 1 HOLDfromP2:\t";
      cout << ((register_content>>8 )&0x1) << " ";
      cout << ((register_content>>9 )&0x1) << " ";
      cout << ((register_content>>10)&0x1) << " ";
      cout << ((register_content>>11)&0x1) << " ";
      cout << ((register_content>>12)&0x1) << " ";
      cout << ((register_content>>13)&0x1) << " ";
      cout << ((register_content>>14)&0x1) << " ";
      cout << ((register_content>>15)&0x1) << endl;
      cout << "Input 1 HOLDfromP3:\t";
      cout << ((register_content>>16)&0x1) << " ";
      cout << ((register_content>>17)&0x1) << " ";
      cout << ((register_content>>18)&0x1) << " ";
      cout << ((register_content>>19)&0x1) << " ";
      cout << ((register_content>>20)&0x1) << " ";
      cout << ((register_content>>21)&0x1) << " ";
      cout << ((register_content>>22)&0x1) << " ";
      cout << ((register_content>>23)&0x1) << endl;
      cout << "Input 1 HOLDfromP4:\t";
      cout << ((register_content>>24)&0x1) << " ";
      cout << ((register_content>>25)&0x1) << " ";
      cout << ((register_content>>26)&0x1) << " ";
      cout << ((register_content>>27)&0x1) << " ";
      cout << ((register_content>>28)&0x1) << " ";
      cout << ((register_content>>29)&0x1) << " ";
      cout << ((register_content>>30)&0x1) << " ";
      cout << ((register_content>>31)&0x1) << endl;

      //
      register_content=input2->read_word(HITHOLD2_STATUS);

      cout << "Input 2 HOLDfromP1:\t";
      cout << ((register_content>>16)&0x1) << " ";
      cout << ((register_content>>17)&0x1) << " ";
      cout << ((register_content>>18)&0x1) << " ";
      cout << ((register_content>>19)&0x1) << endl;
      cout << "Input 2 HOLDfromP2:\t";
      cout << ((register_content>>20)&0x1) << " ";
      cout << ((register_content>>21)&0x1) << " ";
      cout << ((register_content>>22)&0x1) << " ";
      cout << ((register_content>>23)&0x1) << endl;
      cout << "Input 2 HOLDfromP3:\t";
      cout << ((register_content>>24)&0x1) << " ";
      cout << ((register_content>>25)&0x1) << " ";
      cout << ((register_content>>26)&0x1) << " ";
      cout << ((register_content>>27)&0x1) << endl;
      cout << "Input 2 HOLDfromP4:\t";
      cout << ((register_content>>28)&0x1) << " ";
      cout << ((register_content>>29)&0x1) << " ";
      cout << ((register_content>>30)&0x1) << " ";
      cout << ((register_content>>31)&0x1) << endl;

      //
      for(unsigned int fpgaidx=2;fpgaidx<6;fpgaidx++)
        {
          register_content=fpgas[fpgaidx]->read_word(HOLD_STATUS);

          cout << endl;
          cout << "- Processor " << fpgaidx-1 << " HOLDs" << endl;

          cout << "HOLDfromSFP:\t";
          cout << ((register_content>>16)&0x1) << endl;

          cout << "HOLDfromInput2:\t";
          cout << ((register_content>>17)&0x1) << endl;

          cout << "HOLDtoHS:\t";
          cout << ((register_content>>0 )&0x1) << " ";
          cout << ((register_content>>1 )&0x1) << " ";
          cout << ((register_content>>2 )&0x1) << " ";
          cout << ((register_content>>3 )&0x1) << " ";
          cout << ((register_content>>4 )&0x1) << " ";
          cout << ((register_content>>5 )&0x1) << " ";
          cout << ((register_content>>6 )&0x1) << " ";
          cout << ((register_content>>7 )&0x1) << " ";
          cout << ((register_content>>8 )&0x1) << " ";
          cout << ((register_content>>9 )&0x1) << " ";
          cout << ((register_content>>10)&0x1) << " ";
          cout << ((register_content>>11)&0x1) << endl;

          cout << "HOLDtoAMB:\t";
          cout << ((register_content>>12)&0x1) << " ";
          cout << ((register_content>>13)&0x1) << " ";
          cout << ((register_content>>14)&0x1) << " ";
          cout << ((register_content>>15)&0x1) << endl;
        }

      ////
      if(freeze_high){
        cout << "WARNING: FREEZE status is non-zero" << endl;
        aux_freeze_status(slot);
      }
      

      cout << "--- OUTPUT TRACKS ---" << endl;
      cout << "\t" << std::dec << input2->read_word(OUTPUT_TRACKS) << endl;


      return 0;
}



/********************************************************************/
/********************************************************************
 * aux_proc_status
 * *****************************************************************/
/********************************************************************/
bool aux_proc_status( int slot, bool do_details, bool rate_details )
    { 
      VMEInterface *input1=VMEManager::global().aux(slot,1);
      VMEInterface *input2=VMEManager::global().aux(slot,2);
      VMEInterface *proc1 =VMEManager::global().aux(slot,3);
      VMEInterface *proc2 =VMEManager::global().aux(slot,4);
      VMEInterface *proc3 =VMEManager::global().aux(slot,5);
      VMEInterface *proc4 =VMEManager::global().aux(slot,6);

      VMEInterface *fpgas[6]={input1,input2,proc1,proc2,proc3,proc4};

      u_int register_content;
      bool freeze_high = false;
      
      cout << endl;
      cout << setw(22) << "" << setw(22) << "P1" << setw(22) << "P2" << setw(22) << "P3" << setw(22) << "P4" << setw(22) << "Register[Bit]:" << endl;

      // 
      // Check FREEZE status
      //
      for(unsigned int fpgaidx=0;fpgaidx<6;fpgaidx++)
	{
	  register_content=fpgas[fpgaidx]->read_word(FREEZE_STATUS);
	  if(register_content != 0) freeze_high = true;
	}
      register_content=fpgas[1]->read_word(HW_FREEZE_STATUS);
      if(register_content != 0) freeze_high = true;


      //
      //  DF Sync status
      //
      vector<u_int> DFSync_status_words;
      for(unsigned int fpgaidx=3;fpgaidx<=6;fpgaidx++){
	register_content=fpgas[fpgaidx-1]->read_word(DFSYNCH_STATUS);
	DFSync_status_words.push_back(register_content);
      }

      cout << endl;
      cout << "DFSynch Status:" << endl;
      cout << setw(22) << "SCT Enable:" ;
      for(unsigned int i=0;i<4;i++){
	cout << setw(18);
	cout << ((DFSync_status_words.at(i)>>0)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>1)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>2)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>3)&0x1) ;
        cout << ((DFSync_status_words.at(i)>>4)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << DFSYNCH_STATUS <<"[4..0]";
      cout <<endl;

      cout << setw(22) << "Pixel Enable:" ;
      for(unsigned int i=0;i<4;i++){
	cout << setw(17);
	cout << ((DFSync_status_words.at(i)>>5)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>6)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>7)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>8)&0x1) ;
        cout << ((DFSync_status_words.at(i)>>9)&0x1) ;
        cout << ((DFSync_status_words.at(i)>>10)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << DFSYNCH_STATUS <<"[10..5]";
      cout <<endl;

      cout << setw(22) << "Road Enable:" ;
      for(unsigned int i=0;i<4;i++){
	cout << setw(19);
	cout << ((DFSync_status_words.at(i)>>11)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>12)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>13)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>14)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << DFSYNCH_STATUS <<"[14..11]";
      cout <<endl;


      cout << setw(22) << "SCT Ready:" ;
      for(unsigned int i=0;i<4;i++){
	cout << setw(18);
	cout << ((DFSync_status_words.at(i)>>15)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>16)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>17)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>18)&0x1) ;
        cout << ((DFSync_status_words.at(i)>>19)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << DFSYNCH_STATUS <<"[19..15]";
      cout <<endl;


      cout << setw(22) << "Pixel Ready:" ;
      for(unsigned int i=0;i<4;i++){
	cout << setw(17);
	cout << ((DFSync_status_words.at(i)>>20)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>21)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>22)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>23)&0x1) ;
        cout << ((DFSync_status_words.at(i)>>24)&0x1) ;
        cout << ((DFSync_status_words.at(i)>>25)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << DFSYNCH_STATUS <<"[25..20]";
      cout <<endl;

      cout << setw(22) << "Road Ready:" ;
      for(unsigned int i=0;i<4;i++){
	cout << setw(19);
	cout << ((DFSync_status_words.at(i)>>26)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>27)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>28)&0x1) ;
	cout << ((DFSync_status_words.at(i)>>29)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << DFSYNCH_STATUS <<"[29..26]";
      cout <<endl;

      cout << setw(22) << "Header Full:" ;
      for(unsigned int i=0;i<4;i++){
	cout << setw(22) << ((DFSync_status_words.at(i)>>30)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << DFSYNCH_STATUS <<"[30]";
      cout <<endl;

      cout << setw(22) << "Trailer Full:" ;
      for(unsigned int i=0;i<4;i++){
	cout << setw(22) << ((DFSync_status_words.at(i)>>31)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << DFSYNCH_STATUS <<"[31]";
      cout <<endl;

      //
      // AMB Round Robin
      //
      vector<u_int> AMBRR_in_0;
      vector<u_int> AMBRR_in_1;
      vector<u_int> AMBRR_in_2;
      vector<u_int> AMBRR_in_3;
      vector<u_int> AMBRR_out;
      for(unsigned int fpgaidx=3;fpgaidx<=6;fpgaidx++){
	register_content=fpgas[fpgaidx-1]->read_word(AMBRR_MONITOR+(0<<8));
	AMBRR_in_0.push_back(register_content);
	register_content=fpgas[fpgaidx-1]->read_word(AMBRR_MONITOR+(1<<8));
	AMBRR_in_1.push_back(register_content);
	register_content=fpgas[fpgaidx-1]->read_word(AMBRR_MONITOR+(2<<8));
	AMBRR_in_2.push_back(register_content);
	register_content=fpgas[fpgaidx-1]->read_word(AMBRR_MONITOR+(3<<8));
	AMBRR_in_3.push_back(register_content);

	register_content=fpgas[fpgaidx-1]->read_word(AMBRR_MONITOR+(4<<8));
	AMBRR_out.push_back(register_content);
      }

      cout << endl;
      cout << "AMB Round Robin Status:" << endl;
      cout << setw(22) << "Input Ch 0:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << AMBRR_in_0.at(i); 
      }
      cout << setw(18) << "0x" << std::hex << AMBRR_MONITOR+(0<<8);
      cout << endl;
      cout << setw(22) << "Input Ch 1:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << AMBRR_in_1.at(i); 
      }
      cout << setw(18) << "0x" << std::hex << AMBRR_MONITOR+(1<<8);
      cout << endl;
      cout << setw(22) << "Input Ch 2:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << AMBRR_in_2.at(i); 
      }
      cout << setw(18) << "0x" << std::hex << AMBRR_MONITOR+(2<<8);
      cout << endl;
      cout << setw(22) << "Input Ch 3:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << AMBRR_in_3.at(i); 
      }
      cout << setw(18) << "0x" << std::hex << AMBRR_MONITOR+(3<<8);
      cout << endl;
      cout << setw(22) << "Output:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << std::dec << AMBRR_out.at(i); 
      }
      cout << setw(18) << "0x" << std::hex << AMBRR_MONITOR+(4<<8);
      cout << endl;

      //
      //  DO Events / Event Rate
      //
      vector<u_int> DO_WR_event_count;
      vector<u_int> DO_WR_event_rate;
      vector<u_int> DO_RD_event_count;
      vector<u_int> DO_RD_event_rate;
      vector<u_int> DO_road_count;
      vector<u_int> DO_road_rate;
      for(unsigned int fpgaidx=3;fpgaidx<=6;fpgaidx++){
	register_content=fpgas[fpgaidx-1]->read_word(TOTAL_RD_EVENTS);
	DO_RD_event_count.push_back(register_content);

	register_content=fpgas[fpgaidx-1]->read_word(RATE_RD_EVENTS);
	DO_RD_event_rate.push_back(register_content);

	register_content=fpgas[fpgaidx-1]->read_word(TOTAL_WR_EVENTS);
	DO_WR_event_count.push_back(register_content);

	register_content=fpgas[fpgaidx-1]->read_word(RATE_WR_EVENTS);
	DO_WR_event_rate.push_back(register_content);

	register_content=fpgas[fpgaidx-1]->read_word(TOTAL_ROADS);
	DO_road_count.push_back(register_content);

	register_content=fpgas[fpgaidx-1]->read_word(RATE_ROADS);	
	DO_road_rate.push_back(register_content);
      }

      cout << endl;
      cout << "DataOrganizer Status:" << endl;
      cout << " Write Mode:" << endl; 
      cout << setw(22) << "Event Count:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << std::dec << DO_WR_event_count.at(i); 
      }
      cout << setw(18) << "0x" << std::hex << TOTAL_WR_EVENTS;
      cout << endl;
      cout << setw(22) << "Event Rate:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << std::dec << DO_WR_event_rate.at(i); 
      }
      cout << setw(18) << "0x" << std::hex << RATE_WR_EVENTS;
      cout << endl;


      cout << " Read Mode:" << endl; 
      cout << setw(22) << "Event Count:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << std::dec << DO_RD_event_count.at(i); 
      }
      cout << setw(18) << "0x" << std::hex << TOTAL_RD_EVENTS;
      cout << endl;
      cout << setw(22) << "Event Rate:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << std::dec << DO_RD_event_rate.at(i); 
      }
      cout << setw(18) << "0x" << std::hex << RATE_RD_EVENTS;
      cout << endl;

      cout << setw(22) << "Road Count:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << std::dec << DO_road_count.at(i); 
      }
      cout << setw(18) << "0x" << std::hex << TOTAL_ROADS;
      cout << endl;
      cout << setw(22) << "Road Rate:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << std::dec << DO_road_rate.at(i); 
      }
      cout << setw(18) << "0x" << std::hex << RATE_ROADS;
      cout << endl;


      //
      //  DO status
      //
      vector<u_int> DO_status_words;
      for(unsigned int fpgaidx=3;fpgaidx<=6;fpgaidx++){
	register_content=fpgas[fpgaidx-1]->read_word(DO_STATUS);
	DO_status_words.push_back(register_content);
      }

      vector<float> DO_writing_fraction;
      vector<float> DO_reading_fraction;
      vector<float> DO_writingNotReading_fraction;
      vector<float> DO_readingNotWriting_fraction;
      vector<float> DO_waiting_fraction;

      for(unsigned int fpgaidx=3;fpgaidx<=6;fpgaidx++){
	register_content=fpgas[fpgaidx-1]->read_word(CLOCK_FREQUENCY);

	uint clock = register_content;
	uint period = PERIOD;
	uint frequency = FREQUENCY;
	if(clock!=0){//Old firmware. Use default values
	  frequency = clock*1e6;
	  period    = clock*1e7;
	}
	
	register_content=fpgas[fpgaidx-1]->read_word(DO_WRITING_FRACTION);
	DO_writing_fraction.push_back((float)register_content/period);

	register_content=fpgas[fpgaidx-1]->read_word(DO_READING_FRACTION);
	DO_reading_fraction.push_back((float)register_content/period);

	register_content=fpgas[fpgaidx-1]->read_word(DO_WRITINGNOTREADING_FRACTION);
	DO_writingNotReading_fraction.push_back((float)register_content/period);

	register_content=fpgas[fpgaidx-1]->read_word(DO_READINGNOTWRITING_FRACTION);
	DO_readingNotWriting_fraction.push_back((float)register_content/period);

	register_content=fpgas[fpgaidx-1]->read_word(DO_WAITING_FRACTION);
	DO_waiting_fraction.push_back((float)register_content/period);
      }

      cout << setw(22) << "DO Writing:";
      for(unsigned int i=0;i<4;i++){
	cout << setw(22) << std::dec << std::fixed << std::setprecision(3) << DO_writing_fraction.at(i);
      }
      cout << setw(18) << "0x" << std::hex << DO_WRITING_FRACTION;
      cout <<endl;

      cout << setw(22) << "DO Reading:";
      for(unsigned int i=0;i<4;i++){
	cout << setw(22) << std::dec << std::fixed << std::setprecision(3) << DO_reading_fraction.at(i);
      }
      cout << setw(18) << "0x" << std::hex << DO_READING_FRACTION;
      cout <<endl;

      cout << setw(22) << "DO WritingNotReading:";
      for(unsigned int i=0;i<4;i++){
	cout << setw(22) << std::dec << std::fixed << std::setprecision(3) << DO_writingNotReading_fraction.at(i);
      }
      cout << setw(18) << "0x" << std::hex << DO_WRITINGNOTREADING_FRACTION;
      cout <<endl;

      cout << setw(22) << "DO ReadingNotWriting:";
      for(unsigned int i=0;i<4;i++){
	cout << setw(22) << std::dec << std::fixed << std::setprecision(3) << DO_readingNotWriting_fraction.at(i);
      }
      cout << setw(18) << "0x" << std::hex << DO_READINGNOTWRITING_FRACTION;
      cout <<endl;

      cout << setw(22) << "DO Waiting:";
      for(unsigned int i=0;i<4;i++){
	cout << setw(22) << std::dec << std::fixed << std::setprecision(3) << DO_waiting_fraction.at(i);
      }
      cout << setw(18) << "0x" << std::hex << DO_WAITING_FRACTION;
      cout <<endl;


      cout << " Pix Layers:" << endl; 
      cout << setw(22) << "WR done:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(17);
	cout     << ((DO_status_words.at(i)>>0)&0x1); 
	cout     << ((DO_status_words.at(i)>>2)&0x1); 
	cout     << ((DO_status_words.at(i)>>4)&0x1); 
	cout     << ((DO_status_words.at(i)>>6)&0x1); 
	cout     << ((DO_status_words.at(i)>>8)&0x1); 
	cout     << ((DO_status_words.at(i)>>10)&0x1);
      }
      cout << setw(18) << "0x" << std::hex << DO_STATUS <<"[10..0]even";
      cout << endl;

      cout << setw(22) << "RD done:";
      for(unsigned int i=0;i<4;i++){
	cout << setw(17);
	cout << ((DO_status_words.at(i)>>1)&0x1); 
	cout << ((DO_status_words.at(i)>>3)&0x1); 
	cout << ((DO_status_words.at(i)>>5)&0x1); 
	cout << ((DO_status_words.at(i)>>7)&0x1); 
	cout << ((DO_status_words.at(i)>>9)&0x1); 
	cout << ((DO_status_words.at(i)>>11)&0x1);
      }
      cout << setw(18) << "0x" << std::hex << DO_STATUS <<"[11..1]odd";
      cout << endl;;
	  
      cout << "  SCT Layers:" << endl;  
      cout << setw(22) << "WR done:";
      for(unsigned int i=0;i<4;i++){
	cout << setw(18);
	cout << ((DO_status_words.at(i)>>12)&0x1) ;
	cout << ((DO_status_words.at(i)>>14)&0x1) ;
	cout << ((DO_status_words.at(i)>>16)&0x1) ;
	cout << ((DO_status_words.at(i)>>18)&0x1) ;
	cout << ((DO_status_words.at(i)>>20)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << DO_STATUS <<"[20..12]even";
      cout << endl;

      cout << setw(22) << "RD done:";
      for(unsigned int i=0;i<4;i++){
	cout << setw(18) ;
	cout << ((DO_status_words.at(i)>>13)&0x1) ;
	cout << ((DO_status_words.at(i)>>15)&0x1) ;
	cout << ((DO_status_words.at(i)>>17)&0x1) ;
	cout << ((DO_status_words.at(i)>>19)&0x1) ;
	cout << ((DO_status_words.at(i)>>21)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << DO_STATUS <<"[21..13]odd";
      cout << endl;

      cout << setw(22) << "Wait req:" ;
      for(unsigned int i=0;i<4;i++){
	cout << setw(22) << ((DO_status_words.at(i)>>26)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << DO_STATUS <<"[26]";
      cout <<endl;

      cout << setw(22) << "Calibration Success:" ;
      for(unsigned int i=0;i<4;i++){
        cout << setw(22) << ((DO_status_words.at(i)>>22)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << DO_STATUS <<"[22]";
      cout <<endl;
      
      if(do_details){
        vector<u_int> FIFO_status_words_1;
        vector<u_int> FIFO_status_words_2;
        vector<u_int> FIFO_status_words_3;
        for(unsigned int fpgaidx=3;fpgaidx<=6;fpgaidx++){
	  register_content=fpgas[fpgaidx-1]->read_word(DO_SS_FIFO_1);
	  FIFO_status_words_1.push_back(register_content);
	  
	  register_content=fpgas[fpgaidx-1]->read_word(DO_SS_FIFO_2);
	  FIFO_status_words_2.push_back(register_content);
	  
	  register_content=fpgas[fpgaidx-1]->read_word(DO_SS_FIFO_3);
	  FIFO_status_words_3.push_back(register_content);
        }
        
        cout << " SS FIFO status:" << endl;
        cout << setw(22) << "Pix lay0:";	
        for(unsigned int i=0;i<4;i++){
	  cout << setw(22) << std::bitset<8>((FIFO_status_words_1.at(i)>>0)&0xFF) ;
        }
	cout << setw(18) << "0x" << std::hex << DO_SS_FIFO_1 <<"[7..0]";
        cout <<endl;
        
        cout << setw(22) << "Pix lay1:";	
        for(unsigned int i=0;i<4;i++){
          cout << setw(22) << std::bitset<8>((FIFO_status_words_1.at(i)>>8)&0xFF) ;
        }
	cout << setw(18) << "0x" << std::hex << DO_SS_FIFO_1 <<"[15..8]";
        cout <<endl;
        	
        
        cout << setw(22) << "Pix lay2:";	
        for(unsigned int i=0;i<4;i++){
          cout << setw(22) << std::bitset<8>((FIFO_status_words_1.at(i)>>16)&0xFF) ;
        }
	cout << setw(18) << "0x" << std::hex << DO_SS_FIFO_1 <<"[23..16]";
        cout <<endl;
        
        cout << setw(22) << "Pix lay3:";	
        for(unsigned int i=0;i<4;i++){
          cout << setw(22) << std::bitset<8>((FIFO_status_words_1.at(i)>>24)&0xFF) ;
        }
	cout << setw(18) << "0x" << std::hex << DO_SS_FIFO_1 <<"[31..24]";
        cout <<endl;
        
        cout << setw(22) << "Pix lay4:";	
        for(unsigned int i=0;i<4;i++){
          cout << setw(22) << std::bitset<8>((FIFO_status_words_2.at(i)>>0)&0xFF) ;
        }
	cout << setw(18) << "0x" << std::hex << DO_SS_FIFO_2 <<"[7..0]";
        cout <<endl;
        
        cout << setw(22) << "Pix lay5:";	
        for(unsigned int i=0;i<4;i++){
          cout << setw(22) << std::bitset<8>((FIFO_status_words_2.at(i)>>8)&0xFF) ;
        }
	cout << setw(18) << "0x" << std::hex << DO_SS_FIFO_2 <<"[15..8]";
        cout <<endl;
        
        cout << setw(22) << "SCT lay0:";	
        for(unsigned int i=0;i<4;i++){
          cout << setw(22) << std::bitset<8>((FIFO_status_words_2.at(i)>>16)&0xFF) ;
        }
	cout << setw(18) << "0x" << std::hex << DO_SS_FIFO_2 <<"[23..16]";
        cout <<endl;
        
        cout << setw(22) << "SCT lay1:";	
        for(unsigned int i=0;i<4;i++){
          cout << setw(22) << std::bitset<8>((FIFO_status_words_2.at(i)>>24)&0xFF) ;
        }
	cout << setw(18) << "0x" << std::hex << DO_SS_FIFO_2 <<"[31..24]";
        cout <<endl;
        
        cout << setw(22) << "SCT lay2:";	
        for(unsigned int i=0;i<4;i++){
          cout << setw(22) << std::bitset<8>((FIFO_status_words_3.at(i)>>0)&0xFF) ;
        }
	cout << setw(18) << "0x" << std::hex << DO_SS_FIFO_3 <<"[7..0]";
        cout <<endl;
        
        cout << setw(22) << "SCT lay3:";	
        for(unsigned int i=0;i<4;i++){
          cout << setw(22) << std::bitset<8>((FIFO_status_words_3.at(i)>>8)&0xFF) ;
        }
	cout << setw(18) << "0x" << std::hex << DO_SS_FIFO_3 <<"[15..8]";
        cout <<endl;
        
        cout << setw(22) << "SCT lay4:";	
        for(unsigned int i=0;i<4;i++){
          cout << setw(22) << std::bitset<8>((FIFO_status_words_3.at(i)>>16)&0xFF) ;
        }
	cout << setw(18) << "0x" << std::hex << DO_SS_FIFO_3 <<"[23..16]";
        cout <<endl;
      }



      //
      //  TF status
      //
      vector<vector<float>> TFBL_holdRate(4,vector<float>(12,0));

      vector<u_int> TF_status_words;
      vector<u_int> TF_fitdemux_state;
      vector<vector<u_int> > TF_combiner_state;
      
      vector<u_int> TF_total_fit_count;
      vector<u_int> TF_total_fit_rate;
      vector<vector<u_int> > TF_fit_count;
      vector<vector<u_int> > TF_fit_rate;

      vector<u_int> TF_total_track_count;
      vector<u_int> TF_total_track_rate;
      vector<vector<u_int> > TF_track_count;
      vector<vector<u_int> > TF_track_rate;

      vector<u_int> TF_total_event_count;
      vector<u_int> TF_total_event_rate;
      vector<vector<u_int> > TF_event_count;
      vector<vector<u_int> > TF_event_rate;
      vector<u_int> TF_FITTERS_WORKING_FRACTION = {0x000003e0, 0x000003f0, 0x00000400, 0x00000410, 0x00000420, 0x00000430, 0x00000440};
      vector<u_int> TF_FITTERS_WRITING_FRACTION = {0x000004c0, 0x000004d0, 0x000004e0, 0x000004f0, 0x00000500, 0x00000510, 0x00000520};
      vector<u_int> TF_CONSTANTS_READY_FRACTION = {0x00000530, 0x00000540, 0x00000550, 0x00000560, 0x000005a0, 0x000005b0, 0x000005c0};//570, 580 590:GIT_FPGA, GIT_AUXComon,CLOCKFREQ
      vector<u_int> TF_WAITINGFORCONSTS_FRACTION ={0x00000450, 0x00000460, 0x00000470, 0x00000480, 0x00000490, 0x000004a0, 0x000004b0};
      u_int TF_CONSTSREAD_OR_FRACTION = 0x000005d0;

      
      vector<vector<float>> TF_fitters_working_fraction;
      vector<vector<float>> TF_waitingforconsts_fraction;
      vector<vector<float>> TF_fitters_writing_fraction;
      vector<vector<float>> TF_constants_ready_fraction;
      vector<float> TF_constsread_or_fraction;

      for(unsigned int fpgaidx=3;fpgaidx<=6;fpgaidx++){
	register_content=fpgas[fpgaidx-1]->read_word(CLOCK_FREQUENCY);

	uint clock = register_content;
	uint period = PERIOD;
	uint frequency = FREQUENCY;
	if(clock!=0){//Old firmware. Use default values
	  frequency = clock*1e6;
	  period    = clock*1e7;
	}
	
	for(unsigned int i=0; i<12; i++){
	  register_content=fpgas[fpgaidx-1]->read_word(TFBL_HOLDRATE+(i<<8));
	  TFBL_holdRate[fpgaidx-3][i] = (float)register_content/period;
	}

	register_content=fpgas[fpgaidx-1]->read_word(TF_STATUS);
	TF_status_words.push_back(register_content);

	TF_fitdemux_state.push_back( (register_content>>5) & 0x1f ); // bits [9 downto 5]
	TF_combiner_state.push_back(vector<u_int>());
	
	TF_fit_count.push_back(vector<u_int>());
	TF_fit_rate .push_back(vector<u_int>());

	TF_track_count.push_back(vector<u_int>());
	TF_track_rate .push_back(vector<u_int>());

	TF_event_count.push_back(vector<u_int>());
	TF_event_rate .push_back(vector<u_int>());
	
	if (rate_details) {
	  TF_fitters_working_fraction.push_back(vector<float>());
          TF_waitingforconsts_fraction.push_back(vector<float>());
          TF_fitters_writing_fraction.push_back(vector<float>());
          TF_constants_ready_fraction.push_back(vector<float>());
	}
	u_int total_fits     = 0;
	u_int total_fit_rate = 0;

	u_int total_tracks     = 0;
	u_int total_track_rate = 0;

	u_int total_events     = 0;
	u_int total_event_rate = 0;

	//
	// 7 Fitters in the TF
	//
	for(unsigned int fitteridx=0;fitteridx<=6;fitteridx++){
	  TF_combiner_state.back().push_back( (TF_status_words[fpgaidx-3] >> (10+(2*fitteridx))) & 0x3 );
	  
	  register_content=fpgas[fpgaidx-1]->read_word((0x4d<<16)+(fitteridx<<8));
	  TF_fit_count.at(fpgaidx-3).push_back(register_content);
	  total_fits += register_content;

	  register_content=fpgas[fpgaidx-1]->read_word((0x4e<<16)+(fitteridx<<8));
	  TF_fit_rate.at(fpgaidx-3).push_back(register_content);
	  total_fit_rate += register_content;

	  register_content=fpgas[fpgaidx-1]->read_word((0x4f<<16)+(fitteridx<<8));
	  TF_track_count.at(fpgaidx-3).push_back(register_content);
	  total_tracks += register_content;

	  register_content=fpgas[fpgaidx-1]->read_word((0x50<<16)+(fitteridx<<8));
	  TF_track_rate.at(fpgaidx-3).push_back(register_content);
	  total_track_rate += register_content;

	  register_content=fpgas[fpgaidx-1]->read_word((0x51<<16)+(fitteridx<<8));
	  TF_event_count.at(fpgaidx-3).push_back(register_content);
	  total_events += register_content;

	  register_content=fpgas[fpgaidx-1]->read_word((0x52<<16)+(fitteridx<<8));
	  TF_event_rate.at(fpgaidx-3).push_back(register_content);
	  total_event_rate += register_content;

	  if (rate_details) {
	    register_content=fpgas[fpgaidx-1]->read_word(TF_FITTERS_WORKING_FRACTION.at(fitteridx));
	    TF_fitters_working_fraction.at(fpgaidx-3).push_back((float)register_content/period);
	    register_content=fpgas[fpgaidx-1]->read_word(TF_WAITINGFORCONSTS_FRACTION.at(fitteridx));
	    TF_waitingforconsts_fraction.at(fpgaidx-3).push_back((float)register_content/period);
	    register_content=fpgas[fpgaidx-1]->read_word(TF_FITTERS_WRITING_FRACTION.at(fitteridx));
	    TF_fitters_writing_fraction.at(fpgaidx-3).push_back((float)register_content/period);
	    register_content=fpgas[fpgaidx-1]->read_word(TF_CONSTANTS_READY_FRACTION.at(fitteridx));
	    TF_constants_ready_fraction.at(fpgaidx-3).push_back((float)register_content/period);
	  }

	}

	TF_total_fit_count.push_back(total_fits);
	TF_total_fit_rate .push_back(total_fit_rate);
	TF_total_track_count.push_back(total_tracks);
	TF_total_track_rate .push_back(total_track_rate);
	TF_total_event_count.push_back(total_events);
	TF_total_event_rate .push_back(total_event_rate);
	if (rate_details) {
	  register_content=fpgas[fpgaidx-1]->read_word(TF_CONSTSREAD_OR_FRACTION);
	  TF_constsread_or_fraction.push_back((float)register_content/period);
	}
      }

      cout << endl;
      cout << "TrackFitter Status:" << endl;

      //
      //  TF State Machines
      //
      cout << " State machines:" << endl;
      cout << setw(22) << " FitDEMUX:";
      for(unsigned int i=0;i<4;i++){
	cout << setw(22) << std::dec << TF_fitdemux_state.at(i);
      }
      cout << endl;
      for(unsigned int ifitter=0;ifitter<7;ifitter++){
	cout << setw(20) << " Combiner" << ifitter << ":";
	for(unsigned int i=0;i<4;i++){
	  cout << setw(22) << TF_combiner_state.at(i).at(ifitter);
	}
	cout << endl;
      }
      cout << endl;
      
      
      //
      //  TF Fit Counts
      //
      cout << " Fits:" << endl; 
      cout << setw(22) << " Count:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << std::dec << TF_total_fit_count.at(i);
      }
      cout << endl;

      if(do_details){
	cout << endl;
	for(unsigned int ifitter=0;ifitter<7;ifitter++){
	  cout << setw(21) << " fitter index: " << ifitter; 
	  for(unsigned int i=0;i<4;i++){
	    cout     << setw(22) << std::dec << TF_fit_count.at(i).at(ifitter);
	  }
	  cout << endl;
	}
	cout << endl;
      }

      //
      //  TF Fit Rate
      //
      cout << setw(22) << " Rate:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << std::dec << TF_total_fit_rate.at(i);
      }
      cout << endl;

      if(do_details){
	cout << endl;
	for(unsigned int ifitter=0;ifitter<7;ifitter++){
	  cout << setw(21) << " fitter index: " << ifitter; 
	  for(unsigned int i=0;i<4;i++){
	    cout     << setw(22) << std::dec << TF_fit_rate.at(i).at(ifitter);
	  }
	  cout << endl;
	}
	cout << endl;
      }

      //
      //  TF Track Counts
      //
      cout << " Tracks:" << endl; 
      cout << setw(22) << " Count:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << std::dec << TF_total_track_count.at(i);
      }
      cout << endl;

      if(do_details){
	cout << endl;
	for(unsigned int ifitter=0;ifitter<7;ifitter++){
	  cout << setw(21) << " fitter index: " << ifitter; 
	  for(unsigned int i=0;i<4;i++){
	    cout     << setw(22) << std::dec << TF_track_count.at(i).at(ifitter);
	  }
	  cout << endl;
	}
	cout << endl;
      }

      //
      //  TF Track Rate
      //
      cout << setw(22) << " Rate (~/event):";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(16) << std::dec << TF_total_track_rate.at(i);
	cout     << "(" << std::setw(4) << std::setprecision(1) << ((float)TF_total_track_rate.at(i))/(float(TF_total_event_rate.at(i))/7) << ")";
      }
      cout << endl;

      if(do_details){
	cout << endl;
	for(unsigned int ifitter=0;ifitter<7;ifitter++){
	  cout << setw(21) << " fitter index: " << ifitter; 
	  for(unsigned int i=0;i<4;i++){
	    cout     << setw(22) << std::dec << TF_track_rate.at(i).at(ifitter);
	  }
	  cout << endl;
	}
	cout << endl;
      }
      cout << endl;

      //
      //  TF Event Counts
      //
      cout << " Event:" << endl; 
      cout << setw(22) << " Count:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << std::dec << TF_event_count.at(i).at(0);
      }
      cout << endl;

      if(do_details){
	cout << endl;
	for(unsigned int ifitter=0;ifitter<7;ifitter++){
	  cout << setw(21) << " fitter index: " << ifitter; 
	  for(unsigned int i=0;i<4;i++){
	    cout     << setw(22) << std::dec << TF_event_count.at(i).at(ifitter);
	  }
	  cout << endl;
	}
	cout << endl;
      }

      //
      //  TF Event Rate
      //
      cout << setw(22) << " Rate:";
      for(unsigned int i=0;i<4;i++){
	cout     << setw(22) << std::dec << int(float(TF_total_event_rate.at(i))/7);
      }
      cout << endl;

      if(do_details){
	cout << endl;
	for(unsigned int ifitter=0;ifitter<7;ifitter++){
	  cout << setw(21) << " fitter index: " << ifitter; 
	  for(unsigned int i=0;i<4;i++){
	    cout     << setw(22) << std::dec << TF_event_rate.at(i).at(ifitter);
	  }
	  cout << endl;
	}
	cout << endl;
      }
      cout << endl;

      //
      //Details for TF fit rate studies
      //

      if(rate_details){
	cout << " TF Fitter Writing Frac:" << endl;
        cout << endl;
        for(unsigned int ifitter=0;ifitter<7;ifitter++){
          cout << setw(21) << " fitter index: " << ifitter;
          for(unsigned int i=0;i<4;i++){
            cout     << setw(22) << std::dec << std::fixed << std::setprecision(3) << TF_fitters_writing_fraction.at(i).at(ifitter);
          }
	  cout << setw(18) << "0x" << std::hex << TF_FITTERS_WRITING_FRACTION.at(ifitter);
          cout << endl;
        }
        cout << endl;
	cout << " TF WaitingForConsts Frac:" << endl;
	cout << endl;
	for(unsigned int ifitter=0;ifitter<7;ifitter++){
          cout << setw(21) << " fitter index: " << ifitter;
          for(unsigned int i=0;i<4;i++){
            cout     << setw(22) << std::dec << std::fixed << std::setprecision(3) << TF_waitingforconsts_fraction.at(i).at(ifitter);
          }
          cout << setw(18) << "0x" << std::hex << TF_WAITINGFORCONSTS_FRACTION.at(ifitter);
          cout << endl;
        }
        cout << endl;
	cout << " TF Fitter Working Frac:" << endl;
        cout << endl;
	for(unsigned int ifitter=0;ifitter<7;ifitter++){
          cout << setw(21) << " fitter index: " << ifitter;
          for(unsigned int i=0;i<4;i++){
            cout     << setw(22) << std::dec << std::fixed << std::setprecision(3) << TF_fitters_working_fraction.at(i).at(ifitter);
          }
          cout << setw(18) << "0x" << std::hex << TF_FITTERS_WORKING_FRACTION.at(ifitter);
          cout << endl;
        }
        cout << endl;
	cout << " TF Constants Ready Frac:" << endl;
        cout << endl;
	for(unsigned int ifitter=0;ifitter<7;ifitter++){
          cout << setw(21) << " fitter index: " << ifitter;
          for(unsigned int i=0;i<4;i++){
            cout     << setw(22) << std::dec << std::fixed << std::setprecision(3) << TF_constants_ready_fraction.at(i).at(ifitter);
          }
          cout << setw(18) << "0x" << std::hex << TF_CONSTANTS_READY_FRACTION.at(ifitter);
          cout << endl;
        }
	cout << endl;
        cout << " TF Constants Read Frac:" << endl;
	cout << setw(21) << " " << "";
	for(unsigned int i=0;i<4;i++){
	  cout     << setw(22) << std::dec << std::fixed << std::setprecision(3) << TF_constsread_or_fraction.at(i);
	}
	cout << setw(18) << "0x" << std::hex << TF_CONSTSREAD_OR_FRACTION;
        cout << endl;

      }
      cout << endl;
      

      cout << setw(22) << "TFBL Hold Fraction" << setw(88) << "" << setw(18) << "0x" << std::hex << TFBL_HOLDRATE;

      for(unsigned int j=0; j<6; j++){
	cout << setw(22) << "PIX"+std::to_string(j)+" Stream:";
	for(unsigned int i=0;i<4;i++){
	  cout << setw(22) << std::dec << std::fixed << std::setprecision(3) << TFBL_holdRate[i][j];
	}
	cout <<endl;
      }
      for(unsigned int j=6; j<11; j++){
	cout << setw(22) << "SCT"+std::to_string(j-6)+" Stream:";
	for(unsigned int i=0;i<4;i++){
	  cout << setw(22) << std::dec << std::fixed << std::setprecision(3) << TFBL_holdRate[i][j];
	}
	cout <<endl;
      }

      cout << setw(22) << "Road Stream:";
      for(unsigned int i=0;i<4;i++){
	cout << setw(22) << std::dec << std::fixed << std::setprecision(3) << TFBL_holdRate[i][11];
      }
      cout <<endl;

      cout << setw(22) << "Input Full:";
      for(unsigned int i=0;i<4;i++){
	cout << setw(22) << ((TF_status_words.at(i)>>0)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << TF_STATUS << "[0]";
      cout <<endl;

      cout << setw(22) << "Read Ready:";
      for(unsigned int i=0;i<4;i++){
	cout << setw(22) << ((TF_status_words.at(i)>>1)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << TF_STATUS << "[1]";
      cout <<endl;


      cout << setw(22) << "Write Ready:";
      for(unsigned int i=0;i<4;i++){
	cout << setw(22) << ((TF_status_words.at(i)>>2)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << TF_STATUS << "[2]";
      cout <<endl;

      cout << setw(22) << "Tracks Ready:";
      for(unsigned int i=0;i<4;i++){
	cout << setw(22) << ((TF_status_words.at(i)>>3)&0x1) ;
      }
      cout << setw(18) << "0x" << std::hex << TF_STATUS << "[3]";
      cout <<endl;

      cout << setw(22) << "Output hold:";
      for(unsigned int i=0;i<4;i++){
	bool output_ready = ((TF_status_words.at(i)>>4)&0x1);
	cout << setw(22) << !output_ready ;
      }
      cout << setw(18) << "0x" << std::hex << TF_STATUS << "[4]";
      cout <<endl;

      
      if(freeze_high) std::cout << "WARNING: FREEZE status is non-zero" << std::endl;
      return 0;
}

/********************************************************************/
/********************************************************************
 * aux_input_status
 * *****************************************************************/
/********************************************************************/
bool aux_input_status( int slot ){
      
    u_int register_content;

    VMEInterface *input1 =VMEManager::global().aux(slot,1);
    VMEInterface *input2 =VMEManager::global().aux(slot,2);

    vector<int> cluster_count;
    vector<int> module_count;
    vector<u_int> ssid_count;

    vector<std::string> layer_names = {"Pixel 0 odd", "Pixel 0 even", "Pixel 1 odd", "Pixel 1 even", "Pixel 2 odd","Pixel 2 even",  "SCT 0", "SCT 1", "SCT 2", "SCT 3", "SCT 4"};
   
    u_int nLayers = 11;
    for( u_int iLayer=0; iLayer < nLayers; iLayer++){
        if(iLayer < 4){
            register_content=input1->read_word((CLUSTER_COUNT<<16)+(iLayer<<8));
            cluster_count.push_back(register_content);
            if(iLayer < 3) cluster_count.push_back(-999);

            register_content=input1->read_word((MODULE_COUNT<<16)+(iLayer<<8));
            module_count.push_back(register_content);
            if(iLayer < 3) module_count.push_back(-999);

            register_content=input1->read_word((SSID_COUNT<<16)+((iLayer*2)<<8));
            ssid_count.push_back(register_content);

            if(iLayer < 3){
                register_content=input1->read_word((SSID_COUNT<<16)+((iLayer*2+1)<<8));
                ssid_count.push_back(register_content);
            }

        }
        else{
            
            register_content=input2->read_word((CLUSTER_COUNT<<16)+((iLayer-4)<<8));
            cluster_count.push_back(register_content);
    
            register_content=input2->read_word((MODULE_COUNT<<16)+((iLayer-4)<<8));
            module_count.push_back(register_content);
                
            register_content=input2->read_word((SSID_COUNT<<16)+((iLayer-4)<<8));
            ssid_count.push_back(register_content);
        }
    }

    
    std::cout << setw(20) << "Layer" << setw(20) << "Modules" << setw(20) << "Clusters" << setw(20) << "SSIDs" << std::endl;
    for(u_int iLayer = 0; iLayer < nLayers; iLayer++){
        std::cout << setw(20) << layer_names.at(iLayer) 
            << setw(20) << (module_count.at(iLayer) != -999 ? to_string((u_int)module_count.at(iLayer)) : "-")
            << setw(20) << (cluster_count.at(iLayer) != -999 ? to_string((u_int)cluster_count.at(iLayer)) : "-")
            << setw(20) << ssid_count.at(iLayer) <<    std::endl; 
    }
    return true;

}






/********************************************************************/
/********************************************************************
 * aux_error_status
 * *****************************************************************/
/********************************************************************/

bool aux_error_status( int slot, bool do_details )
    { 
      int status;
      u_int ret, register_content;
      u_int vmeaddr, slotaddr; //, fpgaaddr;
      int handle;
      
      
      static VME_MasterMap_t master_map = {0x0, 0x10000000, VME_A32, 0};
      
      slotaddr = ((slot&0x1f) << 27);
      //fpgaaddr = ((1&0x1f) << 24);
      vmeaddr = slotaddr; // (Start the map at FPGA 1)

      ret = VME_Open();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Open' in function" );
	}
      
      master_map.vmebus_address   = vmeaddr;
      master_map.window_size      = 0x7000000;
      master_map.address_modifier = VME_A32;
      master_map.options          = 0;
      ret = VME_MasterMap(&master_map, &handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterMap' in function"  );
	}

      
      cout << endl;
      cout << setw(20) << "";
      cout << setw(20) << "P1";
      cout << setw(20) << "P2";
      cout << setw(20) << "P3";
      cout << setw(20) << "P4";
      cout << endl;
      cout << endl;

      //
      //  GenData Errors
      //
      vector<u_int> proc_gen_error_status_words;
      for(unsigned int fpgaidx=3;fpgaidx<=6;fpgaidx++){
	status = VME_ReadSafeUInt(handle, (fpgaidx<<24)+PROC_GEN_ERROR_STATUS, &register_content);
	CHECK_ERROR(status);
	proc_gen_error_status_words.push_back(register_content);
      }
      cout << setw(20) << "General Proc Error";
      for(unsigned int i=0;i<4;i++){
	cout << setw(20);
	cout << bitset<9>(((proc_gen_error_status_words.at(i)) & 0x1ff)) ;
      }
      cout <<endl;
      cout <<endl;


      //
      //  Proc Hit Channel Errors
      //
      u_int n_proc_hit_channels = 12;
      vector<vector<u_int> > proc_tx_ch_error_status;
      vector<vector<u_int> > proc_rx_ch_error_status;
      for(unsigned int proc_hit_ch_idx=0;proc_hit_ch_idx<n_proc_hit_channels;proc_hit_ch_idx++){
	vector<u_int> this_tx_ch_status_words;
	vector<u_int> this_rx_ch_status_words;
	for(unsigned int fpgaidx=3;fpgaidx<=6;fpgaidx++){
	  status = VME_ReadSafeUInt(handle, (fpgaidx<<24)+(0x55<<16)+(proc_hit_ch_idx<<8)+(3<<4), &register_content);
	  CHECK_ERROR(status);
	  this_tx_ch_status_words.push_back(register_content);

	  status = VME_ReadSafeUInt(handle, (fpgaidx<<24)+(0x56<<16)+(proc_hit_ch_idx<<8)+(3<<4), &register_content);
	  CHECK_ERROR(status);
	  this_rx_ch_status_words.push_back(register_content);

	}
	proc_tx_ch_error_status.push_back(this_tx_ch_status_words);
	proc_rx_ch_error_status.push_back(this_rx_ch_status_words);
      }
      cout <<endl;

      cout << "Proc Rx Errors:" << endl;
      for(unsigned int proc_hit_ch_idx=0;proc_hit_ch_idx<n_proc_hit_channels;proc_hit_ch_idx++){      
	cout << setw(17) << "Channel:" << setw(3) << proc_hit_ch_idx;
	for(unsigned int i=0;i<4;i++){
	  cout << setw(20);
	  cout << bitset<9>(((proc_rx_ch_error_status.at(proc_hit_ch_idx).at(i)) & 0x1ff)) ;
	}
	cout <<endl;
      }
      cout <<endl;

      cout << "Proc Tx Errors:" << endl;
      for(unsigned int proc_hit_ch_idx=0;proc_hit_ch_idx<n_proc_hit_channels;proc_hit_ch_idx++){      
	cout << setw(17) << "Channel:" << setw(3) << proc_hit_ch_idx;
	for(unsigned int i=0;i<4;i++){
	  cout << setw(20);
	  cout << bitset<9>(((proc_tx_ch_error_status.at(proc_hit_ch_idx).at(i)) & 0x3fe00)) ;
	}
	cout <<endl;
      }
      cout <<endl;

      //
      //  Track Ch Errors
      //
      vector<u_int> proc_track_ch_status_words;
      for(unsigned int fpgaidx=3;fpgaidx<=6;fpgaidx++){
	status = VME_ReadSafeUInt(handle, (fpgaidx<<24)+(0x57<<16)+(3<<4), &register_content);
	CHECK_ERROR(status);
	proc_track_ch_status_words.push_back(register_content);
      }
      cout << setw(20) << "Track Error";
      for(unsigned int i=0;i<4;i++){
	cout << setw(20);
	cout << bitset<9>(((proc_track_ch_status_words.at(i)) & 0x1ff)) ;
      }
      cout <<endl;
      cout <<endl;

      //
      //  GenData Errors
      //
      vector<u_int> proc_gendata_status_words;
      for(unsigned int fpgaidx=3;fpgaidx<=6;fpgaidx++){
	status = VME_ReadSafeUInt(handle, (fpgaidx<<24)+(0x58<<16)+(3<<4), &register_content);
	CHECK_ERROR(status);
	proc_gendata_status_words.push_back(register_content);
      }
      cout << setw(20) << "GenData";
      for(unsigned int i=0;i<4;i++){
	cout << setw(20);
	cout << bitset<9>(((proc_gendata_status_words.at(i)) & 0x1ff)) ;
      }
      cout <<endl;
      cout <<endl;
      


      ////
      ret = VME_MasterUnmap(handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterUnMap' in function"  );
	  
	}
      
      ret = VME_Close();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Close' in function"  );
	  
	}
      
      return 0;
}

/********************************************************************/
/********************************************************************
 * aux_proc_fifo_status
 * *****************************************************************/
/********************************************************************/

// Helper functions for aux_proc_fifo_status
void print_fifo_mon_txt(const aux_fifostatus_t fifo_status,u_int reg_value,u_int procidx)    
    {
      cout << setw(8) << std::dec << std::fixed << std::setprecision(3) << fifo_status.holdRate;
      //cout << setw(11) << float(times_full) << " " << period  ;
      cout << " | ";
      cout << setw(5) << std::fixed << std::setprecision(1) << fifo_status.usedw_avg;
      cout << " | (";
      cout << setw(4) << fifo_status.usedw;
      cout << "/";
      cout << setw(4) << fifo_status.size; 
      cout << ")   ";
      if (procidx > 3) {cout << " | 0x" << std::hex << reg_value;}
}
void print_fifo_mon(VMEInterface *vme, u_int procidx, u_int pixLay, u_int buff)
    {
      uint clock = vme->read_word(CLOCK_FREQUENCY);//register has clock frequency in MHz
      uint period = PERIOD;
      uint frequency = FREQUENCY;
      if(clock!=0){//Old firmware. Use default values
          frequency = clock*1e6;
          period    = clock*1e7;
      }

      aux_fifostatus_t fifo_status=aux_fifostatus(vme,(buff<<16)+(pixLay<<8), period);

      print_fifo_mon_txt(fifo_status,(buff<<16)+(pixLay<<8),procidx);
    }


bool aux_proc_fifo_status( int slot )
    { 
      u_int register_content;

      VMEInterface *proc1 =VMEManager::global().aux(slot,3);
      VMEInterface *proc2 =VMEManager::global().aux(slot,4);
      VMEInterface *proc3 =VMEManager::global().aux(slot,5);
      VMEInterface *proc4 =VMEManager::global().aux(slot,6);

      VMEInterface *procs[4]={proc1,proc2,proc3,proc4};

      u_int lpadding = 15;
      
      cout << endl;
      cout << setw(20) << "";
      cout << setw(14) << "P1";
      cout << setw(30) << "P2";
      cout << setw(30) << "P3";
      cout << setw(30) << "P4";
      cout << setw(20) << "Register";
      cout << endl;

      //
      //  DF Sync status
      //
      std::vector<u_int> DFSync_status_words;
      for(unsigned int procidx=1;procidx<=4;procidx++){
	register_content=procs[procidx-1]->read_word(DFSYNCH_STATUS);
	DFSync_status_words.push_back(register_content);
      }

      cout << endl;
      cout << "DFSynch :" << endl;
      cout << "---------" << endl;
      cout << endl;
      cout << setw(lpadding) << "\tKey:  fraction full | ave. depth | (current usage) " << endl;;
      cout << endl;
      
      //
      // Pix Hits
      //
      for(unsigned int pixLay=0;pixLay<=5;pixLay++){
	cout << setw(lpadding-1) << "Pix Hit " << pixLay;
	for(unsigned int procidx=1;procidx<=4;procidx++){
	  print_fifo_mon(procs[procidx-1],procidx, pixLay, 0x48);
	}
	cout << endl;
      }
      cout << endl;

      //
      // Pix SSIDS
      //
      for(unsigned int pixLay=0;pixLay<=5;pixLay++){
	cout << setw(lpadding-1) << "Pix SS " << pixLay;
	for(unsigned int procidx=1;procidx<=4;procidx++){
	  print_fifo_mon(procs[procidx-1],procidx, pixLay, 0x49);
	}
	cout << endl;
      }
      cout << endl;

      //
      // SCT Data
      //
      for(unsigned int sctLay=0;sctLay<=4;sctLay++){
	cout << setw(lpadding-1) << "SCT " << sctLay;
	for(unsigned int procidx=1;procidx<=4;procidx++){
	  print_fifo_mon(procs[procidx-1],procidx, sctLay, 0x4a);
	}
	cout << endl;
      }
      cout << endl;


      //
      // Road Data
      //
      for(unsigned int roadLay=0;roadLay<=3;roadLay++){
	cout << setw(lpadding-1) << "Road " << roadLay;
	for(unsigned int procidx=1;procidx<=4;procidx++){
	  print_fifo_mon(procs[procidx-1],procidx, roadLay, 0x4b);
	}
	cout << endl;
      }
      cout << endl;

      cout << setw(lpadding) << "Thru FIFO " ;
      for(unsigned int procidx=1;procidx<=4;procidx++){
        print_fifo_mon(procs[procidx-1],procidx, 7, 0x4c);
      }
      cout << endl;
      cout << setw(lpadding) << "Sort FIFO " ;
      for(unsigned int procidx=1;procidx<=4;procidx++){
        print_fifo_mon(procs[procidx-1],procidx, 8, 0x4c);
      }
      cout << endl;
      cout << endl;

      cout << setw(lpadding) << "Merged Road " ;
      for(unsigned int procidx=1;procidx<=4;procidx++){
	print_fifo_mon(procs[procidx-1],procidx, 4, 0x4c);
      }
      cout << endl;
      cout << endl;

      //
      // DO Data
      //
      cout << endl;
      cout << "Data Organizer:" << endl;
      cout << "---------------" << endl;
      cout << endl;
      vector<string> DO_headers;
      DO_headers.push_back("Ext Mem RoadID");
      DO_headers.push_back("AMC Input");
      DO_headers.push_back("AMC Output");
      DO_headers.push_back("Prep AM Input");

      for(unsigned int iDO = 0; iDO<4; ++iDO){
	cout << setw(lpadding) << DO_headers.at(iDO) << " "; 
	for(unsigned int procidx=1;procidx<=4;procidx++){
	  print_fifo_mon(procs[procidx-1],procidx, iDO, 0x4c);
	}
	cout << endl;
      }
      cout << endl;



      //
      // TF Data
      //
      cout << endl;
      cout << "DO -> TF Buffers:" << endl;
      cout << "-----------------" << endl;
      cout << endl;
      vector<string> TF_headers;
      TF_headers.push_back("Sectors");
      TF_headers.push_back("Pixel 0");
      TF_headers.push_back("Pixel 1");
      TF_headers.push_back("Pixel 2");
      TF_headers.push_back("Pixel 3");
      TF_headers.push_back("Pixel 4");
      TF_headers.push_back("Pixel 5");
      TF_headers.push_back("SCT 0");
      TF_headers.push_back("SCT 1");
      TF_headers.push_back("SCT 2");
      TF_headers.push_back("SCT 3");
      TF_headers.push_back("SCT 4");
            

      for(unsigned int iTF = 0; iTF<12; ++iTF){
	cout << setw(lpadding) << TF_headers.at(iTF) << " "; 
	for(unsigned int procidx=1;procidx<=4;procidx++){
	  print_fifo_mon(procs[procidx-1],procidx, iTF, 0x53);
	}
      cout << endl;
      }
      cout << endl;


      //
      // TF Road Fifos
      //
      cout << endl;
      cout << "TF Road FIFOs:" << endl;
      cout << "-----------------" << endl;
      cout << endl;
      vector<string> TF_road_headers;
      TF_road_headers.push_back("Nom. Fitter");
      TF_road_headers.push_back("Pix Fitter 0");
      TF_road_headers.push_back("Pix Fitter 1");
      TF_road_headers.push_back("SCT Fitter 0");
      TF_road_headers.push_back("SCT Fitter 1");
      TF_road_headers.push_back("SCT Fitter 2");
      TF_road_headers.push_back("SCT Fitter 3");
         
      for(unsigned int iTF = 0; iTF<7; ++iTF){
	cout << setw(lpadding) << TF_road_headers.at(iTF) << " "; 
	for(unsigned int procidx=1;procidx<=4;procidx++){
	  print_fifo_mon(procs[procidx-1],procidx, iTF, 0x54);
	}
      cout << endl;
      }
      cout << endl;


      //
      // TF Fit Fifos
      //
      cout << endl;
      cout << "TF Fit FIFOs:" << endl;
      cout << "-----------------" << endl;
      cout << endl;
      vector<string> TF_fit_headers;
      TF_fit_headers.push_back("Nom. Fitter");
      TF_fit_headers.push_back("Pix Fitter 0");
      TF_fit_headers.push_back("Pix Fitter 1");
      TF_fit_headers.push_back("SCT Fitter 0");
      TF_fit_headers.push_back("SCT Fitter 1");
      TF_fit_headers.push_back("SCT Fitter 2");
      TF_fit_headers.push_back("SCT Fitter 3");
         
      for(unsigned int iTF = 0; iTF<7; ++iTF){
	cout << setw(lpadding) << TF_fit_headers.at(iTF) << " "; 
	for(unsigned int procidx=1;procidx<=4;procidx++){
	  print_fifo_mon(procs[procidx-1],procidx, iTF+7, 0x54);
	}
      cout << endl;
      }
      cout << setw(lpadding) << "Merged Out " << " "; 
      for(unsigned int procidx=1;procidx<=4;procidx++){
	
	aux_linkstatus_t link_status=aux_load_linkstatus(procs[procidx-1],0x57,0,LINK_STATUS,28);

	print_fifo_mon_txt(link_status.fifo,(procidx<<24)+(0<<8)+(0x57<<16)+(1<<6),procidx);
      }
      cout << endl;


      //
      // Header Trailer
      //
      cout << setw(lpadding) << "Header:" << " ";
      for(unsigned int procidx=1;procidx<=4;procidx++){
	print_fifo_mon(procs[procidx-1],procidx, 4, 0x4b);
      }
      cout << endl;

      cout << setw(lpadding) << "Trailer:" << " ";
      for(unsigned int procidx=1;procidx<=4;procidx++){
	print_fifo_mon(procs[procidx-1],procidx, 5, 0x4b);
      }
      cout << endl;
      



      //
      // Latency
      //
      cout << endl;
      cout << setw(lpadding) << "Latency Monitor "; 
      for(unsigned int procidx=1;procidx<=4;procidx++){
	print_fifo_mon(procs[procidx-1],procidx, 6, 0x4c);
      }
      cout << endl;
      cout << endl;


      
      return 0;
}

/********************************************************************/
/********************************************************************
 * aux_i2_fifo_status
 * *****************************************************************/
/********************************************************************/

// Helper function for aux_i2_fifo_status
int aux_print_fifoinfo(VMEInterface* vme, const std::string& title, u_int addr)
    {
      uint clock = vme->read_word(CLOCK_FREQUENCY);//register has clock frequency in MHz
      uint period = PERIOD;
      uint frequency = FREQUENCY;
      if(clock!=0){//Old firmware. Use default values
          frequency = clock*1e6;
          period    = clock*1e7;
      }

      //std::cout << std::hex << addr << std::dec << std::endl;
      aux_fifostatus_t fifo_status=aux_fifostatus(vme, addr, period);

      std::cout << "| " << std::left << std::setw(31) << title << std::right
		<< "|" << std::setw(5) << ((fifo_status.hold)?"YES":"NO") << " "
		<< "|" << std::setw(9) << fifo_status.holdRate << " "
		<< "|" << std::setw(6) << std::fixed << std::setprecision(0) << fifo_status.usedw << "/" << std::setw(4) << fifo_status.size << " "
		<< "|" << std::setw(6) << std::fixed << std::setprecision(1) << fifo_status.usedw_avg << "/" << std::setw(4) << fifo_status.size << " "
		<< "|" << std::left << std::setw(7) << ((fifo_status.ovrflw)?"BAD":"") << " "
	        << "|" << std::right << " 0x" << std::hex << addr << " " << std::dec
		<< "|" << std::endl;

      return VME_SUCCESS;
    }

bool aux_i2_fifo_status( int slot )
    {
      VMEInterface *input2 = VMEManager::global().aux(slot,2);
      //
      // HEADER
      //
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      std::cout << "|" << std::setw(33)
		<< "|" << std::setw(6) << "HOLD "
		<< "|" << std::setw(10) << "f_{HOLD} "
		<< "|" << std::setw(12) << "USEDW "
		<< "|" << std::setw(12) << "USEDW (AVG)"
		<< "|" << std::left << std::setw(8) << " OVRFLW"
		<< "|" << std::endl;

      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      
      aux_print_fifoinfo(input2,"Latency   Total AUX:",FIFO_LATENCY+(0<<8));
      aux_print_fifoinfo(input2,"Latency    AUX Int.:",FIFO_LATENCY+(3<<8));
      aux_print_fifoinfo(input2,"Latency       SSMap:",FIFO_LATENCY+(1<<8));
      aux_print_fifoinfo(input2,"Latency Hit Warrior:",FIFO_LATENCY+(2<<8));
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      // aux_print_fifoinfo(input2,"Hit Warrior Gx0:",0x3a0000+(0<<8));
      // aux_print_fifoinfo(input2,"Hit Warrior Gx1:",0x3a0000+(1<<8));
      // aux_print_fifoinfo(input2,"Hit Warrior Gx2:",0x3a0000+(2<<8));
      // aux_print_fifoinfo(input2,"Hit Warrior Gx3:",0x3a0000+(3<<8));
      // std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      // aux_print_fifoinfo(input2,"Hit Warrior Tx0:",0x3b0000+(0<<8));
      // aux_print_fifoinfo(input2,"Hit Warrior Tx1:",0x3b0000+(1<<8));
      // aux_print_fifoinfo(input2,"Hit Warrior Tx2:",0x3b0000+(2<<8));
      // aux_print_fifoinfo(input2,"Hit Warrior Tx3:",0x3b0000+(3<<8));
      // std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      aux_print_fifoinfo(input2,"Hit Warrior In0:",FIFO_HW_NO_IH_INPUT+(0<<8));
      aux_print_fifoinfo(input2,"Hit Warrior In1:",FIFO_HW_NO_IH_INPUT+(1<<8));
      aux_print_fifoinfo(input2,"Hit Warrior In2:",FIFO_HW_NO_IH_INPUT+(2<<8));
      aux_print_fifoinfo(input2,"Hit Warrior In3:",FIFO_HW_NO_IH_INPUT+(3<<8));
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      aux_print_fifoinfo(input2,"HW Comparator 0:",FIFO_HW_NO_IH_COMPARATOR+(0<<8));
      aux_print_fifoinfo(input2,"HW Comparator 1:",FIFO_HW_NO_IH_COMPARATOR+(1<<8));
      aux_print_fifoinfo(input2,"HW Comparator 2:",FIFO_HW_NO_IH_COMPARATOR+(2<<8));
      aux_print_fifoinfo(input2,"HW Comparator 3:",FIFO_HW_NO_IH_COMPARATOR+(3<<8));
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      aux_print_fifoinfo(input2,"HW Out Buffer 0:",FIFO_HW_NO_IH_OUTPUT+(0<<8));
      aux_print_fifoinfo(input2,"HW Out Buffer 1:",FIFO_HW_NO_IH_OUTPUT+(1<<8));
      aux_print_fifoinfo(input2,"HW Out Buffer 2:",FIFO_HW_NO_IH_OUTPUT+(2<<8));
      aux_print_fifoinfo(input2,"HW Out Buffer 3:",FIFO_HW_NO_IH_OUTPUT+(3<<8));
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      aux_print_fifoinfo(input2,"Pkt Header 0:",FIFO_HW_NO_IH_PKT_HEADER+(0<<8));
      aux_print_fifoinfo(input2,"Pkt Header 1:",FIFO_HW_NO_IH_PKT_HEADER+(1<<8));
      aux_print_fifoinfo(input2,"Pkt Header 2:",FIFO_HW_NO_IH_PKT_HEADER+(2<<8));
      aux_print_fifoinfo(input2,"Pkt Header 3:",FIFO_HW_NO_IH_PKT_HEADER+(3<<8));
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      aux_print_fifoinfo(input2,"Pkt Data 0:",FIFO_HW_NO_IH_PKT_DATA+(0<<8));
      aux_print_fifoinfo(input2,"Pkt Data 1:",FIFO_HW_NO_IH_PKT_DATA+(1<<8));
      aux_print_fifoinfo(input2,"Pkt Data 2:",FIFO_HW_NO_IH_PKT_DATA+(2<<8));
      aux_print_fifoinfo(input2,"Pkt Data 3:",FIFO_HW_NO_IH_PKT_DATA+(3<<8));
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      aux_print_fifoinfo(input2,"Pkt Trailer 0:",FIFO_HW_NO_IH_PKT_TRAILER+(0<<8));
      aux_print_fifoinfo(input2,"Pkt Trailer 1:",FIFO_HW_NO_IH_PKT_TRAILER+(1<<8));
      aux_print_fifoinfo(input2,"Pkt Trailer 2:",FIFO_HW_NO_IH_PKT_TRAILER+(2<<8));
      aux_print_fifoinfo(input2,"Pkt Trailer 3:",FIFO_HW_NO_IH_PKT_TRAILER+(3<<8));
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      aux_print_fifoinfo(input2,"Pkt Merge 0:",FIFO_HW_NO_IH_PKT_MERGE+(0<<8));
      aux_print_fifoinfo(input2,"Pkt Merge 1:",FIFO_HW_NO_IH_PKT_MERGE+(1<<8));
      aux_print_fifoinfo(input2,"Pkt Merge 2:",FIFO_HW_NO_IH_PKT_MERGE+(2<<8));
      aux_print_fifoinfo(input2,"Pkt Merge 3:",FIFO_HW_NO_IH_PKT_MERGE+(3<<8));
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      //////////////////////
      // Hit Warrior info //
      //////////////////////
      uint hw_status = input2->read_word(HW_STATUS);
      uint hw_input_hf0  = (hw_status>> 0)&1 ? 0 : 1;
      uint hw_input_hf1  = (hw_status>> 1)&1 ? 0 : 1;
      uint hw_input_hf2  = (hw_status>> 2)&1 ? 0 : 1;
      uint hw_input_hf3  = (hw_status>> 3)&1 ? 0 : 1;

      uint hw_comp_rden0 = (hw_status>> 4)&1;
      uint hw_comp_rden1 = (hw_status>> 5)&1;
      uint hw_comp_rden2 = (hw_status>> 6)&1;
      uint hw_comp_rden3 = (hw_status>> 7)&1;

      uint hw_inpf_rden0 = (hw_status>> 8)&1;
      uint hw_inpf_rden1 = (hw_status>> 9)&1;
      uint hw_inpf_rden2 = (hw_status>>10)&1;
      uint hw_inpf_rden3 = (hw_status>>11)&1;

      uint sfp_lff       = (hw_status>>12)&1 ? 0 : 1;

      uint pm_status = input2->read_word(PM_STATUS);
      uint pm_state_n = (pm_status>> 0)&0xf;
      uint pm_stream  = (pm_status>> 4)&0xf;
      uint pm_tmiss0  = (pm_status>> 8)&1;
      uint pm_tmiss1  = (pm_status>> 9)&1;
      uint pm_tmiss2  = (pm_status>>10)&1;
      uint pm_tmiss3  = (pm_status>>11)&1;
      uint pm_hmiss0  = (pm_status>>12)&1;
      uint pm_hmiss1  = (pm_status>>13)&1;
      uint pm_hmiss2  = (pm_status>>14)&1;
      uint pm_hmiss3  = (pm_status>>15)&1;

      std::string pm_state = "invalid state   ";
      if(pm_state_n == 0) pm_state = "reset           ";
      if(pm_state_n == 1) pm_state = "idle            ";
      if(pm_state_n == 2) pm_state = "wait for header ";
      if(pm_state_n == 3) pm_state = "send header     ";
      if(pm_state_n == 4) pm_state = "send data       ";
      if(pm_state_n == 5) pm_state = "wait for trailer";
      if(pm_state_n == 6) pm_state = "send trailer    ";

      std::cout << "--------------------------------------" << std::endl;
      std::cout << "| Hit Warrior Status                 |" << std::endl;
      std::cout << "|  Input Half Full: " << hw_input_hf0  << hw_input_hf1  << hw_input_hf2  << hw_input_hf3  << "             |" << std::endl;
      std::cout << "|  Comparator RDEN: " << hw_comp_rden0 << hw_comp_rden1 << hw_comp_rden2 << hw_comp_rden3 << "             |" << std::endl;
      std::cout << "|  Input FIFO RDEN: " << hw_inpf_rden0 << hw_inpf_rden1 << hw_inpf_rden2 << hw_inpf_rden3 << "             |" << std::endl;
      std::cout << "|          SFP LFF: " << sfp_lff << "                |" << std::endl;
      std::cout << "|        PM  State: " << pm_state << " |" << std::endl;
      std::cout << "|        PM Stream: " << pm_stream << "                |" << std::endl;
      std::cout << "| PM Trailer Error: " << pm_tmiss0 << pm_tmiss1 << pm_tmiss2 << pm_tmiss3  << "             |" << std::endl;
      std::cout << "| PM  Header Error: " << pm_hmiss0 << pm_hmiss1 << pm_hmiss2 << pm_hmiss3  << "             |" << std::endl;
      std::cout << "--------------------------------------" << std::endl;

      
      return 0;
}

bool aux_i2_fifo_status_IH( int slot )
    {
      VMEInterface *input2 = VMEManager::global().aux(slot,2);
      //
      // HEADER
      //
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      std::cout << "|" << std::setw(33)
		<< "|" << std::setw(6) << "HOLD "
		<< "|" << std::setw(10) << "f_{HOLD} "
		<< "|" << std::setw(12) << "USEDW "
		<< "|" << std::setw(12) << "USEDW (AVG)"
		<< "|" << std::left << std::setw(8) << " OVRFLW"
		<< "|" << std::endl;

      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      
      aux_print_fifoinfo(input2,"Latency   Total AUX:",FIFO_LATENCY+(0<<8));
      aux_print_fifoinfo(input2,"Latency    AUX Int.:",FIFO_LATENCY+(3<<8));
      aux_print_fifoinfo(input2,"Latency       SSMap:",FIFO_LATENCY+(1<<8));
      aux_print_fifoinfo(input2,"Latency Hit Warrior:",FIFO_LATENCY+(2<<8));
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      aux_print_fifoinfo(input2,"Hit Warrior In0:",FIFO_HW_INPUT+(0<<8));
      aux_print_fifoinfo(input2,"Hit Warrior In1:",FIFO_HW_INPUT+(1<<8));
      aux_print_fifoinfo(input2,"Hit Warrior In2:",FIFO_HW_INPUT+(2<<8));
      aux_print_fifoinfo(input2,"Hit Warrior In3:",FIFO_HW_INPUT+(3<<8));
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      aux_print_fifoinfo(input2,"HW Comparator 0:",FIFO_HW_COMPARATOR+(0<<8));
      aux_print_fifoinfo(input2,"HW Comparator 1:",FIFO_HW_COMPARATOR+(1<<8));
      aux_print_fifoinfo(input2,"HW Comparator 2:",FIFO_HW_COMPARATOR+(2<<8));
      aux_print_fifoinfo(input2,"HW Comparator 3:",FIFO_HW_COMPARATOR+(3<<8));
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      aux_print_fifoinfo(input2,"HW Data Buffer 0:",FIFO_HW_DATA_BUFFER+(0<<8));
      aux_print_fifoinfo(input2,"HW Data Buffer 1:",FIFO_HW_DATA_BUFFER+(1<<8));
      aux_print_fifoinfo(input2,"HW Data Buffer 2:",FIFO_HW_DATA_BUFFER+(2<<8));
      aux_print_fifoinfo(input2,"HW Data Buffer 3:",FIFO_HW_DATA_BUFFER+(3<<8));
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      aux_print_fifoinfo(input2,"HW Gated Output 0:",FIFO_HW_GATED_OUTPUT+(0<<8));
      aux_print_fifoinfo(input2,"HW Gated Output 1:",FIFO_HW_GATED_OUTPUT+(1<<8));
      aux_print_fifoinfo(input2,"HW Gated Output 2:",FIFO_HW_GATED_OUTPUT+(2<<8));
      aux_print_fifoinfo(input2,"HW Gated Output 3:",FIFO_HW_GATED_OUTPUT+(3<<8));
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      aux_print_fifoinfo(input2,"HW PM Frame:",FIFO_HW_PM_FRAME); // Only 1 channel for frame fifo
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      aux_print_fifoinfo(input2,"HW PM Data 0:",FIFO_HW_PM_DATA+(0<<8));
      aux_print_fifoinfo(input2,"HW PM Data 1:",FIFO_HW_PM_DATA+(1<<8));
      aux_print_fifoinfo(input2,"HW PM Data 2:",FIFO_HW_PM_DATA+(2<<8));
      aux_print_fifoinfo(input2,"HW PM Data 3:",FIFO_HW_PM_DATA+(3<<8));
      std::cout << std::setw(87) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      //////////////////////
      // Hit Warrior info //
      //////////////////////
      uint hw_status = input2->read_word(HW_STATUS);
      uint hw_input_hf0  = (hw_status>> 0)&1 ? 0 : 1;
      uint hw_input_hf1  = (hw_status>> 1)&1 ? 0 : 1;
      uint hw_input_hf2  = (hw_status>> 2)&1 ? 0 : 1;
      uint hw_input_hf3  = (hw_status>> 3)&1 ? 0 : 1;

      uint hw_comp_rden0 = (hw_status>> 4)&1;
      uint hw_comp_rden1 = (hw_status>> 5)&1;
      uint hw_comp_rden2 = (hw_status>> 6)&1;
      uint hw_comp_rden3 = (hw_status>> 7)&1;

      uint hw_inpf_rden0 = (hw_status>> 8)&1;
      uint hw_inpf_rden1 = (hw_status>> 9)&1;
      uint hw_inpf_rden2 = (hw_status>>10)&1;
      uint hw_inpf_rden3 = (hw_status>>11)&1;

      uint sfp_lff       = (hw_status>>12)&1 ? 0 : 1;

      uint pm_status = input2->read_word(PM_STATUS);
      uint pm_state_n = (pm_status>> 0)&0x3;
      uint pm_stream  = (pm_status>> 2)&0x3;

      std::string pm_state = "invalid state   ";
      if(pm_state_n == 0) pm_state = "reset        ";
      if(pm_state_n == 1) pm_state = "sendheader   ";
      if(pm_state_n == 2) pm_state = "senddata     ";
      if(pm_state_n == 3) pm_state = "sendtrailer  ";

      std::cout << "--------------------------------------" << std::endl;
      std::cout << "| Hit Warrior Status                 |" << std::endl;
      std::cout << "|  Input Half Full: " << hw_input_hf0  << hw_input_hf1  << hw_input_hf2  << hw_input_hf3  << "             |" << std::endl;
      std::cout << "|  Comparator RDEN: " << hw_comp_rden0 << hw_comp_rden1 << hw_comp_rden2 << hw_comp_rden3 << "             |" << std::endl;
      std::cout << "|   Data FIFO RDEN: " << hw_inpf_rden0 << hw_inpf_rden1 << hw_inpf_rden2 << hw_inpf_rden3 << "             |" << std::endl;
      std::cout << "|          SFP LFF: " << sfp_lff << "                |" << std::endl;
      std::cout << "|        PM  State: " << pm_state << " |" << std::endl;
      std::cout << "|        PM Stream: " << pm_stream << "                |" << std::endl;
      std::cout << "--------------------------------------" << std::endl;

      return 0;
}

/********************************************************************/
/********************************************************************
 * aux_fifo_status
 * *****************************************************************/
/********************************************************************/
bool aux_fifo_status( int slot, bool showP1, bool showP2, bool showP3, bool showP4 )
    {
      VMEInterface *input1=VMEManager::global().aux(slot,1);
      VMEInterface *input2=VMEManager::global().aux(slot,2);
      VMEInterface *proc1 =VMEManager::global().aux(slot,3);
      VMEInterface *proc2 =VMEManager::global().aux(slot,4);
      VMEInterface *proc3 =VMEManager::global().aux(slot,5);
      VMEInterface *proc4 =VMEManager::global().aux(slot,6);

      VMEInterface *procs[4]={proc1, proc2, proc3, proc4};

      // Firmware types
      uint i2_fw_type=input2->read_word(0x0)>>24;
      
      //
      // HEADER
      //


      /////////////
      // Input 1 // 3 PIX + 1 SCT
      /////////////
      std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      std::cout << std::setw(49) << std::setfill('-') << " INPUT 1 " << std::setw(49) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      std::cout << "|" << std::setw(33)
		<< "|" << std::setw(6) << "HOLD "
		<< "|" << std::setw(10) << "f_{HOLD} "
		<< "|" << std::setw(12) << "USEDW "
		<< "|" << std::setw(12) << "USEDW (AVG)"
		<< "|" << std::left << std::setw(8) << " OVRFLW"
	        << "|" << std::left << std::setw(8) << " REGISTER "
		<< "|" << std::endl;
      std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      // for loop for the pixel layers
      for (int pix_ch = 0 ; pix_ch < 3 ; pix_ch++  ){
          aux_print_fifoinfo(input1, "PIXEL " + std::to_string(pix_ch) + " INPUT"  , FIFO_SSMAP+I1_SSMAP_INPUT           + (pix_ch<<8));
          aux_print_fifoinfo(input1, "PIXEL " + std::to_string(pix_ch) + " HS INPUT", FIFO_SSMAP+I1_SSMAP_HS_INPUT        + (pix_ch<<8));

          std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
          for (int hs_layer = 0; hs_layer < 4; hs_layer++) {
              for (int hs_fifo = 0; hs_fifo < 4; hs_fifo++) {
                  aux_print_fifoinfo(input1, "PIXEL " + std::to_string(pix_ch) + " HSLAYER " + std::to_string(hs_layer) + " FIFO " + std::to_string(hs_fifo), FIFO_SSMAP+I1_SSMAP_HS_LAYERS+ ((16*pix_ch+4*hs_layer+hs_fifo)<<8));
              }
              std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
          }

          aux_print_fifoinfo(input1, "PIXEL " + std::to_string(pix_ch) + " INTERLEAVER EVEN", FIFO_SSMAP+I1_SSMAP_PIX_INTERLEAVER + (pix_ch<<8));
          aux_print_fifoinfo(input1, "PIXEL " + std::to_string(pix_ch) + " INTERLEAVER ODD" , FIFO_SSMAP+I1_SSMAP_PIX_INTERLEAVER + ((pix_ch+1)<<8));
          aux_print_fifoinfo(input1, "PIXEL " + std::to_string(pix_ch) + " PM FRAME EVEN"   , FIFO_SSMAP+I1_SSMAP_PM_FRAME        + (pix_ch<<8));
          aux_print_fifoinfo(input1, "PIXEL " + std::to_string(pix_ch) + " PM FRAME ODD"    , FIFO_SSMAP+I1_SSMAP_PM_FRAME        + ((pix_ch+1)<<8));
          aux_print_fifoinfo(input1, "PIXEL " + std::to_string(pix_ch) + " PM DATA EVEN"    , FIFO_SSMAP+I1_SSMAP_PM_DATA         + (pix_ch<<8));
          aux_print_fifoinfo(input1, "PIXEL " + std::to_string(pix_ch) + " PM DATA ODD"     , FIFO_SSMAP+I1_SSMAP_PM_DATA         + ((pix_ch+1)<<8));
          aux_print_fifoinfo(input1, "PIXEL " + std::to_string(pix_ch) + " AM L1ID EVEN"    , FIFO_SSMAP+I1_SSMAP_AM_L1ID         + (pix_ch<<8));
          aux_print_fifoinfo(input1, "PIXEL " + std::to_string(pix_ch) + " AM L1ID ODD"     , FIFO_SSMAP+I1_SSMAP_AM_L1ID         + ((pix_ch+1)<<8));

          std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
          std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
          std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      }
      for (int sct_ch = 3; sct_ch < 4 ; sct_ch++  ){
          aux_print_fifoinfo(input1, "SCT " + std::to_string(sct_ch-3) + " INPUT"      , FIFO_SSMAP+I1_SSMAP_INPUT           + (sct_ch<<8));
          aux_print_fifoinfo(input1, "SCT " + std::to_string(sct_ch-3) + " SERIALIZER" , FIFO_SSMAP+I1_SSMAP_SCT_SERIALIZER  + (sct_ch<<8));
          aux_print_fifoinfo(input1, "SCT " + std::to_string(sct_ch-3) + " HS INPUT"   , FIFO_SSMAP+I1_SSMAP_HS_INPUT        + (sct_ch<<8));

          std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
          for (int hs_layer = 0; hs_layer < 4; hs_layer++) {
            for (int hs_fifo = 0; hs_fifo < 4; hs_fifo++) {
                aux_print_fifoinfo(input1, "SCT " + std::to_string(sct_ch-3) + " HSLAYER " + std::to_string(hs_layer) + " FIFO " + std::to_string(hs_fifo), FIFO_SSMAP+I1_SSMAP_HS_LAYERS+ ((16*sct_ch+4*hs_layer+hs_fifo)<<8));
            }
            std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
          }
          aux_print_fifoinfo(input1, "SCT " + std::to_string(sct_ch-3) + " PM FRAME"   , FIFO_SSMAP+I1_SSMAP_PM_FRAME        + (sct_ch<<8));
          aux_print_fifoinfo(input1, "SCT " + std::to_string(sct_ch-3) + " PM DATA"    , FIFO_SSMAP+I1_SSMAP_PM_DATA         + (sct_ch<<8));
          aux_print_fifoinfo(input1, "SCT " + std::to_string(sct_ch-3) + " AM L1ID"    , FIFO_SSMAP+I1_SSMAP_AM_L1ID         + (sct_ch<<8));

          std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      }

      /////////////
      // Input 2 // SCT only
      /////////////
      std::cout << "\n";
      std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      std::cout << std::setw(49) << std::setfill('-') << " INPUT 2 " << std::setw(49) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      std::cout << "|" << std::setw(33)
		<< "|" << std::setw(6) << "HOLD "
		<< "|" << std::setw(10) << "f_{HOLD} "
		<< "|" << std::setw(12) << "USEDW "
		<< "|" << std::setw(12) << "USEDW (AVG)"
		<< "|" << std::left << std::setw(8) << " OVRFLW"
	        << "|" << std::left << std::setw(8) << " REGISTER "
		<< "|" << std::endl;
      std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      for (int sct_ch = 0; sct_ch < 4 ; sct_ch++  ){
          aux_print_fifoinfo(input2, "SCT " + std::to_string(sct_ch+1) + " INPUT "      , FIFO_SSMAP+I2_SSMAP_INPUT           + (sct_ch<<8));
          aux_print_fifoinfo(input2, "SCT " + std::to_string(sct_ch+1) + " SERIALIZER " , FIFO_SSMAP+I2_SSMAP_SCT_SERIALIZER  + (sct_ch<<8));
          aux_print_fifoinfo(input2, "SCT " + std::to_string(sct_ch+1) + " HS INPUT "   , FIFO_SSMAP+I2_SSMAP_HS_INPUT        + (sct_ch<<8));

          std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
          for (int hs_layer = 0; hs_layer < 4; hs_layer++) {
            for (int hs_fifo = 0; hs_fifo < 4; hs_fifo++) {
                aux_print_fifoinfo(input2, "SCT "+std::to_string(sct_ch+1) +" HSLAYER " + std::to_string(hs_layer) + " FIFO " + std::to_string(hs_fifo), FIFO_SSMAP+I2_SSMAP_HS_LAYERS+ ((16*sct_ch+4*hs_layer+hs_fifo)<<8));
            }
          std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
          }
          aux_print_fifoinfo(input2, "SCT " + std::to_string(sct_ch+1) + " PM FRAME " , FIFO_SSMAP+I2_SSMAP_PM_FRAME        + (sct_ch<<8));
          aux_print_fifoinfo(input2, "SCT " + std::to_string(sct_ch+1) + " PM DATA "  , FIFO_SSMAP+I2_SSMAP_PM_DATA         + (sct_ch<<8));
          aux_print_fifoinfo(input2, "SCT " + std::to_string(sct_ch+1) + " AM L1ID "  , FIFO_SSMAP+I2_SSMAP_AM_L1ID         + (sct_ch<<8));

          std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
          std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
          std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      }



      aux_print_fifoinfo(input2,"FTK2ROS",FIFO_FTK2ROS);
      std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      std::cout << "\n";
      std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      std::cout << std::setw(48) << std::setfill('-') << " PROCESSOR " << std::setw(48) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      std::cout << "|" << std::setw(33)
		<< "|" << std::setw(6) << "HOLD "
		<< "|" << std::setw(10) << "f_{HOLD} "
		<< "|" << std::setw(12) << "USEDW "
		<< "|" << std::setw(12) << "USEDW (AVG)"
		<< "|" << std::left << std::setw(8) << " OVRFLW"
	        << "|" << std::left << std::setw(8) << " REGISTER "
		<< "|" << std::endl;

      for(uint proc=0;proc<4;proc++)
	{
	  if(proc+1==1 && !showP1) continue;
	  if(proc+1==2 && !showP2) continue;
	  if(proc+1==3 && !showP3) continue;
	  if(proc+1==4 && !showP4) continue;

	  std::ostringstream procTitle;
	  procTitle << "Processor " << (proc+1);
	  
	  //
	  // DF Sync fifos
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Header",FIFO_DFSYNC_HEADER);

	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Trailer",FIFO_DFSYNC_TRAILER);
	  std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Pix Hit 0" ,FIFO_DFSYNC_PIX_HIT+(0<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Pix SSID 0",FIFO_DFSYNC_PIX_SS+(0<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Pix Hit 1" ,FIFO_DFSYNC_PIX_HIT+(1<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Pix SSID 1",FIFO_DFSYNC_PIX_SS+(1<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Pix Hit 2" ,FIFO_DFSYNC_PIX_HIT+(2<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Pix SSID 2",FIFO_DFSYNC_PIX_SS+(2<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Pix Hit 3" ,FIFO_DFSYNC_PIX_HIT+(3<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Pix SSID 3",FIFO_DFSYNC_PIX_SS+(3<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Pix Hit 4" ,FIFO_DFSYNC_PIX_HIT+(4<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Pix SSID 4",FIFO_DFSYNC_PIX_SS+(4<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Pix Hit 5" ,FIFO_DFSYNC_PIX_HIT+(5<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Pix SSID 5",FIFO_DFSYNC_PIX_SS+(5<<8));

	  std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync SCT 0",FIFO_DFSYNC_SCT+(0<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync SCT 1",FIFO_DFSYNC_SCT+(1<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync SCT 2",FIFO_DFSYNC_SCT+(2<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync SCT 3",FIFO_DFSYNC_SCT+(3<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync SCT 4",FIFO_DFSYNC_SCT+(4<<8));

	  std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Road 0",FIFO_DFSYNC_ROAD+(0<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Road 1",FIFO_DFSYNC_ROAD+(1<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Road 2",FIFO_DFSYNC_ROAD+(2<<8));
	  aux_print_fifoinfo(procs[proc],procTitle.str()+" DFSync Road 3",FIFO_DFSYNC_ROAD+(3<<8));
	  std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
	}

      return 0;
}

/********************************************************************/
/********************************************************************
 * aux_linkstatus
 * *****************************************************************/
/********************************************************************/

// Helper for aux_linkstatus and aux_linkerrors
aux_linkstatus_t aux_load_linkstatus(VMEInterface *vme, u_int addr, u_int ch, u_int upaddr, u_int upbit)
{
      u_int baseaddr,register_content,sel;//sel is bits of vme addr 7 downto 4, hence the bitshifts by 4 below
      aux_linkstatus_t link_status;

      uint clock = vme->read_word(CLOCK_FREQUENCY);//register has clock frequency in MHz
      uint period = PERIOD;
      uint frequency = FREQUENCY;
      if(clock!=0){//Old firmware. Use default values
          frequency = clock*1e6;
          period    = clock*1e7;
      }
	
      baseaddr=(addr<<16)+(ch<<8);

      // status
      register_content=vme->read_word(upaddr);

      link_status.up=(register_content>>upbit)&1;

      // nPackets
      sel=0<<4;
      register_content=vme->read_word(baseaddr+sel);
      link_status.nPackets=register_content;

      // ratePackets
      sel=1<<4;
      register_content=vme->read_word(baseaddr+sel);
      //(# per period clock cycles)*(clock cycles per second)/(clock cycles per period) in Hz. Divide by 1000 to get kHz
      //(# per packets per period)*(1/clock cycles per period)*(clock cycles per second) in Hz. Divide by 1000 to get kHz
      link_status.ratePackets=((float)register_content) * 1.0/((float)period) *  ((float)frequency) * 1.0/1000.0;
      //std::cout << " register content " << register_content << " period " << period << " frequency " << frequency << std::endl;
      
       
      // l1id
      sel=2<<4;
      register_content=vme->read_word(baseaddr+sel);
      link_status.l1id=register_content;

      // errors
      sel=3<<4;
      register_content=vme->read_word(baseaddr+sel);
      link_status.errors=register_content;

      // fifo
      sel=4<<4;
      link_status.fifo=aux_fifostatus(vme,baseaddr+sel,period);

      // total hold count
      sel=12<<4;
      register_content = vme->read_word(baseaddr+sel);
      link_status.rising_hold_count = register_content;

      // Clock cycle counter from start of second event to start of last event. Valid only for data flow up to ~25 seconds
      sel=13<<4;
      register_content = vme->read_word(baseaddr+sel);
      float time = 1000*((float)register_content)/frequency;//in ms
      link_status.start_rate = (time>0) ? ((float)link_status.nPackets)/time : 0;

      return link_status;
}

void aux_print_linkinfo(VMEInterface *vme, const std::string& title, u_int addr, u_int ch, u_int upaddr, u_int upbit)
    {
      aux_linkstatus_t link_status=aux_load_linkstatus(vme,addr,ch,upaddr,upbit);

      std::string uptitle;
      if(link_status.up)
	uptitle="ON";
      else
	uptitle="OFF";

      std::string holdtitle;
      if(link_status.fifo.hold)
	holdtitle="ON";
      else
	holdtitle="OFF";

      std::string fifo_ovrflw          = link_status.         fifo.ovrflw ? "OVRFLW!": "";

      std::cout << "|" << " " << std::left << std::setw(24) << title << std::right
		<< "|"    << std::setw( 4) << uptitle << " "
		<< "|"    << std::setw( 5) << holdtitle << " "
		<< "|"    << std::setw( 9) << std::fixed << std::setprecision(3) << link_status.fifo.holdRate << " "
	        << "|"    << std::setw(11) << link_status.rising_hold_count << " "
		<< "|"    << std::setw( 6) << std::fixed << std::setprecision(0) << link_status.fifo.usedw << "/" << std::setw(4) << link_status.fifo.size << " "
		<< "|"    << std::setw( 6) << std::fixed << std::setprecision(0) << link_status.fifo.usedw_avg << "/" << std::setw(4) << link_status.fifo.size << " "
		<< "|"    << std::setw(11) << link_status.nPackets << " "
		<< "|"    << std::setw(11) << std::fixed << std::setprecision(1) << link_status.start_rate << " "
		<< "|"    << std::setw( 7) << std::fixed << std::setprecision(1) << link_status.ratePackets << " "
		<< "| 0x" << std::setw( 9) << std::setfill('0') << std::hex << link_status.l1id << " " << std::setfill(' ')
	        << "|"    << std::setw( 7) << fifo_ovrflw << " "
		<< "|"    << std::dec << std::endl;
}

bool aux_linkstatus( int slot, bool showI1, bool showI2, bool showP1, bool showP2, bool showP3, bool showP4, bool showUnused )
    {
      VMEInterface *input1=VMEManager::global().aux(slot,1);
      VMEInterface *input2=VMEManager::global().aux(slot,2);
      VMEInterface *proc1 =VMEManager::global().aux(slot,3);
      VMEInterface *proc2 =VMEManager::global().aux(slot,4);
      VMEInterface *proc3 =VMEManager::global().aux(slot,5);
      VMEInterface *proc4 =VMEManager::global().aux(slot,6);

      VMEInterface *procs[4]={proc1, proc2, proc3, proc4};

      // Firmware types
      uint i2_fw_type=input2->read_word(0x0)>>24;

      // 
      // Check FREEZE status
      //
      VMEInterface *fpgas[6]={input1,input2,proc1,proc2,proc3,proc4};
      u_int register_content;
      bool freeze_high = false;
      for(unsigned int fpgaidx=0;fpgaidx<6;fpgaidx++)
	{
	  register_content=fpgas[fpgaidx]->read_word(FREEZE_STATUS);
	  if(register_content != 0) freeze_high = true;
	}
      register_content=fpgas[1]->read_word(HW_FREEZE_STATUS);
      if(register_content != 0) freeze_high = true;


      //
      // HEADER
      //
      std::cout << std::setw(167) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      std::cout << "|" << std::setw(26)
		<< "|" << std::setw(5) << "UP "
		<< "|" << std::setw(6) << "HOLD "
		<< "|" << std::setw(10) << "f_{HOLD} "
	        << "|" << std::setw(12) << "HOLD COUNT "
		<< "|" << std::setw(12) << "USEDW "
		<< "|" << std::setw(12) << "USEDW (AVG)"
		<< "|" << std::setw(12) << "PACKETS "
		<< "|" << std::setw(12) << "EARLY RATE "
		<< "|" << std::setw( 8) << "RATE "
		<< "|" << std::setw(13) << "L1ID "
	        << "|" << std::setw( 8) << "OVRFLW "
	        << "|" << std::setw(18) << "L1ID SYNC OVRFLW "
		<< "|" << std::endl;

      std::cout << std::setw(167) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
     
      //
      // QSFP Links
      //
      if(showI1)
	{
	  aux_print_linkinfo(input1,"Input 1 QSFP0",0x30,0,SLINK_STATUS,0);
	  aux_print_linkinfo(input1,"Input 1 QSFP1",0x30,1,SLINK_STATUS,1);
	  aux_print_linkinfo(input1,"Input 1 QSFP2",0x30,2,SLINK_STATUS,2);
	  aux_print_linkinfo(input1,"Input 1 QSFP3",0x30,3,SLINK_STATUS,3);
	}
      if(showI2)
	{
	  aux_print_linkinfo(input2,"Input 2 QSFP0",0x30,0,SLINK_STATUS,0);
	  aux_print_linkinfo(input2,"Input 2 QSFP1",0x30,1,SLINK_STATUS,1);
	  aux_print_linkinfo(input2,"Input 2 QSFP2",0x30,2,SLINK_STATUS,2);
	  aux_print_linkinfo(input2,"Input 2 QSFP3",0x30,3,SLINK_STATUS,3);
	}
      if(showI1 || showI2) std::cout << std::setw(167) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
      
      //
      // Hit Links
      //
      if(showI1)
	{
	  aux_print_linkinfo(input1,"Input 1 Hit 0",0x31,0,LINK_STATUS, 8);
	  aux_print_linkinfo(input1,"Input 1 Hit 1",0x31,1,LINK_STATUS, 9);
	  aux_print_linkinfo(input1,"Input 1 Hit 2",0x31,2,LINK_STATUS,10);
	  aux_print_linkinfo(input1,"Input 1 Hit 3",0x31,3,LINK_STATUS,11);
	  aux_print_linkinfo(input1,"Input 1 Hit 4",0x31,4,LINK_STATUS,12);
	  aux_print_linkinfo(input1,"Input 1 Hit 5",0x31,5,LINK_STATUS,13);
	  aux_print_linkinfo(input1,"Input 1 Hit 6",0x31,6,LINK_STATUS,14);
	  //aux_print_linkinfo(input1,"Input 1 Hit 7",0x31,7,LINK_STATUS,15);
	}
      if(showI2)
	{
	  aux_print_linkinfo(input2,"Input 2 Hit 0",0x31,0,LINK_STATUS, 8);
	  aux_print_linkinfo(input2,"Input 2 Hit 1",0x31,1,LINK_STATUS, 9);
	  aux_print_linkinfo(input2,"Input 2 Hit 2",0x31,2,LINK_STATUS,10);
	  aux_print_linkinfo(input2,"Input 2 Hit 3",0x31,3,LINK_STATUS,11);
	}
      if(showI1 || showI2) std::cout << std::setw(167) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      //
      // AM Links
      //
      if(showI1)
	{
	  aux_print_linkinfo(input1,"Input 1 AM  0",0x32,0,LINK_STATUS, 0);
	  aux_print_linkinfo(input1,"Input 1 AM  1",0x32,1,LINK_STATUS, 1);
	  aux_print_linkinfo(input1,"Input 1 AM  2",0x32,2,LINK_STATUS, 2);
	  aux_print_linkinfo(input1,"Input 1 AM  3",0x32,3,LINK_STATUS, 3);
	  aux_print_linkinfo(input1,"Input 1 AM  4",0x32,4,LINK_STATUS, 4);
	  aux_print_linkinfo(input1,"Input 1 AM  5",0x32,5,LINK_STATUS, 5);
	  aux_print_linkinfo(input1,"Input 1 AM  6",0x32,6,LINK_STATUS, 6);
	  //aux_print_linkinfo(input1,"Input 1 AM  7",0x32,7,LINK_STATUS, 7);
	}
      if(showI2)
	{
	  aux_print_linkinfo(input2,"Input 2 AM  0",0x32,0,LINK_STATUS, 0);
	  aux_print_linkinfo(input2,"Input 2 AM  1",0x32,1,LINK_STATUS, 1);
	  aux_print_linkinfo(input2,"Input 2 AM  2",0x32,2,LINK_STATUS, 2);
	  aux_print_linkinfo(input2,"Input 2 AM  3",0x32,3,LINK_STATUS, 3);
	}
      if(showI1 || showI2) std::cout << std::setw(167) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      //
      // Gendata Tx Links
      //
      if(showI2 && showUnused)
	{
	  aux_print_linkinfo(input2,"Input 2 GenData Tx 0",0x33,0,LINK_STATUS,16);
	  aux_print_linkinfo(input2,"Input 2 GenData Tx 1",0x33,1,LINK_STATUS,17);
	  aux_print_linkinfo(input2,"Input 2 GenData Tx 2",0x33,2,LINK_STATUS,18);
	  aux_print_linkinfo(input2,"Input 2 GenData Tx 3",0x33,3,LINK_STATUS,19);
	}

      //
      // Gendata Rx Links
      //
      if(showI2)
	{
	  aux_print_linkinfo(input2,"Input 2 GenData Rx 0",0x34,0,LINK_STATUS,20);
	  aux_print_linkinfo(input2,"Input 2 GenData Rx 1",0x34,1,LINK_STATUS,21);
	  aux_print_linkinfo(input2,"Input 2 GenData Rx 2",0x34,2,LINK_STATUS,22);
	  aux_print_linkinfo(input2,"Input 2 GenData Rx 3",0x34,3,LINK_STATUS,23);
	}
      if(showI2) std::cout << std::setw(167) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

      //
      // SFP Link
      //
      if(showI2)
	{
	  if(i2_fw_type==0x20)
	    {
	      aux_print_linkinfo(input2,"Input 2 SFP Tx",0x35,0,SLINK_STATUS, 4);
	      aux_print_linkinfo(input2,"Input 2 SFP Rx",0x37,0,SLINK_STATUS,20);
	    }
	  else
	    {
	      aux_print_linkinfo(input2,"Input 2 SFP",0x35,0,SLINK_STATUS,4);
	    }
	  std::cout << std::setw(167) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
	}

      //
      // Processor links
      //
      for(u_int proc=1;proc<=4;proc++)
	{
	  if(proc==1 && !showP1) continue;
	  if(proc==2 && !showP2) continue;
	  if(proc==3 && !showP3) continue;
	  if(proc==4 && !showP4) continue;

	  std::ostringstream procTitle;
	  procTitle << "Processor " << proc;

	  if(showUnused)
	    {
	      aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Tx  0",0x55, 0,LINK_STATUS, 0);
	      aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Tx  1",0x55, 1,LINK_STATUS, 1);
	      aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Tx  2",0x55, 2,LINK_STATUS, 2);
	      aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Tx  3",0x55, 3,LINK_STATUS, 3);
	      aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Tx  4",0x55, 4,LINK_STATUS, 4);
	      aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Tx  5",0x55, 5,LINK_STATUS, 5);
	      aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Tx  6",0x55, 6,LINK_STATUS, 6);
	      aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Tx  7",0x55, 7,LINK_STATUS, 7);
	      aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Tx  8",0x55, 8,LINK_STATUS, 8);
	      aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Tx  9",0x55, 9,LINK_STATUS, 9);
	      aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Tx 10",0x55,10,LINK_STATUS,10);
	      //aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Tx 11",0x55,11,LINK_STATUS,11);
	      std::cout << std::setw(167) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
	    }

	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Rx  0",0x56, 0,LINK_STATUS,12);
	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Rx  1",0x56, 1,LINK_STATUS,13);
	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Rx  2",0x56, 2,LINK_STATUS,14);
	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Rx  3",0x56, 3,LINK_STATUS,15);
	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Rx  4",0x56, 4,LINK_STATUS,16);
	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Rx  5",0x56, 5,LINK_STATUS,17);
	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Rx  6",0x56, 6,LINK_STATUS,18);
	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Rx  7",0x56, 7,LINK_STATUS,19);
	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Rx  8",0x56, 8,LINK_STATUS,20);
	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Rx  9",0x56, 9,LINK_STATUS,21);
	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Rx 10",0x56,10,LINK_STATUS,22);
	  //aux_print_linkinfo(procs[proc-1],procTitle.str()+" Hits Rx 11",0x56,11,LINK_STATUS,23);
	  std::cout << std::setw(167) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Roads 0",0x59, 0,LINK_STATUS,24);
	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Roads 1",0x59, 1,LINK_STATUS,25);
	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Roads 2",0x59, 2,LINK_STATUS,26);
	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Roads 3",0x59, 3,LINK_STATUS,27);
	  std::cout << std::setw(167) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

	  aux_print_linkinfo(procs[proc-1],procTitle.str()+" Tracks",0x57, 0,LINK_STATUS,28);
	  if(showUnused) aux_print_linkinfo(procs[proc-1],procTitle.str()+" GenData",0x58, 0,LINK_STATUS,29);
	  std::cout << std::setw(167) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

	}

      
      if(freeze_high) std::cout << "WARNING: FREEZE status is non-zero" << std::endl;
      return 0;
}


/********************************************************************/
/********************************************************************
 * aux_linkerrors
 * *****************************************************************/
/********************************************************************/
int aux_print_linkerrors(VMEInterface *vme, const std::string& title, u_int addr, u_int ch, u_int upaddr, u_int upbit)
    {
      aux_linkstatus_t link_status=aux_load_linkstatus(vme,addr,ch,upaddr,upbit);

      std::string fifo_ovrflw          = link_status.         fifo.ovrflw ? "OVRFLW!": "";

      std::cout << "|" << " " << std::left << std::setw(24) << title << std::right << "        "
		<< "|" << std::setw( 7) << link_status.errors << " "
		<< "|" << std::setw( 7) << fifo_ovrflw << " " << std::dec
		<< "|" << std::setw( 6) << std::fixed << std::setprecision(0) << link_status.         fifo.usedw    <<"/"<<std::setw(4)<<link_status.         fifo.size<<" "
		<< "|" << std::setw( 6) << std::fixed << std::setprecision(0) << link_status.         fifo.usedw_avg<<"/"<<std::setw(4)<<link_status.         fifo.size<<" "
		<< "|" << std::dec << std::endl;
      
      return VME_SUCCESS;
    }

bool aux_linkerrors( int slot, bool showI1, bool showI2, bool showP1, bool showP2, bool showP3, bool showP4, bool showUnused )
    { 
      VMEInterface *input1=VMEManager::global().aux(slot,1);
      VMEInterface *input2=VMEManager::global().aux(slot,2);
      VMEInterface *proc1 =VMEManager::global().aux(slot,3);
      VMEInterface *proc2 =VMEManager::global().aux(slot,4);
      VMEInterface *proc3 =VMEManager::global().aux(slot,5);
      VMEInterface *proc4 =VMEManager::global().aux(slot,6);

      VMEInterface *procs[4]={proc1, proc2, proc3, proc4};
      
      //
      // HEADER
      //
      std::cout << std::setw(79)<<std::setfill('-')<<""<<"              "<<std::setw(53)<<""<<std::setfill(' ')<< std::endl;
      std::cout << "|" << std::setw(33)
	        << " DATA -> "
		<< "|" << std::left << std::setw( 8) << " ERRORS"
		<< "|" << std::left << std::setw( 8) << " OVRFLW"
	        << "|" << std::left << std::setw(12) << " USEDW"
	        << "|" << std::left << std::setw(12) << " USEDW(AVG)"
		<< "|" << std::left << std::setw(12) << " USEDW"
		<< "|" << std::left << std::setw(12) << " USEDW(AVG)"
		<< "|" << std::left << std::setw( 6) << " HOLD"
		<< "|" << std::left << std::setw( 7) << " f_{HOLD}"
		<< "|" << std::left << std::setw( 8) << " OVRFLW"
		<< "|" << std::right << std::endl;

      std::cout << std::setw(79)<<std::setfill('-')<<""<<"              "<<std::setw(53)<<""<<std::setfill(' ')<< std::endl;
     
      //
      // QSFP Links
      //
      if(showI1)
	{
	  aux_print_linkerrors(input1,"Input 1 QSFP0",0x30,0,SLINK_STATUS,0);
	  aux_print_linkerrors(input1,"Input 1 QSFP1",0x30,1,SLINK_STATUS,1);
	  aux_print_linkerrors(input1,"Input 1 QSFP2",0x30,2,SLINK_STATUS,2);
	  aux_print_linkerrors(input1,"Input 1 QSFP3",0x30,3,SLINK_STATUS,3);
	}
      if(showI2)
	{
	  aux_print_linkerrors(input2,"Input 2 QSFP0",0x30,0,SLINK_STATUS,0);
	  aux_print_linkerrors(input2,"Input 2 QSFP1",0x30,1,SLINK_STATUS,1);
	  aux_print_linkerrors(input2,"Input 2 QSFP2",0x30,2,SLINK_STATUS,2);
	  aux_print_linkerrors(input2,"Input 2 QSFP3",0x30,3,SLINK_STATUS,3);
	}
      if(showI1 || showI2) std::cout << std::setw(79)<<std::setfill('-')<<""<<"              "<<std::setw(53)<<""<<std::setfill(' ')<< std::endl;
      
      //
      // Hit Links
      //
      if(showI1)
	{
	  aux_print_linkerrors(input1,"Input 1 Hit 0",0x31,0,LINK_STATUS, 8);
	  aux_print_linkerrors(input1,"Input 1 Hit 1",0x31,1,LINK_STATUS, 9);
	  aux_print_linkerrors(input1,"Input 1 Hit 2",0x31,2,LINK_STATUS,10);
	  aux_print_linkerrors(input1,"Input 1 Hit 3",0x31,3,LINK_STATUS,11);
	  aux_print_linkerrors(input1,"Input 1 Hit 4",0x31,4,LINK_STATUS,12);
	  aux_print_linkerrors(input1,"Input 1 Hit 5",0x31,5,LINK_STATUS,13);
	  aux_print_linkerrors(input1,"Input 1 Hit 6",0x31,6,LINK_STATUS,14);
	  aux_print_linkerrors(input1,"Input 1 Hit 7",0x31,7,LINK_STATUS,15);
	}
      if(showI2)
	{
	  aux_print_linkerrors(input2,"Input 2 Hit 0",0x31,0,LINK_STATUS, 8);
	  aux_print_linkerrors(input2,"Input 2 Hit 1",0x31,1,LINK_STATUS, 9);
	  aux_print_linkerrors(input2,"Input 2 Hit 2",0x31,2,LINK_STATUS,10);
	  aux_print_linkerrors(input2,"Input 2 Hit 3",0x31,3,LINK_STATUS,11);
	}
      if(showI1 || showI2) std::cout << std::setw(79)<<std::setfill('-')<<""<<"              "<<std::setw(53)<<""<<std::setfill(' ')<< std::endl;

      //
      // AM Links
      //
      if(showI1)
	{
	  aux_print_linkerrors(input1,"Input 1 AM  0",0x32,0,LINK_STATUS, 0);
	  aux_print_linkerrors(input1,"Input 1 AM  1",0x32,1,LINK_STATUS, 1);
	  aux_print_linkerrors(input1,"Input 1 AM  2",0x32,2,LINK_STATUS, 2);
	  aux_print_linkerrors(input1,"Input 1 AM  3",0x32,3,LINK_STATUS, 3);
	  aux_print_linkerrors(input1,"Input 1 AM  4",0x32,4,LINK_STATUS, 4);
	  aux_print_linkerrors(input1,"Input 1 AM  5",0x32,5,LINK_STATUS, 5);
	  aux_print_linkerrors(input1,"Input 1 AM  6",0x32,6,LINK_STATUS, 6);
	  aux_print_linkerrors(input1,"Input 1 AM  7",0x32,7,LINK_STATUS, 7);
	}
      if(showI2)
	{
	  aux_print_linkerrors(input2,"Input 2 AM  0",0x32,0,LINK_STATUS, 0);
	  aux_print_linkerrors(input2,"Input 2 AM  1",0x32,1,LINK_STATUS, 1);
	  aux_print_linkerrors(input2,"Input 2 AM  2",0x32,2,LINK_STATUS, 2);
	  aux_print_linkerrors(input2,"Input 2 AM  3",0x32,3,LINK_STATUS, 3);
	}
      if(showI1 || showI2) std::cout << std::setw(79)<<std::setfill('-')<<""<<"              "<<std::setw(53)<<""<<std::setfill(' ')<< std::endl;

      //
      // Gendata Tx Links
      //
      if(showI2 && showUnused)
	{
	  aux_print_linkerrors(input2,"Input 2 GenData Tx 0",0x33,0,LINK_STATUS,16);
	  aux_print_linkerrors(input2,"Input 2 GenData Tx 1",0x33,1,LINK_STATUS,17);
	  aux_print_linkerrors(input2,"Input 2 GenData Tx 2",0x33,2,LINK_STATUS,18);
	  aux_print_linkerrors(input2,"Input 2 GenData Tx 3",0x33,3,LINK_STATUS,19);
	}

      //
      // Gendata Rx Links
      //
      if(showI2)
	{
	  aux_print_linkerrors(input2,"Input 2 GenData Rx 0",0x34,0,LINK_STATUS,20);
	  aux_print_linkerrors(input2,"Input 2 GenData Rx 1",0x34,1,LINK_STATUS,21);
	  aux_print_linkerrors(input2,"Input 2 GenData Rx 2",0x34,2,LINK_STATUS,22);
	  aux_print_linkerrors(input2,"Input 2 GenData Rx 3",0x34,3,LINK_STATUS,23);
	}
      if(showI2) std::cout << std::setw(79)<<std::setfill('-')<<""<<"              "<<std::setw(53)<<""<<std::setfill(' ')<< std::endl;

      //
      // SFP Link
      //
      if(showI2)
	{
	  aux_print_linkerrors(input2,"Input 2 SFP",0x35,0,SLINK_STATUS,4);
	  std::cout << std::setw(79)<<std::setfill('-')<<""<<"              "<<std::setw(53)<<""<<std::setfill(' ')<< std::endl;
	}

      //
      // Processor links
      //
      for(u_int proc=1;proc<=4;proc++)
	{
	  if(proc==1 && !showP1) continue;
	  if(proc==2 && !showP2) continue;
	  if(proc==3 && !showP3) continue;
	  if(proc==4 && !showP4) continue;

	  std::ostringstream procTitle;
	  procTitle << "Processor " << proc;

	  if(showUnused)
	    {
	      aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Tx  0",0x55, 0,LINK_STATUS, 0);
	      aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Tx  1",0x55, 1,LINK_STATUS, 1);
	      aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Tx  2",0x55, 2,LINK_STATUS, 2);
	      aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Tx  3",0x55, 3,LINK_STATUS, 3);
	      aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Tx  4",0x55, 4,LINK_STATUS, 4);
	      aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Tx  5",0x55, 5,LINK_STATUS, 5);
	      aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Tx  6",0x55, 6,LINK_STATUS, 6);
	      aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Tx  7",0x55, 7,LINK_STATUS, 7);
	      aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Tx  8",0x55, 8,LINK_STATUS, 8);
	      aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Tx  9",0x55, 9,LINK_STATUS, 9);
	      aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Tx 10",0x55,10,LINK_STATUS,10);
	      aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Tx 11",0x55,11,LINK_STATUS,11);
	      std::cout << std::setw(79)<<std::setfill('-')<<""<<"              "<<std::setw(53)<<""<<std::setfill(' ')<< std::endl;
	    }

	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Rx  0",0x56, 0,LINK_STATUS,12);
	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Rx  1",0x56, 1,LINK_STATUS,13);
	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Rx  2",0x56, 2,LINK_STATUS,14);
	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Rx  3",0x56, 3,LINK_STATUS,15);
	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Rx  4",0x56, 4,LINK_STATUS,16);
	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Rx  5",0x56, 5,LINK_STATUS,17);
	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Rx  6",0x56, 6,LINK_STATUS,18);
	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Rx  7",0x56, 7,LINK_STATUS,19);
	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Rx  8",0x56, 8,LINK_STATUS,20);
	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Rx  9",0x56, 9,LINK_STATUS,21);
	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Rx 10",0x56,10,LINK_STATUS,22);
	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Hits Rx 11",0x56,11,LINK_STATUS,23);
	  std::cout << std::setw(79)<<std::setfill('-')<<""<<"              "<<std::setw(53)<<""<<std::setfill(' ')<< std::endl;

	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Roads 0",0x59, 0,LINK_STATUS,24);
	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Roads 1",0x59, 1,LINK_STATUS,25);
	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Roads 2",0x59, 2,LINK_STATUS,26);
	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Roads 3",0x59, 3,LINK_STATUS,27);
	  std::cout << std::setw(79)<<std::setfill('-')<<""<<"              "<<std::setw(53)<<""<<std::setfill(' ')<< std::endl;

	  aux_print_linkerrors(procs[proc-1],procTitle.str()+" Tracks",0x57, 0,LINK_STATUS,28);
	  if(showUnused) aux_print_linkerrors(procs[proc-1],procTitle.str()+" GenData",0x58, 0,LINK_STATUS,29);
	  std::cout << std::setw(79)<<std::setfill('-')<<""<<"              "<<std::setw(53)<<""<<std::setfill(' ')<< std::endl;

	}
      return 0;
}

/********************************************************************/
/********************************************************************
 * aux_error_count
 * Print out the count of the number of error bits that have occured
 * *****************************************************************/
/********************************************************************/
bool aux_error_count(int slot, std::vector<int> FPGA_Nums, bool DO_errors, bool TFBL_errors, bool TFComb_errors, bool Input_freezes, bool Proc_freezes, bool Truncations) {
    // Will be set true if using that fpga
    bool showI1 = false, showI2 = false, showP1 = false, showP2 = false, showP3 = false, showP4 = false, showProc = false;
    // Will be vector of the procs for easy looping
    std::vector<VMEInterface*> procs; 
    VMEInterface *inpt1, *inpt2, *proc1, *proc2, *proc3, *proc4;
    int numfpgas = FPGA_Nums.size();

    // Parameters for printout formatting
    int entry_width = 12; // The width of each entry in the table (32 bits is 10 wide + room to look nice
    int label_width = 30; // The width of the first column, the label of the errors
    std::ostringstream errLabel; // String stream that will hold the err name
    std::ostringstream line; // String stream that will hold a horizontal dividing line
    
    // Horizontal line that divides sections in the printout
    line << "|-" << std::string(label_width + (entry_width+2)*numfpgas, '-') << "-|";
    std::cout << line.str() << std::endl;
    
    // Parse which FPGA's are included and setup the vme objects
    std::cout << "| " << std::setw(label_width) << "Error" << " |";
    for (int i = 0; i < numfpgas; i++) {
        if (FPGA_Nums[i] == 1) {
            inpt1 =VMEManager::global().aux(slot,1);
            std::cout << std::setw(entry_width) << "Input1  " << " |";
            showI1 = true;
        }
        else if (FPGA_Nums[i] == 2) {
            inpt2 =VMEManager::global().aux(slot,2);
            std::cout << std::setw(entry_width) << "Input2  " << " |";
            showI2 = true;
        }
        else if (FPGA_Nums[i] == 3) {
            proc1 = VMEManager::global().aux(slot,3);
            procs.push_back(proc1);
            std::cout << std::setw(entry_width) << "Proc1   " << " |";
            showP1 = true;
        }
        else if (FPGA_Nums[i] == 4) {
            proc2 =VMEManager::global().aux(slot,4);
            procs.push_back(proc2);
            std::cout << std::setw(entry_width) << "Proc2   " << " |";
            showP2 = true;
        }
        else if (FPGA_Nums[i] == 5) {
            proc3 =VMEManager::global().aux(slot,5);
            procs.push_back(proc3);
            std::cout << std::setw(entry_width) << "Proc3   " << " |";
            showP3 = true;
        }
        else if (FPGA_Nums[i] == 6) {
            proc4 =VMEManager::global().aux(slot,6);
            procs.push_back(proc4);
            std::cout << std::setw(entry_width) << "Proc4   " << " |";
            showP4 = true;
        }
        else std::cerr << "Invalid FPGA number " << FPGA_Nums[i] << " passed to aux_error_count." << std::endl;
    }
    showProc = showP1 || showP2 || showP3 || showP4;
    std::cout << std::endl;

    std::cout << line.str() << std::endl;

    ///////////////
    // DO ERRORS //
    ///////////////
    if (DO_errors && showProc) {
        for (int sel = 0; sel < NUM_DO_ERR_TYPES; sel++) {
            errLabel.str(""); // Clear the label string
            if (sel <= 10) errLabel << "NoHitLayer" << sel; // Which bit is which error
            if (sel == 11) errLabel << "AMMapError";
            if (sel == 12) errLabel << "InvalidBitmask";
            std::cout << "| " << std::setw(label_width) << errLabel.str() << " |";

            // Print empty strings for I1 and I2 to maintain formatting
            if (showI1) std::cout << std::setw(entry_width) << "" << " |";
            if (showI2) std::cout << std::setw(entry_width) << "" << " |";

            // Loop over any processors that are in the procs array
            for (std::vector<VMEInterface*>::iterator procs_it = procs.begin(); procs_it < procs.end(); procs_it++) {
                // Load the count of those freeze bits into DO_ERROR_COUNT register
                (*procs_it)->write_word(TMODE, 0x1); // Set Test Mode (enable write)
                // (*procs_it)->write_word(DO_ERROR_COUNT_CTRL_ADDR, sel << DO_ERROR_COUNT_CTRL_BITS); // left-bitshift sel
                (*procs_it)->write_mask(DO_ERROR_COUNT_CTRL_ADDR, sel << DO_ERROR_COUNT_CTRL_BITS, 0x1F << DO_ERROR_COUNT_CTRL_BITS); // left-bitshift sel
                (*procs_it)->write_word(TMODE, 0x0); // Unset Test Mode (disable write)
                
                // Read the error count from reg DO_ERROR_COUNT
                std::cout << std::setw(entry_width) << (*procs_it)->read_word(DO_ERROR_COUNT) << " |";
            }
            std::cout << std::endl;
        }
        std::cout << line.str() << std::endl;
    }

    /////////////////
    // TFBL ERRORS //
    /////////////////
    if (TFBL_errors && showProc) {
        // Freeze bits to show:
        // Loop over i over array of select integers. Have a corresponding array for the error names.
        std::vector<int> TFBLSel(26);
        std::iota(std::begin(TFBLSel), std::end(TFBLSel), 0); // Fill TFBLSel with integers from 0 to 25
        std::vector<std::string> TFBLErrorMsg = {"In N EOR Pix 0",
                                                 "In N EOR Pix 1",
                                                 "In N EOR Pix 2",
                                                 "In N EOR Pix 3",
                                                 "In N EOR Pix 4",
                                                 "In N EOR Pix 5",
                                                 "In N EOR Sct 0",
                                                 "In N EOR Sct 1",
                                                 "In N EOR Sct 2",
                                                 "In N EOR Sct 3",
                                                 "In N EOR Sct 4",
                                                 "In N Sectors",
                                                 "In N Roads,",
                                                 "N Roads In vs Out",
                                                 "Out N EOR Pix 0",
                                                 "Out N EOR Pix 1",
                                                 "Out N EOR Pix 2",
                                                 "Out N EOR Pix 3",
                                                 "Out N EOR Pix 4",
                                                 "Out N EOR Pix 5",
                                                 "Out N EOR Sct 0",
                                                 "Out N EOR Sct 1",
                                                 "Out N EOR Sct 2",
                                                 "Out N EOR Sct 3",
                                                 "Out N EOR Sct 4",
                                                 "Counter FIFO OVFL"};

        if (TFBLSel.size() != TFBLErrorMsg.size()) {
            std::cerr << "Number of TFBL freeze bits does not match number of error messages." << std::endl;
            return 1;
        }

        for (int i = 0; i < static_cast<int>(TFBLSel.size()); i++) {
            std::cout << "| " << std::setw(label_width) << TFBLErrorMsg[i] << " |";
            
            // Print empty strings for I1 and I2 to maintain formatting
            if (showI1) std::cout << std::setw(entry_width) << "" << " |";
            if (showI2) std::cout << std::setw(entry_width) << "" << " |";
            
            // Loop over any processors that are in the procs array
            for (std::vector<VMEInterface*>::iterator procs_it = procs.begin(); procs_it < procs.end(); procs_it++) {
                // Load the count of those freeze bits into DO_ERROR_COUNT register
                (*procs_it)->write_word(TMODE, 0x1); // Set Test Mode (enable write)
                //(*procs_it)->write_word(TFBL_ERROR_COUNTER_SELECTOR, TFBLSel[i] << TFBL_ERROR_COUNTER_SELECTOR_BITSHIFT); // left-bitshift sel
                (*procs_it)->write_mask(TFBL_ERROR_COUNTER_SELECTOR, TFBLSel[i] << TFBL_ERROR_COUNTER_SELECTOR_BITSHIFT, 0x1F << TFBL_ERROR_COUNTER_SELECTOR_BITSHIFT); // left-bitshift sel
                (*procs_it)->write_word(TMODE, 0x0); // Unset Test Mode (disable write)
                
                // Read the error count from reg TFBL_ERROR_COUNTER
                std::cout << std::setw(entry_width) << (*procs_it)->read_word(TFBL_ERROR_COUNTER) << " |";
            }
            std::cout << std::endl;
	}
	std::cout << line.str() << std::endl;
    }

    ////////////////////////
    // TF COMBINER ERRORS //
    ////////////////////////
    if (TFComb_errors && showProc) {
        // Freeze bits to show:
        // Loop over i over array of select integers. Have a corresponding array for the error names.
        std::vector<int> TFCombSel(NNOMFITTERS+NPIXFITTERS+NSCTFITTERS); // Defined in aux_vme_regs.h
        std::iota(std::begin(TFCombSel), std::end(TFCombSel), 0); // Fill TFCombSel with integers starting at 0

        std::vector<std::string> TFCombErrorMsg;
        std::ostringstream oss; // A string stream to store the error label as we build it for each of the different types of fitters
        for (int i = 0; i < static_cast<int>(TFCombSel.size()); i++) {
            if (i < NNOMFITTERS) {
                oss << "Nom Combiner " << i;
                }
            else if (i < NNOMFITTERS+NPIXFITTERS) {
                oss << "Pix Combiner " << i - NNOMFITTERS;
            }
            else if (i < NNOMFITTERS+NPIXFITTERS+NSCTFITTERS) {
                oss << "Sct Combiner " << i - NNOMFITTERS - NPIXFITTERS;
            }
            else {
                std::cerr << "Problem in label of combiner errors in aux_error_cout" << std::endl;
                return 1;
            }
            TFCombErrorMsg.push_back(oss.str()); // Cast as string and append
            oss.str(std::string()); // Clear the string stream
        }

        if (TFCombSel.size() != TFCombErrorMsg.size()) {
            std::cerr << "Number of TFComb freeze bits does not match number of error messages." << std::endl;
            return 1;
        }

        for (int i = 0; i < static_cast<int>(TFCombSel.size()); i++) {
            std::cout << "| " << std::setw(label_width) << TFCombErrorMsg[i] << " |";
            
            // Print empty strings for I1 and I2 to maintain formatting
            if (showI1) std::cout << std::setw(entry_width) << "" << " |";
            if (showI2) std::cout << std::setw(entry_width) << "" << " |";
            
            // Loop over any processors that are in the procs array
            for (std::vector<VMEInterface*>::iterator procs_it = procs.begin(); procs_it < procs.end(); procs_it++) {
                // Load the count of those freeze bits into DO_ERROR_COUNT register
                (*procs_it)->write_word(TMODE, 0x1); // Set Test Mode (enable write)
                //(*procs_it)->write_word(TFComb_ERROR_COUNTER_SELECTOR, TFCombSel[i] << TFComb_ERROR_COUNTER_SELECTOR_BITSHIFT); // left-bitshift sel
                (*procs_it)->write_mask(TFCOMB_ERROR_COUNTER_SELECTOR, TFCombSel[i] << TFCOMB_ERROR_COUNTER_SELECTOR_BITSHIFT, 0x7 << TFCOMB_ERROR_COUNTER_SELECTOR_BITSHIFT); // left-bitshift sel
                (*procs_it)->write_word(TMODE, 0x0); // Unset Test Mode (disable write)
                
                // Read the error count from reg TFComb_ERROR_COUNTER
                std::cout << std::setw(entry_width) << (*procs_it)->read_word(TFCOMB_ERROR_COUNTER) << " |";
            }
            std::cout << std::endl;
        }
        std::cout << line.str() << std::endl;
    }

    ///////////////////
    // Input Freezes //
    ///////////////////
    if (Input_freezes && (showI1 || showI2)) {
        // Freeze bits to show:
        // Loop over i over array of select integers. Have a corresponding array for the error names.
        std::vector<int> CommonSel = {2, 3, 6, 8, 9};
        std::vector<std::string> CommonErrorMsg = {"QSFP L1ID Sync",
                                       "Hit L1ID Sync",
                                       "SSData L1ID Sync",
                                       "QSFP Packet",
                                       "Hit Packet"};

        std::vector<int> I1Sel  = {10, 11, 12};
        std::vector<std::string> I1ErrorMsg = {"HS Ovfl Freeze 1",
                                       "HS Ovfl Freeze 2",
                                       "HS Ovfl Freeze 3"};

        std::vector<int> I2Sel  = {4, 5, 10, 11, 12};
        std::vector<std::string> I2ErrorMsg = {"GenData Rx L1ID Sync",
                                       "GenData Tx L1ID Sync",
                                       "GenData Rx Packet",
                                       "GenData Tx Packet",
                                       "SFP Packet"};

        if (CommonSel.size() != CommonErrorMsg.size()) {
            std::cerr << "Number of common input freeze bits does not match number of error messages." << std::endl;
            return 1;
        }
        if (I1Sel.size() != I1ErrorMsg.size()) {
            std::cerr << "Number of Input 1 freeze bits does not match number of error messages." << std::endl;
            return 1;
        }
        if (I2Sel.size() != I2ErrorMsg.size()) {
            std::cerr << "Number of Input 2 freeze bits does not match number of error messages." << std::endl;
            return 1;
        }

        // Loop over the error bits that are common between I1 and I2
        for (int i = 0; i < static_cast<int>(CommonSel.size()); i++) {
            std::cout << "| " << std::setw(label_width) << CommonErrorMsg[i] << " |";
            
            if (showI1) {
                inpt1->write_word(TMODE, 0x1); // Set Test Mode (enable write)
                //inpt1->write_word(FREEZE_COUNTER_SELECTOR, CommonSel[i] << FREEZE_COUNTER_SELECTOR_BITSHIFT); // left-bitshift sel
                inpt1->write_mask(FREEZE_COUNTER_SELECTOR, CommonSel[i] << FREEZE_COUNTER_SELECTOR_BITSHIFT, 0x1F << FREEZE_COUNTER_SELECTOR_BITSHIFT); // left-bitshift sel
                inpt1->write_word(TMODE, 0x0); // Unset Test Mode (disable write)
      
                // Read the error count from reg FREEZE_COUNTER
                std::cout << std::setw(entry_width) << inpt1->read_word(FREEZE_COUNTER) << " |";
            }

            if (showI2) {
                inpt2->write_word(TMODE, 0x1); // Set Test Mode (enable write)
                //inpt2->write_word(FREEZE_COUNTER_SELECTOR, CommonSel[i] << FREEZE_COUNTER_SELECTOR_BITSHIFT); // left-bitshift sel
                inpt2->write_mask(FREEZE_COUNTER_SELECTOR, CommonSel[i] << FREEZE_COUNTER_SELECTOR_BITSHIFT, 0x1F << FREEZE_COUNTER_SELECTOR_BITSHIFT); // left-bitshift sel
                inpt2->write_word(TMODE, 0x0); // Unset Test Mode (disable write)
      
                // Read the error count from reg FREEZE_COUNTER
                std::cout << std::setw(entry_width) << inpt2->read_word(FREEZE_COUNTER) << " |";
            }
                
            // Print empty strings for processors to maintain formatting
            if (showP1) std::cout << std::setw(entry_width) << "" << " |";
            if (showP2) std::cout << std::setw(entry_width) << "" << " |";
            if (showP3) std::cout << std::setw(entry_width) << "" << " |";
            if (showP4) std::cout << std::setw(entry_width) << "" << " |";
            
            std::cout << std::endl;
        }

        // Loop over the error bits unique to I1
        if (showI1) {
            for (int i = 0; i < static_cast<int>(I1Sel.size()); i++) {
                std::cout << "| " << std::setw(label_width) << I1ErrorMsg[i] << " |";

                inpt1->write_word(TMODE, 0x1); // Set Test Mode (enable write)
                //inpt1->write_word(FREEZE_COUNTER_SELECTOR, I1Sel[i] << FREEZE_COUNTER_SELECTOR_BITSHIFT); // left-bitshift sel
                inpt1->write_mask(FREEZE_COUNTER_SELECTOR, I1Sel[i] << FREEZE_COUNTER_SELECTOR_BITSHIFT, 0x1F << FREEZE_COUNTER_SELECTOR_BITSHIFT); // left-bitshift sel
                inpt1->write_word(TMODE, 0x0); // Unset Test Mode (disable write)
      
                // Read the error count from reg FREEZE_COUNTER
                std::cout << std::setw(entry_width) << inpt1->read_word(FREEZE_COUNTER) << " |";

                // Print empty strings for other fpgas to maintain formatting
                if (showI2) std::cout << std::setw(entry_width) << "" << " |";
                if (showP1) std::cout << std::setw(entry_width) << "" << " |";
                if (showP2) std::cout << std::setw(entry_width) << "" << " |";
                if (showP3) std::cout << std::setw(entry_width) << "" << " |";
                if (showP4) std::cout << std::setw(entry_width) << "" << " |";

                std::cout << std::endl;
            }
        }
        
        // Loop over the error bits unique to I2
        if (showI2) {
            for (int i = 0; i < static_cast<int>(I2Sel.size()); i++) {
                std::cout << "| " << std::setw(label_width) << I2ErrorMsg[i] << " |";

                if (showI1) std::cout << std::setw(entry_width) << "" << " |";

                inpt2->write_word(TMODE, 0x2); // Set Test Mode (enable write)
                //inpt2->write_word(FREEZE_COUNTER_SELECTOR, I2Sel[i] << FREEZE_COUNTER_SELECTOR_BITSHIFT); // left-bitshift sel
                inpt2->write_mask(FREEZE_COUNTER_SELECTOR, I2Sel[i] << FREEZE_COUNTER_SELECTOR_BITSHIFT, 0x1F << FREEZE_COUNTER_SELECTOR_BITSHIFT); // left-bitshift sel
                inpt2->write_word(TMODE, 0x0); // Unset Test Mode (disable write)
      
                // Read the error count from reg FREEZE_COUNTER
                std::cout << std::setw(entry_width) << inpt2->read_word(FREEZE_COUNTER) << " |";

                // Print empty strings for other fpgas to maintain formatting
                if (showP1) std::cout << std::setw(entry_width) << "" << " |";
                if (showP2) std::cout << std::setw(entry_width) << "" << " |";
                if (showP3) std::cout << std::setw(entry_width) << "" << " |";
                if (showP4) std::cout << std::setw(entry_width) << "" << " |";
                
                std::cout << std::endl;
            }
        }
        std::cout << line.str() << std::endl;
    }

    ///////////////////////
    // Processor Freezes //
    ///////////////////////
    if (Proc_freezes && showProc) {
        // Freeze bits to show:
        // Loop over i over array of select integers. Have a corresponding array for the error names.
        std::vector<int> ProcSel  = {2, 3, 4, 5, 6, 7, 8, 9, 20, 21, 22, 23, 24};
        std::vector<std::string> ProcErrorMsg = {"Tx L1ID Sync",
                                     "Rx L1ID Sync",
                                     "Road L1ID Sync",
                                     "Rx Packet",
                                     "Tx Packet",
                                     "Merge Packet",
                                     "Gendata Packet",
                                     "DO Error",
                                     "AMB Repeated Word",
                                     "AMB Repeated Word",
                                     "AMB Repeated Word",
                                     "AMB Repeated Word",
                                     "TF BL Error"};
        if (ProcSel.size() != ProcErrorMsg.size()) {
            std::cerr << "Number of processor freeze bits does not match number of error messages." << std::endl;
            return 1;
        }

        for (int i = 0; i < static_cast<int>(ProcSel.size()); i++) {
            std::cout << "| " << std::setw(label_width) << ProcErrorMsg[i] << " |";
            
            // Print empty strings for I1 and I2 to maintain formatting
            if (showI1) std::cout << std::setw(entry_width) << "" << " |";
            if (showI2) std::cout << std::setw(entry_width) << "" << " |";
            
            // Loop over any processors that are in the procs array
            for (std::vector<VMEInterface*>::iterator procs_it = procs.begin(); procs_it < procs.end(); procs_it++) {
                // Load the count of those freeze bits into DO_ERROR_COUNT register
                (*procs_it)->write_word(TMODE, 0x1); // Set Test Mode (enable write)
                //(*procs_it)->write_word(FREEZE_COUNTER_SELECTOR, ProcSel[i] << FREEZE_COUNTER_SELECTOR_BITSHIFT); // left-bitshift sel
                (*procs_it)->write_mask(FREEZE_COUNTER_SELECTOR, ProcSel[i] << FREEZE_COUNTER_SELECTOR_BITSHIFT, 0x1F << FREEZE_COUNTER_SELECTOR_BITSHIFT); // left-bitshift sel
                (*procs_it)->write_word(TMODE, 0x0); // Unset Test Mode (disable write)
                
                // Read the error count from reg FREEZE_COUNTER
                std::cout << std::setw(entry_width) << (*procs_it)->read_word(FREEZE_COUNTER) << " |";
            }
            std::cout << std::endl;
        }
        std::cout << line.str() << std::endl;
    }

    ///////////////////////
    // Data Truncations  //
    ///////////////////////
    if (Truncations) {
        std::cout << std::endl << std::endl;

        std::cout << line.str() << std::endl;
        std::cout << "|" << std::setw(label_width+2*entry_width) << "" << std::setw(entry_width) << "TRUCATIONS " << std::setw(4*entry_width) << "" << "  |" << std::endl;  
        std::cout << line.str() << std::endl;

        
                                    
        std::cout << "| " << std::setw(label_width) << "Road Limit Reached" << " |";

        // Print empty strings for I1 and I2 to maintain formatting
        if (showI1) std::cout << std::setw(entry_width) << "" << " |";
        if (showI2) std::cout << std::setw(entry_width) << "" << " |";

        // Loop over any processors that are in the procs array
        for (std::vector<VMEInterface*>::iterator procs_it = procs.begin(); procs_it < procs.end(); procs_it++) {
            // Read the error count from reg FREEZE_COUNTER
            std::cout << std::setw(entry_width) << (*procs_it)->read_word(ROAD_LIMIT_REACHED) << " |";
        }
        std::cout << std::endl;

        for(int ifitter=0; ifitter<7; ifitter++){
            std::string name = "Fitter " + std::to_string(ifitter) + " Fit Limit Reached";
            std::cout << "| " << std::setw(label_width) << name << " |";

            // Print empty strings for I1 and I2 to maintain formatting
            if (showI1) std::cout << std::setw(entry_width) << "" << " |";
            if (showI2) std::cout << std::setw(entry_width) << "" << " |";
            for (std::vector<VMEInterface*>::iterator procs_it = procs.begin(); procs_it < procs.end(); procs_it++) 
                std::cout << std::setw(entry_width) << (*procs_it)->read_word( (FIT_LIMIT_REACHED<<16) + (ifitter<<8) ) << " |";
            std::cout << std::endl;
        }
        
        for(int ifitter=0; ifitter<7; ifitter++){
            std::string name = "Fitter " + std::to_string(ifitter) + " Combiner Overflow";
            std::cout << "| " << std::setw(label_width) << name << " |";

            // Print empty strings for I1 and I2 to maintain formatting
            if (showI1) std::cout << std::setw(entry_width) << "" << " |";
            if (showI2) std::cout << std::setw(entry_width) << "" << " |";
            for (std::vector<VMEInterface*>::iterator procs_it = procs.begin(); procs_it < procs.end(); procs_it++) 
                std::cout << std::setw(entry_width) << (*procs_it)->read_word( (COMBINER_OVERFLOW<<16) + (ifitter<<8) ) << " |";
            std::cout << std::endl;
        }
        std::cout << std::endl;



    }
    std::cout << line.str() << std::endl;


    return 0;
}

// Output all monitoring info of the aux2ros firmware blocks
bool aux_aux2ros_status(int slot){
    uint TIMEOUT_THRESHOLD_REG = 0x20;
    uint PACKET_ERR_WORD_REG   = 0x400;

    VMEInterface* input2 =VMEManager::global().aux(slot,2);

    uint timeout_threshold = input2->read_word(TIMEOUT_THRESHOLD_REG);
    if (timeout_threshold == 0) timeout_threshold = 0xFFFFFFFF; // In FW, if 0, autosets to no timeout threshold (=0xFFFFFFFF)

    // General status information
    std::cout << "Packet Error Word: " << std::hex << input2->read_word(PACKET_ERR_WORD_REG) << std::endl;
    std::cout << "Timeout Threshold: " << std::hex << timeout_threshold << std::endl;

    std::cout << std::endl;

    // AUX2ROS FIFO Monitor
    std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
    std::cout << "|" << std::setw(33)
      << "|" << std::setw(6) << "HOLD "
      << "|" << std::setw(10) << "f_{HOLD} "
      << "|" << std::setw(12) << "USEDW "
      << "|" << std::setw(12) << "USEDW (AVG)"
      << "|" << std::left << std::setw(8) << " OVRFLW"
          << "|" << std::left << std::setw(8) << " REGISTER "
      << "|" << std::endl;
    std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;
    aux_print_fifoinfo(input2,"AUX2ROS PacketHandler InFIFO",0x3B0000);
	std::cout << std::setw(98) << std::setfill('-') << "" << std::setfill(' ') << std::endl;

    std::cout << std::endl;

    // AUX2ROS FSM Output Spybuffer 
    std::vector<uint> buffer;
    buffer.push_back(0x1F); // Raw HW output
    buffer.push_back(0x18); // AUX2ROS FSM output
    aux_read_buffer(slot,2,11,buffer); //TODO: update
    
    return 0;
}

} // End namespace ftk
} // End namespace daq

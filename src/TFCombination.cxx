#include "aux/TF/TFCombination.h"
#include <iomanip>
#include <sstream>
#include "aux/AuxUtils.h"
#include "aux/AuxString.h"
#include "aux/Report.h"

namespace daq {
  namespace ftk {
    TFCombination::TFCombination()
      : id( -1 )
      , roadid( 0 )
      , sectorid( 0 )
      , layermap( 0 )
      , guessmap( 0 )
      , overflow( false )
      , passes( false )
      , ChiSquare( -1 )
      , overflow_nominal( false )
      , ChiSquare_nominal( -1 )
    {
      for(unsigned int i = 0 ; i < tf::NLayers ; i++ )
	{
	  Hits[i] = 0;
	  ChiSquare_recovery[i] = -1;
	  overflow_recovery[i] = false;
	}
      for(unsigned  int i = 0 ; i < tf::NCoords ; i++ ) Coords[i] = 0;
    }

    TFCombination::TFCombination( TFCombination *other )
      : id( other->id )
      , roadid( other->roadid )
      , sectorid( other->sectorid )
      , layermap( other->layermap )
      , guessmap( other->guessmap )
      , overflow( other->overflow )
      , passes( other->passes )
      , ChiSquare( other->ChiSquare )
    {
      for(unsigned int i = 0 ; i < tf::NLayers ; i++ ) Hits[i] = other->Hits[i];
      for(unsigned int i = 0 ; i < tf::NCoords ; i++ ) Coords[i] = other->Coords[i];
    }

    TFCombination::~TFCombination()
    { }

    bool TFCombination::operator==( const TFCombination &other ) const
    {
      return ( roadid == other.roadid &&
	       sectorid == other.sectorid &&
	       layermap == other.layermap &&
	       ChiSquare == other.ChiSquare &&
	       Hits[0] == other.Hits[0] &&
	       Hits[1] == other.Hits[1] &&
	       Hits[2] == other.Hits[2] &&
	       Hits[3] == other.Hits[3] &&
	       Hits[4] == other.Hits[4] &&
	       Hits[5] == other.Hits[5] &&
	       Hits[6] == other.Hits[6] &&
	       Hits[7] == other.Hits[7] );
    }

    bool TFCombination::operator<( const TFCombination &other ) const
    {
      if(sectorid<other.sectorid) return true;
      if(sectorid>other.sectorid) return false;
      //same sector ID, sort based on road
      if(roadid<other.roadid) return true;
      if(roadid>other.roadid) return false;
      //same road, sort based on layermap
      if(layermap<other.layermap) return true;
      if(layermap>other.layermap) return false;
      //same layermap, sort based on hit[0]
      if(Hits[0]<other.Hits[0]) return true;
      if(Hits[0]>other.Hits[0]) return false;
      //same layermap, sort based on hit[1]
      if(Hits[1]<other.Hits[1]) return true;
      if(Hits[1]>other.Hits[1]) return false;
      //same layermap, sort based on hit[2]
      if(Hits[2]<other.Hits[2]) return true;
      if(Hits[2]>other.Hits[2]) return false;
      //same layermap, sort based on hit[3]
      if(Hits[3]<other.Hits[3]) return true;
      if(Hits[3]>other.Hits[3]) return false;
      //same layermap, sort based on hit[4]
      if(Hits[4]<other.Hits[4]) return true;
      if(Hits[4]>other.Hits[4]) return false;
      //same layermap, sort based on hit[5]
      if(Hits[5]<other.Hits[5]) return true;
      if(Hits[5]>other.Hits[5]) return false;
      //same layermap, sort based on hit[6]
      if(Hits[6]<other.Hits[6]) return true;
      if(Hits[6]>other.Hits[6]) return false;
      //same layermap, sort based on hit[7]
      return Hits[7]<other.Hits[7];
    }

    void TFCombination::setID(int v) { id = v; }
    void TFCombination::setRoadID(int v) { roadid = v; }
    void TFCombination::setSectorID(unsigned int v) { sectorid = v; }
    void TFCombination::setLayermap(unsigned int v) { layermap = v; }
    void TFCombination::setGuessmap(unsigned int v) { guessmap = v; }
    void TFCombination::setHit(unsigned int ilayer, Int_tf val) { Hits[ilayer] = val; }
    void TFCombination::setCoord(unsigned int icoord, Int_tf val) { Coords[icoord] = val; }
    void TFCombination::setChiSquare(Int_tf val) { ChiSquare = val; }
    void TFCombination::setOverflow(bool v) { overflow = v; }
    void TFCombination::setPasses(bool v) { passes = v; }
    void TFCombination::setChiSquareNominal(Int_tf val) { ChiSquare_nominal = val; }
    void TFCombination::setOverflowNominal(bool v) { overflow_nominal = v; }
    void TFCombination::setChiSquareRecovery(unsigned int ilayer, Int_tf val) { ChiSquare_recovery[ilayer] = val; }
    void TFCombination::setOverflowRecovery(unsigned int ilayer, bool v) { overflow_recovery[ilayer] = v; }

    int TFCombination::getID() const { return id; }
    int TFCombination::getRoadID() const { return roadid; }
    unsigned int TFCombination::getSectorID() const { return sectorid; }
    unsigned int TFCombination::getLayermap() const { return layermap; }
    unsigned int TFCombination::getGuessmap() const { return guessmap; }
    unsigned int TFCombination::getLiveLayermap() const { return aux::getMask(tf::NLayers) & (getLayermap() | ~getGuessmap()); }
    Int_tf  TFCombination::getHit(unsigned int ilayer) const { return Hits[ilayer]; }
    Int_tf TFCombination::getCoord(unsigned int icoord) const { return Coords[icoord]; }
    Int_tf TFCombination::getChiSquare() const { return ChiSquare; }
    bool TFCombination::getOverflow() const { return overflow; }
    bool TFCombination::getPasses() const { return passes; }
    Int_tf TFCombination::getChiSquareNominal() const { return ChiSquare_nominal; }
    bool TFCombination::getOverflowNominal() const { return overflow_nominal; }
    Int_tf TFCombination::getChiSquareRecovery(unsigned int ilayer) const { return ChiSquare_recovery[ilayer]; }
    bool TFCombination::getOverflowRecovery(unsigned int ilayer) const { return overflow_recovery[ilayer]; }
    bool TFCombination::recoveryAttempted() const { return ChiSquare_recovery[0] != -1; }

    double TFCombination::getScaledChiSquare() const
    { return double(ChiSquare) / (1<<26); }

    double TFCombination::getScaledChiSquareNominal() const
    { return double(ChiSquare_nominal) / (1<<26); }

    double TFCombination::getScaledChiSquareRecovery(unsigned int ilayer ) const
    { return double(ChiSquare_recovery[ilayer]) / (1<<26); }

    void TFCombination::extractCoordinates()
    {
      // Coordinate extraction for ttbar testvectors
  
      for(unsigned int ipixlayer = 0 ; ipixlayer < tf::NPixLayers ; ipixlayer++ )
	{
	  // WARNING - inverted coordinate order here

	  // Coord 0 is 27 downto 16 (12-bits)
	  // offset by factor of 2^4
	  Int_tf coord0 = ((Hits[ipixlayer] >> 16) & aux::getMask(12)) << 0;
	  Coords[(ipixlayer*2)+1] = coord0;

	  // Coord 1 is 11 downto 0 (12-bits)
	  // offset by factor of 2^3
	  Int_tf coord1 = (Hits[ipixlayer] & aux::getMask(12)) << 1;
	  //	  Int_tf coord1 = (Hits[ipixlayer] & aux::getMask(12));
	  Coords[(ipixlayer*2)+0] = coord1;
	}

      for(unsigned int isctlayer = 0 ; isctlayer < tf::NSctLayers ; isctlayer++ )
	{
	  // Coord is 10 downto 0 (11-bits) followed by "00"
	  // offset by factor of 2
	  Int_tf coord = (Hits[isctlayer+tf::NPixLayers] & aux::getMask(11)) << 3;
	  //	  Int_tf coord = (Hits[isctlayer+tf::NPixLayers] & aux::getMask(11));
	  Coords[isctlayer+(2*tf::NPixLayers)] = coord;
	}

    }

    void TFCombination::constructPixHit(unsigned int ilayer, Int_tf coord0, Int_tf coord1 )
    {
      Int_tf hit = 0;
      hit += coord1 << 16;
      hit += coord0 >> 1;
      hit &= 0x000000000fffffff;
      Hits[ilayer] = hit;
    }

    void TFCombination::constructSctHit(unsigned int ilayer, Int_tf coord)
    {
      Int_tf hit = 0;
      hit += coord >> 3;//2; //was 3, fixed by Rui to match firmware
      Hits[ilayer] = hit;
    }

    void TFCombination::print() const
    {
      //
      // Print data for debugging
      //
      report::blank( "" );

      report::info( "Dumping TFCombination %i..." , id );
      //report::blank( "// if( comb->getID()==%i && comb->getSectorID()==%i && comb->getRoadID()==%i )" , id , sectorid , roadid );
      report::blank( "RoadID: %i" , roadid );
      report::blank( "SectorID: %i" , sectorid );
      report::blank( "Layermap: %s" , tf::getLayermapStr(layermap).c_str() );
      report::blank( "Guessmap: %s" , tf::getLayermapStr(guessmap).c_str() );

      report::blank( "Hits:" );
      std::string hitline = "";
      for(unsigned int ilayer = 0; ilayer<tf::NLayers; ilayer++)
	{
	  //std::string hitstr = aux::getBit(layermap,ilayer)==1 ? tf::getHitStr(Hits[ilayer]) : "";
	  std::string hitstr = tf::getHitStr(Hits[ilayer]);
	  hitline = auxstring::Format( "%s %15s" , hitline.c_str() , hitstr.c_str() );
	}
      report::blank( hitline );

      report::blank( "Coords:" );
      std::string coordsline = "";
      for(unsigned int icoord = 0; icoord<tf::NCoords; icoord++)
	{
	  if( icoord >= 2 * tf::NPixLayers )
	    {
	      coordsline = auxstring::Format( "%s %15lli" , coordsline.c_str() , Coords[icoord] );
	    }
	  else
	    {
	      coordsline = auxstring::Format( "%s %7lli" , coordsline.c_str() , Coords[icoord] );
	    }
	}
      report::blank( coordsline );

      report::blank( "ChiSquare: %lli, %g" , ChiSquare , getScaledChiSquare() );
      report::blank( "Overflow: %i" , int(overflow) );
      report::blank( "Passes: %i" , int(passes) );

      if( recoveryAttempted() )
	{
	  report::blank( "MajorityRecovery:" );
	  report::blank( " Full ChiSquare = %lli, %g, %i" , ChiSquare_nominal , getScaledChiSquareNominal() , int(overflow_nominal) );
	  for(unsigned int ilayer = 0 ; ilayer < tf::NLayers ; ilayer++ )
	    {
	      report::blank( "   L%i ChiSquare = %lli, %g, %i" , ilayer , ChiSquare_recovery[ilayer] , getScaledChiSquareRecovery(ilayer) , int(overflow_recovery[ilayer]) );
	    }
	}

      report::blank( "" );
    }

  } // namespace ftk
} // namespace daq

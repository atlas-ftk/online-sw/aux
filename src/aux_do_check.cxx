//
// Top level script for DO bit level check
// Created by Michael Hank on July 23, 2019.
// Largely based on Patrick's python code, also using work done by Lesya.
//

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <utility>

#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "aux/aux_vme_regs.h"

#include "aux/Report.h"
#include "aux/aux_check.h"
#include "aux/aux_spybuffer_lib.h"
#include "aux/aux_do_check.h"
#include <aux/DataOrganizer.h>

#include "aux/aux_spy_regs.h"

#include <iostream>
#include <sstream> // ostringstream

#include <stdio.h>
#include <stdlib.h>

using std::cout;  using std::endl;

// Function for DO bit level check

namespace daq {
    namespace ftk {

        void do_toplevel(bool verbose, const std::string& inDirName, int proc, string ambank, bool oldDC, bool allSectors, int inChipOff, bool is_loop)
        {
            //proc=0 means all procs (1,2,3,4)

            //Initialize and read in files
            cout << "Getting events" << endl;
            aux_DO_spies my_DO_spy(inDirName, proc, is_loop);
            cout << "Done getting events" << endl;

            //Loop over processors
            for (u_int iproc = 0; iproc < 4; iproc++) {
                cout << "Running DO bit level check on Processor " << iproc+1 << endl;
                int num_events;
                std::pair<int,int> first_last;

                //Keep track of errors
                bool found_error = false;

                DataOrganizer myDO;

                if(is_loop){//If same event on a loop
                    //do overlap check
                    num_events = my_DO_spy.overlapping_loop(iproc,&found_error);//Just gets the number of events, since we don't care about L1IDs
                    cout << "Checking first " << num_events << " events." << endl;
                    myDO = my_DO_spy.emulate_loop(num_events,ambank, iproc, oldDC, allSectors, inChipOff, verbose, found_error);
                    my_DO_spy.compare_roads(iproc);
                } else {
                    //do overap check
                    first_last = my_DO_spy.overlapping(iproc,&found_error);//gets a pair of L1IDs, first and last
                    cout << "Checking L1IDs from " << first_last.first << " to " << first_last.second << endl;
                    if (first_last.first != -1){
                        myDO = my_DO_spy.emulate(first_last,ambank, iproc, oldDC, allSectors, inChipOff, verbose, found_error);
                    }
                }
               
                // Compare board to sim
                myDO.compare(is_loop);

            }
            return;
        }

    } //namespace daq
} //namespace ftk

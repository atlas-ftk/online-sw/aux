/*           Reconfigure the AUX card using firmware at a specific             *
 *           address in the EPCQ.                                              *
 *           Written for AMBFTK board v1.0   FTK collaboration                 */

#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "aux/aux_vme_regs.h"
#include "ftkvme/VMEManager.h"


namespace daq 
{
  namespace ftk 
  {

    bool aux_lock_internal_bus( int slot )
    { 
      u_int register_content;
      
      for (unsigned int fpga = 3; fpga <= 6; fpga++) {
	VMEInterface* vme = VMEManager::global().aux(slot,fpga);
	vme->write_word(TMODE,0x1);
	vme->write_word(0xe0,0x0);//register that looks for "0FEDC0DE" to unlock sending freeze info to inputs
	vme->write_word(TMODE,0x0);

	register_content = vme->read_word(INFO);
	
        if (register_content == 0x0FEDC0DE) {//unsafe to reconfig, exit
     	  PRINT_LOG("Unable to lock processor InternalAUXbus");
	  return 1;
        }
      }//looped over procs
      PRINT_LOG("Forced InternalAUXbus(7 downto 4) on procs to 'Z'");
      return 0;
      
    }//lock internalbus


    bool aux_reconfigure( int slot, int fpga, uint epcqaddress, bool dontWait )
    { 
      u_int register_content;
      VMEInterface* vme = VMEManager::global().aux(slot,fpga);
            
      // Set the start address
      vme->write_word(TMODE,0x1);
      vme->write_word(RU_RECON,0x400);
      vme->write_word(RU_RECONDATA,epcqaddress);
      vme->write_word(TMODE,0x0);

      // Toggle write of the address
      vme->write_word(TMODE,0x1);
      vme->write_word(RU_RECON,0x1400);
      vme->write_word(TMODE,0x0);

      // Set the watchdog timeout
      vme->write_word(TMODE,0x1);
      vme->write_word(RU_RECON,0x200);
      vme->write_word(RU_RECONDATA,0xff00);
      vme->write_word(TMODE,0x0);

      // Toggle write of the watchdog timout
      vme->write_word(TMODE,0x1);
      vme->write_word(RU_RECON,0x1200);
      vme->write_word(TMODE,0x0);

      // Set the watchdog
      vme->write_word(TMODE,0x1);
      vme->write_word(RU_RECON,0x300);
      vme->write_word(RU_RECONDATA,0x100);
      vme->write_word(TMODE,0x0);

      // Toggle write of the watchdog enable
      vme->write_word(TMODE,0x1);
      vme->write_word(RU_RECON,0x1300);
      vme->write_word(TMODE,0x0);

      // Reconfigure
      vme->write_word(TMODE,0x1);
      vme->write_word(RU_RECON,0x800);
      vme->write_word(TMODE,0x0);

      // Reset registers
      vme->write_word(TMODE,0x1);
      vme->write_word(RU_RECON,0x0);
      vme->write_word(TMODE,0x0);

      // Wait for reconfiguration
      if(!dontWait)
	{
	  register_content=0;
	  do
	    {
	      register_content = vme->read_word(INFO);
	      
	      PRINT_LOG("WAITING FOR FPGA (returned 0x" << std::hex << (register_content&0xFFFFFFE0) << ", sleep for 1 second)");
	      sleep(1);
	    }
	  while((register_content&0xE0)!=0xA0);
	  
	  PRINT_LOG("DONE WITH RECONFIGURATION");
	}
      return 0;
      
    }
    
  } //namespcae daq
} //namespcae ftk


#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("fpga", value< std::string >()->default_value("1"), "The FPGA ID")
    ("epcqaddress", value< std::string >()->default_value("0"), "The start address in the EPCQ")
    ("dontWait", "Don't wait for reconfiguration")
    ;
  
  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  int slot    = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int fpga    = daq::ftk::string_to_int( vm["fpga"].as<std::string>() );
  
  uint epcqaddress  = daq::ftk::string_to_int( vm["epcqaddress"] .as<std::string>() );

  bool dontWait = vm.count("dontWait");
  bool unlocked = daq::ftk::aux_lock_internal_bus(slot);  
  if (unlocked) {
    return -1;
  }
  else {
    return daq::ftk::aux_reconfigure(slot, fpga, epcqaddress, dontWait) ;
  }
}

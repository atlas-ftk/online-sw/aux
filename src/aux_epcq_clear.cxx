/*           Program to issue a routine for read the AUX RAM                   */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */

#include <string>
#include <iostream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "aux/aux_vme_regs.h"
#include "aux/aux_upgrade.h"

namespace daq 
{
  namespace ftk 
  {
    bool aux_epcq_clear( int slot, int fpga )
    { 
      int status;
      u_int ret;
      u_int vmeaddr, slotaddr, fpgaaddr;
      int handle;
      
      
      static VME_MasterMap_t master_map = {0x0, 0x10000000, VME_A32, 0};
      
      slotaddr = ((slot&0x1f) << 27);
      fpgaaddr = ((fpga&0x1f) << 24);
      vmeaddr = slotaddr + fpgaaddr;

      ret = VME_Open();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Open' in function" );
	}
      
      master_map.vmebus_address   = vmeaddr;
      master_map.window_size      = 0x1000000;
      master_map.address_modifier = VME_A32;
      master_map.options          = 0;
      ret = VME_MasterMap(&master_map, &handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterMap' in function"  );
	}

      //
      // WRITE 
      //

      // Reset RU registers
      aux_upgrade_clear_regs(handle);

      // Write the clear command
      status = VME_WriteSafeUInt(handle, TMODE, 0x1);
      CHECK_ERROR(status);
      status = VME_WriteSafeUInt(handle, RU_EPCQ, 0x3);
      CHECK_ERROR(status);
      status = VME_WriteSafeUInt(handle, TMODE, 0x0);
      CHECK_ERROR(status);
      
      // Wait for clear to finish
      aux_upgrade_wait_for_done(handle);

      // Disable remote update again
      aux_upgrade_clear_regs(handle);

      status = VME_WriteSafeUInt(handle, RU_ENABLE, 0);
      CHECK_ERROR(status);

      ret = VME_MasterUnmap(handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterUnMap' in function"  );
	  
	}
      
      ret = VME_Close();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Close' in function"  );
	  
	}
      
      return 0;
      
    }
    
  } //namespcae daq
} //namespcae ftk



#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("fpga", value< std::string >()->default_value("1"), "The FPGA ID")
    ;

  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  int slot    = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int fpga    = daq::ftk::string_to_int( vm["fpga"].as<std::string>() );
  
  return  daq::ftk::aux_epcq_clear(slot, fpga) ;
}

#include "aux/aux.h"
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot.")
    ("fpga", value< std::string >()->default_value("1"), "The FPGA ID.")
    ("tfconstpath", value< std::string >(), "Location of input constants.")
    ("sector", value< std::string >()->default_value("-1"), "Load a sector.")
    ("checksum", bool_switch()->default_value(false),  "Run and print the checksum.")
    ("hex_dump", bool_switch()->default_value(false),  "Dump the checksum to files.")
    ("loadAllSectors", bool_switch()->default_value(false),  "Load all sectors to all procs.")
    ;
  
  positional_options_description p;
  p.add("tfconstpath", 1);

  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  u_int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  u_int fpga = daq::ftk::string_to_int( vm["fpga"].as<std::string>() );
  int sector = daq::ftk::string_to_int( vm["sector"].as<std::string>() );
  std::string tfconstpath  = vm["tfconstpath"].as<std::string>();

  bool checksum = vm["checksum"].as<bool>();
  bool dump = vm["hex_dump"].as<bool>();
  bool loadAllSectors = vm["loadAllSectors"].as<bool>();

  return  daq::ftk::aux_tfconst_load(slot, fpga, tfconstpath, sector, checksum, dump, loadAllSectors) ;
}

#include "aux/aux_inputs.h"
#include "ftkcommon/exceptions.h"

#include "aux/aux_vme_regs.h"

namespace daq {
    namespace ftk {

        using namespace daq::ftk;
        using std::cout; 
        using std::endl;


        input1::input1(uint _slot, std::string _name) : 
            aux_interface(_slot, 1, _name)
        { }

        bool input1::check_link_status(int i)
        {
            bool linksUp = true;
            uint link_status = m_vme->read_word(LINK_STATUS);
            uint slink_status= m_vme->read_word(SLINK_STATUS);
            if(m_source_hits == "df"){
                if((slink_status&0xF) == 0xF){
                    ERS_LOG("Slot "<<m_slot<<" Input1: All QSFP links are up");
                }
                else{
                    ERS_LOG("Slot "<<m_slot<<" Input1: Not all QSFP links are up, Link status: " << std::hex<<std::setfill('0')<<std::setw(1) << (slink_status&0xF) );

                    if(i>=9) {

                        daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(), "Input 1 QSFP", std::to_string(slink_status&0xF)); 
                        throw ex;
                    }
                    else if (i == -1){
                        daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(), "Input 1 QSFP" , std::to_string(slink_status&0xF));
                        throw ex;
                    } 
                    linksUp = false;
                }
            }      

            if((link_status&0xFF) == 0xFF){
                ERS_LOG("Slot "<<m_slot<<" Input1: All SSData links are up");
            }
            else{
                ERS_LOG("Slot "<<m_slot<<" Input1: Not all SSData links are up, Link status: " << std::hex<<std::setfill('0')<<std::setw(2) << (link_status&0xFF) );
                if(i>=9) {
                    daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(), "Input 1 SSData", std::to_string(link_status&0xFF) );
                    throw ex;

                }
                else if (i == -1){
                    daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(), "Input 1 SSData", std::to_string(link_status&0xF));
                    throw ex;
                } 
                linksUp = false;
            }

            if(((link_status>>8)&0xFF) == 0xFF){
                ERS_LOG("Slot "<<m_slot<<" Input1: All Hit links are up");
            }
            else{
                ERS_LOG("Slot "<<m_slot<<" Input1: Not all Hit links are up, Link status: " << std::hex<<std::setfill('0')<<std::setw(2) << ((link_status>>8)&0xFF) );
                if(i>=9) {
                    daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(), "Input 1 Hit", std::to_string((link_status >>8)&0xFF) );
                    throw ex;

                }
                else if (i == -1){
                    daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(),  "Input 1 Hit", std::to_string((link_status >> 8)&0xF));
                    throw ex;
                } 
                linksUp = false;
            }

            return linksUp;
        }

        // void input1::print_freeze_info()
        // {

        // }

        void input1::set_ss_map_path(unsigned int layer, const std::string& s, unsigned int cs)
        {
            m_ss_paths[layer]=s;
            m_ss_cs   [layer]=cs;
        }

        void input1::set_module_path(unsigned int layer, const std::string& s, unsigned int cs)
        {
            m_module_paths[layer]=s;
            m_module_cs   [layer]=cs;
        }

        void input1::load_rams(bool isLoadMode)
        {
            ERS_LOG("Loading Input 1");
            reset_ssmap();
            check_freeze();
            run_ss_check_sums();
            sleep(3); 

            bool reloaded = false;
            for(uint layer=0; layer<4; layer++){
                //check and reload local ssmaps
                try{
                    check_ss_checksum(layer);
                }
                catch(ers::Issue &ex){
                    if(!isLoadMode){
                        daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Input 1 Local SSMap " + std::to_string(layer) + " checksum failed. Not in a configuration partition.", ex);
                        //if(isFatalMode()) throw e;
                        ers::error(e);
                    }
                    else {
                        daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Input 1 Local SSMap " + std::to_string(layer) + " checksum failed. Reloading", ex);
                        reloaded = true;
                        ers::warning(e);
                        load_local_ss_maps(layer);
                        sleep(3); 
                    }
                }

                //check and reload module maps
                try{
                    check_module_checksum(layer);
                }
                catch(ers::Issue &ex){
                    if(!isLoadMode){
                        daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Input 1 Module SSMap " + std::to_string(layer) + " checksum failed. Not in a configuration partition.", ex);
                       // if(isFatalMode()) throw e;
                        ers::error(e);
                    }
                    else {
                        daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Input 1 Module SSMap " + std::to_string(layer) + " checksum failed. Reloading", ex);
                        reloaded = true;
                        ers::warning(e);
                        load_module_maps(layer);
                        sleep(3); 
                    }
                }
            }
            
            ERS_LOG("reloaded? " + std::to_string(reloaded) );
            if(!reloaded){
                ERS_LOG("Input 1 SSMap already loaded");
                check_freeze();
                return;
            }
            else{
                run_ss_check_sums();
                sleep(3); 

                for(uint layer=0; layer<4; layer++){
                    //recheck local ssmap checksums
                    try{
                        check_ss_checksum(layer);
                    }
                    catch(ers::Issue &ex){
                        daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Input 1 Local SSMap " + std::to_string(layer) + " checksum failed after reloading", ex);
                        //if(isFatalMode()) throw e;
                        ers::error(e);
                    }


                    //recheck
                    try{
                        check_module_checksum(layer);
                    }
                    catch(ers::Issue &ex){
                        daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Input 1 Module SSMap " + std::to_string(layer) + " checksum failed after reloading.", ex);
                     //   if(isFatalMode()) throw e;
                        ers::error(e);
                    }
                }
            }
        }



        void input1::set_ignore_freeze(bool ignore) {
            if (ignore) ERS_LOG("Ignore FREEZE in Input2");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(0x10, 16, ignore);
            m_vme->write_word(TMODE, 0x0);
        }

        void input1::set_freeze_mask(u_int mask) {
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_word(FREEZE_MASK_REG, mask);
            m_vme->write_word(TMODE, 0x0);
        }

        void input1::enable_upstream_rx(bool enable)//this function is used to configure when receivers are enabled for data. Hold should be held high when this is enabled
        {
            aux_interface::enable_upstream_rx(enable);
            m_vme->write_word(TMODE, 0x1);
            // QSFP
            m_vme->write_mask(DISABLE_DATA,(0xF*!enable)<<BIT_I1_QSFP_DISABLE_DATA, 0xF<<BIT_I1_QSFP_DISABLE_DATA);
            m_vme->write_word(TMODE, 0x0);      
        }

        void input1::set_upstream_flow(bool enable)//this function should be used to configure hold force. Enable = True for prepare for run, enable = false in stop and configure
        {
            aux_interface::set_upstream_flow(enable);

            m_vme->write_word(TMODE, 0x1);
            // QSFP
            //m_vme->write_mask(DISABLE_DATA,(0xF*!enable)<<BIT_I1_QSFP_DISABLE_DATA, 0xF<<BIT_I1_QSFP_DISABLE_DATA);
            m_vme->write_mask(HOLD_CONTROL,(0xF*!enable)<<BIT_I1_QSFP_FORCE_HOLD,   0xF<<BIT_I1_QSFP_FORCE_HOLD);
            m_vme->write_word(TMODE, 0x0);
        }

        void input1::set_downstream_flow(bool enable)
        {
            aux_interface::set_downstream_flow(enable);


            // Hits to procs
            hold_ignore_proc();

            //
            // AMB
            hold_ignore_amb();

            //m_vme->write_word(TMODE, 0x1);
            //m_vme->write_mask(HOLD_CONTROL,(0xFF*!enable)<<BIT_I1_AMB_IGNORE_HOLD , 0xFF<<BIT_I1_AMB_IGNORE_HOLD);
            //m_vme->write_mask(DISABLE_DATA,(0xFF*!enable)<<BIT_I1_AMB_DISABLE_DATA, 0xFF<<BIT_I1_AMB_DISABLE_DATA);
            //m_vme->write_word(TMODE, 0x0);
        }

        void input1::set_altera_to_altera() {

            m_vme->write_word(TMODE, 0x1);
            m_vme->write_word(SET_ALTERA_REG, SET_ALTERA_WORD);
            m_vme->write_word(TMODE, 0x0);

        }

        void input1::reset_hit_fifos()
        {
            ERS_LOG("reset_hit_fifos");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(INPUT_RESET_HIT_FIFO_REG, INPUT_RESET_HIT_FIFO_BIT, true);
            m_vme->write_bit(INPUT_RESET_HIT_FIFO_REG, INPUT_RESET_HIT_FIFO_BIT, false);
            m_vme->write_word(TMODE, 0x0);
        }

        void input1::load_hit_fifos(const std::string& path)
        {
            try{
                ERS_LOG("load_hit_fifos from: " << path);
                aux_load_tx_fifo(m_slot, 1, I1_HIT_FIFO_REG0, path + "_plane_0.txt");
                aux_load_tx_fifo(m_slot, 1, I1_HIT_FIFO_REG1, path + "_plane_1.txt");
                aux_load_tx_fifo(m_slot, 1, I1_HIT_FIFO_REG2, path + "_plane_2.txt");
                aux_load_tx_fifo(m_slot, 1, I1_HIT_FIFO_REG3, path + "_plane_3.txt");
            }
            catch(ers::Issue &ex){
                daq::ftk::IOError e(ERS_HERE, name_ftk(), "Unable to open TV File", ex);
                throw e;
            }
        }

        void input1::send_hit_fifos_pseudo_df(bool loop) { 

            m_vme->write_word(TMODE, 0x1);
            // if (loop) m_vme->write_word(0x30, 0x22);
            // else      m_vme->write_word(0x30, 0x02); // no loop,
            m_vme->write_bit(INPUT_SEND_PSEUDO_DF_FIFO_DATA_REG, INPUT_SEND_PSEUDO_DF_FIFO_DATA_BIT, true); 
            m_vme->write_bit(INPUT_SEND_PSEUDO_DF_FIFO_DATA_REG, INPUT_SEND_PSEUDO_DF_FIFO_LOOP_BIT, loop); 
            m_vme->write_word(TMODE, 0x0);

        }

        void input1::send_hit_fifos(bool loop)
        {
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(INPUT_SEND_FIFO_REG, INPUT_SEND_HITS_FIFO_LOOP_BIT,  loop);
            m_vme->write_bit(INPUT_SEND_FIFO_REG, INPUT_SEND_HITS_AUX_LOGIC_BIT,  true);
            m_vme->write_word(TMODE, 0x0);
        }


        void input1::send_hit_and_ssid_fifos(bool loop)
        { 

            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(INPUT_SEND_FIFO_REG, INPUT_SEND_HITS_AND_SSIDS_FIFO_LOOP_BIT,  loop);
            m_vme->write_bit(INPUT_SEND_FIFO_REG, INPUT_SEND_HITS_AND_SSIDS_AUX_LOGIC_BIT,  true);
            m_vme->write_word(TMODE, 0x0);

        }


        void input1::stop_hit_fifos() { 

            m_vme->write_word(TMODE, 0x1);
            m_vme->write_word(INPUT_SEND_FIFO_REG, 0x0);
            m_vme->write_word(TMODE, 0x0);

        }

        void input1::reset_slink() {
            ERS_LOG("Reset SLINK");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_word(RESETS, 0xf<<24);
            m_vme->write_word(TMODE, 0x0);
        }

        void input1::reset_slink_fifos() {
            ERS_LOG("Reset SLINK FIFOs");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_word(RESETS, 0x1<<29);
            m_vme->write_word(RESETS, 0x1<<30);
            m_vme->write_word(RESETS, 0x1<<31);
            m_vme->write_word(TMODE, 0x0);
        }

        void input1::reset_ssmap() {
            ERS_LOG("I1: reset SSMap");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(INPUT_RESET_SSMAP_REG, BIT_INPUT_SS_RESET,true);
            m_vme->write_bit(INPUT_RESET_SSMAP_REG, BIT_INPUT_SS_RESET,false);
            m_vme->write_word(TMODE, 0x0);
        }

        void input1::reset_logic()
        {
            aux_interface::reset_logic();
            reset_slink_fifos();
            reset_ssmap();
        }

        void input1::enable_hitsort(bool enable) {
            if (enable) ERS_LOG("I1: enable HitSort");
            else        ERS_LOG("I1: HitSort Disabled");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(0x10, 5, enable);
            m_vme->write_word(TMODE, 0x0);
        }

        void input1::enable_proc_to_input_freeze(bool enable)
        {
            if(enable) { ERS_LOG("I1: Enable freeze from procs to inputs on Internal AUXbus"); }
            else { ERS_LOG("I1: Disable freeze from procs to inputs on Internal AUXbus"); }
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_mask(0x10,(0xF0000000*enable), 0xF0000000);
            m_vme->write_word(TMODE, 0x0);
        }

        void input1::set_hold_ignore_amb(bool ignore)
        {
            m_holdIgnoreAMB = ignore;
        }

        void input1::hold_ignore_amb()
        {
            if(m_holdIgnoreAMB) { ERS_LOG("I1: Disable AMB Holds"); }
            else { ERS_LOG("I1: Enable AMB Holds"); }
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_mask(HOLD_CONTROL,(0xFF*m_holdIgnoreAMB)<<BIT_I1_AMB_IGNORE_HOLD , 0xFF<<BIT_I1_AMB_IGNORE_HOLD);
            m_vme->write_word(TMODE, 0x0);
        }

        void input1::set_hold_ignore_proc(bool ignore)
        {
            m_holdIgnoreProc = ignore;
        }

        void input1::hold_ignore_proc()
        {
            if(m_holdIgnoreProc) { ERS_LOG("I1: Disable Hit Holds"); }
            else { ERS_LOG("I1: Enable Hit Holds"); }
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_word(HIT_HOLD_CONTROL,0xFFFFFFFF*m_holdIgnoreProc);
            m_vme->write_word(TMODE, 0x0);
        }

        void input1::load_local_ss_maps(uint layer)
        {
            static const uint addresses[4]={I1_SS_MAP_FIFO_REG0, I1_SS_MAP_FIFO_REG1, I1_SS_MAP_FIFO_REG2, I1_SS_MAP_FIFO_REG3};

            if(!m_ss_paths[layer].empty()) aux_ssmap_load(m_slot, 1, addresses[layer], m_ss_paths[layer], false);
        }

        void input1::load_module_maps(uint layer)
        {
            static const uint addresses[4]={I1_SS_MAP_FIFO_REG0, I1_SS_MAP_FIFO_REG1, I1_SS_MAP_FIFO_REG2, I1_SS_MAP_FIFO_REG3};

            if(!m_module_paths[layer].empty()) aux_ssmap_load(m_slot, 1, addresses[layer], m_module_paths[layer], true);
        }

        void input1::load_hit_and_ssid_fifos(const std::string& path)
        {
            try{
                aux_load_tx_fifo(m_slot, 1, I1_HIT_SS_FIFO_REG4,  path + "_TXInput_4.txt");
                aux_load_tx_fifo(m_slot, 1, I1_HIT_SS_FIFO_REG5,  path + "_TXInput_5.txt");
                aux_load_tx_fifo(m_slot, 1, I1_HIT_SS_FIFO_REG6,  path + "_TXInput_6.txt");
                aux_load_tx_fifo(m_slot, 1, I1_HIT_SS_FIFO_REG7,  path + "_TXInput_7.txt");
                aux_load_tx_fifo(m_slot, 1, I1_HIT_SS_FIFO_REG8,  path + "_TXInput_8.txt");
                aux_load_tx_fifo(m_slot, 1, I1_HIT_SS_FIFO_REG9,  path + "_TXInput_9.txt");
                aux_load_tx_fifo(m_slot, 1, I1_HIT_SS_FIFO_REG10, path + "_TXInput_10.txt");
                aux_load_tx_fifo(m_slot, 1, I1_HIT_SS_FIFO_REG11, path + "_TXInput_11.txt");
            }
            catch(ers::Issue &ex){
                daq::ftk::IOError e(ERS_HERE, name_ftk(), "Unable to open TV File", ex);
                throw e;
            }
        }


        void input1::load_ssid_fifos(const std::string& path)
        {
            try{
                aux_load_tx_fifo(m_slot, 1, I1_SS_FIFO_REG4,  path + "_AMTXInput_4.txt");
                aux_load_tx_fifo(m_slot, 1, I1_SS_FIFO_REG5,  path + "_AMTXInput_5.txt");
                aux_load_tx_fifo(m_slot, 1, I1_SS_FIFO_REG6,  path + "_AMTXInput_6.txt");
                aux_load_tx_fifo(m_slot, 1, I1_SS_FIFO_REG7,  path + "_AMTXInput_7.txt");
                aux_load_tx_fifo(m_slot, 1, I1_SS_FIFO_REG8,  path + "_AMTXInput_8.txt");
                aux_load_tx_fifo(m_slot, 1, I1_SS_FIFO_REG9,  path + "_AMTXInput_9.txt");
                aux_load_tx_fifo(m_slot, 1, I1_SS_FIFO_REG10, path + "_AMTXInput_10.txt");
                aux_load_tx_fifo(m_slot, 1, I1_SS_FIFO_REG11, path + "_AMTXInput_11.txt");
            }
            catch(ers::Issue &ex){
                daq::ftk::IOError e(ERS_HERE, name_ftk(), "Unable to open TV File", ex);
                throw e;
            }
        }

        void input1::set_checksumWarning(bool warningOnly)
        {
            m_checksumWarning = warningOnly;
        }

        void input1::check_freeze()
        {
            aux_interface::check_freeze();
        }

        void input1::run_ss_check_sums()
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(),"Calculate SS checksums Input 1");
            m_vme->write_word(TMODE, 0x1);

            m_vme->write_bit(INPUT_RUN_SS_CHECKSUM_REG, BIT_INPUT_SS_CHECKSUM_RESET, true);
            m_vme->write_bit(INPUT_RUN_SS_CHECKSUM_REG, BIT_INPUT_SS_CHECKSUM_RESET, false);

            m_vme->write_bit(INPUT_RUN_SS_CHECKSUM_REG, BIT_INPUT_SS_CHECKSUM_MUX,   true);
            m_vme->write_bit(INPUT_RUN_SS_CHECKSUM_REG, BIT_INPUT_SS_CHECKSUM_START, true);
            sleep(3); // seconds
            m_vme->write_bit(INPUT_RUN_SS_CHECKSUM_REG, BIT_INPUT_SS_CHECKSUM_MUX,   false);
            m_vme->write_bit(INPUT_RUN_SS_CHECKSUM_REG, BIT_INPUT_SS_CHECKSUM_START, false);

            m_vme->write_word(TMODE, 0x0);
            
            tLog.end(ERS_HERE);  // End log conter
        }

        void input1::check_ss_checksum(uint layer){
            
            static const uint ss_checksum_addresses[4]={I1_SSID_CHECKSUM0_REG, I1_SSID_CHECKSUM1_REG, I1_SSID_CHECKSUM2_REG, I1_SSID_CHECKSUM3_REG};
            uint ss_cs = m_vme->read_word(ss_checksum_addresses[layer]);

            if(ss_cs!=m_ss_cs[layer])
            {
                std::stringstream cppstringssuck;
                cppstringssuck << "SSMap Local RAM " << layer;
                std::string  ramname  = cppstringssuck.str();
                unsigned int expected = m_ss_cs[layer];
                unsigned int got      = ss_cs;
                
                ERS_LOG("Slot " << m_slot << " SSMap Local RAM " << layer << " Input 1 checksum failed. Expected =" << std::hex<<std::setfill('0')<<std::setw(8)<<m_ss_cs[layer]<<", got = "<<std::setw(8)<<ss_cs);
                daq::ftk::FTKWrongChecksum ex(ERS_HERE, name_ftk(), expected, got, "1", ramname);
                throw ex;
            }
            else
                ERS_LOG("Slot " << m_slot << " SSMap Local RAM " << layer << " Input 1 checksum passes. Expected =" << std::hex<<std::setfill('0')<<std::setw(8)<<m_ss_cs[layer]<<", got = "<<std::setw(8)<<ss_cs);
        }


        void input1::check_module_checksum(uint layer){
            
            static const uint module_checksum_addresses[4]={I1_MODULE_CHECKSUM0_REG, I1_MODULE_CHECKSUM1_REG, I1_MODULE_CHECKSUM2_REG, I1_MODULE_CHECKSUM3_REG};
            uint ss_cs = m_vme->read_word(module_checksum_addresses[layer]);

            if( ss_cs!=m_module_cs[layer])
            {
                std::stringstream cppstringssuck;
                cppstringssuck << "SSMap Module RAM " << layer;
                std::string  ramname  = cppstringssuck.str();
                unsigned int expected = m_module_cs[layer];
                unsigned int got      = ss_cs;
                
                ERS_LOG("Slot " << m_slot << " SSMap Local RAM " << layer << " Input 1 checksum fails. Expected =" << std::hex<<std::setfill('0')<<std::setw(8)<<m_module_cs[layer]<<", got = "<<std::setw(8)<<ss_cs);
                daq::ftk::FTKWrongChecksum ex(ERS_HERE, name_ftk(), expected, got, "1", ramname);
                throw ex;
            }
            else
                ERS_LOG("Slot " << m_slot << " SSMap Local RAM " << layer << " Input 1 checksum passes. Expected =" << std::hex<<std::setfill('0')<<std::setw(8)<<m_module_cs[layer]<<", got = "<<std::setw(8)<<ss_cs);
        }



        input2::input2(unsigned int _slot, std::string _name) :
            aux_interface(_slot, 2, _name),
            m_source_sfp("tracks")
        { }

        bool input2::CheckAUXbusLockCode()
        {
            unsigned int lockRegisterContent;
            lockRegisterContent = m_vme->read_word(0x3d0);
            if (lockRegisterContent == 0xFEEDC0DE) {
                return 1; //safe to let procs drive InternalAUXbus(7 downto 4)
            }
            else {
                return 0;
            }
        }

        void input2::set_ss_map_path(uint layer, const std::string& s, uint cs)
        {
            m_ss_paths[layer-4]=s;
            m_ss_cs   [layer-4]=cs;
        }

        void input2::set_module_path(uint layer, const std::string& s, uint cs)
        {
            m_module_paths[layer-4]=s;
            m_module_cs   [layer-4]=cs;
        }

        // Run Control //
        void input2::configure()
        {
            aux_interface::configure();

            ERS_LOG("Setting QSFP Mux to " << m_source_sfp);
            m_vme->write_word(TMODE, 0x1);
            if(m_source_sfp=="tracks")
                m_vme->write_mask(0x30,0<<16,7<<16);
            else if(m_source_sfp=="qsfp0")
                m_vme->write_mask(0x30,1<<16,7<<16);
            else if(m_source_sfp=="qsfp1")
                m_vme->write_mask(0x30,2<<16,7<<16);
            else if(m_source_sfp=="qsfp2")
                m_vme->write_mask(0x30,3<<16,7<<16);
            else if(m_source_sfp=="qsfp3")
                m_vme->write_mask(0x30,4<<16,7<<16);
            m_vme->write_word(TMODE, 0x0);
        }

        // Helper //

        bool input2::check_link_status(int i)
        {
            bool linksUp = true;
            uint link_status = m_vme->read_word(LINK_STATUS);
            uint slink_status= m_vme->read_word(SLINK_STATUS);

            if(m_source_hits == "df"){
                if((slink_status&0xF) == 0xF){
                    ERS_LOG("Slot "<<m_slot<<" Input2: All QSFP links are up");
                }
                else{
                    ERS_LOG("Slot "<<m_slot<<" Input2: Not all QSFP links are up, Link status: " << std::hex<<std::setfill('0')<<std::setw(1) << (slink_status&0xF) );
                    if(i>=9) {
                        daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(), "Input 2 QSFP", std::to_string(slink_status&0xF) );
                        throw ex;

                    }
                    else if (i == -1){
                        daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(), "Input 2 QSFP", std::to_string(slink_status&0xF));
                        throw ex;
                    } 
                    linksUp = false;
                }      
            }

            if((link_status&0xF) == 0xF){
                ERS_LOG("Slot "<<m_slot<<" Input2: All SSData links are up");
            }
            else{
                ERS_LOG("Slot "<<m_slot<<" Input2: Not all SSData links are up, Link status: " << std::hex<<std::setfill('0')<<std::setw(1) << (link_status&0xF) );
                if(i>=9) {
                    daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(), "Input 2 SSData",  std::to_string(link_status&0xF) );
                    throw ex;

                }
                else if (i == -1){
                    daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(), "Input 2 SSData", std::to_string(link_status&0xF));
                    throw ex;
                } 
                linksUp = false;
            }

            if(((link_status>>8)&0xF) == 0xF){
                ERS_LOG("Slot "<<m_slot<<" Input2: All Hit links are up");
            }
            else{
                ERS_LOG("Slot "<<m_slot<<" Input2: Not all Hit links are up, Link status: " << std::hex<<std::setfill('0')<<std::setw(1) << ((link_status>>8)&0xF) );
                if(i>=9) {
                    daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(), "Input 2 Hit",  std::to_string((link_status>>8)&0xF) );
                    throw ex;

                }
                else if (i == -1){
                    daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(), "Input 2 Hit", std::to_string((link_status>>8)&0xF));
                    throw ex;
                } 
                linksUp = false;
            }

            if(((link_status>>20)&0xF) == 0xF){
                ERS_LOG("Slot "<<m_slot<<" Input2: All Track links from procs are up");
            }
            else{
                ERS_LOG("Slot "<<m_slot<<" Input2: Not Track links from procs links are up, Link status: " << std::hex<<std::setfill('0')<<std::setw(1) << ((link_status>>20)&0xF) );
                if(i>=9) {
                    //daq::ftk::FTKIssue ex(ERS_HERE, "Slot " + std::to_string(m_slot) + " Input2: Not all Track links from procs are up. Link status: "  + std::to_string((link_status>>20)&0xF) );
                    //ers::error(ex);
                    daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(), "Input 2 Track", std::to_string((link_status>>20)&0xF) );
                    throw ex;

                }
                linksUp = false;
            }

            return linksUp;
        }


        bool input2::check_sfp_status(uint i){

            bool linksUp = true;
            uint slink_status= m_vme->read_word(SLINK_STATUS);


            if(((slink_status>>4)&1) == 1){
                ERS_LOG("Slot "<<m_slot<<" Input2: SFP link is up");
            }
            else{
                ERS_LOG("Slot "<<m_slot<<" Input2: SFP link is not up, Link status: " << std::hex<<std::setfill('0')<<std::setw(1) << std::bitset<32>(slink_status)) ;
                if(i>=9) {
                    daq::ftk::FTKLinkDown ex(ERS_HERE, name_ftk(), "Input 2 SFP",  std::to_string(slink_status&0x1F) );
                    throw ex;
                }
                linksUp = false;
            }

            return linksUp;
        }

        void input2::set_source_sfp(const std::string& s)
        {
            m_source_sfp = s;
        }

        void input2::load_rams(bool isLoadMode)
        {
            ERS_LOG("Loading Input 2");
            reset_ssmap();
            check_freeze();
            run_ss_check_sums();

            bool reloaded = false;
            for(uint layer=0; layer<4; layer++){
                //check and reload local ssmaps
                try{
                    check_ss_checksum(layer);
                }
                catch(ers::Issue &ex){
                    if(!isLoadMode){
                        daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Input 2 Local SSMap " + std::to_string(layer) + " checksum failed. Not in a configuration partition.", ex);
                        //if(isFatalMode()) throw e;
                        ers::error(e);
                    }
                    else {
                        daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Input 2 Local SSMap " + std::to_string(layer) + " checksum failed. Reloading", ex);
                        reloaded = true;
                        ers::warning(e);
                        load_local_ss_maps(layer);
                    }
                }

                //check and reload module ssmaps 
                try{
                    check_module_checksum(layer);
                }
                catch(ers::Issue &ex){
                    if(!isLoadMode){
                        daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Input 2 Module SSMap " + std::to_string(layer) + " checksum failed. Not in a configuration partition.", ex);
                        //if(isFatalMode()) throw e;
                        ers::error(e);
                    }
                    else {
                        daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Input 2 Module SSMap " + std::to_string(layer) + " checksum failed. Reloading", ex);
                        reloaded = true;
                        ers::warning(e);
                        load_module_maps(layer);
                    }
                }
            }


            ERS_LOG("reloaded? " + std::to_string(reloaded) );
            if(!reloaded){
                ERS_LOG("Input2 SSMap already loaded");
                check_freeze();
                return;
            }
            else{
                run_ss_check_sums();

                for(uint layer=0; layer<4; layer++){
                    //recheck local ssmap checksums
                    try{
                        check_ss_checksum(layer);
                    }
                    catch(ers::Issue &ex){
                        daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Input 2 Module SSMap " + std::to_string(layer) + " checksum failed after reloading.", ex);
                        //if(isFatalMode()) throw e;
                        ers::error(e);
                    }


                    //recheck
                    try{
                        check_module_checksum(layer);
                    }
                    catch(ers::Issue &ex){
                        daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Input 2 Module SSMap " + std::to_string(layer) + " checksum failed after reloading.", ex);
                        //if(isFatalMode()) throw e;
                        ers::error(e);
                    }
                }
            }

        }

        void input2::set_ignore_freeze(bool ignore) {
            if (ignore) ERS_LOG("Ignore FREEZE in Input2");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(0x10, 16, ignore);
            m_vme->write_word(TMODE, 0x0);
        }

        void input2::set_freeze_mask(u_int mask, u_int HWmask) {
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_word(FREEZE_MASK_REG, mask);
            m_vme->write_word(FREEZE_MASK_HW_REG, HWmask);
            // Write masks to regs
            m_vme->write_word(TMODE, 0x0);
        }

        void input2::set_ignore_SSB_freeze(bool ignore) {
            if (ignore) ERS_LOG("Ignore FREEZE from SSB in Input2");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(0x10, BIT_I2_SSB_IGNORE_FREEZE, ignore);
            m_vme->write_word(TMODE, 0x0);
        }


        void input2::enable_upstream_rx(bool enable)//this function is used to configure when receivers are enabled for data. Hold should be held high when this is enabled
        {
            aux_interface::enable_upstream_rx(enable);
            m_vme->write_word(TMODE, 0x1);
            // QSFP
            m_vme->write_mask(DISABLE_DATA,(0xF*!enable)<<BIT_I2_QSFP_DISABLE_DATA, 0xF<<BIT_I2_QSFP_DISABLE_DATA);
            m_vme->write_word(TMODE, 0x0);      
        }

        void input2::set_upstream_flow(bool enable)
        {
            aux_interface::set_upstream_flow(enable);

            m_vme->write_word(TMODE, 0x1);
            // QSFP
            //m_vme->write_mask(DISABLE_DATA,(0xF*!enable)<<BIT_I2_QSFP_DISABLE_DATA, 0xF<<BIT_I2_QSFP_DISABLE_DATA);
            m_vme->write_mask(HOLD_CONTROL,(0xF*!enable)<<BIT_I2_QSFP_FORCE_HOLD,   0xF<<BIT_I2_QSFP_FORCE_HOLD);
            m_vme->write_word(TMODE, 0x0);
        }

        void input2::set_downstream_flow(bool enable)
        {
            aux_interface::set_downstream_flow(enable);

            // Hits to procs
            hold_ignore_proc();

            // Hits to procs
            // hold_ignore_proc();

            //
            // SFP
            hold_ignore_ssb();
            // m_vme->write_word(TMODE, 0x1);
            // m_vme->write_bit(HIT_HOLD_CONTROL,BIT_I2_SFP_IGNORE_HOLD ,!enable);
            // m_vme->write_word(TMODE, 0x0);
            //m_vme->write_bit(HIT_DISABLE_DATA,BIT_I2_SFP_DISABLE_DATA,!enable);

            //
            // AMB
            hold_ignore_amb();

            //m_vme->write_word(TMODE, 0x1);
            //m_vme->write_mask(HOLD_CONTROL,(0xF*!enable)<<BIT_I2_AMB_IGNORE_HOLD , 0xF<<BIT_I2_AMB_IGNORE_HOLD);
            //m_vme->write_mask(DISABLE_DATA,(0xF*!enable)<<BIT_I2_AMB_DISABLE_DATA, 0xF<<BIT_I2_AMB_DISABLE_DATA);
            //m_vme->write_word(TMODE, 0x0);
        }


        void input2::set_altera_to_altera()
        {
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_word(SET_ALTERA_REG, SET_ALTERA_WORD);
            m_vme->write_word(TMODE, 0x0);
        }

        void input2::reset_slink() {
            ERS_LOG("Reset SLINK");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_word(RESETS, 0xf<<24);
            m_vme->write_word(TMODE, 0x0);
        }

        void input2::reset_slink_fifos() {
            ERS_LOG("Reset SLINK FIFOs");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_word(RESETS, 0x1<<23);// SFP
            m_vme->write_word(RESETS, 0x1<<29);//QSFP
            m_vme->write_word(RESETS, 0x1<<30);// hit
            m_vme->write_word(RESETS, 0x1<<31);//SSID
            m_vme->write_word(TMODE, 0x0);
        }

        void input2::reset_ssmap()
        {
            ERS_LOG("I2: reset SSMap");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(INPUT_RESET_SSMAP_REG, BIT_INPUT_SS_RESET,true);
            m_vme->write_bit(INPUT_RESET_SSMAP_REG, BIT_INPUT_SS_RESET,false);
            m_vme->write_word(TMODE, 0x0);
        }

        void input2::reset_hit_warrior()
        {
            ERS_LOG("I2: reset SSMap");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(RESETS, BIT_HW_RESET,true);
            m_vme->write_bit(RESETS, BIT_HW_RESET,false);
            m_vme->write_word(TMODE, 0x0);
        }

        void input2::reset_logic()
        {
            aux_interface::reset_logic();
            reset_slink_fifos();
            reset_ssmap();
            reset_hit_warrior();
        }

        void input2::set_hold_ignore_proc(bool ignore)
        {
            m_holdIgnoreProc = ignore;
        }

        void input2::hold_ignore_proc()
        {
            if(m_holdIgnoreProc) { ERS_LOG("I2: Disable Hit Holds"); }
            else { ERS_LOG("I2: Enable Hit Holds"); }
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_mask(HIT_HOLD_CONTROL,0xFFFF*m_holdIgnoreProc, 0xFFFF);
            m_vme->write_word(TMODE, 0x0);
        }

        void input2::set_force_hold_proc_out(bool force)
        {
            m_forceHoldProcOut = force;
        }

        void input2::force_hold_proc_out()
        {
            if(!m_forceHoldProcOut) { ERS_LOG("I2: Don't force hold to proc output"); }
            else { ERS_LOG("I2: FORCE HOLD to proc output"); }
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(HOLD_CONTROL,24,m_forceHoldProcOut);
            m_vme->write_bit(HOLD_CONTROL,25,m_forceHoldProcOut);
            m_vme->write_bit(HOLD_CONTROL,26,m_forceHoldProcOut);
            m_vme->write_bit(HOLD_CONTROL,27,m_forceHoldProcOut);
            m_vme->write_word(TMODE, 0x0);
        }
        void input2::enable_proc_to_input_freeze(bool enable)
        {
            if(enable) { ERS_LOG("I2: Enable freeze from procs to inputs on Internal AUXbus"); }
            else { ERS_LOG("I2: Disable freeze from procs to inputs on Internal AUXbus"); }
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_mask(0x10,(0xF0000000*enable), 0xF0000000);
            m_vme->write_word(TMODE, 0x0);
        }

        void input2::set_hold_ignore_amb(bool ignore)
        {
            m_holdIgnoreAMB = ignore;
        }

        void input2::hold_ignore_amb()
        {
            if(m_holdIgnoreAMB) { ERS_LOG("I2: Disable AMB Holds"); }
            else { ERS_LOG("I2: Enable AMB Holds"); }
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_mask(HOLD_CONTROL,(0xF*m_holdIgnoreAMB)<<BIT_I2_AMB_IGNORE_HOLD , 0xF<<BIT_I2_AMB_IGNORE_HOLD);
            m_vme->write_word(TMODE, 0x0);
        }

        void input2::set_hold_ignore_ssb(bool ignore)
        {
            m_holdIgnoreSSB = ignore;
        }

        void input2::hold_ignore_ssb()
        {
            if(m_holdIgnoreSSB) { ERS_LOG("I2: Disable SSB Holds"); }
            else { ERS_LOG("I2: Enable SSB Holds"); }
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(HIT_HOLD_CONTROL,BIT_I2_SFP_IGNORE_HOLD ,m_holdIgnoreSSB);
            m_vme->write_word(TMODE, 0x0);
        }

        void input2::load_local_ss_maps(uint layer)
        {
            static const uint ss_addresses[4]={I2_SS_MAP_FIFO_REG4, I2_SS_MAP_FIFO_REG5, I2_SS_MAP_FIFO_REG6, I2_SS_MAP_FIFO_REG7}; //couldnt figure out how to put this in the class properly
            if(!m_ss_paths[layer].empty()) aux_ssmap_load(m_slot, 2, ss_addresses[layer], m_ss_paths[layer], false);
        }


        void input2::load_module_maps(uint layer)
        {
    
            static const uint module_addresses[4]={I2_SS_MAP_FIFO_REG4, I2_SS_MAP_FIFO_REG5, I2_SS_MAP_FIFO_REG6, I2_SS_MAP_FIFO_REG7};
            if(!m_module_paths[layer].empty()) aux_ssmap_load(m_slot, 2, module_addresses[layer], m_module_paths[layer], true);
        }

        void input2::reset_hit_fifos()
        {
            ERS_LOG("reset_hit_fifos");
            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(INPUT_RESET_HIT_FIFO_REG, INPUT_RESET_HIT_FIFO_BIT, true);
            m_vme->write_bit(INPUT_RESET_HIT_FIFO_REG, INPUT_RESET_HIT_FIFO_BIT, false);
            m_vme->write_word(TMODE, 0x0);
        }

        void input2::load_hit_fifos(const std::string& path)
        {
            try{
                ERS_LOG("load_hit_fifos from: " << path);
                aux_load_tx_fifo(m_slot, 2, I2_HIT_FIFO_REG4, path + "_plane_4.txt");
                aux_load_tx_fifo(m_slot, 2, I2_HIT_FIFO_REG5, path + "_plane_5.txt");
                aux_load_tx_fifo(m_slot, 2, I2_HIT_FIFO_REG6, path + "_plane_6.txt");
                aux_load_tx_fifo(m_slot, 2, I2_HIT_FIFO_REG7, path + "_plane_7.txt");
            }
            catch(ers::Issue &ex){
                daq::ftk::IOError e(ERS_HERE, name_ftk(), "Unable to open TV File", ex);
                throw e;
            }
        }

        void input2::load_hit_and_ssid_fifos(const std::string& path)
        {
            try{
                aux_load_tx_fifo(m_slot, 2, I2_HIT_SS_FIFO_REG0, path + "_TXInput_0.txt");
                aux_load_tx_fifo(m_slot, 2, I2_HIT_SS_FIFO_REG1, path + "_TXInput_1.txt");
                aux_load_tx_fifo(m_slot, 2, I2_HIT_SS_FIFO_REG2, path + "_TXInput_2.txt");
                aux_load_tx_fifo(m_slot, 2, I2_HIT_SS_FIFO_REG3, path + "_TXInput_3.txt");
            }
            catch(ers::Issue &ex){
                daq::ftk::IOError e(ERS_HERE, name_ftk(), "Unable to open TV File", ex);
                throw e;
            }
        }

        void input2::load_ssid_fifos(const std::string& path)
        {
            try{
                aux_load_tx_fifo(m_slot, 2, I2_SS_FIFO_REG0, path + "_AMTXInput_0.txt");
                aux_load_tx_fifo(m_slot, 2, I2_SS_FIFO_REG1, path + "_AMTXInput_1.txt");
                aux_load_tx_fifo(m_slot, 2, I2_SS_FIFO_REG2, path + "_AMTXInput_2.txt");
                aux_load_tx_fifo(m_slot, 2, I2_SS_FIFO_REG3, path + "_AMTXInput_3.txt");
            }
            catch(ers::Issue &ex){
                daq::ftk::IOError e(ERS_HERE, name_ftk(), "Unable to open TV File", ex);
                throw e;
            }
        }


        void input2::send_hit_fifos_pseudo_df(bool loop) { 

            m_vme->write_word(TMODE, 0x1);
            // if (loop) m_vme->write_word(0x30, 0x22);
            // else      m_vme->write_word(0x30, 0x02); // no loop, 
            m_vme->write_bit(INPUT_SEND_PSEUDO_DF_FIFO_DATA_REG, INPUT_SEND_PSEUDO_DF_FIFO_DATA_BIT, true); 
            m_vme->write_bit(INPUT_SEND_PSEUDO_DF_FIFO_DATA_REG, INPUT_SEND_PSEUDO_DF_FIFO_LOOP_BIT, loop); 
            m_vme->write_word(TMODE, 0x0);

        }


        void input2::send_hit_fifos(bool loop)
        { 

            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(INPUT_SEND_FIFO_REG, INPUT_SEND_HITS_FIFO_LOOP_BIT, loop);
            m_vme->write_bit(INPUT_SEND_FIFO_REG, INPUT_SEND_HITS_AUX_LOGIC_BIT, true);
            m_vme->write_word(TMODE, 0x0);

        }


        void input2::send_hit_and_ssid_fifos(bool loop) { 

            m_vme->write_word(TMODE, 0x1);
            m_vme->write_bit(INPUT_SEND_FIFO_REG, INPUT_SEND_HITS_AND_SSIDS_FIFO_LOOP_BIT,  loop);
            m_vme->write_bit(INPUT_SEND_FIFO_REG, INPUT_SEND_HITS_AND_SSIDS_AUX_LOGIC_BIT,  true);
            m_vme->write_word(TMODE, 0x0);

        }


        void input2::stop_hit_fifos() { 

            m_vme->write_word(TMODE, 0x1);
            m_vme->write_word(INPUT_SEND_FIFO_REG, 0x0);
            m_vme->write_word(TMODE, 0x0);

        }

        void input2::set_checksumWarning(bool warningOnly)
        {
            m_checksumWarning = warningOnly;
        }

        void input2::check_freeze()
        {
            aux_interface::check_freeze();
        }

        void input2::run_ss_check_sums() {
            ftkTimeLog tLog(ERS_HERE, name_ftk(),"Calculate SS checksums Input 2");

            m_vme->write_word(TMODE, 0x1);

            m_vme->write_bit(INPUT_RUN_SS_CHECKSUM_REG, BIT_INPUT_SS_CHECKSUM_RESET, true);
            m_vme->write_bit(INPUT_RUN_SS_CHECKSUM_REG, BIT_INPUT_SS_CHECKSUM_RESET, false);
            sleep(1);
            m_vme->write_bit(INPUT_RUN_SS_CHECKSUM_REG, BIT_INPUT_SS_CHECKSUM_MUX,   true);
            m_vme->write_bit(INPUT_RUN_SS_CHECKSUM_REG, BIT_INPUT_SS_CHECKSUM_START, true);
            sleep(2); // seconds
            m_vme->write_bit(INPUT_RUN_SS_CHECKSUM_REG, BIT_INPUT_SS_CHECKSUM_MUX,   false);
            m_vme->write_bit(INPUT_RUN_SS_CHECKSUM_REG, BIT_INPUT_SS_CHECKSUM_START, false);

            m_vme->write_word(TMODE, 0x0);
            
            tLog.end(ERS_HERE);  // End log conter
        }

        void input2::check_ss_checksum(uint layer){
            
            static const uint ss_checksum_addresses[4]={I2_SSID_CHECKSUM4_REG, I2_SSID_CHECKSUM5_REG, I2_SSID_CHECKSUM6_REG, I2_SSID_CHECKSUM7_REG};
            uint ss_cs = m_vme->read_word(ss_checksum_addresses[layer]);

            if(ss_cs!=m_ss_cs[layer])
            {
                std::stringstream cppstringssuck;
                cppstringssuck << "SSMap Local RAM " << layer;
                std::string  ramname  = cppstringssuck.str();
                unsigned int expected = m_ss_cs[layer];
                unsigned int got      = ss_cs;
                
                ERS_LOG("Slot " << m_slot << " SSMap Local RAM " << layer << " Input 2 checksum failed. Expected =" << std::hex<<std::setfill('0')<<std::setw(8)<<m_ss_cs[layer]<<", got = "<<std::setw(8)<<ss_cs);
                
                daq::ftk::FTKWrongChecksum ex(ERS_HERE, name_ftk(), expected, got, "2", ramname);
                throw ex;
            }
            else
                ERS_LOG("Slot " << m_slot << " SSMap Local RAM " << layer << " Input 2 checksum passes. Expected =" << std::hex<<std::setfill('0')<<std::setw(8)<<m_ss_cs[layer]<<", got = "<<std::setw(8)<<ss_cs);
        }


        void input2::check_module_checksum(uint layer){
            
            static const uint module_checksum_addresses[4]={I2_MODULE_CHECKSUM4_REG, I2_MODULE_CHECKSUM5_REG, I2_MODULE_CHECKSUM6_REG, I2_MODULE_CHECKSUM7_REG};
            uint ss_cs = m_vme->read_word(module_checksum_addresses[layer]);

            if( ss_cs!=m_module_cs[layer])
            {
                std::stringstream cppstringssuck;
                cppstringssuck << "SSMap Module RAM " << layer;
                std::string  ramname  = cppstringssuck.str();
                unsigned int expected = m_module_cs[layer];
                unsigned int got      = ss_cs;
                
                daq::ftk::FTKWrongChecksum ex(ERS_HERE, name_ftk(), expected, got, "2", ramname);
                throw ex;
            }
            else
                ERS_LOG("Slot " << m_slot << " SSMap Local RAM " << layer << " Input 2 checksum passes. Expected =" << std::hex<<std::setfill('0')<<std::setw(8)<<m_ss_cs[layer]<<", got = "<<std::setw(8)<<ss_cs);
        }

    }
}



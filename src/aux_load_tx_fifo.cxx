/*           Program to load Tx FIFOS on Aux card                              */
/*           Written for AuxFTK board v1.0   FTK collaboration                 */
#include "aux/aux.h"
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("fpga", value< std::string >()->default_value("1"), "The FPGA ID")
    ("buffer", value< std::string >()->default_value("0x1"), "Buffer to fill")
    ("file", value< std::string >(), "Input file")
    ;
  
  positional_options_description p;
  p.add("buffer", 1);
  p.add("file", 1);

  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  int slot    = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int fpga    = daq::ftk::string_to_int( vm["fpga"].as<std::string>() );

  int buffer  = daq::ftk::string_to_int( vm["buffer"].as<std::string>() );
  std::string file  = vm["file"].as<std::string>();
  
  return  daq::ftk::aux_load_tx_fifo(slot, fpga, buffer, file) ;
}

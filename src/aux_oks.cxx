/*****************************************************************/
/*                                                               */
/*                                                               */
/*****************************************************************/

#include "aux/aux_oks.h"

using namespace daq::ftk;
using namespace std;

aux_oks::aux_oks() {

  cout << "Loading OKS Configuration." << endl;
  Configuration db("oksconfig:ftk/partitions/FTK.data.xml");
  const daq::core::Partition * p = daq::core::get_partition(db, "FTK");

  // /afs/cern.ch/atlas/project/tdaq/databases/v40/daq/partitions/be_test.data.xml
  // Configuration db("oksconfig:daq/partitions/be_test.data.xml");
  // const daq::core::Partition * p = daq::core::get_partition(db, "be_test");

  cout << "p=" << p << endl;
  if (p) {
    db.register_converter(new daq::core::SubstituteVariables(db, *p));
    daq::core::SegConfig top_segment;
    p->get_segment(top_segment, p->get_OnlineInfrastructure()->UID(), false);
    print_segment(top_segment, 0);
  }

  const ftk::dal::ReadoutModule_PU* settings = db.get<ftk::dal::ReadoutModule_PU>("Aux1");
  cout << "slot = " << settings->get_Slot() << endl;

}

void aux_oks::print_segment(const daq::core::SegConfig& seg, unsigned int recursion_level) {

  const std::string s(recursion_level*2, ' ');
  std::cout << s << "S: " << seg.get_segment_id() << std::endl;
  for (auto & s : seg.get_nested_segments())
    print_segment(s, recursion_level + 1);
  
  for (auto & r : seg.get_segment().get_Resources())
    print_resource(r, recursion_level + 1);
}

void aux_oks::print_resource(const daq::core::ResourceBase * res, unsigned int recursion_level)
{
  const std::string s(recursion_level*2, ' ');
  std::cout << s << "R: " << res->UID() << std::endl;
  if (const daq::core::ResourceSet * rset = res->cast<daq::core::ResourceSet>())
    for (auto & r : rset->get_Contains())
      print_resource(r, recursion_level + 1);
}



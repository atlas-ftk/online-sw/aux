/*           Program to issue a routine for read the AUX RAM                   */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "aux/aux_vme_regs.h"

#include "aux/TF/Report.h"
#include "aux/TF/ArgParser.h"
#include "aux/TF/TFGen.h"
#include "aux/TF/TFConstants.h"
#include "aux/TF/TFEventList.h"
#include "aux/TF/TFFitter.h"


#define DEBUG std::cerr << "JS: " << __LINE__ << std::endl

namespace daq {
  namespace ftk {


    void aux_get_hw_bitmask(std::vector<u_int> &buff, 
                            u_int start, u_int ntracks,
                            std::vector<bool> &bitmask) {

      bitmask.clear();

      u_int road_start = 0;
      u_int current_road = 0xffffff;

      // t1 is incoming.
      for (u_int t1 = 0; t1 < ntracks; t1++) {

        u_int t1pos = start + 12*t1;

        bitmask.push_back(true);

        // hit warrior can't deal with more than 16.
        if (((buff[t1pos+1] & 0xffffff) != current_road) ||
            (t1 - road_start == 16)) {
          current_road = (buff[t1pos+1] & 0xffffff);
          road_start = t1;
        }

        // compare to all those that are already in memory.
        for (u_int t2 = road_start; t2 < t1; t2++) {

          u_int t2pos = start + 12*t2;

          u_int nhits1(0), nhits2(0);
          for (int h = 0; h < 8; h++) {
            if (buff[t1pos] & (1 << (h+16))) nhits1++;
            if (buff[t2pos] & (1 << (h+16))) nhits2++;
          }

          int matches = 0;
          for (u_int h = 0; h < 8; h++) { 
            // exact matches on real hits.
            if ((buff[t1pos] & (1 << (h+16))) &&
                buff[t1pos+h+2] == buff[t2pos+h+2]) matches++;
          }

          // for matching hits, check chi2.
          // add bitmask in later.
          if (matches > 5) {

            // take the one with more hits.
            if (nhits1 != nhits2) {
              bitmask[(nhits1 > nhits2) ? t2 : t1] = false;
              break;
            }
            
            if ((buff[t1pos+10] & 0x8ffffff) >
                (buff[t2pos+10] & 0x8ffffff)) {
              bitmask[t1] = false;
            } else {
              bitmask[t2] = false;
            }

          }
        }

      }

      return;

    }


    bool aux_validate_hw1_output(std::map<u_int, std::vector<u_int>> &content) {

      // all of the streams must be "clean" -- 
      // they must only contain the good events.

      std::map<u_int, u_int> stream_pos;
      stream_pos[0x12] = 0;
      stream_pos[0x13] = 0;
      stream_pos[0x14] = 0;
      stream_pos[0x15] = 0;

      u_int stream = 0x12;
      std::vector<u_int> merged_stream;

      std::vector<bool> track_mask;
      while (1) {

        if (stream > 0x15) stream = 0x12; 

        // copy the header from the first stream.
        if (stream == 0x12) {
          merged_stream.push_back(0xb0f0);
          for (int p = 1; p < 7; p++) {
            merged_stream.push_back(content[stream][stream_pos[stream]+p]);
          }
        }
        stream_pos[stream] += 7;

        // how many tracks to send to the "hw"?
        u_int ntracks = 0;
        if (0xa8 == (content[stream][stream_pos[stream]] >> 24)) ntracks = 1;
        // this is safe, because it should only ever land on E0DA.
        while (0xa8 == (content[stream][stream_pos[stream] + 12*ntracks] >> 24)) ntracks++;

        // get a bitmast of the tracks to keep.
        daq::ftk::aux_get_hw_bitmask(content[stream], stream_pos[stream], ntracks, track_mask);

        // use the mask for a "gated output"
        for (u_int t = 0; t < ntracks; t++) {
          if (track_mask[t]) {
            for (u_int p = 0; p < 12; p++) {
              merged_stream.push_back(content[stream][stream_pos[stream]+p]);
            }
          }
          stream_pos[stream] += 12;
        }

        // look for the end...
        while (0xe0f0 != (content[stream][stream_pos[stream]] & 0xffff)) {
          if (stream == 0x15) // only print if it's from the last stream.
            merged_stream.push_back(content[stream][stream_pos[stream]]);
          stream_pos[stream]++;
        }

        // if we're in the last L1ID of the last stream, then break out!
        if (stream == 0x15) {
          merged_stream.push_back(0xe0f0);
          if (stream_pos[stream] == content[stream].size()-1) break;
        }

        // otherwise, increment to the next stream.
        stream_pos[stream]++;
        stream++;;

      }
      // merged_stream and content[0x9] should now hold the 
      // calculated and board values of the HW output.

      // check to make sure that word matches.
      bool retval = true;
      if (merged_stream.size() != content[0x9].size()) retval = false;
      for (u_int pos = 0; 
          pos < merged_stream.size() && 
          pos < content[0x9].size();
          pos++) {
        if (content[0x9][pos] != merged_stream[pos]) retval = false;
      }

      // print out the result...
      if (retval) std::cout << "HW output stream matches expectation." << std::endl;
      else { // if it's wrong, show the output.
        std::cout << "Output stream does not match." << std::endl;
        for (u_int pos = 0; 
            pos < merged_stream.size() && 
            pos < content[0x9].size();
            pos++) {
          std::cout << std::hex << std::setfill('0') << std::setw(8) << content[0x9][pos];
          if (content[0x9][pos] != merged_stream[pos]) 
            std::cout << " <BOARD!CALC> " << std::hex << std::setfill('0') << std::setw(8) << merged_stream[pos];
          std::cout << std::endl;
        }

      }

      return retval;

    }

    bool aux_tf_compare(std::map<u_int, std::map<u_int, std::vector<u_int> > > &data, 
                        std::string tf_consts) {

      // Load events in BL spybuffer -- the input
      std::cout << ">>>> BEGINNING BL" << std::endl;
      TFEventList *bl_events = new TFEventList( "BLO" );
      bl_events->buildFromBLMap(data[3]);

      // Load constants
      TFConstants *consts = new TFConstants();
      consts->buildFromFile(tf_consts);

      // Use the constants to calculate
      // what we think the answer should be.
      TFFitter fitter( consts , bl_events );
      fitter.calculateChiSquares();

      std::cout << ">>>> FINISHED WITH BL" << std::endl;

      // Load events in TF spybuffer -- the output
      // for (auto v : data[2][0x12]) printf("2/0x12: %08x\n", v);
      TFEventList *tf_events = new TFEventList( "TFI2" );
      tf_events->buildFromTFVector(data[2][0x12]);
       

      std::cout << "BL Event Packets >>>>>>>" << std::endl;
      bl_events->printEventPackets();
      std::cout << "TF Event Packets >>>>>>>" << std::endl;
      tf_events->printEventPackets();


      //  Do a track by track comparison
      //  For now I am comparing every event in tf_events to every event in bl_events. Once
      //  we have freeze logic in place we should be able to simplify this and just report
      //  a single boolean.
      //
      std::cout << std::endl
        << "=============================================================================" << std::endl
        << "Looping over all events in " << tf_events->getName() << ".  "
        << "For each event, look at all tracks and compare against " << std::endl
        << "each event in " << bl_events->getName() << ".  "
        << "Table reports nMatchedTracks / nTotalTracks for each event-to-event " << std::endl
        << "comparison." << std::endl
        << std::endl;
      tf_events->compareAllEvents( bl_events );
      std::cout << std::endl
        << "=============================================================================" << std::endl;

      return true;

    }


    bool aux_get_spybuffers( int slot, int fpga, const std::vector<u_int> &buffer, 
                             std::map<u_int, std::vector<u_int> > &data) { 

      int status;
      u_int ret, register_content;
      u_int dim_buffer = 0;
      int handle;

      static VME_MasterMap_t master_map = {0x0, 0x10000000, VME_A32, 0};

      u_int slotaddr = ((slot&0x1f) << 27);
      u_int fpgaaddr = (fpga << 24);
      u_int vmeaddr = slotaddr + fpgaaddr;
      // std::cout << std::hex
      //           << "slotaddr=" << slotaddr << "\t"
      //           << "fpgaaddr=" << fpgaaddr << "\t"
      //           << "vmeaddr=" << vmeaddr << std::endl;

      ret = VME_Open();
      if (ret != VME_SUCCESS) {
        VME_ErrorPrint(ret);
        FTK_VME_ERROR( "Failure when calling the function 'VME_Open' in function" );
      }

      master_map.vmebus_address   = vmeaddr;
      master_map.window_size      = 0x1000000;
      master_map.address_modifier = VME_A32;
      master_map.options          = 0;
      ret = VME_MasterMap(&master_map, &handle);
      if (ret != VME_SUCCESS) {
        VME_ErrorPrint(ret);
        FTK_VME_ERROR( "Failure when calling the function 'VME_MasterMap' in function"  );
      }

      // PRINT_LOG("\n**************************************\n");
      // PRINT_LOG("Send Freeze to spy buffer!!\n");
      // PRINT_LOG("\n**************************************\n");

      //set tmode rx stratix
      status = VME_WriteSafeUInt(handle, TMODE, 0x1);
      CHECK_ERROR(status);

      // Figure out the last addresss
      std::map<u_int, uint> lastAddress;

      for (auto buff : buffer) {
        status = VME_ReadSafeUInt(handle, (buff<<16), &register_content);
        CHECK_ERROR(status);

        lastAddress[buff] = register_content&0xffff;
        if(dim_buffer==0) dim_buffer=(register_content>>16)&0xffff;
      }

      //read the content of the memory of the selected link
      u_int baddr;
      for(u_int i = 0; i < (1<<dim_buffer)-1; ++i) {
        for (auto buff : buffer) {

          baddr=(lastAddress[buff]+i)%((1<<dim_buffer)-1);
          if(baddr==0) baddr=(1<<dim_buffer)-1;

          status = VME_ReadSafeUInt(handle, (buff<<16) + (baddr<<2), &register_content);
          data[buff].push_back(register_content);
          CHECK_ERROR(status);

          // printf("[%.4x]: %08x  ", baddr,register_content);

        }
        // printf("\n");
      }

      // for(auto buff : buffer) {
      //   printf("[last]: %08x  ",lastAddress[buff]);
      // }
      // printf("\n");

      //set tmode rx stratix
      status = VME_WriteSafeUInt(handle, TMODE, 0x0);
      CHECK_ERROR(status);

      ret = VME_MasterUnmap(handle);
      if (ret != VME_SUCCESS) {
        VME_ErrorPrint(ret);
        FTK_VME_ERROR( "Failure when calling the function 'VME_MasterUnMap' in function"  );
        return false;
      }

      ret = VME_Close();
      if (ret != VME_SUCCESS) {
        VME_ErrorPrint(ret);
        FTK_VME_ERROR( "Failure when calling the function 'VME_Close' in function"  );
        return false;
      }

      return true;

    }


    void aux_get_spybuffers_test2(std::map<u_int, std::vector<u_int> > &data) {

      std::ifstream input ("input_ex.txt");
      std::string str;
      u_int val;
      while (input >> str >> std::hex >> val) {
        data[0x12].push_back(val);
        data[0x13].push_back(val);
        data[0x14].push_back(val);
        data[0x15].push_back(val);
      }
      input.close();
      
      std::ifstream output("output_ex.txt");
      while (output >> str >> std::hex >> val) {
        data[0x09].push_back(val);
      }
      output.close();

    }



    void aux_buffers_build_data_structure(std::map<u_int, std::map<u_int, std::vector<u_int> > > &data) {

      for (int f = 1; f <= 6; f++) {
        data[f] = std::map<u_int, std::vector<u_int> >();
      }

      for (auto b : aux_i1_spy_buffers) data[1][b] = std::vector<u_int>();
      for (auto b : aux_i2_spy_buffers) data[2][b] = std::vector<u_int>();

      for (auto b : aux_proc_spy_buffers) {
        for (int f = 3; f <= 6; f++) data[f][b] = std::vector<u_int>();
      }

    }

    void aux_buffers_download_data(int slot, std::map<u_int, std::map<u_int, std::vector<u_int> > > &data) {

      aux_get_from_file(slot, 2, aux_i2_spy_buffers,   data[2]);

      //aux_get_spybuffers(slot, 1, aux_i1_spy_buffers,   data[1]);
      // aux_get_spybuffers(slot, 2, aux_i2_spy_buffers,   data[2]);
      // aux_get_spybuffers(slot, 3, aux_proc_spy_buffers, data[3]);
      // aux_get_spybuffers(slot, 4, aux_proc_spy_buffers, data[4]);
      // aux_get_spybuffers(slot, 5, aux_proc_spy_buffers, data[5]);
      // aux_get_spybuffers(slot, 6, aux_proc_spy_buffers, data[6]);
      // aux_get_spybuffers_test2(data[2]);

    }

    void  aux_trim_buffer(std::vector<u_int> & vec, u_int first, u_int last) {

      // find first and last, iterate between them, 
      // and pop back from the end.
      bool found_first = false;
      u_int trail(0), removeN(0);
      for (u_int pos = 0; pos < vec.size(); pos++) {

        // first look for the first event.
        if (!found_first && 
            (vec[pos] & 0xffff) == 0xb0f0 &&
            pos+3 < vec.size() && 
            vec[pos+3] == first) {
          trail = pos; found_first = true;
        }

        // if we've found the first, 
        // start copying them back in the vector.
        if (found_first) {
          vec[pos-trail] = vec[pos];
        }

        // if we're at the last one, then stop.
        if (found_first &&
            (vec[pos] & 0xffff) == 0xe0f0 &&
            vec[pos-4] == last) {
          removeN = trail + (vec.size() - pos) - 1;
          break;
        }
      }

      for (u_int pop = 0; pop < removeN; pop++) vec.pop_back();

    }

    void  aux_trim_bl_buffer(std::map<u_int, std::vector<u_int> > & BLO) {

      bool found_first = false;
      u_int first = 0; u_int last = 0;
      for (u_int i = 0; i < BLO[0x2c].size(); i++) {
        if ((BLO[0x2c][i] & 0x1ff) == 0x100) {
          if (!found_first) { first = i+1; found_first = true; }
          last = i;
        }
      }

      // find first and last, iterate between them, 
      // and pop back from the end.
      for (u_int pos = 0; pos <= last - first; pos++) {

        for (auto b : {0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c}) {
          BLO[b][pos] = BLO[b][pos+first]; 
        }
      }
 
      u_int removeN = BLO[0x2c].size() - last + first - 1;
      for (auto b : {0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c}) {
        for (u_int pop = 0; pop < removeN; pop++) BLO[b].pop_back();
      }

    }



    void aux_get_first_last(std::vector<u_int> &data, u_int &first_l1a, u_int &last_l1a) {

      first_l1a = 0; last_l1a = 0xffffffff;

      bool first = true;
      for (u_int pos = 0; pos < data.size(); pos++) {

        // find the first B0F0
        if (first &&
            (data[pos] & 0xffff) == 0xb0f0 &&
            pos + 3 < data.size()) {
          first_l1a = data[pos+3];
          first = false;
        }

        // find the last E0F0...
        // overlap checking happens later.
        if ((data[pos] & 0xffff) == 0xe0f0 && pos >= 4) {
          last_l1a = data[pos-4];
        }
      }
    }


    void aux_buffers_find_overlap(std::map<u_int, std::map<u_int, std::vector<u_int> > > &data, bool verbose) {

      u_int first(0), last(0xffffffff); // mod L1A is 2**32
      for (int f = 2; f <= 2; f++) {
        u_int first_temp(0), last_temp(0);
        for (auto & buff : data[f]) {
          daq::ftk::aux_get_first_last(buff.second, first_temp, last_temp);

          // need to be cleverer here? 
          // mod for the L1A is 2**32 = 4.2B...
          // I believe they are unique.
          if (first_temp > first) first = first_temp;
          if (last_temp  < last)  last  = last_temp;
        }
      }

      for (auto & buff : data[2]) daq::ftk::aux_trim_buffer(buff.second, first, last);
      daq::ftk::aux_trim_bl_buffer(data[3]);
      
      for (auto v : data[2][0x12]) printf("2 0x12: %08x\n", v);
      std::cout << "first=" << std::hex << first << "  last=" << last << std::endl;

    }

    void aux_print_all_buffers(std::map<u_int, std::map<u_int, std::vector<u_int> > > &data) {

      for (uint x = 1; x <=6; x++) {
        for (auto & buff : data[x]) {
          for (auto v : buff.second) {
            printf("%d 0x%x: %08x\n", x, buff.first, v);
          }
        }
      }

    }


    bool aux_check_spy_buffers ( int slot, std::string tf_consts ) { 

      bool retval = true;

      /// fpga, buffer, data
      std::map<u_int, std::map<u_int, std::vector<u_int> > > data;

      aux_buffers_build_data_structure(data); // build the map     
      aux_buffers_download_data(slot, data);  // call download the buffers

      // aux_buffers_find_overlap(data);         // identify first and last, delete others.

      // aux_validate_hw1_output(data[2]);

      // aux_tf_compare(data, tf_consts);
      
      aux_print_all_buffers(data);

      return retval;

    }

  } //namespcae daq
} //namespcae ftk


#ifdef STANDALONE
/////////////////////////////////////////////////////////////////////////////
// Stand alone application
//   - Compiled when -DSTANDALONE
/////////////////////////////////////////////////////////////////////////////

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


/** Parsing input parmaeters and calling the am_init function
 *
 */
int main(int argc, char **argv) { 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("11"), "The card slot")
    ("tf_consts", value< std::string >()->default_value("testvectors/TFConstants_ttbar_partial.txt"), "The card slot")
    ("buffer", value< std::vector<std::string> >(), "Buffer to read")
    ;
  
  positional_options_description p;
  p.add("buffer", -1);

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  
  } catch( ... ) { // In case of errors during the parsing process, desc is printed for help 
    std::cerr << desc << std::endl; 
    return 1;
  }
  
  notify(vm);
  
  if (vm.count("help") ) { // if help is required, then desc is printed to output
    std::cout << std::endl <<  desc << std::endl ; 
    return 0;	
  }
  
  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  std::string tf_consts = vm["tf_consts"].as<std::string>();

  return  daq::ftk::aux_check_spy_buffers(slot, tf_consts) ;

}
#endif

/*           Performe remote update of an FPGA on the AUX card                 */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */

#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include "ftkcommon/core.h"
#include "aux/aux_vme_regs.h"
#include "aux/aux_upgrade.h"

#include "aux/AUX_FWMap.h"
#include "aux/AUX_EPCQUpgrade.h"

namespace daq 
{
  namespace ftk 
  {
    bool aux_epcq_upgrade_board_main( int slot, uint epcqaddress, std::vector<uint> fpgas, std::vector<daq::ftk::AUX_FWMap> maps )
    { 
      //
      // Create necessary AUX_EPCQUpgrade objects
      // 
      PRINT_LOG("Initialize AUX_EPCQUpgrade");
      std::vector<AUX_EPCQUpgrade> epcqs;
      for(uint i=0;i<fpgas.size();i++)
	{
	  AUX_EPCQUpgrade epcq(slot,fpgas[i],maps[i]);
	  epcqs.push_back(epcq);
	}

      //
      // Initialize
      //
      PRINT_LOG("Initialize AUX_EPCQUpgrade FSM");
      for(std::vector<AUX_EPCQUpgrade>::iterator epcq=epcqs.begin();
	  epcq!=epcqs.end();
	  epcq++)
	{
	  // Reset EPCQ Control
	  epcq->reset_control();
	  // Clear anything in the buffers
	  epcq->clear_buffer();
	  // Initialize write FSM
	  epcq->write_init(epcqaddress);
	}

      //
      // Read data, word by word
      //
      std::cout << std::endl<< std::endl<< std::endl<< std::endl<< std::endl<< std::endl;
      bool done=false;
      while(!done)
	{
	  // Erase next sector
	  //PRINT_LOG("SERASE " << std::setfill('0') << std::setw(8) << std::hex << epcqaddress+i*4);
	  for(std::vector<AUX_EPCQUpgrade>::iterator epcq=epcqs.begin();
	      epcq!=epcqs.end();
	      epcq++)
	    {
	      if(!epcq->isDone())
		{
		  epcq->sector_erase();
		}
	    }
	  
	  for(uint gidx=0;gidx<256;gidx++)
	    { // 256x256 = 65536 bytes in a sector
	      // Write next 256-byte group
	      for(std::vector<AUX_EPCQUpgrade>::iterator epcq=epcqs.begin();
		  epcq!=epcqs.end();
		  epcq++)
		{
		  if(!epcq->isDone()) 
		    {
		      epcq->write_page();
		    }
		}
	    }

	  // Check to see if everything is done
	  done=true;
	  for(std::vector<AUX_EPCQUpgrade>::iterator epcq=epcqs.begin();
	      epcq!=epcqs.end();
	      epcq++)
	    {
	      done&=epcq->isDone();
	    }
	}
      
      //
      // Deinit
      //
      for(std::vector<AUX_EPCQUpgrade>::iterator epcq=epcqs.begin();
	  epcq!=epcqs.end();
	  epcq++)
	{
	  // Reset EPCQ Control
	  epcq->reset_control();

	  // Disable remote upgrade
	  epcq->write_deinit();
	}
      
      PRINT_LOG("DONE WITH FIRMWARE UPDATE");

      return 0;      
    }

  } //namespcae daq
} //namespcae ftk



#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  positional_options_description p;
  p.add("file", 1);

  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("file", value< std::string >(), "The txt file containing firmware lists.")
    ("epcqaddress", value< std::string >()->default_value("0"), "The start address in the EPCQ")
    ;
  
  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  int slot    = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  
  std::string file = vm["file"].as<std::string>();
  uint epcqaddress  = daq::ftk::string_to_int( vm["epcqaddress"] .as<std::string>() );

  //
  // Extract information from a file
  //
  std::ifstream fh;
  fh.open(file);
  if(!fh.is_open())
    {
      std::cerr << "Error opening file: " << file << std::endl;
      return -1;
    }
  
  uint fpga;
  std::string firmware_base;

  std::vector<daq::ftk::AUX_FWMap> maps;
  std::vector<uint> fpgas;
  while(fh >> fpga >> firmware_base)
    {
      std::cout << fpga << " - " << firmware_base << std::endl;
      daq::ftk::AUX_FWMap map(firmware_base+".rpd",firmware_base+".map");

      fpgas.push_back(fpga);
      maps.push_back(map);
    }
  
  fh.close();
  return  daq::ftk::aux_epcq_upgrade_board_main(slot, epcqaddress, fpgas, maps) ;
}


/*           Reset all of the registers and clocks in the AUX                  */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */

#include "aux/aux.h"
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help"       , "produce help message")
    ("slot"       , value< std::string >()->default_value("15"), "The card slot")
    ("fpga"       , value< std::string >()->default_value("1"), "The FPGA ID")
    ("main"       , "Reset the main board.")
    ("txfifo"     , "Empty the Tx FIFOs.")
    ("reg"        , "Set all registers to 0.")
    ("err"        , "Reset the error counters.")
    ("xcvr"       , "Reset the transceivers.")
    ("sync"       , "Reset transceiver synchronization.")
    ("spybuffers" , "Reset spybuffers.")
    ("freeze"     , "Reset freeze.")
    ("errlatch"   , "Reset error latches.")
    ;
  
  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  int slot    = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int fpga    = daq::ftk::string_to_int( vm["fpga"].as<std::string>() );
  
  bool doMain       = vm.count("main");
  bool doXcvr       = vm.count("xcvr");
  bool doSync       = vm.count("sync");
  bool doReg        = vm.count("reg");
  bool doErr        = vm.count("err");
  bool doTxFIFO     = vm.count("txfifo");
  bool doSpybuffers = vm.count("spybuffers");
  bool doFreeze     = vm.count("freeze");
  bool doErrLatch   = vm.count("errlatch");

  if(!doMain && !doXcvr && !doReg && !doErr && !doTxFIFO && !doSync && !doSpybuffers && !doFreeze && !doErrLatch)
    {
      doMain=true;
      doXcvr=true;
      doSync=true;
      doReg=true;
      doErr=true;
      doTxFIFO=true;
      doSpybuffers=true;
      doFreeze=true;
      doErrLatch=true;
    }

  return  daq::ftk::aux_reset(slot, fpga, doMain, doXcvr, doSync, doReg, doErr, doTxFIFO, doSpybuffers, doFreeze, doErrLatch) ;
}

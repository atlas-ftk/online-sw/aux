/*           Program to issue a routine for read the AUX RAM                   */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */
#include "aux/aux.h"
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot.")
    ("fpga", value< std::string >()->default_value("1"), "The FPGA ID.")
    ("module", "Input corresponds to the module map.")
    ("address", value< std::string >()->default_value("0x1"), "SSMAP address to fill.")
    ("ssmappath", value< std::string >(), "Location of input SSMAP.")
    ;
  
  positional_options_description p;
  p.add("address", 1);
  p.add("ssmappath", 1);

  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  u_int slot    = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  u_int fpga    = daq::ftk::string_to_int( vm["fpga"].as<std::string>() );

  u_int address = daq::ftk::string_to_int( vm["address"].as<std::string>() );
  std::string ssmappath  = vm["ssmappath"].as<std::string>();

  bool isModule = vm.count("module");

  return  daq::ftk::aux_ssmap_load(slot, fpga, address, ssmappath, isModule);
}

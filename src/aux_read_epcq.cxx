/*           Performe remote update of an FPGA on the AUX card                 */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */

#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "aux/aux_vme_regs.h"
#include "aux/aux_upgrade.h"



namespace daq 
{
  namespace ftk 
  {
    uint8_t lookup[16] = {
      0x0, 0x8, 0x4, 0xC,
      0x2, 0xA, 0x6, 0xE,
      0x1, 0x9, 0x5, 0xD,
      0x3, 0xB, 0x7, 0xF };
    
    uint flip( uint x )
    {
      //This should be just as fast and it is easier to understand.
      //return (lookup[n%16] << 4) | lookup[n/16];
      uint y=0;
      y|=(x&0xFF)<<24;
      x=x>>8;
      y|=(x&0xFF)<<16;
      x=x>>8;
      y|=(x&0xFF)<<8;
      x=x>>8;
      y|=(x&0xFF)<<0;
      return y;
    }


    bool aux_read_epcq( int slot, int fpga, uint epcqaddress, uint npages, std::string file )
    { 
      int status, i;
      u_int ret, register_content, register_content_flip, theaddr;
      u_int vmeaddr, slotaddr, fpgaaddr;
      int handle;
            
      static VME_MasterMap_t master_map = {0x0, 0x10000000, VME_A32, 0};
      
      slotaddr = ((slot&0x1f) << 27);
      fpgaaddr = ((fpga&0x1f) << 24);
      vmeaddr = slotaddr + fpgaaddr;

      ret = VME_Open();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Open' in function" );
	}
      
      master_map.vmebus_address   = vmeaddr;
      master_map.window_size      = 0x1000000;
      master_map.address_modifier = VME_A32;
      master_map.options          = 0;
      ret = VME_MasterMap(&master_map, &handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterMap' in function"  );
	}

      //
      // Prepare output file if needed
      bool doFile=(file!="");
      std::ofstream fh;
      if(doFile)
	{
	  fh.open(file.c_str(),std::ios::out | std::ios::binary);
	}

      //
      // START
      // 
      theaddr=0;
      for(uint page=0;page<npages;page++)
	{
	  // Reset EPCQ Control
	  aux_upgrade_clear_regs(handle);
	  
	  // Start read operation
	  status = VME_WriteSafeUInt(handle, TMODE, 0x1);
	  CHECK_ERROR(status);
	  
	  status = VME_WriteSafeUInt(handle, RU_EPCQ, 0x1);
	  CHECK_ERROR(status);
	  status = VME_WriteSafeUInt(handle, RU_ADDR, epcqaddress+(page<<14));
	  CHECK_ERROR(status);

	  status = VME_WriteSafeUInt(handle, TMODE, 0x0);
	  CHECK_ERROR(status);
	  
	  printf("addr %08x\n",epcqaddress+(page<<14));
	  // Wait for read to finish
	  register_content=0;
	  do
	    {
	      status = VME_ReadSafeUInt(handle, RU_STATUS, &register_content);
	      CHECK_ERROR(status);
	    }
	  while((register_content&2)!=2);

	  // Read the contents of the buffer
	  status = VME_WriteSafeUInt(handle, TMODE, 0x1);
	  CHECK_ERROR(status);
	  
	  int dim_buffer = 12;
	  for(i = 0; i < (1<<dim_buffer); ++i)  
	    {
	      status = VME_ReadSafeUInt(handle, EPCQ_CONTENTS + (i<<2), &register_content);
	      CHECK_ERROR(status);

	      register_content_flip=flip(register_content);	      
	      printf("%08x\t%08x\n",theaddr,register_content);
	      if(doFile) fh.write(reinterpret_cast<const char *>(&register_content_flip),sizeof(register_content));
	      theaddr+=4;
	    }
	  
	  status = VME_WriteSafeUInt(handle, TMODE, 0x0);
	  CHECK_ERROR(status);
	}
      

      // Reset EPCQ Control
      aux_upgrade_clear_regs(handle);

      //
      // END
      // 
      if(doFile) fh.close();


      ret = VME_MasterUnmap(handle);
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_MasterUnMap' in function"  );
	  
	}
      
      ret = VME_Close();
      if (ret != VME_SUCCESS)
	{
	  VME_ErrorPrint(ret);
	  FTK_VME_ERROR( "Failure when calling the function 'VME_Close' in function"  );
	  
	}

      return 0;
      
    }
    
  } //namespcae daq
} //namespcae ftk


#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  positional_options_description p;
  p.add("epcqaddress", 1);
  p.add("npages", 1);

  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("fpga", value< std::string >()->default_value("1"), "The FPGA ID")
    ("epcqaddress", value< std::string >()->default_value("0"), "The start address in the EPCQ")
    ("npages", value< std::string >()->default_value("256"), "The number of pages to read from the EPCQ.")
    ("file", value< std::string >()->default_value(""), "File to dump the contents into")
    ;
  
  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  int slot    = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int fpga    = daq::ftk::string_to_int( vm["fpga"].as<std::string>() );
  
  uint epcqaddress  = daq::ftk::string_to_int( vm["epcqaddress"] .as<std::string>() );
  uint npages       = daq::ftk::string_to_int( vm["npages"] .as<std::string>() );

  std::string file = vm["file"].as<std::string>();
  
  return  daq::ftk::aux_read_epcq(slot, fpga, epcqaddress, npages, file) ;
}

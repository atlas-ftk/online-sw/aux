// functions for StatusRegisterAuxFactory.h

#include "aux/StatusRegisterAuxFactory.h"

#include "ftkcommon/exceptions.h"

namespace daq {
namespace ftk {

StatusRegisterAuxFactory::StatusRegisterAuxFactory(
	std::string auxName,
	u_int auxSlot,
	std::string registersToRead,
	std::string isServerName,
	IPCPartition ipcPartition
	) : StatusRegisterVMEFactory (auxName, auxSlot, registersToRead, isServerName) 
	{

	std::unique_ptr<StatusRegisterAuxStatusNamed> m_srAuxSNamed;
	try 
	{
		m_srAuxSNamed = std::make_unique<StatusRegisterAuxStatusNamed>(ipcPartition, m_isProvider.c_str());
	} 
	catch(ers::Issue &ex)  {
		daq::ftk::ISException e(ERS_HERE, "Error while creating StatusRegisterAuxStatusNamed object", ex);  // Append the original exception (ex) to the new one 
		ers::warning(e);  //or throw e;
	}

	// Store the name and slot of the board in StatusRegisterAuxStatusNamed
	m_srAuxSNamed->Name = m_name;
	m_srAuxSNamed->Slot = m_slot;
	
	// To read different registers, modify the following lines. To add multiple collections, call the setupCollection function multiple times, once for each collection.
//	setupCollection('1',1,0x110,0x3b0,0x10,"Input1 Aux Status Register Reading: [0x110->0x3b0]","AuxI1",
//				&(m_srAuxSNamed->I1_sr_v),&(m_srAuxSNamed->I1_sr_v_info));
//	//setupCollection('2',2,0x110,0x3b0,0x10,"Input2 Aux Status Register Reading: [0x110->0x3b0]","AuxI2",  // Ruo, input 2 is also output
//	setupCollection('2',2,0x110,0x3f0+2*0x10,0x10,"Input2 Aux Status Register Reading: [0x110->0x3b0]","AuxI2",  // Ruo, input 2 is also output
//				&(m_srAuxSNamed->I2_sr_v),&(m_srAuxSNamed->I2_sr_v_info));
//	setupCollection('3',3,0x110,0x300,0x10,"Processor1 Aux Status Register Reading: [0x110->0x300]","AuxP1",
//				&(m_srAuxSNamed->P1_sr_v),&(m_srAuxSNamed->P1_sr_v_info));
//	setupCollection('4',4,0x110,0x300,0x10,"Processor2 Aux Status Register Reading: [0x110->0x300]","AuxP2",
//				&(m_srAuxSNamed->P2_sr_v),&(m_srAuxSNamed->P2_sr_v_info));
//	setupCollection('5',5,0x110,0x300,0x10,"Processor3 Aux Status Register Reading: [0x110->0x300]","AuxP3",
//				&(m_srAuxSNamed->P3_sr_v),&(m_srAuxSNamed->P3_sr_v_info));
//	setupCollection('6',6,0x110,0x300,0x10,"Processor4 Aux Status Register Reading: [0x110->0x300]","AuxP4",
//				&(m_srAuxSNamed->P4_sr_v),&(m_srAuxSNamed->P4_sr_v_info));

	setupCollection('1',1,0x110,0x700,0x10,"Input1 Aux Status Register Reading: [0x110->0x3b0]","AuxI1",
				&(m_srAuxSNamed->I1_sr_v),&(m_srAuxSNamed->I1_sr_v_info));
	setupCollection('2',2,0x110,0x700,0x10,"Input2 Aux Status Register Reading: [0x110->0x3b0]","AuxI2",  // Ruo, input 2 is also output
				&(m_srAuxSNamed->I2_sr_v),&(m_srAuxSNamed->I2_sr_v_info));
	setupCollection('3',3,0x110,0x700,0x10,"Processor1 Aux Status Register Reading: [0x110->0x300]","AuxP1",
				&(m_srAuxSNamed->P1_sr_v),&(m_srAuxSNamed->P1_sr_v_info));
	setupCollection('4',4,0x110,0x700,0x10,"Processor2 Aux Status Register Reading: [0x110->0x300]","AuxP2",
				&(m_srAuxSNamed->P2_sr_v),&(m_srAuxSNamed->P2_sr_v_info));
	setupCollection('5',5,0x110,0x700,0x10,"Processor3 Aux Status Register Reading: [0x110->0x300]","AuxP3",
				&(m_srAuxSNamed->P3_sr_v),&(m_srAuxSNamed->P3_sr_v_info));
	setupCollection('6',6,0x110,0x700,0x10,"Processor4 Aux Status Register Reading: [0x110->0x300]","AuxP4",
				&(m_srAuxSNamed->P4_sr_v),&(m_srAuxSNamed->P4_sr_v_info));
	
	// Link and fifo statuses
	setupCollection('1',1,0x30<<16,0x46<<16,0x10,"Input1 Aux Spy Buffer Reading: [0x35->0x33]","AuxI1_sb",
				&(m_srAuxSNamed->I1_sb_v),&(m_srAuxSNamed->I1_sb_v_info)); 
	setupCollection('2',2,0x30<<16,0x46<<16,0x10,"Input2 Aux Spy Buffer Reading: [0x35->0x36]","AuxI2_sb",
				&(m_srAuxSNamed->I2_sb_v),&(m_srAuxSNamed->I2_sb_v_info)); 
	setupCollection('3',3,0x48<<16,0x5a<<16,0x10,"Processor1 Aux Status Register Reading: [0x52<<16->0x5a<<16]","AuxI3",
				&(m_srAuxSNamed->P1_sb_v),&(m_srAuxSNamed->P1_sb_v_info));
	setupCollection('4',4,0x48<<16,0x5a<<16,0x10,"Processor2 Aux Status Register Reading: [0x52<<16->0x5a<<16]","AuxP2",
				&(m_srAuxSNamed->P2_sb_v),&(m_srAuxSNamed->P2_sb_v_info));
	setupCollection('5',5,0x48<<16,0x5a<<16,0x10,"Processor3 Aux Status Register Reading: [0x52<<16->0x5a<<16]","AuxP3",
				&(m_srAuxSNamed->P3_sb_v),&(m_srAuxSNamed->P3_sb_v_info));
	setupCollection('6',6,0x48<<16,0x5a<<16,0x10,"Processor4 Aux Status Register Reading: [0x52<<16->0x5a<<16]","AuxP4",
				&(m_srAuxSNamed->P4_sb_v),&(m_srAuxSNamed->P4_sb_v_info));
	

    setupCollection('1',1,0x05<<16,0x08<<16,0x10,"Input1 Aux Spy Buffer Reading: [0x05->0x08]","AuxI1_cc",
            &(m_srAuxSNamed->I1_cc_v),&(m_srAuxSNamed->I1_cc_v_info)); //cluster, module, and SSID counts
    setupCollection('2',2,0x05<<16,0x08<<16,0x10,"Input2 Aux Spy Buffer Reading: [0x05->0x08]","AuxI2_cc",
            &(m_srAuxSNamed->I2_cc_v),&(m_srAuxSNamed->I2_cc_v_info)); //cluster, module, and SSID counts

    setupCollection('3',3,0x58<<16,0x580700,0x100,"P1 fit limit counts","AuxP1_fl",
            &(m_srAuxSNamed->P1_fl_v),&(m_srAuxSNamed->P1_fl_v_info)); //P1 fit limit counts
    setupCollection('4',4,0x58<<16,0x580700,0x100,"P2 fit limit counts","AuxP2_fl",
            &(m_srAuxSNamed->P2_fl_v),&(m_srAuxSNamed->P2_fl_v_info)); //P4 fit limit counts
    setupCollection('5',5,0x58<<16,0x580700,0x100,"P3 fit limit counts","AuxP3_fl",
            &(m_srAuxSNamed->P3_fl_v),&(m_srAuxSNamed->P3_fl_v_info)); //P4 fit limit counts
    setupCollection('6',6,0x58<<16,0x580700,0x100,"P4 fit limit counts","AuxP4_fl",
            &(m_srAuxSNamed->P4_fl_v),&(m_srAuxSNamed->P4_fl_v_info)); //P4 fit limit counts

    setupCollection('3',3,0x5c<<16,0x5c0700,0x100,"P1 combiner overflow counts","AuxP1_co",
            &(m_srAuxSNamed->P1_co_v),&(m_srAuxSNamed->P1_co_v_info)); //P1 combiner overflow counts
    setupCollection('4',4,0x5c<<16,0x5c0700,0x100,"P2 combiner overflow counts","AuxP2_co",
            &(m_srAuxSNamed->P2_co_v),&(m_srAuxSNamed->P2_co_v_info)); //P4 combiner overflow counts
    setupCollection('5',5,0x5c<<16,0x5c0700,0x100,"P3 combiner overflow counts","AuxP3_co",
            &(m_srAuxSNamed->P3_co_v),&(m_srAuxSNamed->P3_co_v_info)); //P4 combiner overflow counts
    setupCollection('6',6,0x5c<<16,0x5c0700,0x100,"P4 combiner overflow counts","AuxP4_co",
            &(m_srAuxSNamed->P4_co_v),&(m_srAuxSNamed->P4_co_v_info)); //P4 combiner overflow counts
    setupCollection('3',3,0x5c<<16,0x5c0700,0x100,"P1 combiner overflow counts","AuxP1_co",
            &(m_srAuxSNamed->P1_co_v),&(m_srAuxSNamed->P1_co_v_info)); //P1 combiner overflow counts
    setupCollection('4',4,0x5c<<16,0x5c0700,0x100,"P2 combiner overflow counts","AuxP2_co",
            &(m_srAuxSNamed->P2_co_v),&(m_srAuxSNamed->P2_co_v_info)); //P4 combiner overflow counts
    setupCollection('5',5,0x5c<<16,0x5c0700,0x100,"P3 combiner overflow counts","AuxP3_co",
            &(m_srAuxSNamed->P3_co_v),&(m_srAuxSNamed->P3_co_v_info)); //P4 combiner overflow counts
    setupCollection('6',6,0x5c<<16,0x5c0700,0x100,"P4 combiner overflow counts","AuxP4_co",
            &(m_srAuxSNamed->P4_co_v),&(m_srAuxSNamed->P4_co_v_info)); //P4 combiner overflow counts



    // Pass the StatusRegisterAuxStatusNamed to SRVMEFactory so it can be checked into IS during readout()
	m_srxNamed = std::move(m_srAuxSNamed);
}

StatusRegisterAuxFactory::~StatusRegisterAuxFactory() {
	// No need to delete m_srAuxSNamed here as it's done by StatusRegisterVMEFactory when it deletes m_srxNamed
}
/*
// Unused. Will need to use something like this to reduce the number of registers published to IS
void StatusRegisterAuxFactory::setupAuxCollections(std::unique_ptr<StatusRegisterAuxStatusNamed> &m_srAuxSNamed) {
    std::unique_ptr<aux_registers> reg_getter(new aux_registers()); // Deleted when function returns
    std::map<std::string, std::map<std::string,uint> > ISRegs; // First string gives FPGA, second gives name of register, last gives the address
    ISRegs = reg_getter->getISRegs(); // Map of all the registers to do in setupCollection

    for (auto const& RegMapEntry : ISRegs.at("I1")) {
        setupCollection('1',1,RegMapEntry.second,RegMapEntry.second+1,0x10, RegMapEntry.first, "I1 Regs", // setupCollection takes range & stepSize. Very hacky solution to publish only one register
				            &(m_srAuxSNamed->I1_sr_v),&(m_srAuxSNamed->I1_sr_v_info));
    }

    // Input 2 registers    
    for (auto const& RegMapEntry : ISRegs.at("I2")) {
        setupCollection('2',2,RegMapEntry.second,RegMapEntry.second+1,0x10, RegMapEntry.first, "I2 Regs",
				            &(m_srAuxSNamed->I2_sr_v),&(m_srAuxSNamed->I2_sr_v_info));
    }

    // Proc registers
    for (auto const& RegMapEntry : ISRegs.at("Proc")) {
        setupCollection('3',3,RegMapEntry.second,RegMapEntry.second+1,0x10, RegMapEntry.first,"Proc1 Regs",
				            &(m_srAuxSNamed->P1_sr_v),&(m_srAuxSNamed->P1_sr_v_info));
        setupCollection('4',4,RegMapEntry.second,RegMapEntry.second+1,0x10, RegMapEntry.first,"Proc2 Regs",
				            &(m_srAuxSNamed->P2_sr_v),&(m_srAuxSNamed->P2_sr_v_info));
        setupCollection('5',5,RegMapEntry.second,RegMapEntry.second+1,0x10, RegMapEntry.first,"Proc3 Regs",
				            &(m_srAuxSNamed->P3_sr_v),&(m_srAuxSNamed->P3_sr_v_info));
        setupCollection('6',6,RegMapEntry.second,RegMapEntry.second+1,0x10, RegMapEntry.first,"Proc4 Regs",
				            &(m_srAuxSNamed->P4_sr_v),&(m_srAuxSNamed->P4_sr_v_info));
    }
}*/

} // namespace ftk
} // namespace daq

/*           Program to issue a routine for read the AUX RAM                   */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */
#include "aux/aux.h"
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("source",value<std::string>()->default_value("vme"), "Where to get the buffers from (vme, emon, txt)")
    ("fpga", value< std::string >()->default_value("1"),  "The FPGA ID")
    ("dim_buff", value< std::string >()->default_value("0"),  "depth of buffer 2**(dim_buff)")
    ("buffer", value< std::vector<std::string> >(), "Buffer to read")
    ;
  
  positional_options_description p;
  p.add("buffer", -1);

  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  int slot         = daq::ftk::string_to_int( vm["slot"]    .as<std::string>() );
  int fpga         = daq::ftk::string_to_int( vm["fpga"]    .as<std::string>() );
  u_int dim_buffer = daq::ftk::string_to_int( vm["dim_buff"].as<std::string>() );
  std::string source  = vm["source"].as<std::string>();

  std::vector<u_int> buffer;
  for(std::vector<std::string>::const_iterator bufferIter=vm["buffer"].as<std::vector<std::string> >().begin();
      bufferIter!=vm["buffer"].as<std::vector<std::string> >().end();
      bufferIter++)
    buffer.push_back(daq::ftk::string_to_int( *bufferIter ));
  
  return  daq::ftk::aux_read_buffer(slot, fpga, dim_buffer, buffer, source) ;
}

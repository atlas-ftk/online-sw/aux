#include "aux/TF/TFRoadBuilder.h"

#include "aux/TF/TFGen.h"
#include "aux/AuxUtils.h"

namespace daq {
  namespace ftk {
    TFRoadBuilder::TFRoadBuilder()
    { }

    TFRoadBuilder::~TFRoadBuilder()
    { }

    void TFRoadBuilder::addSectorID(unsigned int sectorID)
    { _SectorIDs.push_back(sectorID); }

    void TFRoadBuilder::addRoadID(unsigned int roadID)
    { _RoadIDs.push_back(roadID); }

    void TFRoadBuilder::addLayermap(unsigned int layermap)
    { _Layermaps.push_back(layermap); }

    void TFRoadBuilder::addGuessmap(unsigned int guessmap)
    { _Guessmaps.push_back(guessmap); }

    void TFRoadBuilder::addHitWord(unsigned int ilayer, Int_tf hit, bool eor)
    {
      _HitWords[ilayer].push_back(tf::HitWord(hit,eor));
    }

    std::vector<TFRoad*> TFRoadBuilder::generateRoads()
    {
      std::vector<TFRoad*> vec_roads;

      // basic validity checks
      report::printassert( _SectorIDs.size()==_RoadIDs.size()   , "Trying to process event but NSectorIDs=%i != NRoadIDs=%i"   , _SectorIDs.size(), _RoadIDs.size()   );
      report::printassert( _SectorIDs.size()==_Layermaps.size() , "Trying to process event but NSectorIDs=%i != NLayermaps=%i" , _SectorIDs.size(), _Layermaps.size() );
      report::printassert( _SectorIDs.size()==_Guessmaps.size() , "Trying to process event but NSectorIDs=%i != NGuessmaps=%i" , _SectorIDs.size(), _Guessmaps.size() );

      // ptrs to keep track of which hit to process next in each layer
      unsigned int hit_ptr[tf::NLayers];
      unsigned int max_ptr[tf::NLayers];
      for(unsigned int ilayer = 0; ilayer < tf::NLayers; ilayer++ )
	{
	  hit_ptr[ilayer] = 0;
	  max_ptr[ilayer] = _HitWords[ilayer].size();
	}

      for(unsigned int iroad = 0 ; iroad < _SectorIDs.size() ; iroad++ )
	{
	  TFRoad *tmp_road = new TFRoad();
	  tmp_road->setID      ( _RoadIDs  [iroad] );
	  tmp_road->setSectorID( _SectorIDs[iroad] );
	  tmp_road->setLayermap( _Layermaps[iroad] );
	  tmp_road->setGuessmap( _Guessmaps[iroad] );

	  for(unsigned int ilayer = 0; ilayer < tf::NLayers; ilayer++ )
	    {
	      if( aux::getBit(_Layermaps[iroad],ilayer)==1 )
		{
		  tf::HitWord tmp_hitword;
		  do
		    {
		      report::printassert(hit_ptr[ilayer]<max_ptr[ilayer],"Last hit does not have EOR");

		      tmp_hitword = _HitWords[ilayer][hit_ptr[ilayer]];

		      tmp_road->addHit( ilayer , tmp_hitword.hit );
		      hit_ptr[ilayer]++;
		    }
		  while(!tmp_hitword.eor);
		}
	    }

	  vec_roads.push_back( tmp_road );
	}

      // Clean up
      _SectorIDs.clear();
      _RoadIDs  .clear();
      _Layermaps.clear();

      return vec_roads;
    }

    void TFRoadBuilder::print()
    {
      report::blank( "" );
      report::info ( "Printing roads...");
      report::blank( "Sector IDs:" );
      for(unsigned int i = 0 ; i < _SectorIDs.size() ; i++ )
	{
	  report::blank( "%12i" , _SectorIDs[i] );
	}
      report::blank( "Road IDs:" );
      for(unsigned int i = 0 ; i < _RoadIDs.size() ; i++ )
	{
	  report::blank( "%12i" , _RoadIDs[i] );
	}
      report::blank( "Layermaps:" );
      for(unsigned int i = 0 ; i < _Layermaps.size() ; i++ )
	{
	  report::blank( "%12s" , tf::getLayermapStr(_Layermaps[i]).c_str() );
	}
      report::blank( "HitWords:" );
      for(unsigned int i = 0 ; i < tf::getMaxPerLayer(_HitWords) ; i++ )
	{
	  std::string line = "";
	  for(unsigned int ilayer = tf::NLayers-1; ilayer+1 > 0; ilayer--)
	    {
	      std::string hitstr = i < _HitWords[ilayer].size() ? tf::getHitWordStr(_HitWords[ilayer][i]) : "";
	      line = auxstring::Format( "%s %14s" , line.c_str() , hitstr.c_str() );
	    }
	  report::blank( line );
	}

      report::blank( "" );
    }
  } // namespace ftk
} // namespace daq

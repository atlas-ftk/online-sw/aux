#include "aux/Report.h"

namespace report
{
  unsigned int dbg_lvl = 0;

  void vreport(const std::string& tag, const char *fmt , va_list ap , bool incl_space )
  {
    if( suppress_output ) return;
    std::cout << tag;
    vprintf( fmt , ap );
    if( incl_space ) std::cout << std::endl;
  }

  void vreport(const std::string& tag, const std::string& str )
  {
    if( suppress_output ) return;
    std::cout << tag << str << std::endl;
  }

  void vreport_color(const std::string& color, const std::string& tag, const char *fmt, va_list ap, bool incl_space)
  {
    if( suppress_output ) return;
    // coloring doesn't work on local machine, so overriding
    vreport( tag , fmt , ap , incl_space );
    return;
    std::string echocommand = auxstring::Format( "echo -e -n \"${%s}%s${FONTNONE}\"" , color.c_str() , tag.c_str() );
    std::system( echocommand.c_str() );
    vprintf( fmt , ap );
    if( incl_space ) std::cout << std::endl;
  }

  void vreport_color(const std::string& color, const std::string& tag, const std::string& str )
  {
    if( suppress_output ) return;
    // coloring doesn't work on local machine, so overriding
    vreport( tag , str );
 }

  void info(const char *fmt , ... )  { va_list ap; va_start(ap,fmt); vreport("INFO  : ",fmt,ap); }
  void info(const std::string& str )  { vreport("INFO  : ",str); }
  void exam(const char *fmt , ... )  { va_list ap; va_start(ap,fmt); vreport_color("FONTPURPLE","EXAM  : ",fmt,ap); }
  void exam(const std::string& str )  { vreport_color("FONTPURPLE","EXAM  : ",str); }
  void warn(const char *fmt , ... )  { va_list ap; va_start(ap,fmt); vreport_color("FONTYELLOW","WARN  : ",fmt,ap); }
  void warn(const std::string& str )  { vreport_color("FONTYELLOW","WARN  : ",str); }
  void error(const char *fmt , ... ) { va_list ap; va_start(ap,fmt); vreport_color("FONTRED","ERROR : ",fmt,ap); }
  void error(const std::string& str ) { vreport_color("FONTRED","ERROR : ",str); }
  void blank(const char *fmt , ... ) { va_list ap; va_start(ap,fmt); vreport("  ",fmt,ap); }
  void blank(const std::string& str ) { vreport("  ",str); }
  void debug(const char *fmt , ... )  { if(dbg_lvl==0) return; va_list ap; va_start(ap,fmt); vreport_color("FONTCYAN","DEBUG : ",fmt,ap); }
  void debug(const std::string& str ) { if(dbg_lvl==0) return; vreport_color("FONTCYAN","DEBUG : ",str); }
  void line() { std::cout << std::endl; }

  void spaceprint( const char *fmt , ... ) { va_list ap; va_start(ap,fmt); vreport("      > ",fmt,ap); }

  void debug( int lvl , const char *fmt , ... )
  {
    if( lvl > dbg_lvl ) return;
    va_list ap; va_start(ap,fmt); vreport("DEBUG : ",fmt,ap); 
  }

  void warnif( bool cond , const char *fmt , ... )
  {
    if( ! cond ) return;
    va_list ap; va_start(ap,fmt); vreport_color("FONTYELLOW","WARN  : ",fmt,ap);
  }

  void printassert( bool cond , const char* fmt , ... )
  {
    if( cond ) return;
    va_list ap; va_start(ap,fmt); vreport("ASSERT FAILED : ",fmt,ap);
    assert( false );
  }

  void title( const char *_info , const char *_exam )
  {
    info( _info );
    exam( _exam );
  }

}

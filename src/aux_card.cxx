#include "aux/aux_card.h"
#include "aux/aux_spy_regs.h"


namespace daq {
    namespace ftk {


        aux::aux(unsigned int slot, unsigned int crate) :
            m_slot(slot),
            m_source_hits("df"),m_source_roads(0xF)
        {

            ERS_LOG("Creating AUX in slot " << m_slot << ".");

            std::stringstream name;
            name << "FTK-AUX-" << std::to_string(crate) << "-" << std::to_string(slot);
            m_name = name.str();

            m_i1=std::make_shared<input1>(m_slot, m_name);
            ERS_LOG("Created Input1");
            m_i2=std::make_shared< input2>(m_slot, m_name);
            ERS_LOG("Created Input2");
            for (uint x = 3; x <= 6; x++) {
                m_procs.push_back(shared_ptr<proc>(std::make_shared<proc>(m_slot, x, m_name)));
                ERS_LOG("Created Processor"<<x-3 << " with name " << m_name);
            }
            // Make list of all FPGAs for common functionality
            m_fpgas.push_back(m_i1);
            m_fpgas.push_back(m_i2);
            m_aux_interface_inputs.push_back(m_i1);
            m_aux_interface_inputs.push_back(m_i2);
            for(auto proc : m_procs){
                m_fpgas.push_back(proc);
                m_fpgas_procsFirst.push_back(proc);
                m_aux_interface_procs.push_back(proc);
            }
            m_fpgas_procsFirst.push_back(m_i2);
            m_fpgas_procsFirst.push_back(m_i1);

            // Default configuration
            m_holdIgnoreProc=false;
            m_forceHoldProcOut=false;
            m_holdIgnoreAMB=false;      
            m_holdIgnoreHW=false;
            m_holdIgnoreSSB=false;
            m_allowFreezeFromProcsToInputs=true;

            m_enableAMBFreeze=false;

            m_enable_hitsort=false;
            ERS_LOG("Created AUX in slot " << m_slot << ".");

        }


        aux::~aux() noexcept {
        }

        void aux::setup(const dal::ReadoutModule_PU * module_dal) {
            bool disable_block_writes = module_dal->get_DisableBlockWrites();
            for(auto fpga : m_fpgas) fpga->disable_block_writes(disable_block_writes);

            // Hold ignores
            m_holdIgnoreProc  =module_dal->get_HoldIgnoreProc();
            m_forceHoldProcOut=module_dal->get_ForceHoldProcOut();
            m_holdIgnoreAMB   =module_dal->get_HoldIgnoreAMB();
            m_holdIgnoreHW    =module_dal->get_HoldIgnoreHW();
            m_holdIgnoreSSB   =module_dal->get_HoldIgnoreSSB();
            m_allowFreezeFromProcsToInputs =module_dal->get_AllowFreezeFromProcsToInputs();

            m_enableAMBFreeze = module_dal->get_EnableAMBFreeze();

            m_enable_hitsort  = module_dal->get_Enable_hitsort();
            m_ignore_freeze   = module_dal->get_Ignore_Freeze();
            m_ignore_SSB_freeze =module_dal->get_Ignore_SSB_Freeze();

            m_ProcFreezeMask= module_dal->get_ProcFreezeMask();
            m_I1FreezeMask  = module_dal->get_I1FreezeMask();
            m_I2FreezeMask  = module_dal->get_I2FreezeMask();
            m_HWFreezeMask  = module_dal->get_HWFreezeMask();
            //m_name	       = module_dal->get_Name();
            m_crate          = module_dal->get_Crate();

            m_MaxFitsPerRoad = module_dal->get_MaxFitsPerRoad();
            m_MaxRoadsPerEvent = module_dal->get_MaxRoadsPerEvent();


            // firmware versions
            ERS_LOG("Get Expected Firmware Versions");
            std::string FPGATitles[6]={"Input 1",
                "Input 2",
                "Processor 1",
                "Processor 2",
                "Processor 3",
                "Processor 4"};

            VMEInterface *input1=VMEManager::global().aux(m_slot,1);
            VMEInterface *input2=VMEManager::global().aux(m_slot,2);
            VMEInterface *proc1 =VMEManager::global().aux(m_slot,3);
            VMEInterface *proc2 =VMEManager::global().aux(m_slot,4);
            VMEInterface *proc3 =VMEManager::global().aux(m_slot,5);
            VMEInterface *proc4 =VMEManager::global().aux(m_slot,6);

            VMEInterface *fpgas[6]={input1,input2,proc1,proc2,proc3,proc4};


            const daq::ftk::dal::firmware *firmware = module_dal->get_firmwareVersions();
            if(firmware!=0){

                ERS_LOG("firmware expected on I1 " << std::hex << firmware->get_Input1());
                //	bool requireMatch = firmware->get_requireMatch();
                //	ERS_LOG("Require firmware version: "<<requireMatch);
                //	for(auto fpga : m_fpgas) fpga->set_requireMatch(requireMatch);

                m_fpgas[0]->set_firmware(firmware->get_Input1());
                m_fpgas[1]->set_firmware(firmware->get_Input2());
                m_fpgas[2]->set_firmware(firmware->get_Processor1());
                m_fpgas[3]->set_firmware(firmware->get_Processor2());
                m_fpgas[4]->set_firmware(firmware->get_Processor3());
                m_fpgas[5]->set_firmware(firmware->get_Processor4());
                ERS_LOG("Set Expected Firmware Versions");
            }

            for(unsigned int fpgaidx=0;fpgaidx<6;fpgaidx++)
            {

                ERS_LOG("Firmware on " << FPGATitles[fpgaidx] << ":\t 0x" << std::hex << fpgas[fpgaidx]->read_word(INFO) << "\t (" << fpgas[fpgaidx]->read_word(CHIPID1) << fpgas[fpgaidx]->read_word(CHIPID2) << ") Git (FPGA): " << fpgas[fpgaidx]->read_word(GIT_FPGA) <<   " Git (AUXCommon): " << fpgas[fpgaidx]->read_word(GIT_AUXCOMMON) << " Clock: " << std::dec << fpgas[fpgaidx]->read_word(CLOCK_FREQUENCY) << " (MHz) "   );



            }

            m_towerID = module_dal->get_towerID();

            // Event rates
            uint event_rate=module_dal->get_EventRate();
            for(auto fpga : m_fpgas) {
                fpga->set_event_rate(event_rate);
                fpga->set_isLoadMode(daq::ftk::isLoadMode());
            }

            const daq::ftk::dal::AuxConstants *consts = module_dal->get_AMConstants();
            const daq::ftk::dal::PatternBankConf *PatternBank_dal = module_dal->get_PatternBankConf();

            // AMmap
            if(consts!=0 || (PatternBank_dal!=0 && PatternBank_dal->get_FilePath() != ""))
            {

                if (PatternBank_dal){
                    ERS_LOG("Using PatternBankConf class");
                    ERS_LOG("Pattern Bank = " << PatternBank_dal->get_FilePath() << " NumPattPerChip: " << PatternBank_dal->get_NumPattPerChip() << " InChipOff: " << PatternBank_dal->get_InChipOff() );
                    set_FTKPatternBank();
                    set_am_constants(PatternBank_dal->get_FilePath());
                    set_NPattPerChip(PatternBank_dal->get_NumPattPerChip());
                    set_banktype(PatternBank_dal->get_BankType());
                    set_ssoffset(PatternBank_dal->get_SSOffset());
                    set_InChipOff(PatternBank_dal->get_InChipOff());
                    m_procs[0]->m_bankChecksum = PatternBank_dal->get_Proc1Checksum();
                    m_procs[1]->m_bankChecksum = PatternBank_dal->get_Proc2Checksum();
                    m_procs[2]->m_bankChecksum = PatternBank_dal->get_Proc3Checksum();
                    m_procs[3]->m_bankChecksum = PatternBank_dal->get_Proc4Checksum();
                }else{
                    ERS_LOG("Using aux_constants class. TIME TO UPDATE!");
                    ERS_LOG("AM Constants = " << consts->get_Path());
                    set_am_constants(consts->get_Path());
                    set_NPattPerChip(2048);
                    set_banktype(1);
                    set_ssoffset(0);
                    set_InChipOff(1);
                }
            }
            else
            {
                ERS_LOG("No AM Constants");
            }

            // SSmap (local)
            for(std::vector<const daq::ftk::dal::AuxConstants*>::const_iterator citer = module_dal->get_SSMapPath().begin();
                    citer != module_dal->get_SSMapPath().end();
                    citer++ )
            {
                consts =(*citer);
                ERS_LOG("SS Map = " << consts->get_Index() << "  " << consts->get_Path() << "  " << std::hex << consts->get_Checksum());
                set_ss_map_path(consts->get_Index(), consts->get_Path(), consts->get_Checksum());
            }

            // SSmap (modules)
            for(std::vector<const daq::ftk::dal::AuxConstants*>::const_iterator citer = module_dal->get_ModuleMap().begin();
                    citer != module_dal->get_ModuleMap().end();
                    citer++ )
            {
                consts = (*citer);
                ERS_LOG("Module Map = " << consts->get_Index() << "  " << consts->get_Path() << "  " << std::hex << consts->get_Checksum());
                set_module_path(consts->get_Index(), consts->get_Path(), consts->get_Checksum());
            }

            // TF sector constants
            for(std::vector<const daq::ftk::dal::AuxConstants*>::const_iterator citer  = module_dal->get_TFConstants().begin();
                    citer != module_dal->get_TFConstants().end();
                    citer++ )
            {
                consts = (*citer);
                ERS_LOG("TF Constants = " << consts->get_Index() << "  " << consts->get_Path() << "  " << std::hex << consts->get_Checksum());
                set_loadAllSectors(consts->get_Index(), consts->get_LoadAllSectors());
                set_tf_constants(consts->get_Index(), consts->get_Path(), consts->get_Checksum());
            }

            set_checksumWarning(module_dal->get_ChecksumWarning());

            // Testvectors
            const daq::ftk::dal::AuxTestParams *test = module_dal->get_AuxTestSetup();
            if (test)
            {
                set_loop     (test->get_Loop());
                set_road_path(test->get_RoadPath());
                set_hit_path (test->get_HitPath());
                set_ss_path  (test->get_SSPath());

                set_source_hits (module_dal->get_SourceHits());
                set_source_roads(module_dal->get_SourceRoads());
            }

            // QSFP passthrough
            m_i2->set_source_sfp(module_dal->get_SourceSFP());
        }


        void aux::checkConfig(){
            for(auto fpga : m_aux_interface_procs)
                fpga->check_fw_versions();
            for(auto fpga : m_aux_interface_inputs)
                fpga->check_fw_versions();
        }



        // Run Control FSMs //
        void aux::configure()
        {
            enable_proc_to_input_freeze(false);
            for(auto fpga :  m_fpgas)
                fpga->reset_freeze();

            for(auto fpga :  m_fpgas){
                fpga->configure();

                //check proc RSLB lines
                try{
                    if(fpga->get_fpga_idx() > 2){
                        for(uint i = 0; i<10; i++){
                            if(m_procs[fpga->get_fpga_idx()-3]->check_link_status(i)) break;
                            ERS_LOG("Resetting xcvr of previous fpga");
                            m_fpgas[fpga->get_fpga_idx()-1]->reset_xcvr();
                            usleep(1e5);

                            if(fpga->get_fpga_idx() > 3){//toggle RSLB on previous proc
                                m_procs[fpga->get_fpga_idx()-4]->enable_rslb();
                                usleep(1e5);
                            }

                            ERS_LOG("Resetting sync of this fpga");
                            fpga->reset_sync();
                            usleep(1e5);
                        }
                    }
                }
                catch(ers::Issue &ex){
                    daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Processor link down", ex);
                    throw e;
                }

                for(auto input : m_aux_interface_inputs) input->check_freeze();
            }

            m_FTKPatternBank.reset(); // So that if delete again in destructor, no memory corruption
            
            try{
                ERS_LOG("Loading TVs");
                load_tvs();
            }
            catch(ers::Issue &ex){
                daq::ftk::IOError e(ERS_HERE, name_ftk(), "Unable to open TV File", ex);
                throw e;
            }

            enable_hitsort(m_enable_hitsort);

            enable_amb_freeze(m_enableAMBFreeze);

            set_force_hold_proc_out(m_forceHoldProcOut);
            set_hold_ignore_proc(m_holdIgnoreProc);
            set_hold_ignore_ssb (m_holdIgnoreSSB);
            set_hold_ignore_hw  (m_holdIgnoreHW);
            set_hold_ignore_amb (m_holdIgnoreAMB);
            set_ignore_freeze   (m_ignore_freeze);
            set_ignore_SSB_freeze(m_ignore_SSB_freeze);
            set_freeze_mask     (m_ProcFreezeMask, m_I1FreezeMask, m_I2FreezeMask, m_HWFreezeMask);
            set_MaxFitsPerRoad (m_MaxFitsPerRoad);
            set_MaxRoadsPerEvent (m_MaxRoadsPerEvent);


            for(auto fpga : m_fpgas) fpga->check_freeze();

            bool safe = m_i2->CheckAUXbusLockCode();

            if (m_allowFreezeFromProcsToInputs && safe) {
                for (auto proc : m_procs) proc->UnlockProcsDriveAUXbus();
                enable_proc_to_input_freeze(true);
                ERS_LOG("Allowing FREEZE to propagate from procs to inputs.");
            } else {
                ERS_LOG("WARNING: FREEZE from procs to Inputs is not enabled.");
            }

            for(auto fpga : m_fpgas) fpga->check_freeze();

            //towerID
            m_procs[0]->set_towerID(m_towerID);
            m_procs[1]->set_towerID(m_towerID);
            m_procs[2]->set_towerID(m_towerID);
            m_procs[3]->set_towerID(m_towerID);

            for(auto fpga :  m_fpgas_procsFirst)
                fpga->reset_freeze();
            for(auto fpga :  m_fpgas_procsFirst)
                fpga->reset_spybuffers();
            for(auto fpga :  m_fpgas_procsFirst)
                fpga->reset_error_counters();

            for(auto fpga : m_fpgas) fpga->check_freeze();

        }

        void aux::connect()
        {
            m_ftkemonDataOut = reinterpret_cast< daq::ftk::FtkEMonDataOut* >(ROS::DataOut::sampled());

            ERS_LOG("Enter");
            for(auto fpga : m_aux_interface_procs){
                fpga->enable_upstream_rx(true);
            }
            if(m_source_hits == "df"){//Only Enable QSFP Rx if running from DataFormatter inputs
                for(auto fpga : m_aux_interface_inputs)
                    fpga->enable_upstream_rx(true);
            }

            for(auto fpga :  m_fpgas){
                fpga->connect();
            }
            try{
                for(auto fpga :  m_fpgas){
                    for(int i = 0; i < 10; i++){
                        if     (fpga->get_fpga_idx() == 1){
                            if(i==0) usleep(1e5);//first fpga to be checked takes a bit to be ready
                            if(m_i1->check_link_status(i)) break;
                        }
                        else if(fpga->get_fpga_idx() == 2){
                            if(m_i2->check_link_status(i)) break;
                        }
                        else{
                            if(m_procs[fpga->get_fpga_idx()-3]->check_link_status(i)) break;
                        }
                        fpga->connect();
                        usleep(1e5);
                    }
                }
            }
            catch(ers::Issue &ex){
                daq::ftk::IOError e(ERS_HERE, name_ftk(), "Link down", ex);
                throw e;
            }

            for(auto fpga : m_fpgas) fpga->check_freeze();

        }

        // void aux::reset_links_to_amb()
        // {
        //   ERS_LOG("AMB requested link reset - slot "<<m_slot);
        //   m_i1->reset_xcvr();
        //   m_i2->reset_xcvr();
        //   m_i1->reset_sync();
        //   m_i2->reset_sync();
        //   m_aux_interface_procs[0]->reset_sync();
        // }

        void aux::prepareForRun()
        {
            // we can only check this here, but probably don't want to
            // for(uint i=0; i<10; i++) if(m_i2->check_sfp_status(i)) break;
            //for(auto fpga : m_fpgas) fpga->check_freeze();
            for(auto fpga :  m_fpgas_procsFirst)
                fpga->set_downstream_flow(true);
            for(auto fpga :  m_fpgas_procsFirst)
                fpga->set_upstream_flow(true); 

            send_tvs();
        }

        void aux::stop()
        {
            //Truncate last event and reset. 
            for(auto fpga :  m_fpgas)
                fpga->set_upstream_flow(false);

            //reset SimpleSimplex/SSMap/HitSort/DFSynch/DO/TF/HW
            for(auto fpga : m_fpgas)
                fpga->reset_logic();

            for(auto fpga :  m_fpgas_procsFirst)
                fpga->reset_spybuffers();
            // for(auto fpga :  m_fpgas_procsFirst)
            // 	fpga->reset_error_counters();
            for(auto fpga :  m_fpgas_procsFirst)
                fpga->reset_freeze();
            for(auto fpga :  m_fpgas_procsFirst)
                fpga->reset_error_latches();

            for(auto fpga : m_fpgas)
                fpga->reset_logic();
        }

        void aux::publishFullStats()
        {           

            std::vector< std::pair< std::shared_ptr<SpyBuffer>, uint32_t> >  vec_AuxSpyBuffer;
            vec_AuxSpyBuffer.clear();


            int iBuffer = 0;

            //input channels

            ERS_LOG("Reading AUX Input 1 SpyBuffers");
            for(uint ich=0; ich<aux_i1_all.size(); ich++){
                unsigned int channel = aux_i1_all[ich];
                std::shared_ptr< daq::ftk::AuxSpyBuffer > buff = std::make_shared< daq::ftk::AuxSpyBuffer >(1, channel, iBuffer, m_slot, m_crate);

                std::pair< std::shared_ptr<SpyBuffer>, uint32_t> pair2ship = make_pair(buff, buff->getSourceID());
                vec_AuxSpyBuffer.push_back( pair2ship);
                iBuffer++;
            }

            ERS_LOG("Reading AUX Input 2 SpyBuffers");
            for(uint ich=0; ich<aux_i2_all.size(); ich++){
                unsigned int channel = aux_i2_all[ich];
                std::shared_ptr< daq::ftk::AuxSpyBuffer > buff = std::make_shared< daq::ftk::AuxSpyBuffer >(2, channel, iBuffer, m_slot, m_crate);

                std::pair< std::shared_ptr<SpyBuffer>, uint32_t> pair2ship = make_pair(buff, buff->getSourceID());
                vec_AuxSpyBuffer.push_back( pair2ship);
                iBuffer++;
            }

            for(uint ip=0; ip<4; ip++){
                ERS_LOG("Reading AUX Processor " << ip << " SpyBuffers");

                for(uint ich=0; ich<aux_proc_all.size(); ich++){

                    unsigned int channel = aux_proc_all[ich];
                    std::shared_ptr< daq::ftk::AuxSpyBuffer > buff = std::make_shared< daq::ftk::AuxSpyBuffer >(ip+3, channel, iBuffer, m_slot, m_crate);

                    std::pair< std::shared_ptr<SpyBuffer>, uint32_t> pair2ship = make_pair(buff, buff->getSourceID());
                    vec_AuxSpyBuffer.push_back( pair2ship);
                    iBuffer++;
                }
            }

            //ship them to EMon
            ERS_LOG("Shipping " << vec_AuxSpyBuffer.size() << " aux spybuffers to EMon");
            if ( vec_AuxSpyBuffer.size() == 0){
                daq::ftk::FTKInfoNotAcquired ex(ERS_HERE, name_ftk(), "Error reading AUX SpyBuffers");
                ers::warning(ex);
            }
            else{
                m_ftkemonDataOut->sendData(vec_AuxSpyBuffer);
            }

        }

        void aux::check_links()
        {
            //-1 just checks once and issues a warning
            try{
                for(auto fpga :  m_fpgas){
                    if     (fpga->get_fpga_idx() == 1){
                        usleep(1e5);//first fpga to be checked takes a bit to be ready
                        if(m_i1->check_link_status(-1));
                    }
                    else if(fpga->get_fpga_idx() == 2){
                        if(m_i2->check_link_status(-1));
                    }
                    else{
                        if(m_procs[fpga->get_fpga_idx()-3]->check_link_status(-1));
                    }
                }
            }
            catch(ers::Issue &ex){
                daq::ftk::IOError e(ERS_HERE, name_ftk(), "Link down", ex);
                throw e;
            }


        }


        // Send a single event after running a non-loop test //
        void aux::loadAndSendEvent()
        {
            load_tvs();
            send_tvs();      
        }

        // Implementation //
        shared_ptr<proc> aux::p(uint x) {

            if (x == 0 || x > 4)
            {
                daq::ftk::FTKIssue ex(ERS_HERE, name_ftk(), "Invalid proc " + std::to_string(x));
                ers::warning(ex);
                return 0;
            }

            return m_procs.at(x-1);

        }

        void aux::set_loadAllSectors(int pidx, const bool l) 
        {
            p(pidx)->set_loadAllSectors(l);
        }

        void aux::set_tf_constants(int pidx, const std::string& s, uint cs) 
        {
            p(pidx)->set_tf_constants(s, cs);
        }

        void aux::set_ss_map_path(int layer, const std::string& s, uint cs) 
        {
            switch (layer) 
            {
                case 0: case 1: case 2: case 3:
                    m_i1->set_ss_map_path(layer, s, cs);
                    break;

                case 4: case 5: case 6: case 7:
                    m_i2->set_ss_map_path(layer, s, cs);
                    break;

                default:
                    ERS_LOG("WARNING: Requested to set SS Map for non-existent layer " << layer);
                    break;
            }
        }

        void aux::set_module_path(int layer, const std::string& s, uint cs) {

            switch (layer) {

                case 0: case 1: case 2: case 3:
                    m_i1->set_module_path(layer, s, cs);
                    break;

                case 4: case 5: case 6: case 7:
                    m_i2->set_module_path(layer, s, cs);
                    break;

                default:
                    ERS_LOG("WARNING: Requested to set module path for non-existent layer " << layer);
                    break;
            }

        }

        void aux::set_checksumWarning(bool warningOnly)
        {
            for(auto fpga : m_fpgas)
            {
                fpga->set_checksumWarning(warningOnly);
            }
        }

        // bool aux::check_link_status()
        // {
        //   for(auto fpga : m_fpgas)
        // 	{
        // 	  ERS_LOG("auto loop over m_fpgas?");
        // 	  return fpga->check_link_status();
        // 	}
        // }


        //THIS ISNT USED
        void aux::load_rams()
        {
            for(auto fpga : m_fpgas)
            {
                try{
                    fpga->load_rams(daq::ftk::isLoadMode());//calls load_rams in aux_interface which connects to the load_rams function in each fpga
                }
                catch(ers::Issue &ex){
                    daq::ftk::IOError e(ERS_HERE, name_ftk(), "Problem loading RAMs", ex);
                    if(isFatalMode) throw e;
                    else ers::error(e);
                }
            }
        }

        void aux::enable_hitsort(bool enable)
        {
            m_i1->enable_hitsort(enable);
        }

        void aux::reset_all()
        {
            for (auto f : m_fpgas) f->reset_all();
        }

        void aux::reset_main() {

            m_i1->reset_main();
            m_i2->reset_main();
            for (auto pi : m_procs)
                pi->reset_main();

        }

        void aux::reset_sync() {

            m_i1->reset_sync();
            m_i2->reset_sync();
            for (auto pi : m_procs)
                pi->reset_sync();

        }

        void aux::reset_spybuffers()
        {
            for(auto fpga : m_fpgas)
                fpga->reset_spybuffers();
        }

        void aux::reset_tf()
        {
            for(auto p : m_procs)
                p->reset_tf();
        }

        void aux::load_tvs()
        {
            // Reset necessary FIFOs
            m_i1->reset_hit_fifos();
            m_i2->reset_hit_fifos();
            for (auto p : m_procs) p->reset_hit_and_road_fifos();

            // Load the necesasry FIFOs
            if(m_source_roads == 0x0){
                for(u_int procIdx=0; procIdx<4; procIdx++){
                    try{
                        m_procs[procIdx]->load_road_fifos(m_road_path);
                    }
                    catch(ers::Issue &ex){
                        daq::ftk::IOError e(ERS_HERE, name_ftk(), "Unable to open TV File", ex);
                        throw e;
                    }
                }
            }

            if(m_source_hits=="ssmap") // from before input ssmap
            {
                try{
                    m_i1->load_hit_fifos(m_hit_path);
                    m_i2->load_hit_fifos(m_hit_path);
                }
                catch(ers::Issue &ex){
                    daq::ftk::IOError e(ERS_HERE, name_ftk(), "Unable to open TV File", ex);
                    throw e;
                }
            }
            else if (m_source_hits=="inptx") // hit and ssid fifos
            {
                try{
                    m_i1->load_hit_and_ssid_fifos(m_hit_path);
                    m_i2->load_hit_and_ssid_fifos(m_hit_path);
                }
                catch(ers::Issue &ex){
                    daq::ftk::IOError e(ERS_HERE, name_ftk(), "Unable to open TV File", ex);
                    throw e;
                }

                try{
                    m_i1->load_ssid_fifos(m_ss_path);
                    m_i2->load_ssid_fifos(m_ss_path);
                }
                catch(ers::Issue &ex){
                    daq::ftk::IOError e(ERS_HERE, name_ftk(), "Unable to open TV File", ex);
                    throw e;
                }
            }

        }


        void aux::send_tvs()
        {
            for(u_int procIdx=0; procIdx<4; procIdx++)
                if(!((m_source_roads>>procIdx)&1))//use road fifos if source roads is 0 for this proc.
                    m_procs[procIdx]->send_road_fifos(m_loop); 

            if(m_source_hits=="ssmap")
            {
                m_i1->send_hit_fifos(m_loop);
                m_i2->send_hit_fifos(m_loop);
            }
            else if (m_source_hits=="inptx")
            {
                m_i1->send_hit_and_ssid_fifos(m_loop);
                m_i2->send_hit_and_ssid_fifos(m_loop);
            }
        }

        void aux::enable_rslb()
        {
            for(auto p : m_procs)
                p->enable_rslb();
        }

        void aux::set_hold_ignore_proc(bool ignore)
        {
            m_i1->set_hold_ignore_proc(ignore);
            m_i2->set_hold_ignore_proc(ignore);
        }

        void aux::set_force_hold_proc_out(bool force)
        {
            m_i2->set_force_hold_proc_out(force);
        }

        void aux::set_hold_ignore_amb(bool ignore)
        {
            m_i1->set_hold_ignore_amb(ignore);
            m_i2->set_hold_ignore_amb(ignore);
        }

        void aux::set_hold_ignore_hw(bool ignore)
        {
            for(auto p : m_procs)
                p->set_hold_ignore_hw(ignore);
        }

        void aux::set_hold_ignore_ssb(bool ignore)
        {
            m_i2->set_hold_ignore_ssb(ignore);
        }

        void aux::set_ignore_freeze(bool ignore)
        {
            m_i1->set_ignore_freeze(ignore);
            m_i2->set_ignore_freeze(ignore);
            for(auto p : m_procs)
                p->set_ignore_freeze(ignore);
        }

        void aux::set_ignore_SSB_freeze(bool ignore)
        {
            m_i2->set_ignore_SSB_freeze(ignore);
        }

        void aux::set_freeze_mask(u_int ProcMask, u_int I1Mask, u_int I2Mask, u_int HWMask) {
            m_i1->set_freeze_mask(I1Mask);
            m_i2->set_freeze_mask(I2Mask, HWMask);
            for (auto p: m_procs)
                p->set_freeze_mask(ProcMask);
        }

        void aux::set_MaxFitsPerRoad(u_int MaxFitsPerRoad) {
            for (auto p: m_procs)
                p->set_MaxFitsPerRoad(MaxFitsPerRoad);
        }
        void aux::set_MaxRoadsPerEvent(u_int MaxRoadsPerEvent) {
            for (auto p: m_procs)
                p->set_MaxRoadsPerEvent(MaxRoadsPerEvent);
        }


        void aux::enable_proc_to_input_freeze(bool enable)
        {
            m_i1->enable_proc_to_input_freeze(enable);
            m_i2->enable_proc_to_input_freeze(enable);
        }

        void aux::enable_amb_freeze(bool enable)
        {
            for (auto fpga : m_fpgas)
                fpga->enable_amb_freeze(enable);
        }

        void aux::set_loop(bool b)
        {
            m_loop = b;
        }

        void aux::set_FTKPatternBank()
        {
            m_FTKPatternBank= std::make_unique<FTKPatternBank>();
            for (auto p : m_procs)
                p->set_FTKPatternBank(m_FTKPatternBank.get());
        }

        void aux::set_am_constants(const std::string& s)
        {
            for (auto p : m_procs)
                p->set_am_constants(s);
        }

        void aux::set_NPattPerChip(const unsigned int n)
        {
            for (auto p : m_procs)
                p->set_NPattPerChip(n);
        }

        void aux::set_banktype(const unsigned int t)
        {
            for (auto p : m_procs)
                p->set_banktype(t);
        }

        void aux::set_ssoffset(const bool o)
        {
            for (auto p : m_procs)
                p->set_ssoffset(o);
        }

        void aux::set_InChipOff(const unsigned int c)
        {
            for (auto p : m_procs)
                p->set_InChipOff(c);
        }

        void aux::set_road_path(const std::string& s)
        {
            m_road_path = s;
        }

        void aux::set_hit_path(const std::string& s)
        {
            m_hit_path = s;
        }

        void aux::set_ss_path(const std::string& s)
        {
            m_ss_path = s;
        }

        void aux::set_source_hits(std::string s)
        {
            m_source_hits = s;
            m_i1->set_source_hits(s);
            m_i2->set_source_hits(s);
        }

        void aux::set_source_roads(uint mask)
        {
            m_source_roads = mask;
            for(u_int procIdx=0; procIdx<4; procIdx++)
                m_procs[procIdx]->set_source_roads((bool)((mask>>procIdx)&1));
        }

        std::vector<u_int> aux::get_clock_freqs() {
            std::vector<u_int> clock_freqs;
            for (int fpgaidx = 0; fpgaidx < 6; fpgaidx++) 
                clock_freqs.push_back(m_fpgas[fpgaidx]->m_vme->read_word(CLOCK_FREQUENCY));
            return clock_freqs;
        }

        std::vector<u_int> aux::get_periods(){
            std::vector<u_int> periods;
            u_int period = 0;
            for(int fpgaidx = 0; fpgaidx < 6; fpgaidx++ ){
                period = m_fpgas[fpgaidx]->m_vme->read_word(MONITORING_PERIOD);
                ERS_LOG("Getting AUX sampling periods for FPGA " << fpgaidx << " : " << (period > 0 ? period : m_fpgas[fpgaidx]->m_vme->read_word(CLOCK_FREQUENCY)*1e5) );
                periods.push_back( (period > 0 ? period : m_fpgas[fpgaidx]->m_vme->read_word(CLOCK_FREQUENCY)*1e7) ); // in newer fw, write out the period to a register, if that hasn't been done, just do 1e5*clock freq (old default)
            }
            return periods;
        }

    }
}



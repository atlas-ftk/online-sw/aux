#include "aux/TF/TFFitter.h"

#include "aux/AuxUtils.h"

namespace daq {
  namespace ftk {
    TFFitter::TFFitter( TFConstants *_constants , TFEventList *_events ) 
      : constants(_constants), events( _events)
    {}

    /*! \brief Destructor, does nothing
     */
    TFFitter::~TFFitter()
    {}

    /*! \brief Calculate chi2 for all events
     *
     * Loop over all combinations
     * For each combination, check to see if there are any missing layers
     * and do the corresponding chi-square calculation
     *
     */

    void TFFitter::calculateChiSquares(bool allSectors, int procID)
    {

      int total_combs ( 0 );
      int total_passing_combs( 0 );
      int total_pix_combs ( 0 );
      int total_passing_pix_combs ( 0 );
      int total_sct_combs ( 0 );
      int total_passing_sct_combs ( 0 );
      int total_nom_combs ( 0 );
      int total_passing_nom_combs ( 0 );

      for(unsigned int ievent = 0 ; ievent < events->getNEvents() ; ievent++ )
	{
	  TFEvent *event = events->getEvent(ievent);
	  report::debug("----------------------------------------------");
	  report::debug("CALCULATE CHI2 FOR EVENT %08x",event->getID());
	  for(unsigned int iroad = 0 ; iroad < event->getNRoads() ; iroad++ )
	    {
	      TFRoad *road = event->getRoad(iroad);

	      for(unsigned int icomb = 0 ; icomb < road->getNCombinations() ; icomb++ )
		{	      
		  TFCombination *comb = road->getCombination(icomb);
		  
		  TFSector *sector;
		  if(allSectors)
		    sector = constants->getSector(comb->getSectorID());
		  else
		    sector = constants->getSector((comb->getSectorID()<<2) + procID - 1);

		  report::debug("BEFORE FIT");
		  if(report::dbg_lvl>=1)
		    comb->print();

		  total_combs++;
		  unsigned int livelayermap = comb->getLiveLayermap();

		  if( ~livelayermap & 0b111 )
		    {
		      report::debug("Use pixel majority fit");
		      calculatePixChiSquare(comb,sector);
		      total_pix_combs++;
		      if( comb->getPasses() ) total_passing_pix_combs++;
		    }
		  else if( ~livelayermap & 0b11111000 )
		    {
		      report::debug("Use SCT majority fit");
		      calculateSctChiSquare(comb,sector);
		      total_sct_combs++;
		      if( comb->getPasses() ) total_passing_sct_combs++;
		    }
		  else
		    {
		      report::debug("Use nominal fit");
		      calculateNomChiSquare(comb,sector);
		      total_nom_combs++;
		      if( comb->getPasses() ) total_passing_nom_combs++;
		    }

		  if( comb->getPasses() ) total_passing_combs++;

		  report::debug("AFTER FIT");
		  if(report::dbg_lvl>=1) comb->print();

		  // temporary crash after combination with wildcard so I can study it closely
		  //if( ~comb->getGuessmap() & 0b11111111 ) assert( false );

		}

	    }
      
	}

      report::debug( "Total passing combinations = %i / %i" , total_passing_combs , total_combs );
      report::debug( "Total passing Nom combinations = %i / %i" , total_passing_nom_combs , total_nom_combs );
      report::debug( "Total passing Pix combinations = %i / %i" , total_passing_pix_combs , total_pix_combs );
      report::debug( "Total passing Sct combinations = %i / %i" , total_passing_sct_combs , total_sct_combs );
    }

    void TFFitter::calculatePixChiSquare( TFCombination *comb , TFSector *sector )
    {
      bool overflow     ( false );

      unsigned int missinglayer = tf::getMissingLayer(comb->getLiveLayermap());
      unsigned int missingcoords[2];
      missingcoords[0] = (missinglayer * 2) + 0;
      missingcoords[1] = (missinglayer * 2) + 1;

      report::debug( "Missing layer = %i", missinglayer );
      // Calculate the chi-partials
      Int_tf chipartials[tf::NChiComponents];
      for(unsigned int ichi = 0 ; ichi < tf::NChiComponents ; ichi++ )
	{
	  Int_tf partial = sector->getH(ichi) << (tf::SConstOffset+tf::HitOffset-tf::HConstOffset);
	  for(unsigned int icoord = 0 ; icoord < tf::NCoords ; icoord++ )
	    {
	      // only include the coordinate if it is not from a missing layer
	      if( icoord == missingcoords[0] || icoord == missingcoords[1] ) continue;
	      //report::info( "Multiplying %g x %g" , comb->getCoord(icoord) / auxmath::Power(2.0,4) , sector->getS(ichi,icoord) / auxmath::Power(2.0,12) );
	      Int_tf tmp_part = sector->getS(ichi,icoord) * comb->getCoord(icoord);
	      bool tmp_overflow = false;
	      aux::shiftRight( tmp_part , 1 , 33 , tmp_overflow );
	      partial += tmp_part;
	    }
	  aux::shiftRight( partial , tf::ChiPartialShift , tf::ChiWidth , overflow );
	  chipartials[ichi] = partial;
	}
      report::debug( "Chi-Partials = [ %016llx , %016llx , %016llx , %016llx , %016llx , %016llx ]" , chipartials[0] , chipartials[1] , chipartials[2] , chipartials[3] , chipartials[4] , chipartials[5] );

      // Calculate the t-vector
      Int_tf t[2];
      for(unsigned int imissing = 0 ; imissing < 2 ; imissing++ )
	{
	  t[imissing] = 0;
	  for(unsigned int ichi = 0 ; ichi < tf::NChiComponents ; ichi++ )
	    {
	      t[imissing] += chipartials[ichi] * sector->getS(ichi,missingcoords[imissing]);
	    }
	  aux::shiftRight( t[imissing] , tf::TShift , tf::ChiWidth , overflow );
	}
      report::debug( "T = [ %llx , %llx ] = [ %g , %g ]" , t[0] , t[1] , t[0]/auxmath::Powerof2(9) , t[1]/auxmath::Powerof2(9) );
      const int pixscale_map[2] = {2, 1}; //Rui
      // Calculate missing hits
      Int_tf guesses[2];
      for(unsigned int imissing = 0 ; imissing < 2 ; imissing++ )
	{
	  guesses[imissing] = 0;
	  for(unsigned int jmissing = 0 ; jmissing < 2 ; jmissing++ )
	    {
	      int pixguessid = imissing + jmissing;
	      report::debug( "PixGuess(%i) = %llx e %lli" , pixguessid , sector->getPixGuessFrac(missinglayer,pixguessid) , sector->getPixGuessExp(missinglayer,pixguessid) );
	      Int_tf tmp_prod = t[jmissing] * sector->getPixGuessFrac(missinglayer,pixguessid);
	      //	      aux::shiftRight( tmp_prod , 6+sector->getPixGuessExp(missinglayer,pixguessid) , 40 , overflow );
	      //	      if (sector->getID() == 5411) std::cout << std::dec << "Rui: guess[imiss] = t[jmiss] * sector->getPixGuessFrac(missinglayer,pixguessid). sector->getSctGuessFrac(missinglayer,pixguessid): t[jmiss]: " << t[jmissing] << " PixGuessFrac: " << sector->getPixGuessFrac(missinglayer,pixguessid) << std::endl;
	      //	      if (sector->getID() == 5411) std::cout << std::dec << "Rui: guess[imiss]: " << imissing << " " << tmp_prod << " shift: 5+ " << sector->getPixGuessExp(missinglayer,pixguessid) << std::endl;
	      aux::shiftRight( tmp_prod , 5+sector->getPixGuessExp(missinglayer,pixguessid) , 40 , overflow ); //Rui
	      guesses[imissing] += tmp_prod;
	    }
	  //	  if (sector->getID() == 5411) std::cout << std::dec << "Rui: shifted guesses[imiss] (before): " << imissing << " " << guesses[imissing] << std::endl;
	  //	  aux::shiftRight( guesses[imissing] , pixscale_map[imissing] , tf::ChiWidth , overflow ); //Rui
	  //	  guesses[imissing] = guesses[imissing] << pixscale_map[imissing]; //Rui
	  aux::shiftRight( guesses[imissing] , 0 , tf::ChiWidth , overflow );
	  //	  if (sector->getID() == 5411) std::cout << std::dec << "Rui: shifted guesses[imiss]: " << imissing << " " << guesses[imissing] << std::endl; 
	}
      report::debug( "X = [ %llx , %llx ] = [ %g , %g ]" , guesses[0] , guesses[1] , guesses[0]/auxmath::Powerof2(3) , guesses[1]/auxmath::Powerof2(3) );
  
      // Correct the chi-partials for the missing hits
      for(unsigned int ichi = 0 ; ichi < tf::NChiComponents ; ichi++ )
	{
	  Int_tf tmp_prod = 0;
	  for(unsigned int imissing = 0 ; imissing < 2 ; imissing++ )
	    {
	      tmp_prod += guesses[imissing] * sector->getS(ichi,missingcoords[imissing]);
	      //	      if (sector->getID() == 5411) std::cout << std::dec << "Rui: tmp_prod+=guesses[imissing] * sector->getS(ichi,missingcoords[imissing]):  "<< imissing << " " << ichi << " : " << guesses[imissing] << " * " << sector->getS(ichi,missingcoords[imissing]) << std::endl;
	    }
	  //	  aux::shiftRight( tmp_prod , tf::GuessedChiShift , tf::ChiWidth , overflow );
	  //	  if (sector->getID() == 5411) std::cout << std::dec << "Rui: tmp_prod: " << ichi << " " << tmp_prod << std::endl;
	  aux::shiftRight( tmp_prod , 1+tf::GuessedChiShift , tf::ChiWidth , overflow );
	  //	  if (sector->getID() == 5411) std::cout << std::dec << "Rui: tmp_prod shifted: " << ichi << " " << tmp_prod << std::endl;
	  chipartials[ichi] += tmp_prod;
	  aux::shiftRight( chipartials[ichi] , 0 , tf::ChiWidth , overflow );
	  //	  if (sector->getID() == 5411) std::cout << std::dec << "Rui: final chipartials[ichi]: " << ichi << " " << chipartials[ichi] << std::endl;
	}

      // Calculate the chi-square
      Int_tf chisquare ( 0 );
      for(unsigned int ichi = 0 ; ichi < tf::NChiComponents ; ichi++ )
	{
	  chisquare += chipartials[ichi] * chipartials[ichi];
	}
      comb->setCoord( missingcoords[0] , guesses[0] );
      comb->setCoord( missingcoords[1] , guesses[1] );
      comb->constructPixHit( missinglayer , guesses[0] , guesses[1] );
      Int_tf hit = 0;
      hit += guesses[1] << 16;
      hit += guesses[0] >> 1;
      //      hit &= 0x000000000fffffff;
      //      if (chisquare < tf::PixChiSquareCut && !overflow && comb->getSectorID() == 5411) std::cout << "Rui: hit: " << std::hex << hit << std::endl;
      hit &= 0x000000000fffffff;
      //      if (chisquare < tf::PixChiSquareCut && !overflow && comb->getSectorID() == 5411) std::cout << "Rui: hit shortened: " << std::hex << hit << std::dec <<std::endl;
      comb->setChiSquare( chisquare );
      comb->setOverflow( overflow );
      comb->setPasses( chisquare < tf::PixChiSquareCut && !overflow && guesses[0] >= 0 && guesses[1] >= 0);
    }

    /*! \brief Calculate the sct chi2 for a combination
     *
     * First guess the mixing SCT Hits, then compute the ChiSquare
     */
    void TFFitter::calculateSctChiSquare(TFCombination *comb, TFSector *sector)
    {
      bool overflow     ( false );
      unsigned int missinglayer = tf::getMissingLayer(comb->getLiveLayermap());
      unsigned int missingcoord = missinglayer + tf::NPixLayers;

      // Calculate the chi-partials
      Int_tf chipartials[tf::NChiComponents];
      for(unsigned int ichi = 0 ; ichi < tf::NChiComponents ; ichi++ )
	{
	  Int_tf partial = sector->getH(ichi) << (tf::SConstOffset+tf::HitOffset-tf::HConstOffset);
	  for(unsigned int icoord = 0 ; icoord < tf::NCoords ; icoord++ )
	    {
	      // only include the coordinate if it is not from a missing layer
	      if( icoord == missingcoord ) continue;
	      Int_tf tmp_part = sector->getS(ichi,icoord) * comb->getCoord(icoord);
	      bool tmp_overflow = false;
	      // if (sector->getID() == 372) std::cout << std::hex << "Rui: sector->getS(ichi,icoord) * comb->getCoord(icoord): " << ichi << " " << icoord << " " << sector->getS(ichi,icoord) << " " << comb->getCoord(icoord) << " product: " << tmp_part << std::endl;
	      aux::shiftRight( tmp_part , 1 , 33 , tmp_overflow ); //ONLY Pix in FW, no difference
	      assert( ! tmp_overflow ); // overflow should be impossible here
	      partial += tmp_part;
	    }
	  // if (sector->getID() == 372) std::cout << std::hex << "Rui:chipartial: " << partial << std::endl;
	  aux::shiftRight( partial , tf::ChiPartialShift , tf::ChiWidth , overflow );
	  chipartials[ichi] = partial;
	  // if (sector->getID() == 372) std::cout << std::hex << "Rui: shifted chipartial: " << partial << std::endl;
	  report::debug( "partial %i = %llx" , ichi , partial );
	}

      // Calculate the t-vector
      Int_tf t ( 0 );
      for(unsigned int ichi = 0 ; ichi < tf::NChiComponents ; ichi++ )
	{
	  t += chipartials[ichi] * sector->getS(ichi,missingcoord);
	  // if (sector->getID() == 372) std::cout << std::hex << "Rui: chipartials[ichi]: " << ichi << " " << chipartials[ichi] << " S(ichi, missingcoord: " << missingcoord << " " << sector->getS(ichi,missingcoord) << " product: " << chipartials[ichi] * sector->getS(ichi,missingcoord) << std::endl;
	  report::debug("consts = %llx", sector->getS(ichi,missingcoord));
	}
      aux::shiftRight( t , tf::TShift , tf::ChiWidth , overflow );
      //      // if (sector->getID() == 372) std::cout << "Rui: shifted T: " << t << std::endl;
      report::debug( "t = %llx" , t );

      // Calculate missing hits
      Int_tf guess = t * sector->getSctGuessFrac(missinglayer-tf::NPixLayers);
      // if (sector->getID() == 372) std::cout << std::hex << "Rui: guess = t * sector->getSctGuessFrac(missinglayer-tf::NPixLayers). sector->getSctGuessFrac(missinglayer-tf::NPixLayers): " << sector->getSctGuessFrac(missinglayer-tf::NPixLayers) << std::endl;
      // if (sector->getID() == 372) std::cout << std::hex << "Rui: guess: " << guess << " shift: 5+ " << sector->getSctGuessExp(missinglayer-tf::NPixLayers) << std::endl;
      //      aux::shiftRight( guess , 6 + sector->getSctGuessExp(missinglayer-tf::NPixLayers) , tf::ChiWidth , overflow );
      aux::shiftRight( guess , 5 + sector->getSctGuessExp(missinglayer-tf::NPixLayers) , tf::ChiWidth , overflow ); //Rui: to be consistant with chipartial calculation for non guessed hits
      aux::shiftRight( guess , 3 , tf::ChiWidth , overflow ); //Rui
      guess = guess << 3; //Rui
      ///// if (sector->getID() == 372) std::cout << "Rui: shifted guess: " << guess << std::endl;
      // Correct the chi-partials for the missing hits
      for(unsigned int ichi = 0 ; ichi < tf::NChiComponents ; ichi++ )
	{
	  Int_tf tmp_prod = 0;
	  tmp_prod += guess * sector->getS(ichi,missingcoord);
	  aux::shiftRight( tmp_prod , 1+tf::GuessedChiShift , tf::ChiWidth , overflow ); //Rui
	  //	  aux::shiftRight( tmp_prod , tf::GuessedChiShift , tf::ChiWidth , overflow );
	  chipartials[ichi] += tmp_prod;
	  aux::shiftRight( chipartials[ichi] , 0 , tf::ChiWidth , overflow );
	  //	  // if (sector->getID() == 372) std::cout << "Rui: final chipartials[ichi]: " << ichi << " " << chipartials[ichi] << std::endl;
	}

      // Calculate the chi-square
      Int_tf chisquare ( 0 );
      for(unsigned int ichi = 0 ; ichi < tf::NChiComponents ; ichi++ )
	{
	  chisquare += chipartials[ichi] * chipartials[ichi];
	}

      // if (sector->getID() == 372)  std::cout << std::hex << " sector: " << std::hex << comb->getSectorID() << " road: " <<  std::hex << comb->getRoadID() << " chisqaure: " << std::hex << chisquare << " int: " << std::dec << double(chisquare) / (1<<26) << " ovfl: " << overflow << std::endl;

      // if (chisquare < tf::SctChiSquareCut && !overflow && sector->getID() == 372) {
      // 	std::cout << " sector: " << std::hex << comb->getSectorID() << " road: " <<  std::hex << comb->getRoadID() << " chisqaure: " << std::hex << chisquare << " int: " << std::dec << double(chisquare) / (1<<26) << std::endl;
      // 	for(unsigned int icoord = 0 ; icoord < tf::NCoords ; icoord++ )
      // 	  {
      // 	    std::cout << std::hex << "hit: " << comb->getCoord(icoord) << std::endl;
      // 	  }
      // }
      report::debug( "guess = %llx", guess);
      comb->setCoord( missingcoord , guess );
      comb->constructSctHit( missinglayer , guess );
      comb->setChiSquare( chisquare );
      comb->setOverflow( overflow );
      comb->setPasses( chisquare < tf::SctChiSquareCut && !overflow && guess >= 0);

    }

    void TFFitter::calculateNomChiSquare(TFCombination *comb, TFSector *sector)
    {
      Int_tf chisquare  ( 0 );
      bool overflow     ( false );

      for(unsigned int ichi = 0 ; ichi < tf::NChiComponents ; ichi++ )
	{
	  Int_tf chicomponent = sector->getH(ichi) << (tf::SConstOffset+tf::HitOffset-tf::HConstOffset); //12+3-6
	  report::debug("H %d: %08x = %d",ichi,chicomponent,chicomponent);
	  for(unsigned int icoord = 0 ; icoord < tf::NCoords ; icoord++ )
	    {
	      Int_tf tmp_chi = sector->getS(ichi,icoord) * comb->getCoord(icoord);
	      report::debug("S %d,%d: %08x * %08x = %d = %08x",ichi,icoord,sector->getS(ichi,icoord),comb->getCoord(icoord),tmp_chi,tmp_chi);
	      bool tmp_overflow = false;
	      aux::shiftRight( tmp_chi , 1 , 33 , tmp_overflow );
	      assert( ! tmp_overflow ); // overflow should be impossible here
	      chicomponent += tmp_chi;
	    }
	  aux::shiftRight( chicomponent , tf::ChiShift , tf::ChiWidth , overflow );

	  Int_tf chicomponent2=chicomponent * chicomponent;
	  report::debug( "chicomponent %i = 0x%08x%08x, squared = 0x%08x%08x" , ichi , aux::getBitRange(chicomponent,32,63) , aux::getBitRange(chicomponent, 0,31) , aux::getBitRange(chicomponent2,32,63) , aux::getBitRange(chicomponent2, 0,31) );
	  chisquare += chicomponent2;
	}
      report::debug("nominal chi2 0x%08x%08x", aux::getBitRange(chisquare,32,63), aux::getBitRange(chisquare, 0,31));

      comb->setChiSquareNominal( chisquare );
      comb->setOverflowNominal( overflow );

      // determine whether or not to do the majority recovery
      if( chisquare < tf::TightChiSquareCut )
      	{
	  comb->setPasses( !overflow );
	  comb->setChiSquare( chisquare );
	  comb->setOverflow( overflow );
	}
      else if( chisquare < tf::LooseChiSquareCut or true) // Loose cut always passes, same in firmware for now
      	{
      	  report::debug("starting recovery fit");
      	  comb->setPasses( false );
      	  comb->setChiSquare( chisquare );
      	  comb->setOverflow( overflow );

      	  TFCombination *best_rec_comb = 0;
      	  Int_tf best_chi2 = comb->getChiSquare();
      	  unsigned int best_layer = 0;
      	  for(unsigned int ilayer = 0 ; ilayer < tf::NLayers ; ilayer++ )
      	    {
      	      report::debug("recovery fit with layer %d missing",ilayer);
      	      TFCombination *rec_comb = new TFCombination( comb );
      	      rec_comb->setLayermap( ~(1 << ilayer) & comb->getLayermap() );

      	      if( ilayer < tf::NPixLayers )
      		calculatePixChiSquare( rec_comb , sector );
      	      else
      		calculateSctChiSquare( rec_comb , sector );

      	      comb->setChiSquareRecovery( ilayer , rec_comb->getChiSquare() );
      	      comb->setOverflowRecovery( ilayer , rec_comb->getOverflow() );
	  
      	      if( rec_comb->getOverflow()==false && rec_comb->getChiSquare() < best_chi2 )
      		{ // There is a new candidate for the best combination!
      		  if(best_rec_comb!=0) // Clean up the old one
      		    delete best_rec_comb;
      		  best_rec_comb=rec_comb;
      		  best_chi2=rec_comb->getChiSquare();
      		  best_layer=ilayer;
      		}
      	      else // New guess not better, get rod if it
      		delete rec_comb;
      	    }

      	  if(best_rec_comb) // There was a better combination, update information
      	    {
      	      report::debug("Recovered layer %d",best_layer);
      	      comb->setChiSquare( best_rec_comb->getChiSquare() ); // Update with the lowest 
      	      comb->setLayermap( best_rec_comb->getLayermap() );
      	      //std::cout << "REPLACE HIT " << rec_comb->getHit
      	      comb->setHit( best_layer, best_rec_comb->getHit(best_layer) );
      	      //	      std::cout << "Guessed Hit: " << std::hex << best_rec_comb->getHit(best_layer) << std::endl;
      	      comb->setPasses( best_rec_comb->getPasses() );
      	      delete best_rec_comb;
      	    }

      	}
      else
      	{
      	  comb->setPasses( false );
      	  comb->setChiSquare( chisquare );
      	  comb->setOverflow( overflow );
      	}
    }
  } // namespace ftk
} // namespace daq

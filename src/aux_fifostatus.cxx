/*           Program to print out the status of the AUX card FIFOs
             Written for AMBFTK board v1.0   FTK collaboration                 */
#include "aux/aux.h"
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("input",  value< std::vector<int> >()->multitoken(), "List of Inputs to show status for.")
    ("proc",   value< std::vector<int> >()->multitoken(), "List of Processors to show status for.")    
    ("doHS",  "Do details of HitSort monitoring")
    ;
  
  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }

  //bool showI1=false;
  //bool showI2=false;
  bool showP1=false;
  bool showP2=false;
  bool showP3=false;
  bool showP4=false;
  bool doHS=false;
  if(vm.count("doHS")) doHS = true;

  if(vm.count("input")==0 && vm.count("proc")==0)
    { // Default is all info
      //showI1=true;
      //showI2=true;
      showP1=true;
      showP2=true;
      showP3=true;
      showP4=true;
    }

  // Enable any requested processors
  if(vm.count("proc")>0)
    {
      std::vector<int> procs=vm["proc"].as<std::vector<int> >();
      for(std::vector<int>::const_iterator iter=procs.begin();
	  iter!=procs.end();
	  iter++)
	{
	  switch(*iter)
	    {
	    case 1:
	      showP1=true;
	      break;
	    case 2:
	      showP2=true;
	      break;
	    case 3:
	      showP3=true;
	      break;
	    case 4:
	      showP4=true;
	      break;
	    default:
	      break;
	    }
	}
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  int slot    = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  return  daq::ftk::aux_fifostatus_main(slot,showP1,showP2,showP3,showP4,doHS) ;
}

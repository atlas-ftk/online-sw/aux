/*           Program to issue a routine for read the AUX RAM                   */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */

/////////////////////////////////////////////////////////////////////////////
// Stand alone application
//   - Compiled when -DSTANDALONE
/////////////////////////////////////////////////////////////////////////////
#include "aux/aux_vmetest.h"
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


/** Parsing input parmaeters and calling the am_init function
 *
 */
int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("fpga", value< std::string >()->default_value("-1"), "The FPGA ID (-1 for random)")
    ("addr", value< std::string >()->default_value("-1"), "The VME address (-1 for random)")
    ("n", value< std::string >()->default_value("10"), "Number of tests to run")
    ;
  
  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  int n = daq::ftk::string_to_int( vm["n"].as<std::string>() );
  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int fpga = daq::ftk::string_to_int( vm["fpga"].as<std::string>() );
  int addr = daq::ftk::string_to_int( vm["addr"].as<std::string>() );
  u_int value;

  bool doRandomFPGA = (fpga==-1);
  bool doRandomAddr = (addr==-1);

  int good[72];
  int fail[72];
  for(int i=0;i<72;i++)
    {
      good[i]=0;
      fail[i]=0;
    }

  for(int i=0;i<n;i++)
    {
      //if(doRandomFPGA) fpga = 3 + (rand() % 4);
      if(doRandomFPGA) fpga = 1+(rand() % 6);

      if(doRandomAddr) addr = (1+rand()%12)<<4;

      value = ((rand()%16)<<0)
	+ ((rand()%16)<<4)
	+ ((rand()%16)<<8)
	+ ((rand()%16)<<12)
	+ ((rand()%16)<<16)
	+ ((rand()%16)<<20)
	+ ((rand()%16)<<24)
	+ ((rand()%16)<<28);

      if(daq::ftk::aux_vmetest(slot, fpga, addr, value)==0)
	good[(fpga-1)*12+(addr>>4)-1]++;
      else
	fail[(fpga-1)*12+(addr>>4)-1]++;
    }

  std::cout << "\tGOOD\tBAD"  << std::endl;
  for(fpga=0;fpga<6;fpga++)
    {
      std::cout << "FPGA: " << fpga+1 << std::endl;
      for(addr=0;addr<12;addr++)
	{
	  std::cout << "0x" << std::hex << ((addr+1)<<4) << ":\t" << good[fpga*12+addr] << "\t" << fail[fpga*12+addr] << std::endl;
	}
    }

    return 0;
}

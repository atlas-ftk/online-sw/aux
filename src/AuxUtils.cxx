#include "aux/AuxUtils.h"

#include "aux/AuxString.h"

#include <sstream>
//FIXIT
#include <string>

namespace aux
{
  int getBit(unsigned int word, unsigned int ibit)
  { return (word >> ibit) % 2; }

  Int_tf readBin(std::string n)
  {
    if( n.find("0b")==0 ) n.replace(0,2,"");
    return auxstring::BaseConvertLL(n,2);
  }

  Int_tf readHex(std::string n)
  {
    if( n.find("0x")==0 ) n.replace(0,2,"");
    return auxstring::BaseConvertLL(n,16);
  }

  Int_tf read(const std::string& n)
  {
    if( n.find("0b") == 0 ) return readBin(n);
    if( n.find("0x") == 0 ) return readHex(n);
    return auxstring::BaseConvertLL(n,10);
  }

  Int_tf sgn(Int_tf v) { return (v < 0 ? -1 : 1 ); }

  Int_tf getMask(unsigned int nbits)
  {
    static Int_tf one_tf=1;
    Int_tf mask = (one_tf<<nbits);
    mask -= one_tf;
    return mask;
  }

  void shiftRight(Int_tf &val, unsigned int nbits, unsigned int maxwidth, bool &overflow)
  {
    Int_tf val_sgn = sgn(val);
    Int_tf pos_val = (val >> nbits) * val_sgn;
    Int_tf pos_val_masked = pos_val & getMask(maxwidth-1); // -1 because of sign bit
    if( pos_val_masked != pos_val ) overflow = true;
    val = pos_val_masked * val_sgn;
  }

  Int_tf getBitRange(Int_tf word, unsigned int begin,unsigned int end)
  {
    Int_tf result = word >> begin;
    return result & getMask( 1 + end - begin );
  }

    void zfill( std::string &s , unsigned int ndigits )
  {
    while( s.length() < ndigits ) s = "0"+s;
  }

  std::string getBinFromHexDigit(char h)
  {
    if( h=='0') return "0000";
    if( h=='1') return "0001";
    if( h=='2') return "0010";
    if( h=='3') return "0011";
    if( h=='4') return "0100";
    if( h=='5') return "0101";
    if( h=='6') return "0110";
    if( h=='7') return "0111";
    if( h=='8') return "1000";
    if( h=='9') return "1001";
    if( h=='a') return "1010";
    if( h=='b') return "1011";
    if( h=='c') return "1100";
    if( h=='d') return "1101";
    if( h=='e') return "1110";
    if( h=='f') return "1111";
    assert( false );
  }

  std::string getBinFromHex(const std::string& h)
  {
    std::string result = "";
    for(unsigned int ichar = 0 ; ichar < h.length() ; ichar++ )
      result = result + getBinFromHexDigit(h[ichar]);

    return result;
  }

  std::string getSignedBin( const std::string &v , unsigned int nbits )
  {
    // TODO simplifty
    Int_tf n = std::stoll(v,0,10);
    std::ostringstream oss;
    oss << std::hex << n;
    std::string hexstr = oss.str();
    std::string binstr = getBinFromHex( hexstr );
    if( binstr.length() > nbits ) binstr.erase( 0 , binstr.length() - nbits );
    zfill( binstr , nbits );
    return binstr;
  }

  std::string getSignedHex(const std::string &v , unsigned int nbits)
  {
    std::string binstr = getSignedBin( v , nbits );
    Int_tf n = std::stoll(binstr,0,2);
    std::ostringstream oss;
    oss << std::hex << n;
    return oss.str();
  }

  std::string getHexStr(Int_tf v, unsigned int ndigits)
  {
    std::ostringstream oss;
    oss << std::hex << v;
    std::string hexstr = oss.str();
    zfill( hexstr , ndigits );
    return hexstr;
  }

  std::string getBinStr(Int_tf v, unsigned int ndigits)
  {
    std::ostringstream oss;
    oss << std::hex << v;
    std::string hexstr = oss.str();
    std::string binstr = getBinFromHex(hexstr);
    zfill( binstr , ndigits );
    return binstr;
  }
}

#include "aux/aux.h"
#include <bitset>
#include <iterator>

#include "aux/aux_inputs.h"
namespace daq 
{
    namespace ftk 
    {

        void dummy()
        { ERS_LOG("Dummy"); }

        // bool aux_checksums( uint slot )
        // { 

        //   proc *procs[4];
        //   for(uint pIdx=0;pIdx<4;pIdx++) procs[pIdx]=new proc(slot,3+pIdx);

        //   //
        //   // TF checksums
        //   for(uint pIdx=0;pIdx<4;pIdx++)
        // 	{
        // 	  uint pNum=pIdx+1;
        // 	  std::cout << "Processor " << pNum << " TF Checksum = ";
        // 	  std::cout << std::hex << std::setw(8) << std::setfill('0') << procs[pIdx]->calc_tf_check_sum() << std::dec << std::endl;
        // 	}

        //   ////
        //   return 0;
        // }


        bool aux_reset_amb(int slot){
            bool input1 = aux_reset(slot, 1, false, true, false, false, false, false, false, false, false);
            bool input2 = aux_reset(slot, 2, false, true, false, false, false, false, false, false, false);

            return (input1 and input2);
        }

        bool aux_reset(int slot, int fpga, bool doMain, bool doXcvr, bool doSync, bool doReg, bool doErr, bool doTxFIFO, bool doSpybuffers, bool doFreeze, bool doErrLatch)
        { 
            int status;
            u_int ret, register_content;
            u_int vmeaddr, slotaddr, fpgaaddr;
            int handle;


            static VME_MasterMap_t master_map = {0x0, 0x10000000, VME_A32, 0};

            slotaddr = ((slot&0x1f) << 27);
            fpgaaddr = ((fpga&0x1f) << 24);
            vmeaddr = slotaddr + fpgaaddr;

            ret = VME_Open();
            if (ret != VME_SUCCESS)
            {
                VME_ErrorPrint(ret);
                FTK_VME_ERROR( "Failure when calling the function 'VME_Open' in function" );
            }

            master_map.vmebus_address   = vmeaddr;
            master_map.window_size      = 0x5f0000;
            master_map.address_modifier = VME_A32;
            master_map.options          = 0;
            ret = VME_MasterMap(&master_map, &handle);
            if (ret != VME_SUCCESS)
            {
                VME_ErrorPrint(ret);
                FTK_VME_ERROR( "Failure when calling the function 'VME_MasterMap' in function"  );
            }

            //set tmode
            status = VME_WriteSafeUInt(handle, TMODE, 0x1);
            CHECK_ERROR(status);

            //
            // Clear TX fifos
            //
            if(doTxFIFO)
            {
                PRINT_LOG("Reset Tx FIFO.");
                status = VME_WriteSafeUInt(handle, TX_FIFOWRITE, 0x10);
                CHECK_ERROR(status);
                status = VME_WriteSafeUInt(handle, TX_FIFOWRITE, 0x0);
                CHECK_ERROR(status);
            }

            //
            // WRITE 0 to all of the registers
            //

            if(doReg)
            {
                PRINT_LOG("Reset registers.");
                //write the data
                for(int regaddr=1;regaddr<=15;regaddr++)
                {
                    status = VME_WriteSafeUInt(handle, regaddr<<4, 0);
                    CHECK_ERROR(status);
                }
            }

            //
            // Reset error counters
            //      
            if(doErr)
            {
                PRINT_LOG("Reset error counters.");

                // start by reading current value, so we don't mess up other operations
                status = VME_WriteSafeUInt(handle, RESETS, 0x2);
                CHECK_ERROR(status);
            }

            //set tmode
            status = VME_WriteSafeUInt(handle, TMODE, 0x0);
            CHECK_ERROR(status);

            //
            // Reset transceivers
            //
            if(doXcvr)
            {
                PRINT_LOG("Reset transceivers.");
                status = VME_WriteSafeUInt(handle, RESET_XCVR, 0x1);
                CHECK_ERROR(status);
            }

            //
            // Reset transceiver synchronization
            //
            if(doSync)
            {
                PRINT_LOG("Reset synchronization.");

                //set tmode
                status = VME_WriteSafeUInt(handle, TMODE, 0x1);
                CHECK_ERROR(status);

                // start by reading current value, so we don't mess up other operations
                status = VME_ReadSafeUInt(handle, RESET_SYNC, &register_content);
                CHECK_ERROR(status);

                status = VME_WriteSafeUInt(handle, RESET_SYNC, register_content|0x2);
                CHECK_ERROR(status);
                status = VME_WriteSafeUInt(handle, RESET_SYNC, register_content);
                CHECK_ERROR(status);

                //unset tmode
                status = VME_WriteSafeUInt(handle, TMODE, 0x0);
                CHECK_ERROR(status);
            }

            //
            // Main board reset
            //
            if(doMain)
            {
                PRINT_LOG("Reset main board.");
                status = VME_WriteSafeUInt(handle, RESET, 0x1);
                CHECK_ERROR(status);
            }


            //
            // Reset spybuffers
            //      
            if(doSpybuffers)
            {
                PRINT_LOG("Reset spybuffers.");

                // start by reading current value, so we don't mess up other operations
                status = VME_WriteSafeUInt(handle, RESETS, 0x1);
                CHECK_ERROR(status);
            }

            //
            // Reset freeze
            //      
            if(doFreeze)
            {
                PRINT_LOG("Reset freeze.");

                // start by reading current value, so we don't mess up other operations
                status = VME_WriteSafeUInt(handle, RESETS, 0x100);
                CHECK_ERROR(status);
            }


            //
            // Error Bits Latch Reset
            //      
            if(doErrLatch)
            {
                PRINT_LOG("Error Bits Latch Reset.");

                // start by reading current value, so we don't mess up other operations
                status = VME_WriteSafeUInt(handle, RESETS, 0x200);
                CHECK_ERROR(status);
            }


            ret = VME_MasterUnmap(handle);
            if (ret != VME_SUCCESS)
            {
                VME_ErrorPrint(ret);
                FTK_VME_ERROR( "Failure when calling the function 'VME_MasterUnMap' in function"  );

            }

            ret = VME_Close();
            if (ret != VME_SUCCESS)
            {
                VME_ErrorPrint(ret);
                FTK_VME_ERROR( "Failure when calling the function 'VME_Close' in function"  );

            }

            return 0;

        }



        bool aux_read_buffer( int slot, int fpga, u_int dim_buffer, std::vector<u_int> buffer, const std::string& output, const std::string& source)
        { 
            FILE *fp=stdout;
            if(!output.empty() &&
                    (fp=fopen(output.c_str(), "w"))==NULL)
            {
                fprintf(stderr,"Cannot open file: %s.\n", output.c_str());
                exit(1);
            }

            if( source == "vme"){
                if(output.empty())
                {
                    PRINT_LOG("\n**************************************\n");
                    PRINT_LOG("Send Freeze to spy buffer!!\n");
                    PRINT_LOG("\n**************************************\n");
                }

                VMEInterface *vme=VMEManager::global().aux(slot,fpga);

                //set tmode rx stratix
                vme->write_word(TMODE,0x1);

                // Figure out the last addresss
                std::vector<u_int> lastAddress;
                std::vector<bool>  overflow;

                for(u_int buffIdx=0;buffIdx<buffer.size();buffIdx++)
                {
                    u_int buff=buffer[buffIdx];
                    u_int buffinfo=vme->read_word(buff<<16);

                    lastAddress.push_back(buffinfo&0x3fff);
                    overflow.push_back((buffinfo>>14)&1);
                    if(dim_buffer==0) dim_buffer=(buffinfo>>16)&0x3fff;
                }

                //read the content of the memory of the selected link
                std::vector<std::vector<u_int > > datas;
                std::vector<u_int> data;
                for(u_int buffIdx=0;buffIdx<buffer.size();buffIdx++)
                {
                    data=vme->read_block(buffer[buffIdx]<<16,(1<<dim_buffer)+1);
                    datas.push_back(data);
                }

                // print the contents
                u_int baddr;
                for(int i = 0; i < (1<<dim_buffer)-1; ++i)
                {
                    for(u_int buffIdx=0;buffIdx<buffer.size();buffIdx++)
                    {
                        baddr=(lastAddress[buffIdx]+i)%((1<<dim_buffer)-1);
                        if(baddr==0) baddr=(1<<dim_buffer)-1;

                        u_int word= datas[buffIdx][baddr];

                        fprintf(fp,"[%.4x]: %08x  ",baddr,word);
                    }
                    fprintf(fp,"\n");
                }

                for(u_int buffIdx=0;buffIdx<buffer.size();buffIdx++)
                    fprintf(fp,"[last]: %08x  ",lastAddress[buffIdx]);
                fprintf(fp,"\n");

                for(u_int buffIdx=0;buffIdx<buffer.size();buffIdx++)
                    fprintf(fp,"[ovfl]: %s  ",overflow[buffIdx]?"     YES":"      NO");
                fprintf(fp,"\n");


                //set tmode rx stratix
                vme->write_word(TMODE, 0x0);

                if (!output.empty()) fclose(fp);

                return 0;
            }

        }



        bool aux_load_tx_fifo(u_int slot, u_int fpga, u_int buffer, const std::string& file)
        {
            VMEInterface *vme=VMEManager::global().aux(slot, fpga);
            //u_int register_content;

            //
            // READ
            //
            //Assert write
            vme->write_word(TMODE, 0x1);

            // Actually perform the write
            std::string ctrlstr, datastr;
            std::string line;
            std::ifstream fh(file.c_str());

            if(!fh.is_open()) // Error opening file
            {
                daq::ftk::IOError ex(ERS_HERE,name_ftk(), file);
                throw ex;
            }
            while(std::getline(fh,line))
            {
                if(line=="") continue;

                std::istringstream iss(line);
                iss >> ctrlstr >> datastr;
                vme->write_word(buffer<<16, daq::ftk::string_to_int(ctrlstr));
                vme->write_word(buffer<<16, daq::ftk::string_to_int(datastr));
                ERS_LOG(std::hex << std::setfill('0') << "Buffer 0x" << std::setw(8) << buffer << " got " << std::setw(8) << daq::ftk::string_to_int(datastr) << "(ctrl = " << daq::ftk::string_to_int(ctrlstr) << ")");
            }
            fh.close();


            //Assert write
            vme->write_word(TMODE, 0x0);

            return 0;

        }


        bool aux_ssmap_load( u_int slot, u_int fpga, u_int address, const std::string& ssmappath, bool isModule)
        { 
            ftkTimeLog tLog(ERS_HERE, name_ftk(), "SSmap load: " + ssmappath);
            VMEInterface *vme=VMEManager::global().aux(slot,fpga);

            //set tmode
            vme->write_word(TMODE, 0x1);

            // Read the ssmap file and load it
            u_int offset=0;

            std::ifstream fh(ssmappath);
            std::string line;
            // u_int RAMSIZE = 512;
            u_int RAMSIZE = 131072;

            // Error checking
            if(!fh.is_open())
            {
                daq::ftk::IOError ex(ERS_HERE,name_ftk(), ssmappath);
                throw ex;
            }

            ERS_LOG("Opened ssmap file " << ssmappath);
            clock_t start = clock(), diff;
            u_int nWords=0;
            int lastAddr=0;

            std::vector<uint> loadedAddresses;
            loadedAddresses.clear();

            for(uint iWord=0; iWord < RAMSIZE; iWord++){
                //if( std::find(loadedAddresses.begin(), loadedAddresses.end(), iWord) != loadedAddresses.end()) continue;

                u_int ssaddr=(address<<16)+((iWord<<2)&0xFFFF);
                //u_int ssdata = 0x00000000; //should eventually be 0xFFFFFFFF
                u_int ssdata = 0xFFFF0000; //should eventually be 0xFFFFFFFF
                if(isModule) ssdata|=0x80000000;

                vme->write_word(ssaddr, ssdata);
            }


            while(!fh.eof())
            {
                fh >> line;
                // Split line into parts
                std::string bytecountstr=line.substr(1,2);
                std::string addressstr=line.substr(3,4);
                std::string typestr=line.substr(7,2);
                if(typestr=="01") break; // EOF code

                int bytecount=daq::ftk::string_to_int("0x"+bytecountstr);
                std::string datastr=line.substr(9,bytecount*2);

                u_int ssaddr=daq::ftk::string_to_int("0x"+addressstr)+offset;
                u_int ssdata=daq::ftk::string_to_int("0x"+datastr);

                if(typestr=="04") // New offset
                {
                    offset=ssdata<<16;
                    continue;
                }

                lastAddr = ssaddr;
                loadedAddresses.push_back(ssaddr);

                ssdata|=(ssaddr<<2)&0xFFFF0000;
                ssaddr=(address<<16)+((ssaddr<<2)&0xFFFF);

                if(isModule) ssdata|=0x80000000;
                vme->write_word(ssaddr, ssdata);

                nWords++;
            }
            fh.close();
            /*      ERS_LOG("nWords: " << nWords << " last addr " << lastAddr);     
                    ERS_LOG("Filling in the rest of the RAM");
            //fill the rest of the ram for correct checksum calculation
            if(nWords < RAMSIZE){
            for(uint iWord=0; iWord < RAMSIZE; iWord++){
            if( std::find(loadedAddresses.begin(), loadedAddresses.end(), iWord) != loadedAddresses.end()) continue;

            u_int ssaddr=(address<<16)+((iWord<<2)&0xFFFF);
            //u_int ssdata = 0x00000000; //should eventually be 0xFFFFFFFF
            u_int ssdata = 0x0000000; //should eventually be 0xFFFFFFFF

            vme->write_word(ssaddr, ssdata);
            }
            }*/


            diff = clock() - start;

            int msec = diff * 1000 / CLOCKS_PER_SEC;
            ERS_LOG("SSMap speed: " << nWords << " words / " << ((float)msec)/1000. << " seconds = " << 4*((float)nWords)/(((float)msec)/1000.)/1000. << " kBytes/s");

            //unset tmode
            vme->write_word(TMODE, 0x0);


            if(fpga == 1){
                std::shared_ptr<input1> i1 = std::make_shared<input1>(slot, "");
                i1->run_ss_check_sums();
            }
            if(fpga == 2){
                std::shared_ptr<input2> i2 = std::make_shared<input2>(slot, "");
                i2->run_ss_check_sums();
            }

            tLog.end(ERS_HERE);  // End log conter

            return 0;
        }


        void calibrate_EMIF(VMEInterface *vme, u_int proc)
        {
            static u_int vme_ctrl_20 = 0x20;
            //static u_int vme_ctrl_40 = 0x40;
            u_int do_status;
            u_int calibration_status = 0x0;
            int calibration_attempts = 1;
            // Read DO_Status to get calibration success

            while (calibration_status == 0x0 and calibration_attempts < 101) { 
                do_status=vme->read_word(DO_STATUS);
                calibration_status=(do_status>>22)&0x1;
                if (calibration_status == 0x1) {
                    ERS_LOG("EMIF calibration success. Attempt: "+to_string(calibration_attempts-1));
                } else {
                    ERS_INFO("EMIF calibration failure, trying soft reset. Attempt " << calibration_attempts);
                    vme->write_word(TMODE, 0x1);
                    //vme->write_word(vme_ctrl_40,0x2);
                    vme->write_word(vme_ctrl_20,0x1);
                    usleep(2e5);
                    //vme->write_word(vme_ctrl_40,0x0);
                    //usleep(1);
                    vme->write_word(vme_ctrl_20,0x0);
                    vme->write_word(TMODE, 0x0);
                    usleep(2e5);
                };
                calibration_attempts += 1;
            };
            if (calibration_attempts == 101 and calibration_status == 0x0){
                daq::ftk::FTKOperationTimedOut ex(ERS_HERE, name_ftk(), "EMIF Calibration failed");
                ers::warning(ex);
            }
        }


        void zero_emif(VMEInterface *vme)
        {
            static u_int vme_ctrl_70 = 0x70;
            // Zero external memory
            u_int do_status;
            u_int zero_done = 0x0;
            int zeroing_waits = 1;

            vme->write_word(TMODE, 0x1);
            vme->write_word(vme_ctrl_70,0x80000);
            vme->write_word(TMODE, 0x0);      
            usleep(40000);//sleep for 40 milliseconds. should take less than 40

            while (zero_done == 0x0 and zeroing_waits < 10) { 

                do_status=vme->read_word(DO_STATUS);
                zero_done=(do_status>>26)&0x1;
                if (zero_done == 0x1) {
                    ERS_LOG("AMBank has been cleared");
                } else {
                    ERS_INFO("Waiting for AMBank to clear. " << zeroing_waits);
                    usleep(10000);//sleep for 10 milliseconds
                };
                zeroing_waits += 1;
            };

            vme->write_word(TMODE, 0x1);
            vme->write_word(vme_ctrl_70,0x0);
            vme->write_word(TMODE, 0x0);

            //if (zeroing_waits == 11)
            //throw ftk::AuxEMIFCalibration(ERS_HERE,proc);

        }


        bool send_data(VMEInterface *vme, const std::vector<u_int>& vme_data)
        {
            static u_int vme_ctrl_70 = 0x70;
            static u_int vme_data_11 = 0x110000;

            if(vme_data.size()==0) return 0;

            vme->write_block(vme_data_11,vme_data);

            // Set send_data
            vme->write_word(vme_ctrl_70, 0x20);

            // unset tmode
            vme->write_word(TMODE, 0x0);

            // //cout << "Reading FIFO " << fpga << endl;
            // // set  tmode
            // vme->write_word(TMODE, 0x1);

            // aux_fifostatus_t ammap_loading_word =  aux_fifostatus(vme, AMLOAD_ID_USEDW,0);
            // //cout << setw(15) << ammap_loading_word.usedw << "/" << setw(4) << ammap_loading_word.size << endl; ;
            // vme->write_word(TMODE, 0x0);

            // u_int nIterations = 0;
            // while(ammap_loading_word.usedw != 0){
            // 	//cout << setw(15) << ammap_loading_word.usedw << "/" << setw(4) << ammap_loading_word.size << endl; ;
            // 	//cout << "Reading FIFO " << fpga << endl;
            // 	cout << "Waiting" << endl;
            // 	vme->write_word(TMODE, 0x1);

            // 	ammap_loading_word =  aux_fifostatus(vme, AMLOAD_ID_USEDW,0);

            // 	vme->write_word(TMODE, 0x0);

            // 	++nIterations; 

            // 	if(nIterations > 100){
            // 	  cout << "Breaking..." <<endl;
            // 	  break;
            // 	}

            // }

            vme->write_word(TMODE, 0x1);

            vme->write_word(vme_ctrl_70, 0x10);

            return 0;
        }

        uint readAMCheckSum(u_int slot, u_int fpga){
            VMEInterface *vme=VMEManager::global().aux(slot, fpga);

            vme->write_word(TMODE, 0x1);

            vme->write_bit(PROC_DO_VMECONTROL, BIT_AMMAP_CHECKSUM_RESET, true);
            vme->write_bit(PROC_DO_VMECONTROL, BIT_AMMAP_CHECKSUM_RESET, false);

            vme->write_bit(PROC_DO_VMECONTROL, BIT_AMMAP_MUX_CHECKSUM,   true);
            vme->write_bit(PROC_DO_VMECONTROL, BIT_AMMAP_CHECKSUM_START, true);

            vme->write_word(TMODE, 0x0);

            // Wait till it is ready
            bool done=false;
            do
                done=vme->read_bit(DO_STATUS,BIT_AMMAP_CHECKSUM_DONE);
            while(!done);

            vme->write_word(TMODE, 0x1);
            vme->write_bit(PROC_DO_VMECONTROL, BIT_AMMAP_CHECKSUM_START, false);      
            vme->write_bit(PROC_DO_VMECONTROL, BIT_AMMAP_MUX_CHECKSUM,   false);
            vme->write_word(TMODE, 0x0);

            return vme->read_word(PROC_AM_READ_CHECKSUM_WORD);
        }

        std::string checkCache(std::string ammappath){
            std::string cachedCheckSumValue = "NotCached";
            std::string ammapdir="";
            unsigned int pos = 0;
            for (unsigned int i=0;i<ammappath.length();i++){
                if (ammappath.substr(i) == "/"){
                    pos = i;
                }
            }
            if (pos != 0){ammapdir = ammappath.substr(0, pos)+"/";}
            std::ifstream inFile;
            inFile.open(ammapdir+"ambankCheckSumCache.txt");
            std::string line;
            while (std::getline(inFile, line)){
                std::istringstream buf(line);
                std::istream_iterator<std::string> beg(buf), end;

                std::vector<std::string> tokens(beg, end);
                if (tokens.size() > 0 ){
                    if (tokens[0] == ammappath.substr(pos,ammappath.length())){
                        cachedCheckSumValue = tokens[1];
                    }
                }
            }
            inFile.close();
            return cachedCheckSumValue;
        }

        void updateCheckSum(uint16_t data, uint16_t& sum1, uint16_t& sum2)
        {
            sum2 = static_cast<uint16_t>(sum2 + sum1 + data)%0xFFFF;
            sum1 = static_cast<uint16_t>(sum1 + data       )%0xFFFF;
        }

        uint calcCheckSum(std::vector<std::bitset<144>> &AMMap, unsigned int nBits = 22, bool debug=true)
        {
            std::string ExpectedCheckSum;
            uint16_t data=0;
            uint16_t sum1=0, sum2=0;
            std::bitset<16> data_word_bs;
            for (uint i=0 ; i < pow(2,nBits); ++i)
            {

                if (debug == true)
                {
                    std::cout << std::hex << AMMap[i] << std::endl;
                    std::cout << std::hex << i << std::endl;
                }

                for (uint iByte= 0; iByte < 9; ++ iByte)
                {
                    data=0;
                    for(uint iBit=0; iBit<16; iBit++)
                        data|=AMMap[i][iByte*16+iBit]<<iBit;

                    updateCheckSum(data,sum1,sum2);

                    if (debug == true)
                    {
                        std::cout << iByte << " " << data_word_bs << std::endl;
                        std::cout <<  "\t" << std::hex << sum1 << "  " << std::hex << sum2 << std::endl;
                    }
                }
            }
            //delete [] AMMap;//why was this here? Breaks everything...

            std::ostringstream sum1_ss;
            std::ostringstream sum2_ss;
            sum1_ss << std::setw(4) << std::setfill('0') << std::hex << sum1;
            sum2_ss << std::setw(4) << std::setfill('0') << std::hex << sum2;
            std::string sum1_str=sum1_ss.str();
            std::string sum2_str=sum2_ss.str();

            ExpectedCheckSum = "0x"+sum2_str+sum1_str;
            std::cout << "checksum: " << ExpectedCheckSum << std::endl;

            return daq::ftk::string_to_int(ExpectedCheckSum);
        }

        uint calcCheckSumhex(string ammappath, unsigned int nBits = 22, bool debug=false, bool cache = false)
        {
            std::string cachedCheckSumValue = checkCache(ammappath);
            std::string ExpectedCheckSum;
            if (cachedCheckSumValue == "NotCached" or cache == false)
            {
                //
                // Init AMMap
                //
                uint a = pow(2,nBits);

                std::vector<std::bitset<144>> AMMap(a);
                //
                // Read AMMap
                // now set up for hex files
                int hexWordCount=0;
                std::ifstream inFile(ammappath);
                std::string line;
                std::string word_bin;
                std::string road_bin;
                std::string data_bin;
                unsigned long road_ID;
                std::stringstream data_word_shifted_ss;
                std::stringstream ones16b_ss;
                std::string data_word_16b_s;

                //      cout << "Read ammap" << endl;

                // Read the hex file into the AMMap array
                std::bitset<256> word;
                std::bitset<256> fullword;
                while (std::getline(inFile, line))
                {
                    std::stringstream lines(line);
                    unsigned long n;
                    lines >> std::hex >> n;

                    word=n;
                    word<<=32*hexWordCount;
                    fullword|=word;

                    hexWordCount++;
                    if (hexWordCount == 8)
                    {
                        word_bin = fullword.to_string();
                        road_bin = word_bin.substr(233,22);
                        data_bin = word_bin.substr(89,144);

                        std::bitset<22> road_bs(road_bin);
                        road_ID   = road_bs.to_ulong();

                        AMMap[road_ID] = std::bitset<144>(data_bin);

                        hexWordCount = 0;
                        fullword.reset();
                    }
                }
                // Calculate the checksum

                uint16_t data=0;
                uint16_t sum1=0, sum2=0;
                std::bitset<16> data_word_bs;
                for (uint i=0 ; i < a; ++i)
                {

                    for (uint iByte= 0; iByte < 9; ++ iByte)
                    {
                        data=0;
                        for(uint iBit=0; iBit<16; iBit++)
                            data|=AMMap[i][iByte*16+iBit]<<iBit;

                        updateCheckSum(data,sum1,sum2);

                        if (debug == true)
                        {
                            std::cout << std::hex << AMMap[i] << std::endl;
                            std::cout << std::hex << i << std::endl;
                            std::cout << iByte << " " << data_word_bs << std::endl;
                            std::cout <<  "\t" << std::hex << sum1 << "  " << std::hex << sum2 << std::endl;
                        }
                    }
                }

                std::ostringstream sum1_ss;
                std::ostringstream sum2_ss;
                sum1_ss << std::setw(4) << std::setfill('0') << std::hex << sum1;
                sum2_ss << std::setw(4) << std::setfill('0') << std::hex << sum2;
                std::string sum1_str=sum1_ss.str();
                std::string sum2_str=sum2_ss.str();

                ExpectedCheckSum = "0x"+sum2_str+sum1_str;
                std::cout << "checksum: " << ExpectedCheckSum << std::endl;

                std::ofstream cache("ambankCheckSumCache.txt");
                cache << ammappath+" "+ExpectedCheckSum+"\n";
                cache.close();
            }
            else
            {
                std::cout << "Using Cached CheckSum Value" << std::endl;
                ExpectedCheckSum = cachedCheckSumValue;
            }
            return daq::ftk::string_to_int(ExpectedCheckSum);
        }

        //bool compareCheckSum(u_int slot, u_int fpga,std::bitset<144> *AMMap, unsigned int nBits = 22, bool debug=false){
        bool compareAMCheckSum(uint slot, uint fpga, uint expected, bool warningOnly=false, bool debug=false){
            //uint expected = calcCheckSum(AMMap, nBits, debug);
            uint actual = readAMCheckSum(slot, fpga);
            if (expected == actual){
                std::cout << "checksum OK ... " << std::hex << std::setw(0) << std::setfill('0') << actual << std::dec << std::endl;
                return true;
            }else{
                std::cout << "checksum SNAFU ... " << std::endl;
                std::cout << "Expected: " << std::hex << std::setw(8) << std::setfill('0') << expected << std::dec << std::endl;
                std::cout << "Found:    " << std::hex << std::setw(8) << std::setfill('0') << actual   << std::dec << std::endl;

                return false;
            }
        }

        bool compareAMCheckSumhex(u_int slot, u_int fpga,string ammappath, unsigned int nBits = 22){
            uint expected = calcCheckSumhex(ammappath, nBits, false, true);
            uint actual = readAMCheckSum(slot, fpga);
            if (expected == actual){
                std::cout << "checksum OK ... " << std::hex << std::setw(0) << std::setfill('0') << actual << std::endl;
                return 1;
            }else{
                std::cout << "checksum SNAFU ... " << std::endl;
                std::cout << "Expected: " << std::hex << std::setw(8) << std::setfill('0') << expected << std::dec << std::endl;
                std::cout << "Found:    " << std::hex << std::setw(8) << std::setfill('0') << actual   << std::dec << std::endl;
                return 0;
            }
            //return 0;
        }

        bool aux_ammap_load_hex( u_int slot, u_int fpga, const std::string& ammappath, bool debug)
        {
            VMEInterface *vme=VMEManager::global().aux(slot, fpga);

            if(debug){
                cout << "In aux_ammap_load " << endl;
                cout << " slot: " << slot << endl;
                cout << " fpga: " << fpga << endl;
            }
            //check checksum before loading.
            bool alreadyLoaded = daq::ftk::compareAMCheckSumhex(slot,fpga,ammappath,22);
            if(alreadyLoaded) {
                ERS_LOG("Checksum matches: AMMap has already been loaded.");
                return 0;
            }
            ERS_LOG("Checksum does not match: Loading AMMap");
            //make sure EMIF is calibrated
            calibrate_EMIF(vme,fpga-2);

            //zero the contents of the external memory
            zero_emif(vme);

            //
            // set tmode
            //
            vme->write_word(TMODE, 0x1);

            static u_int vme_ctrl_70 = 0x70;

            vme->write_word(vme_ctrl_70, 0x10);

            // Read the ammap file and load it
            u_int        nFIFOsWritten = 0;
            std::vector<u_int> vme_data;

            // get number of lines in file for progress bar
            std::ifstream f(ammappath);
            std::string line;
            long int lines = 0;
            for (long int i = 1; std::getline(f, line); ++i) lines = i;
            f.close();
            cout << "Number of lines in AMMap Hex File: " << lines << endl;
            cout << endl;
            cout << endl;

            std::ifstream fh(ammappath);
            long int lineNumber = 0;
            uint nPercent;
            while(!fh.eof())
            {
                fh >> line;
                ++lineNumber;
                // Split line into parts
                std::string inputword_str = line.substr(0,8);

                u_int inputword = daq::ftk::string_to_int("0x"+inputword_str);
                //u_int vme_addr = 0x110000;
                //std::cout << "inputword " << std::hex << inputword << std::endl;
                vme_data.push_back(inputword);

                if(vme_data.size() == 1024){
                    //std::cout << "clearing fifo " << std::endl;
                    send_data(vme,vme_data);
                    vme_data.clear();
                    ++nFIFOsWritten;

                    // if( (nFIFOsWritten % 1000) == 0 ){
                    //   std::cout << "Written  " << nFIFOsWritten << " FIFOs " << std::endl;
                    // }
                }
                if (lines > 0){
                    nPercent = lineNumber*100/lines;
                } else {
                    cout << "ERROR NO LINES IN AMMAP FILE?"<<endl;
                    nPercent = 0;
                }

                //Check progress and report using ASCI control characters. "\033[L"=begining of previous line "\033[K" = clear line "\033[E" = next line
                if (lines > 200){
                    if (lineNumber%(lines/100) == 0 || lineNumber == lines){
                        std::cout<<"\033[F";
                        std::cout<<"\033[K"<<std::setw(51)<<std::string(nPercent/2+1,'-')<<std::setw(8)<<"Loaded "<<std::dec<<nPercent<<"% ("<<lineNumber<<"/"<<lines<<")"<<std::flush;
                        std::cout<<"\033[E";
                    }
                }
                else{
                    if (lineNumber == lines){
                        std::cout<<"\033[F";
                        std::cout<<"\033[K"<<std::setw(51)<<std::string(nPercent/2+1,'-')<<std::setw(8)<<"Loaded "<<std::dec<<nPercent<<"% ("<<lineNumber<<"/"<<lines<<")"<<std::flush;
                        std::cout<<"\033[E";
                    }
                }
            }
            cout << endl;
            fh.close();
            //
            //  send data
            //
            send_data(vme,vme_data);

            vme->write_word(vme_ctrl_70, 0x0);

            //unset tmode
            vme->write_word(TMODE, 0x0);
            daq::ftk::compareAMCheckSumhex(slot,fpga,ammappath,22);

            return 0;
        }

        bool aux_ammap_load( u_int slot, u_int fpga, const std::string& ammappath, u_int npatterns, 
                unsigned int nPatternsPerChip, unsigned int banktype, unsigned int SSOffset, unsigned int InChipOff, bool loadAllSectors, bool debug,
                FTKPatternBank* b, uint bankChecksum, bool checksum)
        {
            if(debug) cout << "make vme interface" << endl;
            VMEInterface *vme;
            if(!checksum) vme=VMEManager::global().aux(slot, fpga);

            if(debug){
                cout << "In aux_ammap_load " << endl;
                cout << " slot: " << slot << endl;
                cout << " fpga: " << fpga << endl;
            }

            // EMIF must be calibrated before it can be accessed to compute checksum.
            if(!checksum) calibrate_EMIF(vme,fpga-2);

            uint expected;
            if(bankChecksum != 0){
                cout << "Using checksum from pbank schema" << endl;
                expected = bankChecksum;
                bool alreadyLoaded = daq::ftk::compareAMCheckSum(slot,fpga,expected,true,debug);
                if(alreadyLoaded) {
                    ERS_LOG("Slot "<<slot<<" FPGA "<<fpga<<" AMMap Checksum matches pbank schema expected value: AMMap has already been loaded.");
                    return 0;
                }
                ERS_LOG("Slot "<<slot<<" FPGA "<<fpga<<" AMMap Checksum does not match pbank schema expected value: Reload pattern bank");

                if(daq::ftk::isLoadMode()){
                    daq::ftk::ftkException ex(ERS_HERE, name_ftk(),"Slot " +std::to_string(slot)+ " FPGA " +std::to_string(fpga)+ " AMMap Checksum does not match pbank schema expected value: Reload pattern bank");
                    ers::warning(ex);
                }

                if(!daq::ftk::isLoadMode()){
                    daq::ftk::ftkException ex(ERS_HERE, name_ftk(), "Slot " + std::to_string(slot) + " AMMap checksum does not match. Not in a configuration partition");
                    ers::warning(ex); //change to error eventually
                    return 0;
                }
            }


            //
            // Init AMMap for checksum
            //
            if(debug) cout << "Initialize AMMap bitset" << endl;
            unsigned int nBits = 22;
            uint a = pow(2,nBits);
            std::vector<std::bitset<144>> AMMap(a);

            if(debug) cout << "Check if bank is loaded to SBC memory yet" << endl;
            unsigned int nlayer = 8;
            //statements below have no effect
            //SSoffset is always passed as zero in argument to this function

#if 0
            if(!b->getIsOK()){//bank has not been loaded to SBC memory yet
                if(debug) cout << "bank not loaded to sbc memory. setNLayers,setSSOffset" << endl;
                for (u_int il=0;il < nlayer;++il) b->setSSOffset(il,SSOffset);
            }
#endif
            unsigned int maxPatterns = (nPatternsPerChip*16 < npatterns/4) ? nPatternsPerChip*16 : npatterns/4;
            u_int fpga_offset = (fpga - 3)*maxPatterns;

            if(loadAllSectors) fpga_offset = 0;

            //int p;

            std::vector<uint32_t> ss(nlayer);
            std::vector<uint32_t> dc(nlayer);
            std::vector<uint32_t> pattern(nlayer);


            for (int x = 0; x < 8; x++) {
                ss[x] = x; dc[x] = x; pattern[x] = x;
            }


            //check checksum before loading.
            std::bitset<32> oneword;
            std::bitset<256> amword;
            uint sec = 72;
            cout << "Compute CheckSum" << endl;
            //p = 
            b->loadPatternBank(ammappath.c_str(), 0, npatterns, debug);
            for (unsigned int pattid=0;pattid < maxPatterns;++pattid){
                unsigned int hardwarePattID = b->getHWRoadID(pattid, nPatternsPerChip, InChipOff);
                b->getPatternAUX(fpga_offset+pattid, pattern, ss, dc, sec, loadAllSectors, fpga - 3);

                // Make the amword
                amword.reset();

                amword|=sec;
                // Add the SSIDs
                for (u_int i=nlayer;i > 0;i--){
                    amword<<=16;
                    //amword|=(ss[i-1]<<2|dc[i-1]);
                    amword|=pattern[i-1];
                }

                // Add the roads
                amword<<=22;
                //amword|=pattid;
                amword|=hardwarePattID;

                // Add 1
                amword<<=1;
                amword |= 1;

                for(u_int ibit=0;ibit<144;ibit++) AMMap[hardwarePattID][ibit]=amword[23+ibit];
            }
            expected = calcCheckSum(AMMap, nBits, debug);

            if(checksum) return 0;//only compute checksum, do not load

            bool alreadyLoaded = daq::ftk::compareAMCheckSum(slot,fpga,expected,true,debug);
            if(alreadyLoaded) {
                ERS_LOG("Slot "<<slot<<" FPGA "<<fpga<<" Checksum matches calculation from root file: AMMap has already been loaded.");
                return 0;
            }
            ERS_LOG("Slot "<<slot<<" FPGA "<<fpga<<" Checksum does not match: Loading AMMap");
            //p = 
            b->loadPatternBank(ammappath.c_str(), 0, npatterns, debug);

            calibrate_EMIF(vme,fpga-2);

            //zero the contents of the external memory
            zero_emif(vme);

            //
            // set tmode
            //
            vme->write_word(TMODE, 0x1);

            static u_int vme_ctrl_70 = 0x70;

            vme->write_word(vme_ctrl_70, 0x10);

            // Read the ammap file and load it
            uint nFIFOsWritten = 0;
            std::vector<u_int> vme_data;
            for (unsigned int pattid=0;pattid < maxPatterns;++pattid){
                unsigned int hardwarePattID = b->getHWRoadID(pattid, nPatternsPerChip, InChipOff);
                b->getPatternAUX(fpga_offset+pattid, pattern, ss, dc, sec, loadAllSectors);

                // Make the amword
                amword.reset();

                amword|=sec;
                //Add the SSIDs
                for (u_int i=nlayer;i > 0;i--){
                    amword<<=16;
                    //amword|=(ss[i-1]<<2|dc[i-1]);
                    amword|=pattern[i-1];
                }

                //Add the roads
                amword<<=22;
                amword|=hardwarePattID;

                //Add 1
                amword<<=1;
                amword |= 1;

                //for(u_int ibit=0;ibit<144;ibit++) AMMap[hardwarePattID][ibit]=amword[23+ibit];

                for (u_int i=0;i < nlayer;++i){
                    for(u_int ibit=0;ibit<32;ibit++) oneword[ibit]=amword[i*32+ibit];

                    vme_data.push_back(oneword.to_ulong());

                    //if(vme_data.size() == 2048){
                    if(vme_data.size() == 1024){
                        //std::cout << "clearing fifo " << std::endl;
                        send_data(vme,vme_data);
                        vme_data.clear();
                        ++nFIFOsWritten;

                        if( (nFIFOsWritten % 1000) == 0 ){
                            std::cout << "Written FIFO " << nFIFOsWritten << " times " << std::endl;
                        }
                    }
                }
                }

                //
                //  send data
                //
                send_data(vme,vme_data);

                vme->write_word(vme_ctrl_70, 0x0);

                //unset tmode
                vme->write_word(TMODE, 0x0);
                daq::ftk::compareAMCheckSum(slot,fpga,expected,false,debug);

                return 0;
            }


        bool aux_ammap_load_rc( u_int slot, u_int fpga, const std::string& ammappath, u_int npatterns, 
                unsigned int nPatternsPerChip, unsigned int banktype, unsigned int SSOffset, unsigned int InChipOff, bool loadAllSectors, bool debug,
                FTKPatternBank* b, uint bankChecksum)
        {
            ftkTimeLog tLog(ERS_HERE, name_ftk(), "Download patternbank proc " + std::to_string(fpga-2));
            VMEInterface *vme;
            vme=VMEManager::global().aux(slot, fpga);

            //
            // Init AMMap for checksum
            //
            unsigned int nBits = 22;
            uint a = pow(2,nBits);
            std::vector<std::bitset<144>> AMMap(a);

            unsigned int nlayer = 8;

            unsigned int maxPatterns = (nPatternsPerChip*16 < npatterns/4) ? nPatternsPerChip*16 : npatterns/4;
            u_int fpga_offset = (fpga - 3)*maxPatterns;

            if(loadAllSectors) fpga_offset = 0;

            //int p;

            std::vector<uint32_t> ss(nlayer);
            std::vector<uint32_t> dc(nlayer);
            std::vector<uint32_t> pattern(nlayer);


            for (int x = 0; x < 8; x++) {
                ss[x] = x; dc[x] = x; pattern[x] = x;
            }


            //check checksum before loading.
            std::bitset<32> oneword;
            std::bitset<256> amword;
            uint sec = 72;
            ERS_LOG("Compute CheckSum");
            b->loadPatternBank(ammappath.c_str(), 0, npatterns, debug);
            for (u_int il=0;il < nlayer;++il) b->setSSOffset(il,SSOffset);
            for (unsigned int pattid=0;pattid < maxPatterns;++pattid){
                unsigned int hardwarePattID = b->getHWRoadID(pattid, nPatternsPerChip, InChipOff);
                b->getPatternAUX(fpga_offset+pattid, pattern, ss, dc, sec, loadAllSectors, fpga - 3);

                // Make the amword
                amword.reset();

                amword|=sec;
                // Add the SSIDs
                for (u_int i=nlayer;i > 0;i--){
                    amword<<=16;
                    //amword|=(ss[i-1]<<2|dc[i-1]);
                    amword|=pattern[i-1];
                }

                // Add the roads
                amword<<=22;
                //amword|=pattid;
                amword|=hardwarePattID;

                // Add 1
                amword<<=1;
                amword |= 1;

                for(u_int ibit=0;ibit<144;ibit++) AMMap[hardwarePattID][ibit]=amword[23+ibit];
            }
            
            b->loadPatternBank(ammappath.c_str(), 0, npatterns, debug);

            calibrate_EMIF(vme,fpga-2);

            //zero the contents of the external memory
            zero_emif(vme);

            //
            // set tmode
            //
            vme->write_word(TMODE, 0x1);

            static u_int vme_ctrl_70 = 0x70;

            vme->write_word(vme_ctrl_70, 0x10);

            // Read the ammap file and load it
            uint nFIFOsWritten = 0;
            std::vector<u_int> vme_data;
            for (unsigned int pattid=0;pattid < maxPatterns;++pattid){
                unsigned int hardwarePattID = b->getHWRoadID(pattid, nPatternsPerChip, InChipOff);
                b->getPatternAUX(fpga_offset+pattid, pattern, ss, dc, sec, loadAllSectors);

                // Make the amword
                amword.reset();

                amword|=sec;
                //Add the SSIDs
                for (u_int i=nlayer;i > 0;i--){
                    amword<<=16;
                    //amword|=(ss[i-1]<<2|dc[i-1]);
                    amword|=pattern[i-1];
                }

                //Add the roads
                amword<<=22;
                amword|=hardwarePattID;

                //Add 1
                amword<<=1;
                amword |= 1;

                //for(u_int ibit=0;ibit<144;ibit++) AMMap[hardwarePattID][ibit]=amword[23+ibit];

                for (u_int i=0;i < nlayer;++i){
                    for(u_int ibit=0;ibit<32;ibit++) oneword[ibit]=amword[i*32+ibit];

                    vme_data.push_back(oneword.to_ulong());

                    //if(vme_data.size() == 2048){
                    if(vme_data.size() == 1024){
                        //std::cout << "clearing fifo " << std::endl;
                        send_data(vme,vme_data);
                        vme_data.clear();
                        ++nFIFOsWritten;

                        if( (nFIFOsWritten % 1000) == 0 ){
                            std::cout << "Written FIFO " << nFIFOsWritten << " times " << std::endl;
                        }
                    }
                }
                }
                tLog.end(ERS_HERE);  // End log conter
            
                ftkTimeLog tLog2(ERS_HERE, name_ftk(), "Write patternbank to board proc " + std::to_string(fpga-2));
                //
                //  send data
                //
                send_data(vme,vme_data);

                vme->write_word(vme_ctrl_70, 0x0);

                //unset tmode
                vme->write_word(TMODE, 0x0);
                tLog2.end(ERS_HERE);  // End log conter

                return 0;
            }

            bool aux_ammap_convert_old(std::string ammappath, u_int npatterns, bool debug, bool loadAllSectors)
            {

                if(debug){
                    cout << "In aux_ammap_convert " << endl;
                }
                //unsigned int nBits = 22;
                //
                // Init AMMap for checksum
                //
                //uint a = pow(2,nBits);

                u_int fpga = 3;
                u_int nPatternsPerChip = 2048;
                u_int InChipOff = 1;
                unsigned int maxPatterns = (nPatternsPerChip*16 < npatterns/4) ? nPatternsPerChip*16 : npatterns/4;

                //proc to lamb mapping
                // std::map<unsigned int, unsigned int> procToLamb;
                // procToLamb[1] = 0;
                // procToLamb[2] = 1;
                // procToLamb[3] = 2;
                // procToLamb[4] = 3;

                // u_int fpga_offset = procToLamb[fpga-2]*maxPatterns;
                u_int fpga_offset = (fpga - 3)*maxPatterns;

                unsigned int nlayer = 8;
                FTKPatternBank b;
                for (u_int il=0;il < nlayer;++il) b.setSSOffset(il,0);
                //int p = 
                b.loadPatternBank(ammappath.c_str(), 0, npatterns, debug);
                std::vector<uint32_t> ss(nlayer);
                std::vector<uint32_t> dc(nlayer);
                std::vector<uint32_t> pattern(nlayer);

                for (int x = 0; x < 8; x++) {
                    ss[x] = x; dc[x] = x; pattern[x] = x;
                }

                unsigned int sec = 0;

                std::ofstream pattfile;
                std::string ammappathhex;
                ammappathhex = ammappath.replace(ammappath.length()-4,4,"hex");
                pattfile.open(ammappathhex);

                std::bitset<32> oneword;
                std::bitset<256> amword;      
                for (unsigned int pattid=0;pattid < npatterns;++pattid){
                    unsigned int hardwarePattID = b.getHWRoadID(fpga_offset+pattid, nPatternsPerChip, InChipOff);
                    b.getPatternAUX(fpga_offset+pattid, pattern, ss, dc, sec, loadAllSectors);
                    if(debug) cout << std::hex << "pattid: " << pattid << " hardwarePattID: " << hardwarePattID << endl; 
                    //b.getPatternAUX(pattid, pattern, ss, dc, sec);

                    // Make the amword
                    amword.reset();

                    amword|=sec;
                    // Add the SSIDs
                    for (u_int i=nlayer;i > 0;i--){
                        if(debug) cout << "Layer[" << i-1 << "]: " << ss[i-1] << " " << dc[i-1] << " -> pattern: " << pattern[i-1] << endl;
                        amword<<=16;
                        //amword|=(ss[i-1]<<2|dc[i-1]);
                        amword|=pattern[i-1];
                    }

                    // Add the roads
                    amword<<=22;
                    //amword|=pattid;
                    amword|=hardwarePattID;

                    // Add 1
                    amword<<=1;
                    amword |= 1;

                    for (u_int i=0;i < nlayer;++i){
                        for(u_int ibit=0;ibit<32;ibit++){
                            oneword[ibit]=amword[i*32+ibit];
                        }
                        std::stringstream outpatts;
                        outpatts << std::hex << oneword.to_ulong();
                        pattfile << std::setfill ('0') << std::setw(8) << outpatts.str() << "\n";
                    }
                }
                return 0;
            }
            bool aux_ammap_convert(std::string ammappath, u_int npatterns, bool debug, u_int InChipOff, bool loadAllSectors)
            {

                if(debug){
                    cout << "In aux_ammap_convert " << endl;
                }
                //unsigned int nBits = 22;
                //
                // Init AMMap for checksum
                //
                //uint a = pow(2,nBits);

                //u_int fpga = 3;
                //u_int nPatternsPerChip = 2048;
                //u_int InChipOff = 1;
                //unsigned int maxPatterns = (nPatternsPerChip*16 < npatterns/4) ? nPatternsPerChip*16 : npatterns/4;

                //proc to lamb mapping
                // std::map<unsigned int, unsigned int> procToLamb;
                // procToLamb[1] = 0;
                // procToLamb[2] = 1;
                // procToLamb[3] = 2;
                // procToLamb[4] = 3;

                // u_int fpga_offset = procToLamb[fpga-2]*maxPatterns;
                //u_int fpga_offset = (fpga - 3)*maxPatterns;

                unsigned int nlayer = 8;
                FTKPatternBank b;
                for (u_int il=0;il < nlayer;++il) b.setSSOffset(il,0);
                if(debug) cout << "loading pattern bank" << endl;
                b.loadPatternBank(ammappath.c_str(), 0, npatterns, debug);
                std::vector<uint32_t> ss(nlayer);
                std::vector<uint32_t> dc(nlayer);
                std::vector<uint32_t> pattern(nlayer);

                for (int x = 0; x < 8; x++) {
                    ss[x] = x; dc[x] = x; pattern[x] = x;
                }

                unsigned int sec = 72;

                std::ofstream pattfile;
                std::string ammappathhex;
                ammappathhex = ammappath.replace(ammappath.length()-4,4,"hex");
                pattfile.open(ammappathhex);
                std::bitset<16> oneword;
                std::bitset<144> amword;
                for (unsigned int pattid=0;pattid < npatterns;++pattid){
                    //unsigned int hardwarePattID = b.getHWRoadID(fpga_offset+pattid, nPatternsPerChip, InChipOff);
                    unsigned int hardwarePattID = pattid + InChipOff;
                    b.getPatternAUX(pattid, pattern, ss, dc, sec, loadAllSectors);

                    // Make the amword
                    amword.reset();

                    amword|=sec;
                    if(debug) cout << "SectorID: " << std::hex << sec << endl;
                    // Add the SSIDs
                    for (u_int i=nlayer;i > 0;i--){
                        amword<<=16;
                        if(debug) cout << "Layer: " << std::hex << i-1 << " SSID: " << ss[i-1] << " DC: " << dc[i-1] << " pattern[i-1]: " << pattern[i-1] << endl;
                        //amword|=(ss[i-1]<<2|dc[i-1]);
                        amword|=pattern[i-1];
                    }

                    // write road address
                    pattfile << hardwarePattID << "\n";

                    //write ammap word in 16 bit chunks, starting from 16 msb
                    for (int i=8;i >= 0;i--){
                        for(u_int ibit=0;ibit<16;ibit++){
                            oneword[ibit]=amword[i*16+ibit];
                        }
                        std::stringstream outpatts;
                        outpatts << std::hex << oneword.to_ulong();
                        pattfile << std::setfill ('0') << std::setw(4) << outpatts.str();
                        if(debug) cout << std::setfill ('0') << std::setw(4) << outpatts.str();
                    }
                    pattfile << "\n";
                    if(debug) cout << endl;

                }

                return 0;
            }


            signed long long aux_asr(signed long long input , int shift, int width) {

                // deal with the sign separately
                unsigned long long shifted = abs(input);

                // make the baseline shift
                if (shift > 0) shifted = (shifted << shift);
                if (shift < 0) {
                    // clear bits that will "go negative"
                    shifted &= ~((static_cast<long long>(1) << -shift) - 1);
                    shifted = (shifted >> -shift);
                }

                // save bits within the width (subtracting one bit for sign)
                signed long long output = shifted & ((static_cast<long long>(1) << (width-1))-1);

                bool overflow = false;
                if (static_cast<unsigned long long>(llabs(output)) != shifted) overflow = true;

                // reinstate the sign.
                if (input < 0) output *= -1;

                if (overflow)  output = (1 << (width-1)) - 1;

                return output;

            }



            bool aux_tfconst_load(u_int slot, u_int fpga, const std::string& gcon, int SECTOR, bool checksum, bool dump, bool loadAllSectors) {
                ftkTimeLog tLog(ERS_HERE, name_ftk(), "Download TF Constants Proc " + std::to_string(fpga-2));
                //loadAllSectors = load all sectors in file onto all procs
                //if SECTOR == -1, load all of the sectors in the file

                std::vector<u_int> words;

                const int NSECTORS = 4096; 
                const int NCOORDS = 11;
                const int NCHI = 6;
                const int NPIX = 3;
                const int NSCT = 5;

                const int NBITS_EXPO  = 5;
                const int NBITS_FIXED = 18;
                const int NBITS_FLOAT = 13;
                const int KERN_SHIFT = 12;
                const int KAVE_SHIFT = 4;
                const float const_scale_map[11] = {1, 16/16.88, 1, 16/16.88, 1, 16/16.88, 1, 1, 1, 1, 1};

                std::vector<int> sector_list;

                // dummy variables to read this into.
                std::string stra; int sector; int procSector; 
                double kaverage[NCHI];
                double kernel[NCHI][NCOORDS];
                double majkk[NCOORDS][NCOORDS];
                std::vector<double> majinvkk;

                // variables for the check sum
                std::bitset<1540> bm_cs;
                for (int i = 0; i < 16; i++) bm_cs.set(i);
                unsigned sum1 = 0, sum2 = 0, curr = 0;
                unsigned currval = 0;

                // read in the file.
                std::ifstream input (gcon.c_str());

                if(!input.is_open())
                {
                    daq::ftk::IOError ex(ERS_HERE,name_ftk(), gcon);
                    throw ex;
                }

                // Determine if AUX-a, AUX-b, or neither (ie 64-PU config)
                // Set the control bits appropriately
                VMEInterface *vme;
                vme=VMEManager::global().aux(slot,fpga);
                vme->write_word(TMODE,0x1);
                if (gcon[gcon.length()-6] == 'a') {
                    ERS_LOG("Setting AUXA");
                    vme->write_bit(AUX_ID_REG,AUX_ID_BIT,false);
                    vme->write_bit(AUX_128PU_REG,AUX_128PU_BIT,true);
                } else if (gcon[gcon.length()-6] == 'b') {
                    ERS_LOG("Setting AUXB");
                    vme->write_bit(AUX_ID_REG,AUX_ID_BIT,true);
                    vme->write_bit(AUX_128PU_REG,AUX_128PU_BIT,true);
                } else {
                    ERS_LOG("Setting 64PU");
                    vme->write_bit(AUX_ID_REG,AUX_ID_BIT,false);
                    vme->write_bit(AUX_128PU_REG,AUX_128PU_BIT,false);
                }
                vme->write_word(TMODE,0x0);

                while (input >> stra) {


                    // look for the "sector" keyword
                    if (stra != "sector") continue;

                    words.clear();
                    majinvkk.clear();

                    input >> sector;
                    if (SECTOR >= 0 && SECTOR != sector) continue; //Only loading a specific sector, and it isn't this one


                    if(!loadAllSectors){//use mod 4 sectors if not loading all sectors on all procs
                        if((int)(sector%4) != (int)(fpga-3)) continue; //This sector is not loaded on this proc when loading "all" sectors
                        //	  if ((int)(sector%4) != (int)(fpga-3)) continue;

                        procSector = sector >> 2;
                        //	  std::cout << "loading proc sector: " << procSector << std::endl;
                        //	  std::cout << "proc Sector: " << procSector << "  sector: " << sector << std::endl;
                    }else{
                        procSector = sector;
                        //	  std::cout << "loading sector: " << SECTOR << std::endl;
                    }
                    // sector += 2048;
                    if (procSector >= NSECTORS) break;

                    // if it _is_ right, look for kaverages and kernel
                    // load these into memory.
                    while (input >> stra) if (stra == "kaverages") break;
                    for (int i = 0; i < NCHI; i++) {
                        input >> kaverage[i]; 
                    }
                    while (input >> stra) if (stra == "kernel")    break;
                    for (int i = 0; i < NCHI; i++) {
                        for (int j = 0; j < NCOORDS; j++) {
                            input >> kernel[i][j];
                            kernel[i][j] = const_scale_map[j] * kernel[i][j];
                        }
                    }

                    // calculate the majority recovery constants
                    for (int i = 0; i < NCOORDS; i++) {
                        for (int j = 0; j < NCOORDS; j++) {
                            majkk[i][j] = 0.;

                            // we actually just need the block diagonal bit,
                            // but make the whole matrix, regardless.
                            for (int r = 0; r < NCHI; r++) 
                                majkk[i][j] += kernel[r][i] * kernel[r][j];
                        }
                    }


                    // calculate the matrix inverses.
                    for (int pi = 0; pi < 2*NPIX; pi += 2) {

                        //get determinant.
                        double det = majkk[pi][pi] * majkk[pi+1][pi+1] - majkk[pi+1][pi] * majkk[pi][pi+1];

                        // negatives to help with t-vector calculation
                        majinvkk.push_back( - majkk[pi+1][pi+1]/det);
                        majinvkk.push_back(   majkk[pi+1][pi  ]/det);
                        majinvkk.push_back( - majkk[pi  ][pi  ]/det);
                    }
                    for (int si = NPIX*2; si < NPIX*2 + NSCT; si++) {
                        majinvkk.push_back( - 1./majkk[si][si]);
                    }


                    ///////
                    // At this point we have all the constants ready.
                    // Now we just need to arrange the bits for the TF.
                    // Put these into bitsets of the appropriate size,
                    // and then copy them into one gigantic set of 'bits'
                    // (1540 = 77 * 20 long) as the full set of constants
                    // for that procSector. 

                    int currbit = 0;
                    std::bitset<1540> bits;

                    for (int i = 0; i < NCHI; i++) {
                        for (int j = 0; j < NCOORDS; j++) {
                            //            std::bitset<18> kerbit(daq::ftk::aux_asr(round(kernel[i][j] * pow(2., KERN_SHIFT)), 0, NBITS_FIXED));
                            std::bitset<18> kerbit(daq::ftk::aux_asr(trunc(kernel[i][j] * pow(2., KERN_SHIFT)), 0, NBITS_FIXED));
                            for (int bi = 0; bi < NBITS_FIXED; bi++, currbit++) bits.set(currbit, kerbit[bi]);
                        }
                        //          std::bitset<18> kavbit(daq::ftk::aux_asr(round(kaverage[i] * pow(2., KAVE_SHIFT)), 0, NBITS_FIXED));
                        std::bitset<18> kavbit(daq::ftk::aux_asr(trunc(kaverage[i] * pow(2., KAVE_SHIFT)), 0, NBITS_FIXED));
                        for (int bi = 0; bi < NBITS_FIXED; bi++, currbit++) bits.set(currbit, kavbit[bi]);
                    }


                    for (auto v : majinvkk) {

                        int expo   = NBITS_FLOAT - ceil(log2(fabs(v))) - 1;
                        int signif = daq::ftk::aux_asr(round(v * pow(2, expo)), 0, NBITS_FLOAT);

                        std::bitset<17> conbit((signif << 5) | expo);
                        for (int bi = 0; bi < NBITS_EXPO+NBITS_FLOAT-1; bi++, currbit++) bits.set(currbit, conbit[bi]);
                    }


                    // just a mask to save 20 bits at a time.
                    // this is the size of the constants.
                    std::bitset<1540> bm;
                    for (int i = 0; i < 20; i++) bm.set(i);

                    // "rev up" the system.
                    words.clear();
                    unsigned long long tmp_word = ((bits & bm).to_ullong() << 20) | (procSector << 1) | 1;
                    for (int i = 0; i < 3; i++) {
                        words.push_back(tmp_word & 0xffffffff);
                        words.push_back((tmp_word >> 32) & 0xffffffff);
                    }

                    for (int i = 0; i < 77; i++) { // forwards

                        // shift the bits past the 20-bit mask, saving one word.
                        unsigned long long data = ((bits >> (i*20)) & bm).to_ullong();

                        // (20 bits of data ) + (7 bits of addr) + (12 bits of procSector) + ("write")
                        unsigned long long word = (data << 20) | (i << 13) | (procSector << 1) | 1;

                        // split the 64 bit word into two pieces for VME
                        words.push_back(word & 0xffffffff);
                        words.push_back((word >> 32) & 0xffffffff);

                    }

                    // turn off writing.
                    words.push_back(procSector << 1); 
                    words.push_back(0);

                    if (dump) {
                        FILE *fp=stdout;
                        char outfile [50];
                        if(loadAllSectors){
                            sprintf (outfile, "tf_hex_const/sec%04d", sector);
                            cout << "writing " << sector << " to: " << outfile << endl;
                            if ((fp=fopen(outfile, "w")) == NULL) {
                                fprintf(stderr,"Cannot open file: %s.\n", outfile);
                                exit(1);
                            } else {
                                for (auto w : words) fprintf(fp, "%08x\n", w);
                                fclose(fp);
                                sector_list.push_back(sector);
                            }
                        } else {
                            sprintf (outfile, "tf_hex_const/proc%d/sec%04d", (fpga-2),procSector);
                            cout << "writing " << sector << " to: " << outfile << endl;
                            if ((fp=fopen(outfile, "w")) == NULL) {
                                fprintf(stderr,"Cannot open file: %s.\n", outfile);
                                exit(1);
                            } else {
                                for (auto w : words) fprintf(fp, "%08x\n", w);
                                fclose(fp);
                                sector_list.push_back(procSector);
                            }
                        }
                    }


                    if (checksum) { // checksum

                        for (int x = 0; x < 97; x++) {

                            curr = ((bits >> (x*16)) & bm_cs).to_ullong();
                            sum2 = ((curr + sum1 + sum2) & 0xffff) % 65535;
                            sum1 = ((curr + sum1) & 0xffff) % 65535;

                        }

                        if (SECTOR >= 0) {

                            unsigned stemp1 = sum1;
                            unsigned stemp2 = sum2;
                            //            for (int sx = sector + 1; sx < NSECTORS; sx++)
                            for (int sx = procSector + 1; sx < NSECTORS; sx++) {
                                for (int x = 0; x < 97; x++) {

                                    curr = 0;
                                    stemp2 = ((curr + stemp1 + stemp2) & 0xffff) % 65535;
                                    stemp1 = ((curr + stemp1) & 0xffff) % 65535;

                                }
                            }
                            currval = ((stemp2 << 16) + stemp1);
                        } 
                        else currval = ((sum2 << 16) + sum1);
                    }


                    if (!dump ) {//and !checksum) 
                        vme=VMEManager::global().aux(slot,fpga);
                        vme->write_word(TMODE,0x1);
                        vme->write_word(0x70,0x1);

                        vme->write_block(0x12 << 16, words);
                        // std::cout << "vme->write_block(0x12 << 16, words)" << std::endl;
                        // std::cout << "words: " << words.at(0) << std::endl;
                        // std::cout << "words: " << words.at(1) << std::endl;
                        // std::cout << "words: " << words.at(2) << std::endl;


                        vme->write_word(0x70, 0x0);
                        vme->write_word(0x70, 0x2);      
                        vme->write_word(TMODE,0x0);
                    }
                    // vme->write_word(TMODE,0x1);
                    // vme->write_word(0x70, 0x4);
                    // vme->write_word(TMODE,0x0);
                    // //	vme->read_word(0x1E0);
                    // sleep(2);
                    // std::cout << "checksum on board: " << std::hex << vme->read_word(0x1E0) << " checksum expected: " << currval << std::dec << std::endl;

                    if (SECTOR >= 0) break;
                }
                tLog.end(ERS_HERE);  // End log conter
                
                
                ftkTimeLog tLog2(ERS_HERE, name_ftk(), "Write TF Constants to board Proc " + std::to_string(fpga-2));

                input.close(); // close the file -- done with loading
                ERS_LOG("Loaded " << procSector << " sectors out of " << NSECTORS << " from gcon");
                if(procSector < NSECTORS-1){

                    for(int sec = procSector+1; sector < NSECTORS; sector++){
                        words.clear();

                        for (int i = 0; i < 3; i++) {
                            words.push_back(0x00000000 & 0xffffffff);
                            words.push_back(0x00000000 & 0xffffffff);
                        }
                        for (int i = 0; i < 77; i++) { // forwards

                            // shift the bits past the 20-bit mask, saving one word.
                            unsigned long long data = 0x00000000;

                            // (20 bits of data ) + (7 bits of addr) + (12 bits of procSector) + ("write")
                            unsigned long long word = (data << 20) | (i << 13) | (sec << 1) | 1;

                            // split the 64 bit word into two pieces for VME
                            words.push_back(word & 0xffffffff);
                            words.push_back((word >> 32) & 0xffffffff);

                        }

                        // turn off writing.
                        words.push_back(sec << 1); 
                        words.push_back(0);
                        VMEInterface *vme;
                        if (!dump ) {//and !checksum) 
                            vme=VMEManager::global().aux(slot,fpga);
                            vme->write_word(TMODE,0x1);
                            vme->write_word(0x70,0x1);

                            vme->write_block(0x12 << 16, words);
                            // std::cout << "vme->write_block(0x12 << 16, words)" << std::endl;
                            // std::cout << "words: " << words.at(0) << std::endl;
                            // std::cout << "words: " << words.at(1) << std::endl;
                            // std::cout << "words: " << words.at(2) << std::endl;


                            vme->write_word(0x70, 0x0);
                            vme->write_word(0x70, 0x2);      
                            vme->write_word(TMODE,0x0);
                        }
                    }
                }


                tLog2.end(ERS_HERE);  // End log conter
                
                // if (dump) {
                // 	std::cout << "list of sectors dumped: [ ";
                // 	for (auto sec:sector_list){
                // 	  std::cout << sec << " ";
                // 	}
                // 	std::cout << "]" << std::endl;
                // }
                if (checksum) std::cout << "checksum: " << std::hex << currval << std::endl;

                // let's see what we've got!!!
                // for (auto w : words) std::cout << std::setfill('0') << std::setw(8) << std::hex << w << std::endl;

                return false;

            }


            bool aux_tfconst_check( u_int slot, u_int fpga, const std::string& tfconstpath )
            { 

                int status;
                u_int ret, register_content;
                u_int vmeaddr, slotaddr, fpgaaddr;
                int handle;

                static VME_MasterMap_t master_map = {0x0, 0x10000000, VME_A32, 0};

                slotaddr = ((slot&0x1f) << 27);
                fpgaaddr = ((fpga&0x1f) << 24);
                vmeaddr = slotaddr + fpgaaddr;

                ret = VME_Open();
                if (ret != VME_SUCCESS)
                {
                    VME_ErrorPrint(ret);
                    FTK_VME_ERROR( "Failure when calling the function 'VME_Open' in function" );
                }

                master_map.vmebus_address   = vmeaddr;
                master_map.window_size      = 0x1000000;
                master_map.address_modifier = VME_A32;
                master_map.options          = 0;
                ret = VME_MasterMap(&master_map, &handle);
                if (ret != VME_SUCCESS)
                {
                    VME_ErrorPrint(ret);
                    FTK_VME_ERROR( "Failure when calling the function 'VME_MasterMap' in function"  );
                }
                ///


                //
                // set tmode
                //
                status = VME_WriteSafeUInt(handle, TMODE, 0x1);
                CHECK_ERROR(status);

                u_int vme_ctrl_70 = 0x70;

                status = VME_WriteSafeUInt(handle, vme_ctrl_70, 0x1);
                CHECK_ERROR(status);


                // Read the tfconst file and load it
                std::ifstream fh(tfconstpath);
                std::string line;

                while( getline(fh, line) )
                {
                    std::string inputword_str = line.substr(0,8);

                    u_int inputword = daq::ftk::string_to_int("0x"+inputword_str);
                    u_int vme_addr = 0x120000;
                    //std::cout << "Address is " << std::hex << vmeaddr << " + " << std::hex << vme_addr << "\tdata: " << std::hex << handle << std::endl;
                    status = VME_WriteSafeUInt(handle, vme_addr, inputword);
                    CHECK_ERROR(status);

                }
                fh.close();

                status = VME_WriteSafeUInt(handle, vme_ctrl_70, 0x0);
                CHECK_ERROR(status);

                status = VME_WriteSafeUInt(handle, vme_ctrl_70, 0x2);
                CHECK_ERROR(status);

                //unset tmode
                status = VME_WriteSafeUInt(handle, TMODE, 0x0);
                CHECK_ERROR(status);


                //unset tmode
                status = VME_WriteSafeUInt(handle, TMODE, 0x1);
                CHECK_ERROR(status);

                std::ofstream outFile;
                outFile.open(".sector_found");
                for(unsigned int i =0; i< 79; ++i){
                    u_int vme_check_add = 0x230000;
                    status = VME_ReadSafeUInt(handle, vme_check_add, &register_content);
                    CHECK_ERROR(status);
                    outFile.fill('0');
                    outFile.width(8);
                    outFile << std::hex << register_content <<endl;
                }
                outFile.close();

                //unset tmode
                status = VME_WriteSafeUInt(handle, TMODE, 0x0);
                CHECK_ERROR(status);

                ret = VME_MasterUnmap(handle);
                if (ret != VME_SUCCESS)
                {
                    VME_ErrorPrint(ret);
                    FTK_VME_ERROR( "Failure when calling the function 'VME_MasterUnMap' in function"  );

                }

                ret = VME_Close();
                if (ret != VME_SUCCESS)
                {
                    VME_ErrorPrint(ret);
                    FTK_VME_ERROR( "Failure when calling the function 'VME_Close' in function"  );

                }

                return 0;

            }


            bool aux_vmeblocktest( u_int slot, u_int fpga, u_int address, u_int nWords )
            { 
                VMEInterface *vme=VMEManager::global().aux(slot,fpga);

                vme->write_word(TMODE,0x1);

                //
                // WRITE
                //
                std::vector<u_int> data;
                for(u_int i=0;i<nWords;i++)
                    data.push_back((i+1)+((i+1)<<16));

                clock_t start = clock(), diff;
                vme->write_block(address << 16, data);
                diff = clock() - start;

                int w_msec = diff * 1000 / CLOCKS_PER_SEC;


                //
                // READ
                //
                start = clock();
                data=vme->read_block(address << 16,nWords);
                diff = clock() - start;

                for(u_int i=0;i<data.size();i++)
                {
                    std::cout << "[0x" << std::setw(4) << std::setfill('0') << std::right << std::hex << i << "] 0x" << std::setw(8) << std::setfill('0') << std::right << std::hex << data[i];
                    if(data[i]!=(i+1)+((i+1)<<16))
                        std::cout << "\tERROR";
                    std::cout << std::endl;
                }
                std::cout << std::dec;

                int r_msec = diff * 1000 / CLOCKS_PER_SEC;

                vme->write_word(TMODE,0x0);


                // Status
                std::cout << "write speed: " << nWords << " words / " << ((float)w_msec)/1000. << " seconds = " << 4*((float)nWords)/(((float)w_msec)/1000.)/1000. << " kBytes/s" << std::endl;
                std::cout << "read speed: " << nWords << " words / " << ((float)r_msec)/1000. << " seconds = " << 4*((float)nWords)/(((float)r_msec)/1000.)/1000. << " kBytes/s" << std::endl;

                return 0;

            }


            bool aux_upgrade( int slot, int fpga, uint epcqaddress, std::string file, uint startaddress, uint endaddress)
            { 
                int status;
                u_int ret;
                u_int vmeaddr, slotaddr, fpgaaddr;
                int handle;

                static VME_MasterMap_t master_map = {0x0, 0x10000000, VME_A32, 0};

                slotaddr = ((slot&0x1f) << 27);
                fpgaaddr = ((fpga&0x1f) << 24);
                vmeaddr = slotaddr + fpgaaddr;

                ret = VME_Open();
                if (ret != VME_SUCCESS)
                {
                    VME_ErrorPrint(ret);
                    FTK_VME_ERROR( "Failure when calling the function 'VME_Open' in function" );
                }

                master_map.vmebus_address   = vmeaddr;
                master_map.window_size      = 0x1000000;
                master_map.address_modifier = VME_A32;
                master_map.options          = 0;
                ret = VME_MasterMap(&master_map, &handle);
                if (ret != VME_SUCCESS)
                {
                    VME_ErrorPrint(ret);
                    FTK_VME_ERROR( "Failure when calling the function 'VME_MasterMap' in function"  );
                }


                //
                // START
                // 

                uint nWords=(endaddress-startaddress)/4+1;

                // Reset EPCQ Control
                status = VME_WriteSafeUInt(handle, TMODE, 0x1);
                CHECK_ERROR(status);

                status = VME_WriteSafeUInt(handle, RU_EPCQ, 0x0);
                CHECK_ERROR(status);
                status = VME_WriteSafeUInt(handle, RU_ADDR, 0x0);
                CHECK_ERROR(status);

                status = VME_WriteSafeUInt(handle, TMODE, 0x0);
                CHECK_ERROR(status);

                // Clear anything in the buffers
                status = VME_WriteSafeUInt(handle, TMODE, 0x1);
                CHECK_ERROR(status);
                status = VME_WriteSafeUInt(handle, RU_EPCQ, 0x10);
                CHECK_ERROR(status)
                    status = VME_WriteSafeUInt(handle, TMODE, 0x0);
                CHECK_ERROR(status);

                status = VME_WriteSafeUInt(handle, TMODE, 0x1);
                CHECK_ERROR(status);
                status = VME_WriteSafeUInt(handle, RU_EPCQ, 0x0);
                CHECK_ERROR(status);
                status = VME_WriteSafeUInt(handle, TMODE, 0x0);
                CHECK_ERROR(status);

                // Read data, word by word
                std::ifstream f(file.c_str(), std::ios::binary);
                f.seekg(startaddress);

                bool done=false;
                bool needwrite=false;
                uint waitforserase=0;
                uint nextaddr=epcqaddress;
                uint i=0;

                char c[4];
                uint data;
                while(f && !done)
                {
                    // Erase this sector if needed
                    if(i%0x4000==0)
                    { // Sector erase
                        PRINT_LOG("SERASE " << std::setfill('0') << std::setw(8) << std::hex << epcqaddress+i*4);

                        status = VME_WriteSafeUInt(handle, TMODE, 0x1);
                        CHECK_ERROR(status);
                        status = VME_WriteSafeUInt(handle, RU_EPCQ, 0x4);
                        CHECK_ERROR(status);
                        status = VME_WriteSafeUInt(handle, RU_ADDR, epcqaddress+i*4);
                        CHECK_ERROR(status);
                        status = VME_WriteSafeUInt(handle, TMODE, 0x0);
                        CHECK_ERROR(status);

                        waitforserase=4;
                    }

                    status = VME_WriteSafeUInt(handle, TMODE, 0x1);
                    CHECK_ERROR(status);
                    unsigned int wi;
                    for(wi=0;wi<64;wi++)  // wi = word index
                    {
                        // Read the next data word
                        f.read(c,sizeof(c));
                        data=0;
                        data|=(((unsigned char)c[3])<<0);
                        data|=(((unsigned char)c[2])<<8);
                        data|=(((unsigned char)c[1])<<16);
                        data|=(((unsigned char)c[0])<<24);
                        //PRINT_LOG("DATA " << std::setfill('0') << std::setw(8) << std::hex << data);

                        // Load the data...
                        status = VME_WriteSafeUInt(handle, RU_DATA, data);
                        CHECK_ERROR(status);

                        i++;
                        needwrite=true;
                        if(i==nWords)
                        {
                            done=true;
                            break;
                        }
                    }
                    status = VME_WriteSafeUInt(handle, TMODE, 0x0);
                    CHECK_ERROR(status);

                    if(waitforserase!=0)
                    {
                        // Wait for previous erase operation to finish
                        if(aux_upgrade_wait_for_done(handle)!=0) return false;

                        // Reset EPCQ Control
                        waitforserase=false;

                        status = VME_WriteSafeUInt(handle, TMODE, 0x1);
                        CHECK_ERROR(status);

                        status = VME_WriteSafeUInt(handle, RU_EPCQ, 0x0);
                        CHECK_ERROR(status);
                        status = VME_WriteSafeUInt(handle, RU_ADDR, 0x0);
                        CHECK_ERROR(status);

                        status = VME_WriteSafeUInt(handle, TMODE, 0x0);
                        CHECK_ERROR(status);
                    }

                    if(needwrite)
                    {
                        // Write the contents as 256 byte buffer is full
                        status = VME_WriteSafeUInt(handle, TMODE, 0x1);
                        CHECK_ERROR(status);

                        //printf("WRITE %08X\n",nextaddr);
                        status = VME_WriteSafeUInt(handle, RU_EPCQ, 0x2);
                        CHECK_ERROR(status);
                        status = VME_WriteSafeUInt(handle, RU_ADDR, nextaddr);
                        CHECK_ERROR(status);

                        status = VME_WriteSafeUInt(handle, TMODE, 0x0);
                        CHECK_ERROR(status);
                        PRINT_LOG("BEGIN WRITE AT " << std::setfill('0') << std::setw(8) << std::hex << nextaddr);

                        // Wait for previous erase operation to finish
                        if(aux_upgrade_wait_for_done(handle)!=0) return false;

                        // Reset EPCQ Control
                        needwrite=false;

                        status = VME_WriteSafeUInt(handle, TMODE, 0x1);
                        CHECK_ERROR(status);

                        status = VME_WriteSafeUInt(handle, RU_EPCQ, 0x0);
                        CHECK_ERROR(status);
                        status = VME_WriteSafeUInt(handle, RU_ADDR, 0x0);
                        CHECK_ERROR(status);

                        status = VME_WriteSafeUInt(handle, TMODE, 0x0);
                        CHECK_ERROR(status);

                    }

                    // Reset EPCQ Control
                    status = VME_WriteSafeUInt(handle, TMODE, 0x1);
                    CHECK_ERROR(status);

                    status = VME_WriteSafeUInt(handle, RU_EPCQ, 0x0);
                    CHECK_ERROR(status);
                    status = VME_WriteSafeUInt(handle, RU_ADDR, 0x0);
                    CHECK_ERROR(status);

                    status = VME_WriteSafeUInt(handle, TMODE, 0x0);
                    CHECK_ERROR(status);

                    nextaddr=nextaddr+256;
                }

                f.close();

                // Reset EPCQ Control
                status = VME_WriteSafeUInt(handle, TMODE, 0x1);
                CHECK_ERROR(status);

                status = VME_WriteSafeUInt(handle, RU_EPCQ, 0x0);
                CHECK_ERROR(status);
                status = VME_WriteSafeUInt(handle, RU_ADDR, 0x0);
                CHECK_ERROR(status);

                status = VME_WriteSafeUInt(handle, TMODE, 0x0);
                CHECK_ERROR(status);

                // Disable remote update again
                status = VME_WriteSafeUInt(handle, RU_ENABLE, 0);
                CHECK_ERROR(status);

                //
                // END
                // 
                ret = VME_MasterUnmap(handle);
                if (ret != VME_SUCCESS)
                {
                    VME_ErrorPrint(ret);
                    FTK_VME_ERROR( "Failure when calling the function 'VME_MasterUnMap' in function"  );

                }

                ret = VME_Close();
                if (ret != VME_SUCCESS)
                {
                    VME_ErrorPrint(ret);
                    FTK_VME_ERROR( "Failure when calling the function 'VME_Close' in function"  );

                }

                PRINT_LOG("DONE WITH FIRMWARE UPDATE");

                return 0;

            }

            ////// HELPERS //////
            int aux_upgrade_wait_for_done(int handle)
            {
                int status;
                u_int register_content=0;
                do
                {
                    status = VME_ReadSafeUInt(handle, RU_STATUS, &register_content);
                    CHECK_ERROR(status);
                }
                while((register_content&2)!=2);
                return 0;
            }

            int aux_upgrade_clear_regs(int handle)
            {
                int status;
                //u_int register_content=0;

                status = VME_WriteSafeUInt(handle, TMODE, 0x1);
                CHECK_ERROR(status);

                status = VME_WriteSafeUInt(handle, RU_EPCQ, 0x0);
                CHECK_ERROR(status);
                status = VME_WriteSafeUInt(handle, RU_ADDR, 0x0);
                CHECK_ERROR(status);
                //status = VME_WriteSafeUInt(handle, RU_DATA, 0x0);

                status = VME_WriteSafeUInt(handle, TMODE, 0x0);
                CHECK_ERROR(status);
                return 0;
            }
        }
    }

/*           Program to issue a routine for read the AUX RAM                   */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */

#include <string>
#include <iostream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "aux/aux_vme_regs.h"
#include "aux/aux_vmetest.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("fpga", value< std::string >()->default_value("-1"), "The FPGA ID (-1 for random)")
    ("addr", value< std::string >()->default_value("-1"), "The VME address (-1 for random)")
    ("n", value< std::string >()->default_value("10"), "Number of tests to run")
    ;
  
  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }
  
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }
  
  int n = daq::ftk::string_to_int( vm["n"].as<std::string>() );
  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int fpga = daq::ftk::string_to_int( vm["fpga"].as<std::string>() );
  int addr = daq::ftk::string_to_int( vm["addr"].as<std::string>() );

  bool doRandomFPGA = (fpga==-1);
  bool doRandomAddr = (addr==-1);

  int good[48];
  int fail[48];
  for(int i=0;i<48;i++)
    {
      good[i]=0;
      fail[i]=0;
    }

  for(int i=0;i<n;i++)
    {
      if(doRandomFPGA) fpga = 3 + (rand() % 4);

      if(doRandomAddr) addr = (1+rand()%8)<<4;

      if(daq::ftk::aux_vmetest(slot, fpga, addr, 0x10101010)==0)
	good[(fpga-1)*8+(addr>>4)-1]++;
      else
	fail[(fpga-1)*8+(addr>>4)-1]++;
    }

  for(fpga=0;fpga<6;fpga++)
    {
      std::cout << "FPGA: " << fpga+1 << std::endl;
      for(addr=0;addr<8;addr++)
	{
	  std::cout << "0x" << std::hex << ((addr+1)<<4) << ":\t" << good[fpga*8+addr] << "\t" << fail[fpga*8+addr] << std::endl;
	}
    }

    return 0;
}

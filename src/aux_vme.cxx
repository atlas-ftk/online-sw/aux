//
// AUX VME helper functions
//
#include "aux/aux_vme.h"

#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "aux/aux_vme_regs.h"

namespace daq 
{
  namespace ftk 
  {
    int aux_vme_write_bit(int handle, u_int address, u_int bit, u_int value)
    { 
      int status;
      u_int register_content;

      //Read last value
      status = VME_ReadSafeUInt(handle, address, &register_content);
      CHECK_ERROR(status);

      register_content ^= (-value ^ register_content) & (1 << bit);
      
      //write the data
      status = VME_WriteSafeUInt(handle, address, register_content);
      CHECK_ERROR(status);

      return VME_SUCCESS;
    }
  }
}

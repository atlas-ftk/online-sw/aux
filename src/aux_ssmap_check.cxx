#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <bitset>
#include <fstream>
#include <utility>

#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "aux/aux_vme_regs.h"

#include "aux/Report.h"
#include "aux/TF/TFGen.h"
#include "aux/TF/TFConstants.h"
#include "aux/TF/TFEventList.h"
#include "aux/TF/TFFitter.h"
#include "aux/aux_check.h"
#include "aux/aux_spybuffer_lib.h"
#include "aux/aux_ssmap_check.h"

#include "aux/aux_spy_regs.h"

#include <iostream>
#include <sstream> // ostringstream

#include <stdio.h>
#include <stdlib.h>

using std::cout;  using std::endl;

// Function for SSMap bit level check

namespace daq {
    namespace ftk {

        void ssmap_toplevel(bool verbose, const std::string& inDirName, string tower, string ssbase, bool doHS=true, bool is_loop=false)
        {   

            ssmap_buffer input_buffers[11] = { {"sct","4",inDirName+"ss_i2.dat",0,inDirName+"do_input_i2.dat",0}, {"sct","5",inDirName+"ss_i2.dat",1,inDirName+"do_input_i2.dat",1}, {"sct","6",inDirName+"ss_i2.dat",2,inDirName+"do_input_i2.dat",2}, {"sct","7",inDirName+"ss_i2.dat",3,inDirName+"do_input_i2.dat",3}, {"pixel_even","0",inDirName+"ss_i1.dat",0,inDirName+"do_input_i1.dat",1}, {"pixel_odd","0",inDirName+"ss_i1.dat",0,inDirName+"do_input_i1.dat",2}, {"pixel_even","1",inDirName+"ss_i1.dat",1,inDirName+"do_input_i1.dat",3}, {"pixel_odd","1",inDirName+"ss_i1.dat",1,inDirName+"do_input_i1.dat",4}, {"pixel_even","2",inDirName+"ss_i1.dat",2,inDirName+"do_input_i1.dat",5}, {"pixel_odd","2",inDirName+"ss_i1.dat",2,inDirName+"do_input_i1.dat",6}, {"sct","3",inDirName+"ss_i1.dat",3,inDirName+"do_input_i1.dat",0}};

            vector<vector<string>> in_packets;
            vector<vector<string>> out_packets;
            std::pair <vector<vector<string>>,vector<vector<string>>> overlap_packets; //first is input, second output
            vector<vector<string>> ov_in;
            vector<vector<string>> ov_out;
            vector<vector<string>> emu_packets;
            bool error=false;
            bool error_any = false;
            bool nooverlap = false;

            if(verbose){
                cout << "CHECKING....." << endl;
            }

            //Loop over input buffers
            for (u_int i = 0; i < 11; i++) {
               
                //Read input file
                in_packets = ssmap_read_file(input_buffers[i].mode,input_buffers[i].infile,input_buffers[i].incol);
                if(verbose){
                    cout << "Input packets..." << endl;
                    for (u_int i = 0; i < in_packets.size(); i++) {
                        cout << "L1ID " << in_packets[i][3] << endl;
                    }
                }
            
                //Read output file
                out_packets = ssmap_read_file(input_buffers[i].mode,input_buffers[i].outfile,input_buffers[i].outcol);

                if(verbose){
                    cout << "Output packets..." << endl;
                    for (u_int i = 0; i < out_packets.size(); i++) {
                        cout << "L1ID " << out_packets[i][3] << endl;
                    }
                }

                //Check for overlap
                overlap_packets = ssmap_overlap(in_packets,out_packets,is_loop);
                ov_in = overlap_packets.first;
                ov_out= overlap_packets.second;      
                if (ov_in.size() == 0){
                    nooverlap = true;
                }
                if(verbose){
                    cout << "Found " << ov_in.size() << " overlapping events" << endl;
                    cout << "SSMap emulation" << endl;
                }

                //Emulate
                emu_packets=ssmap_emulation(ov_in,doHS,input_buffers[i].mode,input_buffers[i].plane,tower,ssbase);

                bool is_sort = false;
                if (is_loop){
                    is_sort = true;
                }

                //Compare
                if(verbose){
                    cout << "Packet check" << endl;
                }
                error = ssmap_compare(emu_packets,ov_out,is_loop,is_sort,verbose,input_buffers[i].mode,input_buffers[i].plane);
                error_any = error or error_any;
            }
            if (nooverlap){
                cout << "ONE OF THE STREAMS HAS NO OVERLAP !!!!!!!!!!" << endl;
            }
            if (error_any){
                cout << "!!!!! ONE OF THE STREAMS FAILED THE CHECK !!!!!" << endl;
            } else {
                cout << "SSMap: All good" << endl;
            }
            return;
        }

        vector<vector<string>> ssmap_read_file(string mode, string file, int col)
        {
            vector<string> data;
            vector<string> cut_data;
            bool overflow=false;
            int first=0;

            std::ifstream infile(file);
            for( std::string line; getline( infile, line ); )
            {
                vector<string> parts = split(line,' ');

                if (parts[0]=="[ovfl]:") {
                    if (parts[6]=="YES"){
                        overflow=true;
                    }
                } else {

                    string addr = parts[3*col];
                    string word = parts[3*col+1];
                    if(addr=="[0001]:"){
                        first=data.size();
                        data.push_back(word);
                    } else if (addr=="[last]:") { 
                        continue;
                    } else {
                        data.push_back(word);
                    }
                }
                /*
                if (!overflow){ 
                    cout << "first " << first << endl;
                    vector<string> cut_data(&data[first],&data[data.size()-1]); //TODO this line gives core dump
                    //data=data[first:];
                } else {
                    cut_data=data;
                }
                */
            }
            //parse data
            bool seen_b0f0=false;
            vector<string> packet;
            vector<vector<string>> new_data;
            for (u_int j = 0; j < data.size(); j++) {
               string word = data[j];
               //TODO Are these still needed? I don't see any f7 in file...
               /*
               if (word.rfind("b0f0", 0) == 0 or ends_with(word,"b0f0") == 1 ) {
                    data[j]=
                    //replace f7 in word with 00
               }
               if (word.rfind("e0da", 0) == 0 or ends_with(word,"e0da") == 1 ) {
                    data[j]=
                    //replace f7 in word with 00
               } 
               if (word.rfind("e0f0", 0) == 0 or ends_with(word,"e0f0") == 1 ) {
               }
               */
               if (word.rfind("b0f0", 0) == 0 or ends_with(word,"b0f0") == 1 ) {
                    seen_b0f0=true;
               }
               if(seen_b0f0){
                    packet.push_back(word);
               }
               if (word.rfind("e0f0", 0) == 0 or ends_with(word,"e0f0") == 1 ) {
                    if(seen_b0f0){
                        new_data.push_back(packet);               
                    }
                    seen_b0f0=false;
                    packet.clear();
               }

            }

            return new_data;
        }

        std::pair<vector<vector<string>>, vector<vector<string>>> ssmap_overlap(vector<vector<string>> in_packets,vector<vector<string>> out_packets,bool is_loop)
        {
            vector<vector<string>> cut_in;
            vector<vector<string>> cut_out;
            int npackets;
            if(!is_loop){
                //Check L1IDs
                int lvl1ID_in;
                int lvl1ID_out;
                for (u_int i = 0; i < in_packets.size(); i++) {
                    lvl1ID_in = std::stoi(in_packets[i][3], nullptr, 16);
                    for (u_int j = 0; j < out_packets.size(); j++) {
                        lvl1ID_out = std::stoi(out_packets[j][3], nullptr, 16);
                        if (lvl1ID_in==lvl1ID_out){
                            cut_in.push_back(in_packets[i]);
                            cut_out.push_back(out_packets[j]);
                        }
                    }
                }
            } else {
                npackets=min(in_packets.size(),out_packets.size());
                for (u_int i = 0; i < npackets; i++) {
                    cut_in.push_back(in_packets[i]);
                    cut_out.push_back(out_packets[i]);
                }
            }
            return std::make_pair(cut_in, cut_out);
        }
    
        vector<vector<string>> ssmap_emulation(vector<vector<string>> in_packets, bool doHS, string mode, string plane, string tower, string ssbase)
        {
            //The TODOs might be fine, but areas to look if something goes wrong in script
            //TODO Order w/o HitSort 
            //TODO "seen this ssid before"
            //TODO if hitcoord != "40000000"           
 
            int nonvalidhit = 0;
            int hitcount = 0;
 
            //Load SSMaps 
            SSMap localmap=SSMap(ssbase+"_ssid_tower_" + tower + "_plane_"+plane+".hex");
            SSMap modulemap=SSMap(ssbase+"_module_tower_" + tower + "_plane_"+plane+".hex");           
 
            vector<vector<string>> emu_packets;
            //Loop over events
            for (u_int event = 0; event < in_packets.size(); event++) {
                vector<string> emu_event;

                //Copy header
                emu_event.push_back("0000b0f0");
                for (u_int i = 1; i < 7; i++) {
                    emu_event.push_back(in_packets[event][i]);
                }

                //Actual data
                int mod_id;
                string mod_id_str;
                vector<std::tuple <int, int, string>> hit_and_ssid; //stores ssid local, then ssid global, then hit
                for (u_int i = 7; i < in_packets[event].size()-6; i++) {
                    //If bit 1 high, it's a module

                    long word_dec = std::stol(in_packets[event][i], nullptr, 16);
                    
                    if ((word_dec >> 31)==1){
                        if (mode=="sct"){
                            mod_id = std::stoi(in_packets[event][i].substr(4,4), nullptr, 16) & 8191;//13 bits
                        } else { //pixel
                            mod_id = std::stoi(in_packets[event][i].substr(4,4), nullptr, 16) & 2047;//11 bits
                        }
                        std::stringstream stream1;
                        stream1 << std::setfill('0') << std::setw(4) << std::hex << mod_id;
                        mod_id_str = stream1.str();
                    } else { //hit

                        if(mode=="sct"){
                            long word_dec_fix = word_dec & 4294934527; //set bit 15 to 0
                            std::stringstream stream;
                            stream << std::setfill('0') << std::setw(8) << std::hex << word_dec_fix;
                            string fixbit = stream.str();

                            string hit1 = fixbit.substr(4,4);

                            int hitkey1 = (word_dec_fix & 2047) >> 1; //get last 11 bits, right shift 1
                            int ssid1_loc_int=localmap.get(hitkey1);
                            int ssid1_glob_int=modulemap.get(mod_id);

                            hit_and_ssid.push_back(std::make_tuple(ssid1_loc_int,ssid1_glob_int,hit1));

                            if(((word_dec >> 15) & 1)==0){//Bit 15 tells if hit2 is empty
                                string hit2 = in_packets[event][i].substr(0,4);

                                int hitkey2 = ((word_dec_fix >> 16) & 2047) >> 1; //get bits 26-16 to get coord. Then right shift 1
                                int ssid2_loc_int=localmap.get(hitkey2);
                                int ssid2_glob_int=modulemap.get(mod_id);
                                hit_and_ssid.push_back(std::make_tuple(ssid2_loc_int,ssid2_glob_int,hit2));
                            }

                        } else {

                            string hit = in_packets[event][i];

                            int coord = word_dec & 2147483647;
                            bitset<8> part_1((coord >> 20) & 255);
                            bitset<9> part_2((coord >> 3) & 511);
                            int hitkey = std::stoi(part_1.to_string() + part_2.to_string(),nullptr,2);

                            int ssid_loc_int=localmap.get(hitkey);
                            int ssid_glob_int=modulemap.get(mod_id);
                            if ( mode=="pixel_even" and ((ssid_loc_int+ssid_glob_int)>>3)%2==1 ) {
                                continue;
                            }
                            if ( mode=="pixel_odd" and ((ssid_loc_int+ssid_glob_int)>>3)%2==0 ) {
                                continue;
                            }

                            hitcount += 1;
                            if ( (ssid_loc_int+ssid_glob_int)>65535) { //coresponds to >4 hex digits
                                nonvalidhit += 1;   
                            }

                            if (nonvalidhit>0){
                                cout << "ERROR: See " << nonvalidhit << " nonsense hits out of " << hitcount << " total hits in " << mode << " " << plane << endl; 
                            }

                            hit_and_ssid.push_back(std::make_tuple(ssid_loc_int,ssid_glob_int,hit));

                        }


                    }
                }

                if(doHS) { //do HitSort
                    sort(hit_and_ssid.begin(),hit_and_ssid.end());
                }

                //First in tuple is local ssid (int). Second is global ssid (int). 3rd is hit (string).
                for ( u_int i = 0; i < hit_and_ssid.size(); i++){
                    int ssid_int = get<0>(hit_and_ssid[i])+get<1>(hit_and_ssid[i]);
                    std::stringstream stream_2;
                    stream_2 << std::setfill('0') << std::setw(4) << std::hex << ssid_int;
                    string ssid = stream_2.str();
                    if ( mode == "sct" ) {
                        emu_event.push_back(ssid+get<2>(hit_and_ssid[i]));
                    } else { //pixel
                        emu_event.push_back("8000"+ssid);
                        emu_event.push_back(get<2>(hit_and_ssid[i]));
                    }
                }

                //Copy trailer
                emu_event.push_back("0000e0da");
                for (u_int i = in_packets[event].size()-5; i < in_packets[event].size()-1; i++) {
                    if (i==in_packets[event].size()-2){
                        //Update event count
                        std::stringstream stream;
                        stream << std::setfill('0') << std::setw(8) << std::hex << emu_event.size()-11;//-7 for header, -4 for trailer so far
                        emu_event.push_back(stream.str());
                    } else {
                        emu_event.push_back(in_packets[event][i]);
                    }
                }
                emu_event.push_back("0000e0f0");

                emu_packets.push_back(emu_event);

                emu_event.clear();
            }
            return emu_packets;
        }

        bool ssmap_compare(vector<vector<string>> in_packets,vector<vector<string>> out_packets,bool is_loop, bool is_sort, bool verbose, string mode, string plane)
        {
            bool error=false;
            bool error_event=false;
            
            //Loop over events
            for (u_int event = 0; event < in_packets.size(); event++) { 
                error_event = false;
                if(verbose){
                    cout << "comparing L1ID " << in_packets[event][3] << " calculated, " << out_packets[event][3] << " on board" << endl;
                }
                if(is_loop){//Want to ignore L1IDs in comparison
                    in_packets[event][3]="11111111";
                    in_packets[event][in_packets[event].size()-5]="11111111";
                    out_packets[event][3]="11111111";
                    out_packets[event][out_packets[event].size()-5]="11111111";
                    if(is_sort){
                        cout << "WARNING: Have to sort data to compare in loop mode, which means any hitsort issues will be missed" << endl;
                        if(mode=="sct"){
                          //SCT sort every word
                          sort(in_packets[event].begin()+7, in_packets[event].end()-6);
                          sort(out_packets[event].begin()+7, out_packets[event].end()-6);
                        } else {
                            vector<std::pair <string, string>> in_by_two;
                            vector<std::pair <string, string>> out_by_two;
                            //Pixel sort by two
                            for (u_int pos = 7; pos < in_packets[event].size()-6; pos++){//Skip header and trailer
                                if(pos % 2 == 0) {//If pos even
                                    in_by_two.push_back(std::make_pair(in_packets[event][pos-1], in_packets[event][pos]));
                                }
                            }
                            for (u_int pos = 7; pos < out_packets[event].size()-6; pos++){//Skip header and trailer
                                if(pos % 2 == 0) {//If pos even
                                    out_by_two.push_back(std::make_pair(out_packets[event][pos-1], out_packets[event][pos]));
                                }
                            }
                            sort(in_by_two.begin(),in_by_two.end());
                            sort(out_by_two.begin(),out_by_two.end());

                            int y=0;
                            for (u_int pos = 7; pos < in_packets[event].size()-6; pos++){//Skip header and trailer
                                if(pos % 2 == 1) {//If pos odd
                                    in_packets[event][pos]=in_by_two[y].first;
                                    in_packets[event][pos+1]=in_by_two[y].second;
                                    y++;
                                }
                            }
                            
                            y = 0;
                            for (u_int pos = 7; pos < out_packets[event].size()-6; pos++){//Skip header and trailer
                                if(pos % 2 == 1) {//If pos odd
                                    out_packets[event][pos]=out_by_two[y].first;
                                    out_packets[event][pos+1]=out_by_two[y].second;
                                    y++;
                                }
                            }
                            in_by_two.clear();
                            out_by_two.clear();
                        }
                    }
                }
                //If different number of words, obviously they don't match
                if (in_packets[event].size() != out_packets[event].size()){
                    error_event = true;
                } else {
                    //Check each word
                    for (u_int pos = 0; pos < in_packets[event].size() && pos < out_packets[event].size(); pos++) {
                        if (in_packets[event][pos] != out_packets[event][pos]) error_event = true;
                    }
                }
                if (not error_event){
                    if(verbose) {
                        cout << "NO ERRORS: " << mode << " " << plane << endl;
                    }
                } else { //If it's wrong, show output
                    cout << "SSMap output stream does not match." << endl;
                    cout << "emulation, dataoutput, plane, number" << endl;
                    cout << mode << " " << plane << endl;
                    if (in_packets[event].size()!=out_packets[event].size()){
                        cout << "Sizes don't match" << endl;
                        cout << out_packets[event].size() <<  " <BOARD!CALC> " << in_packets[event].size() << endl;
                    } else {
                        cout << out_packets[event].size() <<  " <BOARD=CALC> " << in_packets[event].size() << endl;
                    } 
                    for (u_int pos = 0; pos < in_packets[event].size() || pos < out_packets[event].size(); pos++) {
                        if (pos < out_packets[event].size()) {
                            cout << std::hex << std::setfill('0') << std::setw(8) << out_packets[event][pos];
                        } else {
                            cout << "        ";
                        }
                        if (pos < in_packets[event].size()) {
                            cout << " " << std::hex << std::setfill('0') << std::setw(8) << in_packets[event][pos];
                        } else {
                            cout << "        ";
                        }
                        cout << " " << mode << " " << plane;
                        if (pos >= out_packets[event].size() || pos >= in_packets[event].size()) {
                            cout << " <";
                        } else {
                            if (out_packets[event][pos] != in_packets[event][pos]) {
                                cout << " <";
                            }
                        }
                        cout << endl;
                    }
                    cout << "!!!!! THERE WAS AN ERROR IN THE STREAM !!!!!" << endl;
                }
                error = error or error_event;
            }

            return error;
        }

        template<typename Out>
        void split(const std::string &s, char delim, Out result) {
            std::stringstream ss(s);
            std::string item;
            while (std::getline(ss, item, delim)) {
                *(result++) = item;
            }
        }

        std::vector<std::string> split(const std::string &s, char delim) {
            std::vector<std::string> elems;
            split(s, delim, std::back_inserter(elems));
            return elems;
        }

        bool ends_with(std::string const &fullString, std::string const &end) {
            if (fullString.length() >= end.length()) {
                return (0 == fullString.compare (fullString.length() - end.length(), end.length(), end));
            } else {
                return false;
            }
        }


    } //namespace daq
} //namespace ftk




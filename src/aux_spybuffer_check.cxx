#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "aux/aux_vme_regs.h"

#include "aux/Report.h" 
#include "aux/TF/TFGen.h" 
#include "aux/TF/TFConstants.h" 
#include "aux/TF/TFEventList.h" 
#include "aux/TF/TFFitter.h" 
#include "aux/aux_check.h"
#include "aux/aux_spybuffer_lib.h"

#include "aux/aux_spy_regs.h"

#include "aux/aux_ssmap_check.h"
#include "aux/aux_do_check.h"

#include <iostream>
#include <sstream> // ostringstream

#include <stdio.h>
#include <stdlib.h>

using std::cout;  using std::endl; 

namespace daq {
  namespace ftk {
    bool aux_compare_events(std::vector<u_int> calculated_stream, std::vector<u_int> observed_stream, bool verbose);
    void aux_get_hw_bitmask_v2(std::vector<std::vector<u_int>> &buff, u_int ntracks, std::vector<bool> &bitmask, bool verbose);
    std::vector<u_int> aux_calculate_hw(std::vector<std::vector<std::vector<u_int>>> &content, bool verbose, bool ROS, bool is_loop);
    std::vector<std::vector<std::vector<std::vector<u_int>>>> Parse_Before_Calc_hw(std::map<u_int, std::vector<u_int>> &content, bool verbose, bool ROS, bool is_loop);
    void print_event(std::vector<u_int> calculated_stream, bool verbose);
    bool aux_ssmap_compare(bool verbose, const std::string& inDirName, bool doHS=true, bool is_loop=false){
      cout << "SS-Map bit-check ..." ;
      if (doHS)	cout << "including HitSort algorithm ...";

      if(verbose)
	    system(("echo python checkSSMap.py "+inDirName+" --debug").c_str());

      FILE* stream;
      if(is_loop)
        if(verbose)
	        if (doHS)
	            stream = popen(("python checkSSMap.py "+inDirName+" --debug --doHS --loop --sort").c_str(),"r");
            else
	            stream = popen(("python checkSSMap.py "+inDirName+" --debug --loop --sort").c_str(),"r");
        else
    	    if (doHS)
	            stream = popen(("python checkSSMap.py "+inDirName+" --doHS --loop --sort").c_str(),"r");
            else
	            stream = popen(("python checkSSMap.py "+inDirName+" --loop --sort").c_str(),"r");
      else
        if(verbose)
            if (doHS)
                stream = popen(("python checkSSMap.py "+inDirName+" --debug --doHS").c_str(),"r");
            else
                stream = popen(("python checkSSMap.py "+inDirName+" --debug").c_str(),"r");
        else
            if (doHS)
                stream = popen(("python checkSSMap.py "+inDirName+" --doHS").c_str(),"r");
            else
                stream = popen(("python checkSSMap.py "+inDirName).c_str(),"r");
      std::ostringstream output;

      while( !feof( stream ) && !ferror( stream ))
      {
          char buf[128];
          int bytesRead = fread( buf, 1, 128, stream );
          output.write( buf, bytesRead );
      }
      std::string result = output.str();
      bool allGood = (result.find("All good") != std::string::npos);
      bool nooverlap = (result.find("OVERLAP") != std::string::npos);
      if(verbose){
          std::cout << result << std::endl;
      }
      if (nooverlap) cout << "no overlap" << endl;
      else if(!allGood) cout << "FAILED" << endl;
      else         cout << "passed" << endl;
      return allGood;
    }

    bool aux_TX_inputs_to_procs_compare(bool verbose){
      bool allGood = false;
      if(verbose) cout << "The TX input to proc check is not yet implemented" << endl;

      cout << "TX (Input to Procs)...." ;
      if(!allGood) cout << "FAILED (not yet implemented)" << endl;
      else         cout << "passed" << endl;
      return allGood;
    }

    bool aux_TX_procs_to_input2_compare(bool verbose){
      bool allGood = false;
      if(verbose) cout << "The TX proc to input2 check is not yet implemented" << endl;
      cout << "TX (Procs to Input2)...." ;
      if(!allGood) cout << "FAILED (not yet implemented)" << endl;
      else         cout << "passed" << endl;
      return allGood;
    }


    bool aux_DO_compare(bool verbose, const std::string& inDirName, std::string proc, std::string ambank, bool oldDC, bool allSectors, std::string inChipOff, bool is_loop=false){
      cout << "DO-Map bit-check ..." ;
      std::string command = "python NewDOSpyBitLevelCheck.py -p "+proc+" -d "+inDirName+" --inChipOff "+inChipOff;
      //ambank="/afs/cern.ch/work/m/mhank/private/config_for_bitlevel/user.sschmitt.patterns_DataAlignment2017_xm05_ym09_Reb64_v9.HW2.SSEL3.180511-64PU_30x128x72Ibl-NB7-NE4-BM0_partitioning4_180601-095309_reg40_sub0.pbank.root";
      if(ambank!="") command = command + " -a " + ambank;
      if(oldDC) command = command + " --oldDC";
      if(allSectors) command = command + " --allSectors";

      if(verbose)
	system(("echo "+command+" -v").c_str());

      FILE* stream;
      if(verbose && is_loop){
	    system((command+" -v -i").c_str());
	    return true;
      } else if (verbose){
        system((command+" -v").c_str());
        return true;
      } else if(is_loop){
        system((command+" -i").c_str());
        return true;
      }
 
      stream = popen(command.c_str(),"r");
      std::ostringstream output;

      while( !feof( stream ) && !ferror( stream ))
	{
	  char buf[128];
	  int bytesRead = fread( buf, 1, 128, stream );
	  output.write( buf, bytesRead );
	}
      std::string result = output.str();
      bool noOverlap      = (result.find("WARNING:  NO OVERLAP") != std::string::npos);
      bool somethingWrong = (result.find("Get back to work.")    != std::string::npos);
      if(verbose){
	std::cout << result << std::endl;
      }
      if     (noOverlap)      cout << "no overlap" << endl;
      else if(somethingWrong) cout << "FAILED"     << endl;
      else                    cout << "passed"     << endl;
      bool allGood = !somethingWrong && !noOverlap;
      return allGood;
    }

    //Second version to get the bitmask when event is a vector of tracks
    void aux_get_hw_bitmask_v2(std::vector<std::vector<u_int>> &buff, u_int ntracks, std::vector<bool> &bitmask, bool verbose) { 
      //buff is a vector of tracks, each of which is a vector of words. The first and last "tracks" are actually the header and trailer.
      bitmask.clear();
      
      u_int road_start = 0;
      u_int current_road = 0xffffff;

      if(verbose) cout << "Get HW bitmask out of ntracks = " << ntracks << endl;
      
      //initialize bitmask
      for (u_int t1 = 0; t1 < ntracks; t1++) {
        bitmask.push_back(true);
      }
      
      // t1 is incoming.
      for (u_int t1 = 0; t1 < ntracks; t1++) {

        if(!bitmask[t1]) continue;//This track was already killed

            // hit warrior can't deal with more than 32.
        if (((buff[t1+1][1] & 0xffffff) != current_road) ||
           (t1 - road_start == 32)) { //Switching to the next line removes the track/road limit, allowing matching with sim
          //if ((buff[t1+1][1] & 0xffffff) != current_road) {
            if(verbose && (t1 - road_start == 32)) cout << "Got 32 tracks with same road. Treat as new road" << endl;
            if(verbose && ((buff[t1+1][1] & 0xffffff) != current_road)) cout << "New Road:" << std::hex << buff[t1+1][1] << endl;
            current_road = (buff[t1+1][1] & 0xffffff);
            road_start = t1;
        }
        
        // compare to all those that are already in memory.
        for (u_int t2 = road_start; t2 < t1; t2++) {
          u_int t1road = (buff[t1+1][1] & 0xffffff);
          u_int t2road = (buff[t2+1][1] & 0xffffff);
          if(t1road != t2road) cout << "ERROR: Trying to compare tracks from different roads!! Must be a packet problem" << endl;

          //if(!bitmask[t2]) continue;//this track was already killed. But, to match with the board, we still want to compare to it.
        if(verbose) {
            cout << "Comparing t1(road),t2(road) = "<<std::dec<<t1<<"("<<std::hex<<buff[t1+1][1]<<"),"<<std::dec<<t2<<"("<<std::hex<<buff[t2+1][1]<<")"<< endl;
            cout << "t1(road) = " << std::hex<<std::setfill('0') << std::setw(8) << buff[t1+1][0] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t1+1][1] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t1+1][2] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t1+1][3] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t1+1][4] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t1+1][5] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t1+1][6] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t1+1][7] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t1+1][8] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t1+1][9] << endl;
            cout << "t2(road) = " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t2+1][0] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t2+1][1] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t2+1][2] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t2+1][3] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t2+1][4] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t2+1][5] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t2+1][6] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t2+1][7] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t2+1][8] << " " << std::hex<<std::setfill('0') << std::setw(8)<<buff[t2+1][9] << endl;
            cout << "buff[t1+1][0] = " << std::hex << buff[t1+1][0] << " buff[t2+1][0] = " << buff[t2+1][0] << endl;
        }

        u_int nhits1(0), nhits2(0);
        for (int h = 0; h < 8; h++) {
            if (buff[t1+1][0] & (1 << (h+16))) nhits1++;
            if (buff[t2+1][0] & (1 << (h+16))) nhits2++;
        }
        
        int matches = 0;
        for (u_int h = 0; h < 8; h++) {
            // exact matches on real hits.
            //          hitmask is 1 for track1 and hitmask is 1 for track2      and    track1 hit equals track2 hit
            //if ( true ){ //Use this instead of next line if you want to count guessed hits in duplicate hits
            if (  (buff[t1+1][0] & (1 << (h+16))) && (buff[t2+1][0] & (1 << (h+16))) ){
                if(buff[t1+1][h+2] == buff[t2+1][h+2]){
                    matches++;
                    if(verbose) cout << std::hex << std::setw(8) << buff[t1+1][h+2] << " == " << std::setw(8) << buff[t2+1][h+2] << endl;
                }
                else{
                    if(verbose) cout << std::hex << std::setw(8) << buff[t1+1][h+2] << " != " << std::setw(8) << buff[t2+1][h+2] << endl;
                }
            }
            else{
                if(verbose) cout << ((buff[t1+1][0] & (1 << (h+16))) ? "    REAL" : "   GUESS") << " ?? " << ((buff[t2+1][0] & (1 << (h+16))) ? "    REAL" : "   GUESS") << endl;
            }
        }

        // for matching hits, check chi2.
        // add bitmask in later.
        if(verbose) cout << "# of Matching hits: " << matches << endl;
        if (matches > 5) {//Matched hit requirement
            // take the one with more hits.
            if (nhits1 != nhits2) {
                bitmask[(nhits1 > nhits2) ? t2 : t1] = false;
                if(verbose && (nhits1 > nhits2)) cout << "Kill t2="<<std::dec<<t2<<" because nhits1 > nhits2 ("<<nhits1 << ">" << nhits2 <<")"<<endl;
                if(verbose && (nhits1 < nhits2)) cout << "Kill t1="<<std::dec<<t1<<" because nhits1 < nhits2 ("<<nhits1 << "<" << nhits2 <<")"<<endl;

                if(verbose){//print bitmask
                    cout << "updated bitmask: ";
                    for (u_int t = 0; t < ntracks; t++) cout << (int)bitmask[t];
                    cout << endl;
                }
                continue;
            }

            unsigned long msb_t1  = ((unsigned long)buff[t1+1][10])<<28;
            unsigned long lsb_t1  =  (unsigned long)buff[t1+1][11];
            unsigned long chi2_t1 = msb_t1|lsb_t1;

            unsigned long msb_t2  = ((unsigned long)buff[t2+1][10])<<28;
            unsigned long lsb_t2  =  (unsigned long)buff[t2+1][11];
            unsigned long chi2_t2 = msb_t2|lsb_t2;

            if (chi2_t1 > chi2_t2) {
                bitmask[t1] = false;
            if(verbose) cout << "Kill t1="<<std::dec<< t1 << " because chi2_t1 = " << chi2_t1 << " >  " << chi2_t2 << " = chi2_t2" << endl;
            } else {
                bitmask[t2] = false;
                if(verbose) cout << "Kill t2="<<std::dec<< t2 << " because chi2_t1 = " << chi2_t1 << " <= " << chi2_t2 << " = chi2_t2" << endl;
            }
            if(verbose){//print bitmask
                cout << "updated bitmask: ";
                for (u_int t = 0; t < ntracks; t++) cout << (int)bitmask[t];
                    cout << endl;
            }
          }//matches > 5
        }//t2
      }//t1
      
      if(verbose){//print bitmask
        cout << endl << "Final bitmask: ";
        for (u_int t = 0; t < ntracks; t++) cout << (int)bitmask[t];
        cout << endl;
      }
      
      return;

    }

    void print_event(std::vector<u_int> calculated_stream, bool verbose) {
        if(verbose) {
            cout << "Event stream" << endl;
            for (u_int pos = 0;
              pos < calculated_stream.size();
              pos++) {
            //if (0xa8 == (calculated_stream[pos] >> 24 ) || 0x96 == (calculated_stream[pos] >> 24) || 0x86 == (calculated_stream[pos] >> 24)){//starts with a8
              if (1 == (calculated_stream[pos] >> 31) and 0b11 != (calculated_stream[pos] >> 30) and 0xe0f0 != (calculated_stream[pos] >> 16) and 0xb0f0 != (calculated_stream[pos] >> 16) and 0xe0da != (calculated_stream[pos] >> 16)){
                cout << endl;
                cout << std::hex << std::setfill('0') << std::setw(1) << calculated_stream[pos]/16/16/16/16/16%16 << calculated_stream[pos]/16/16/16/16%16 << " " << calculated_stream[pos]/16/16/16%16 << calculated_stream[pos]/16/16%16 << calculated_stream[pos]/16%16 << calculated_stream[pos]%16 << " ";
                pos++;
                cout << std::hex << std::setfill('0') << std::setw(1) << calculated_stream[pos]/16/16/16/16/16%16 << calculated_stream[pos]/16/16/16/16%16 << calculated_stream[pos]/16/16/16%16 << calculated_stream[pos]/16/16%16 << calculated_stream[pos]/16%16 << calculated_stream[pos]%16 << " ";
            } else if (0xe0da0000 == calculated_stream[pos]){//end of track
                return;
            } else {
                cout << std::hex << std::setfill('0') << std::setw(8) << calculated_stream[pos] << " ";
            }
          }
        }
        return;
    }

    bool aux_compare_events(std::vector<u_int> calculated_stream, std::vector<u_int> observed_stream, bool verbose=false) {
      // check to make sure that word matches.
      bool retval = true;
      bool allGood = true;
      if (calculated_stream.size() != observed_stream.size()) retval = false;
      for (u_int pos = 0;
          pos < calculated_stream.size() &&
          pos < observed_stream.size();
          pos++) {
        if (observed_stream[pos] != calculated_stream[pos]) retval = false;
      }
      // print out the result...
      if (retval){
        if(verbose) cout << "HW output stream matches expectation." << endl;
        for (u_int pos = 0;
              pos < calculated_stream.size() &&
              pos < observed_stream.size();
              pos++) {
            cout << std::hex << std::setfill('0') << std::setw(8) << observed_stream[pos];
            cout << endl;
          }
      } else { // if it's wrong, show the output.
        allGood = false;
        if(verbose){
          cout << "Output stream does not match." << endl;
          if (calculated_stream.size()!=observed_stream.size()){
            cout << "Sizes don't match" << endl;
            cout << observed_stream.size() <<  " <BOARD!CALC> " << calculated_stream.size() << endl;
          } else {
            cout << observed_stream.size() <<  " <BOARD=CALC> " << calculated_stream.size() << endl;
          }
          for (u_int pos = 0;
              pos < calculated_stream.size() &&
              pos < observed_stream.size();
              pos++) {
            cout << std::hex << std::setfill('0') << std::setw(8) << observed_stream[pos];
            if (observed_stream[pos] != calculated_stream[pos])
              cout << " <BOARD!CALC> " << std::hex << std::setfill('0') << std::setw(8) << calculated_stream[pos];
            cout << endl;
          }
        }
      }
      
      return retval;

    }
    
    std::vector<std::vector<std::vector<std::vector<u_int>>>> Parse_Before_Calc_hw(std::map<u_int, std::vector<u_int>> &content, bool verbose=false, bool ROS = false, bool is_loop = false) {
      //Takes the content and parses it into a 4d vector: parsed_data->event->stream->track->word
      //First stream is the output from board, next 4 are inputs
      //The first and last "tracks" are actually the header and trailer
      std::vector<std::vector<std::vector<std::vector<u_int>>>> parsed_data;
      // all of the streams must be "clean" --
      // they must only contain the good events.
      if(verbose) std::cout << "Checking Input Map:" << std::endl;
      bool overlap = false;
      for (const auto &p : content) {
        //MH- should this only be checking streams 12-15???
        if(p.second.size() > 0) overlap = true;
        if(verbose) std::cout << "content[" << p.first << "].size() = " << p.second.size() << '\n';
      }
      
      std::map<u_int, u_int> stream_pos;
      stream_pos[0x9] = 0;
      stream_pos[0x12] = 0;
      stream_pos[0x13] = 0;
      stream_pos[0x14] = 0;
      stream_pos[0x15] = 0;


      u_int stream = 0x9;

      //std::vector<bool> track_mask;
      u_int event_tracks(0);
      if(overlap){
      u_int event_num = 0;

      while (1) {
        //Doesn't make sense to do this loop if size is 0
        if (content[0x15].size() == 0) {
            cout << "Overlap empty, skipping overlap check" << endl;
            break;
        }
        if (stream > 0x15) stream = 0x9;
        u_int stream_num = stream-0x12+1;
        if (stream == 0x9) {
            stream_num = 0;
            std::vector<std::vector<std::vector<u_int>>> parsed_event;
            parsed_data.push_back(parsed_event);
            std::vector<std::vector<u_int>> parsed_stream;
            
            //5 streams: 0->0x9, 1->0x12, 2->0x13, 3->0x14, 4->0x15
            parsed_data[event_num].push_back(parsed_stream);
            parsed_data[event_num].push_back(parsed_stream);
            parsed_data[event_num].push_back(parsed_stream);
            parsed_data[event_num].push_back(parsed_stream);
            parsed_data[event_num].push_back(parsed_stream);
            
            //track vector (header is 0th)
            std::vector<u_int> parsed_track;
            parsed_data[event_num][0].push_back(parsed_track);
            parsed_data[event_num][1].push_back(parsed_track);
            parsed_data[event_num][2].push_back(parsed_track);
            parsed_data[event_num][3].push_back(parsed_track);
            parsed_data[event_num][4].push_back(parsed_track);
            u_int track_num = 0;
            
            // First add header
            u_int header_length = 7;
            if(ROS) header_length = 10;
            for (u_int p = 0; p < header_length; p++) {
                parsed_data[event_num][stream_num][track_num].push_back(content[stream][stream_pos[stream]+p]);
            }
            if(is_loop && ROS) parsed_data[event_num][stream_num][track_num][6]=0x11111111;
            if(is_loop && !ROS) parsed_data[event_num][stream_num][track_num][3]=0x11111111;
            stream_pos[stream] += header_length;
            track_num++;
                
            // Now add tracks
            while (0xe0da0000 != (content[stream][stream_pos[stream]])){
                parsed_data[event_num][0].push_back(parsed_track);
                for (u_int p = 0; p < 10; p++) {
                    parsed_data[event_num][stream_num][track_num].push_back(content[stream][stream_pos[stream]+p]);
                }
                stream_pos[stream] += 10;
                track_num++;
            }
                
            // Now add trailer
            parsed_data[event_num][0].push_back(parsed_track);
            while ((0xe0f0 != (content[stream][stream_pos[stream]] & 0xffff)) && (0xe0f00000 != (content[stream][stream_pos[stream]]))){ //TODO: check first 4 digits
                parsed_data[event_num][stream_num][track_num].push_back(content[stream][stream_pos[stream]]);
                stream_pos[stream]++;
            }
            // add in last line
            parsed_data[event_num][stream_num][track_num].push_back(content[stream][stream_pos[stream]]);
            stream_pos[stream]++;
            if(is_loop && !ROS) parsed_data[event_num][stream_num][track_num][parsed_data[event_num][stream_num][track_num].size()-5]=0x11111111;
            stream=0x12;
        }
        else {
            // copy the header from the first stream.
            if (stream == 0x12) {
                if(ROS){
                    parsed_data[event_num][stream_num][0].push_back(0xb0f0b0f0);
                    parsed_data[event_num][stream_num][0].push_back(0xee1234ee);
                    parsed_data[event_num][stream_num][0].push_back(0x00000009);
                    parsed_data[event_num][stream_num][0].push_back(0x03010001);
                    parsed_data[event_num][stream_num][0].push_back(0x007f0000);
                    //Last word in input header not in output header
                    for (int p = 2; p < 6; p++) {
                        parsed_data[event_num][stream_num][0].push_back(content[stream][stream_pos[stream]+p]);
                    }
                    parsed_data[event_num][stream_num][0].push_back(0x00000000);
                    //Placeholder for L1ID if done in loop
                    if(is_loop) parsed_data[event_num][stream_num][0][6]=0x11111111;
                } else {    //SSB packet structure
                    parsed_data[event_num][stream_num][0].push_back(0xb0f00000);
                    for (int p = 1; p < 7; p++) {
                        parsed_data[event_num][stream_num][0].push_back(content[stream][stream_pos[stream]+p]);
                    }
                    if(is_loop) parsed_data[event_num][stream_num][0][3]=0x11111111;
               }
            }
            stream_pos[stream] += 7;
            // how many tracks to send to the "hw"?
            u_int ntracks = 0;
            //if (0xa8 == (content[stream][stream_pos[stream]] >> 24) || 0x96 == (content[stream][stream_pos[stream]] >> 24) || 0x86 == (content[stream][stream_pos[stream]] >> 24)) ntracks = 1;
            if (1 == (content[stream][stream_pos[stream]] >> 31) and 0b11 != (content[stream][stream_pos[stream]] >> 30)) ntracks = 1;
            // this is safe, because it should only ever land on E0DA.
            while (0xa8 == (content[stream][stream_pos[stream] + 12*ntracks] >> 24) || 0x96 == (content[stream][stream_pos[stream] + 12*ntracks] >> 24) || 0x86 == (content[stream][stream_pos[stream] + 12*ntracks] >> 24) || 0x95 == (content[stream][stream_pos[stream] + 12*ntracks] >> 24)) ntracks++;//TODO make this line nicer. Commented one below doesn't work.
           // while (1 == (content[stream][stream_pos[stream] + 12*ntracks] >> 31) and 0b11 != (content[stream][stream_pos[stream]] >> 30)) ntracks++;
            // get a bitmast of the tracks to keep.
            if(verbose) cout << endl << "--------------" << endl << "Processing stream:" << stream-0x12 << endl;
            // use the mask for a "gated output"
            for (u_int t = 0; t < ntracks; t++) {
                std::vector<u_int> parsed_track;
                parsed_data[event_num][stream_num].push_back(parsed_track);
                //Increase to 12 to include chi2 words
                for (u_int p = 0; p < 12; p++) {
                    parsed_data[event_num][stream_num][t+1].push_back(content[stream][stream_pos[stream]+p]);
                }
                event_tracks += 1;
                stream_pos[stream] += 12;
            }
            // look for the end...
            //add an vector for the trailer
            std::vector<u_int> parsed_track;
            parsed_data[event_num][stream_num].push_back(parsed_track);
            while (0xe0f0 != (content[stream][stream_pos[stream]] & 0xffff)){
                //Don't print cause the trailers aren't at all the same
                if (stream == 0x15 && (!ROS)) {// only print if it's from the last stream.
                    parsed_data[event_num][stream_num][ntracks+1].push_back(content[stream][stream_pos[stream]]);
                }
                stream_pos[stream]++;
            }
            // if we're in the last L1ID of the last stream, then break out!
            if (stream == 0x15) {
                //Here's the trailer
                if(ROS){
                    parsed_data[event_num][stream_num][ntracks+1].push_back(0xf700e0da);
                    parsed_data[event_num][stream_num][ntracks+1].push_back(0x00000000);
                    parsed_data[event_num][stream_num][ntracks+1].push_back(0x00000000);
                    parsed_data[event_num][stream_num][ntracks+1].push_back(0x00000002);
                    parsed_data[event_num][stream_num][ntracks+1].push_back(0x00000000);
                    parsed_data[event_num][stream_num][ntracks+1].push_back(0x00000000);
                    parsed_data[event_num][stream_num][ntracks+1].push_back(0xe0f0e0f0);

                    u_int e0da_pos = parsed_data[event_num][stream_num][ntracks+1].size()-7;
                    parsed_data[event_num][stream_num][ntracks+1][e0da_pos] = 0xe0da0000;

                    u_int count_pos = parsed_data[event_num][stream_num][ntracks+1].size()-3;
                    parsed_data[event_num][stream_num][ntracks+1][count_pos] = 10*event_tracks+1;
                } else {    //SSB format
                    parsed_data[event_num][stream_num][ntracks+1].push_back(0xe0f00000);

                    u_int e0da_pos = parsed_data[event_num][stream_num][ntracks+1].size()-6;
                    parsed_data[event_num][stream_num][ntracks+1][e0da_pos] = 0xe0da0000;

                    u_int count_pos = parsed_data[event_num][stream_num][ntracks+1].size()-2;
                    parsed_data[event_num][stream_num][ntracks+1][count_pos] = 10*event_tracks;

                    //Placeholder L1ID so they match
                    if(is_loop) parsed_data[event_num][stream_num][ntracks+1][e0da_pos+1]=0x11111111;
                }          
                event_tracks = 0;
          
                if (stream_pos[stream] == content[stream].size()-1) break;
                event_num += 1;
            }
            // otherwise, increment to the next stream.
            stream_pos[stream]++;
            stream++;
        }
      }
      } else {
        if (verbose) cout << "No overlap" << endl;
      }
      // merged_stream and content[0x9] should now hold the
      // calculated and board values of the HW output.
      if(is_loop){
      //Sort the tracks for each stream/event to make them easier to compare TODO
        for (u_int event = 0; event < parsed_data.size(); event++) {
            for (u_int stream = 0; stream < 1; stream++) {
                sort(parsed_data[event][stream].begin()+1, parsed_data[event][stream].end()-1);
            }
        }
      }
      //Merge BOARD vector
      for (int event = 0; event<parsed_data.size(); event++) {
        for (int i = 1; i<parsed_data[event][0].size(); i++) {
            parsed_data[event][0][0].insert(parsed_data[event][0][0].end(), parsed_data[event][0][i].begin(), parsed_data[event][0][i].end());
        }
      }
      return parsed_data;

    }
    
    std::vector<u_int> aux_calculate_hw(std::vector<std::vector<std::vector<u_int>>> &content, bool verbose=false, bool ROS=false, bool is_loop=false) {
      //Takes a 4d vector: parsed_data->event->stream->track->word
      //First stream is the output from board, next 4 are inputs
      //The first and last "tracks" are actually the header and trailer
      u_int stream = 0x12;
      std::vector<std::vector<u_int>> merged_stream_vector;
      std::vector<u_int> merged_stream;
      std::vector<u_int> parsed_track;

      std::vector<bool> track_mask;
      u_int event_tracks(0);
      while (1) {
        //Doesn't make sense to do this loop if size is 0
        if (content[0x15-0x11].size() == 0) {
            cout << "Overlap empty, skipping overlap check" << endl;
            break;
        }

        // copy the header from the first stream.
        if (stream == 0x12) {
            merged_stream_vector.push_back(parsed_track);
            for (int p = 0; p < content[stream-0x11][0].size(); p++) {
                merged_stream_vector[0].push_back(content[stream-0x11][0][p]);
            }
        }

        // how many tracks to send to the "hw"?
        u_int ntracks = content[stream-0x11].size()-2;

        // get a bitmast of the tracks to keep.
    if(verbose) cout << endl << "--------------" << endl << "Processing stream:" << stream-0x12 << endl;
        aux_get_hw_bitmask_v2(content[stream-0x11], ntracks, track_mask, verbose);
        // use the mask for a "gated output"
        for (u_int t = 0; t < ntracks; t++) {
          if (track_mask[t]) {
            //-2 so as not to include 2 chi2 words
            merged_stream_vector.push_back(parsed_track);
            for (u_int p = 0; p < content[stream-0x11][t+1].size()-2; p++) {
              merged_stream_vector[event_tracks+1].push_back(content[stream-0x11][t+1][p]);
            }
            event_tracks += 1;
          }
        }
        // look for the end...

        if (stream == 0x15) {// only print if it's from the last stream.
          merged_stream_vector.push_back(parsed_track);
          cout << "merged_stream_vector.size(), event_tracks " << merged_stream_vector.size() << event_tracks << endl;
          for (u_int p = 0; p < content[stream-0x11][content[stream-0x11].size()-1].size(); p++) {
            merged_stream_vector[event_tracks+1].push_back(content[stream-0x11][content[stream-0x11].size()-1][p]);
          }
        
        // if we're in the last L1ID of the last stream, then break out!
          if(ROS){
            u_int count_pos = merged_stream_vector[event_tracks+1].size()-3;
            merged_stream_vector[event_tracks+1][count_pos] = 10*event_tracks+1;
          } else {
            u_int count_pos = merged_stream_vector[event_tracks+1].size()-2;
            merged_stream_vector[event_tracks+1][count_pos] = 10*event_tracks;
          }
          event_tracks = 0;
          if (stream == 0x15) break;
        }
        // otherwise, increment to the next stream.
        stream++;
      }
      //Sort tracks TODO
      if(is_loop){
      //Sort the tracks for each stream/event to make them easier to compare
        sort(merged_stream_vector.begin()+1, merged_stream_vector.end()-1);
      }
      //Merge merged_stream_vector
      //loop over tracks
      for (int i=0; i<merged_stream_vector.size(); i++) {
        //loop over words
        for (int j=0; j<merged_stream_vector[i].size(); j++) {
            merged_stream.push_back(merged_stream_vector[i][j]);
        }
      }
      return merged_stream;

    }
    void  aux_trim_buffer(std::vector<u_int> & vec, u_int first, u_int last, u_int L1ID_header_idx, u_int L1ID_trailer_idx) {

      if (first > last || last == 0xffffffff) {
        vec.clear();
        return;
      }

      // find first and last, iterate between them, 
      // and pop back from the end.
      bool found_first = false;
      u_int trail(0), pos(0);
      for (; pos < vec.size(); pos++) {

        // first look for the first event.
        if (!found_first && 
            ((vec[pos] & 0xffff) == 0xb0f0 || (vec[pos]>>16 & 0xffff) == 0xb0f0) &&
            pos+L1ID_header_idx < vec.size() && 
            vec[pos+L1ID_header_idx] == first) {
          trail = pos; found_first = true;
        }

        // if we've found the first, 
        // start copying them back in the vector.
        if (found_first) vec[pos-trail] = vec[pos];

        // if we're at the last one, then stop.
        if (found_first &&
            ((vec[pos] & 0xffff) == 0xe0f0 || (vec[pos]>>16 & 0xffff) == 0xe0f0) &&
            vec[pos-trail-L1ID_trailer_idx] == last) break;
      }

      // for (u_int pop = 0; pop < removeN; pop++) vec.pop_back();
      while (vec.size() > pos-trail+1) vec.pop_back();

    }

    void  aux_trim_bl_buffer(std::map<u_int, std::vector<u_int> > & BLO) {

      bool found_first = false;
      u_int first = 0; u_int last = 0;
      for (u_int i = 0; i < BLO[0x2c].size(); i++) {
        if ((BLO[0x2c][i] & 0x1ff) == 0x100) {
          if (!found_first) { first = i+1; found_first = true; }
          else last = i;
        }
      }

      if (!last) { // no full event found
        for (auto b : {0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c}) BLO[b].clear();
        return;
      }

      // find first and last, iterate between them, 
      // and pop back from the end.
      for (u_int pos = 0; pos <= last - first; pos++) {

        for (auto b : {0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c}) {
          BLO[b][pos] = BLO[b][pos+first]; 
        }
      }
 
      u_int removeN = BLO[0x2c].size() - last + first - 1;
      for (auto b : {0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c}) {
        for (u_int pop = 0; pop < removeN; pop++) BLO[b].pop_back();
      }

    }



    void aux_get_first_last(std::vector<u_int> &data, u_int &first_l1a, u_int &last_l1a, bool verbose, u_int L1ID_header_idx, u_int L1ID_trailer_idx) {

      first_l1a = 0; last_l1a = 0xffffffff;

      bool first = true;
      u_int last_pos = 0;
      u_int current_L1ID=0;
      for (u_int pos = 0; pos < data.size(); pos++) {
	//find L1ID in headers
	if ( ((data[pos] & 0xffff) == 0xb0f0 || (data[pos]>>16 & 0xffff) == 0xb0f0) && (pos + L1ID_header_idx < data.size()) ) {
	  current_L1ID = data[pos+L1ID_header_idx];
	  if (first){ // find the first B0F0
	    if(verbose) cout << "Found First L1ID: " << std::hex << current_L1ID << endl;
	    first_l1a = current_L1ID;
	    first = false;
	  }
        }

        // find the last E0F0...
        // overlap checking happens later.
        if (((data[pos] & 0xffff) == 0xe0f0 || (data[pos]>>16 & 0xffff) == 0xe0f0) && (pos >= L1ID_trailer_idx) ) {
	  if(L1ID_trailer_idx){
	    last_l1a = data[pos-L1ID_trailer_idx];
	    last_pos = pos;
	  }
	  else{//take L1ID from header
	    last_l1a = current_L1ID;
	  }
        }
      }
      if(last_pos >= L1ID_trailer_idx && verbose) cout << "Found Last L1ID: " << std::hex << last_l1a << endl;
    }


    void aux_buffers_find_overlap(std::map<u_int, std::map<u_int, std::vector<u_int> > > &data, bool verbose, bool &ROS, bool is_loop=false) {
      
      u_int first(0), last(0xffffffff), minlength(0xffffffff); // mod L1A is 2**32
      std::vector<u_int> first_vector;
      u_int buff_idx = 0;
      ROS = false;

      //detect if using ROS firmware
    //for (u_int pos = 0; pos < data[2].size(); pos++) {
    //}
      for (u_int pos = 0; pos < data[2][9].size(); pos++) {
	    if(data[2][9][pos] == 0xee1234ee) {
	        ROS = true;
	    }
      }

      if(ROS) cout << "Found ee1234ee in HW output, assuming ROS packet structure" << endl;
      
      u_int first_temp(0), last_temp(0), length_temp(0);
      buff_idx = 0;
      //for (auto & buff : data[2]) {
      for(auto b : aux_i2_SB_HW_in_out){
	    //std::map<u_int, std::vector<u_int> > buff = data[2][b];

    	//for (u_int b = 0; b < data[f].size(); b++){
	    if(verbose) cout << "Getting First and Last (HitWarrior):" << endl;
        if(b == 9 && ROS) daq::ftk::aux_get_first_last(data[2][b], first_temp, last_temp, verbose, 6, 0);
	    else                       daq::ftk::aux_get_first_last(data[2][b], first_temp, last_temp, verbose, 3, 4);
	
	    if(verbose) cout << "First: " << std::hex << first_temp << "  Last: " << last_temp << endl;
    	// need to be cleverer here? 
	    // mod for the L1A is 2**32 = 4.2B...
    	// I believe they are unique.
        if(!is_loop) {	
	        if (first_temp > first) first = first_temp;
	        if (last_temp  < last)  last  = last_temp;
        } else { //if same event running in loop, can check even if L1IDs don't match
            if(verbose) cout << "Running on loop, so not requiring strict overlap" << endl;
            first_vector.push_back(first_temp);
            //Still want length so we can compare
            length_temp = last_temp-first_temp;
            if (length_temp < minlength) minlength = length_temp;
        }
      }
      if (first > last){
	cout << "Warning no overlapping events in four HW streams." << endl;
	cout << "first=" << std::hex << first << "  last=" << last << endl;
      }
      
      //for (auto & buff : data[2]){
      for(auto b : aux_i2_SB_HW_in_out){
        if(is_loop){
            if(b==9) first=first_vector[0];
            else first=first_vector[b-0x11];
            last=first+minlength;
        }
    	if(b == 9 && ROS) daq::ftk::aux_trim_buffer(data[2][b], first, last, 6, 0);
	    else                       daq::ftk::aux_trim_buffer(data[2][b], first, last, 3, 4);
      }
      
      daq::ftk::aux_trim_bl_buffer(data[3]);
      if(verbose) cout << "exit aux_buffers_find_overlap" << endl;
    }

    void aux_print_all_buffers(const std::map<u_int, std::map<u_int, std::vector<u_int> > > &data) {

      for (uint x = 1; x <=6; x++) {
        for (auto & buff : data.at(x)) {
          for (auto v : buff.second) {
            printf("%d 0x%x: %08x\n", x, buff.first, v);
          }
        }
      }

    }


    bool aux_check_spy_buffers ( std::string tf_consts, std::string ambank, bool oldDC, bool allSectors, std::string inChipOff, const std::string& inDirName,
                 std::string tower, std::string ssbase,
				 bool doSS = true, bool doHS = true, bool doTX = true, bool doDO = true, bool doTF=true, bool doHW=true, 
				 bool debug = true, bool bitstream = false, bool loop = false, int proc = 0, bool split = false ) { 

      bool retval = true;

      /// fpga, buffer, data
      std::map<u_int, std::map<u_int, std::vector<u_int> > > data;
      std::map<u_int, std::map<u_int, std::vector<u_int> > > data_tf;
      std::vector<std::vector<std::vector<std::vector<u_int>>>> parsed_data;

      if(debug) cout << "aux_buffers_build_data_structure" << endl;
      aux_buffers_build_data_structure(data);     // build the map
      if(debug) cout << "aux_buffers_download_data" << endl;
      aux_buffers_download_data(data, inDirName, debug); // call download the buffers
      data_tf=data; // TF does its own overlap


      bool ROS;
      if(debug) cout << "aux_buffers_find_overlap" << endl;
      aux_buffers_find_overlap(data, debug, ROS, loop);         // identify first and last, delete others.

      bool passSS = true;
      bool passTX = true;
      bool passDO1 = true;
      bool passDO2 = true;
      bool passDO3 = true;
      bool passDO4 = true;
      bool passTF1 = true;
      bool passTF2 = true;
      bool passTF3 = true;
      bool passTF4 = true;
      bool passHW = true;

      if(debug) cout << "Start Bit Level Checks" << endl;
      //Outdated code to run python script instead of C++. If you're having problems with the C++ you can try it though...
      //if(doSS) passSS = aux_ssmap_compare(debug, inDirName, doHS, loop);
      if(doSS) ssmap_toplevel(debug, inDirName, tower, ssbase, doHS, loop);
      if(doTX) passTX = aux_TX_inputs_to_procs_compare(debug);
      if(doDO) {
            int inChipOff_int = std::stoi(inChipOff, nullptr, 10);
            //Run DO check in C++
            do_toplevel(debug, inDirName, proc, ambank, oldDC, allSectors, inChipOff_int, loop); 
      
            /* 
            //Outdated code to run python script instead of C++. If you're having problems with the C++ you can try it though...
	        if(proc == 0 || proc == 1){
	            std::cout << "Proc1: ";
	            passDO1 = aux_DO_compare(debug, inDirName, "1", ambank, oldDC, allSectors, inChipOff, loop);
	        }
	        if(proc == 0 || proc == 2){
	            std::cout << "Proc2: ";
	            passDO2 = aux_DO_compare(debug, inDirName, "2", ambank, oldDC, allSectors, inChipOff, loop);
	        }
	        if(proc == 0 || proc == 3){
	            std::cout << "Proc3: ";
	            passDO3 = aux_DO_compare(debug, inDirName, "3", ambank, oldDC, allSectors, inChipOff, loop);
	        }
	        if(proc == 0 || proc == 4){
	            std::cout << "Proc4: ";
	            passDO4 = aux_DO_compare(debug, inDirName, "4", ambank, oldDC, allSectors, inChipOff, loop);
	        }
            */       
      };
      if(doTF) {
	if(proc == 0 || proc == 1){
	  std::cout << "Proc1: ";
	  if(debug) cout << endl << "tf_consts " << tf_consts << " data_tf.size() " << data_tf.size() << endl;
	  passTF1 = aux_tf_compare(tf_consts, data_tf[3], data_tf[2][0x12], bitstream, "1", debug, allSectors, loop, split);
	}
	if(proc == 0 || proc == 2){
	  std::cout << "Proc2: ";
	  passTF2 = aux_tf_compare(tf_consts, data_tf[4], data_tf[2][0x13], bitstream, "2", debug, allSectors, loop, split);
	}
	if(proc == 0 || proc == 3){
	  std::cout << "Proc3: ";
	  passTF3 = aux_tf_compare(tf_consts, data_tf[5], data_tf[2][0x14], bitstream, "3", debug, allSectors, loop, split);
	}
	if(proc == 0 || proc == 4){
	  std::cout << "Proc4: ";
	  passTF4 = aux_tf_compare(tf_consts, data_tf[6], data_tf[2][0x15], bitstream, "4", debug, allSectors, loop, split);
	}
      };
      if(doTX) passTX = aux_TX_procs_to_input2_compare(debug);
      if(doHW) {
        parsed_data = Parse_Before_Calc_hw(data[2], debug, ROS, loop);
        std::vector<std::vector<u_int>> merged_stream; 
        for (u_int event = 0; event < parsed_data.size(); event++) {
            std::cout << "Checking event " << event << endl;
            merged_stream.push_back(aux_calculate_hw(parsed_data[event], debug, ROS, loop));
            aux_compare_events(merged_stream[event], parsed_data[event][0][0], debug);
            //Output streams, as these are useful for comparing
            cout << "Calculated stream" << endl;
            print_event(merged_stream[event], debug);
            cout << "Board stream" << endl;
            print_event(parsed_data[event][0][0], debug);
        }
        std::cout << "Checked " << parsed_data.size() << " events" << endl;
        if(loop){
            cout << "Doing consistency check" << endl;
            //Merge parsed_data vector
            for (int event = 0; event<parsed_data.size(); event++) {
                for (u_int stream = 0; stream < 4; stream++) {
                    sort(parsed_data[event][stream+1].begin()+1, parsed_data[event][stream+1].end()-1);
                    for (int i = 1; i<parsed_data[event][stream+1].size(); i++) {
                        parsed_data[event][stream+1][0].insert(parsed_data[event][stream+1][0].end(), parsed_data[event][stream+1][i].begin(), parsed_data[event][stream+1][i].end());
                    }
                }
            }
            //Check each input stream
            bool comp_retval;
            for (u_int stream = 0; stream < 4; stream++) {
                comp_retval = true;
                cout << "Input check column " << stream << endl;
                for (u_int event = 1; event < parsed_data.size(); event++) {//TODO change this back: parsed_data.size(); event++) {   
                    comp_retval = comp_retval && aux_compare_events(parsed_data[0][stream+1][0], parsed_data[event][stream+1][0], debug);
                }
                if(comp_retval) {
                    if(debug) cout << "HW input stream " << stream << " self-consistent" << endl;
                } else {
                    if(debug) cout << "HW input stream " << stream << " NOT self-consistent" << endl;
                }
            }

            cout << "Calculated check" << endl;
            comp_retval = true;
            bool comp_retval_temp;
            for (u_int event = 1; event < merged_stream.size(); event++) {//TODO change this back: parsed_data.size(); event++) {
                //comp_retval True if matching
                comp_retval_temp = aux_compare_events(merged_stream[0], merged_stream[event], debug);
                comp_retval = comp_retval && comp_retval_temp;
            }
            if(comp_retval) {
                if(debug) cout << "HW Calcualted stream self-consistent" << endl;
            } else {
                if(debug) cout << "HW Calculated stream NOT self-consistent" << endl;
            }       

            cout << "Output check" << endl;
            comp_retval = true;
            for (u_int event = 1; event < parsed_data.size(); event++) {//TODO change this back: parsed_data.size(); event++) {
                //comp_retval True if matching
                comp_retval_temp = aux_compare_events(parsed_data[0][0][0], parsed_data[event][0][0], debug);
                comp_retval = comp_retval && comp_retval_temp;
            }
            if(comp_retval) {
                if(debug) cout << "HW output stream self-consistent" << endl;
            } else {
                if(debug) cout << "HW output stream NOT self-consistent" << endl;
            }
        }
      }
	
      return retval;

    }

  } //namespcae daq
} //namespcae ftk


#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


int main(int argc, char **argv) { 
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
   // ("tf_consts", value< std::string >()->default_value("testvectors/TFConstants_ttbar_full.txt"), "TF Constants File")
    ("tf_consts,c", value< std::string >()->default_value("testvectors/kk_corrgen_raw_8L_reg45.gcon"), "TF Constants File")
    ("ambank,a", value< std::string >()->default_value(""), "AMBank file")
    ("ssbase,s", value< std::string >()->default_value("/afs/cern.ch/work/m/mhank/private/config_for_bitlevel/ttbarTV"), "SSMap file base")
    ("tower,t", value< std::string >()->default_value("22"), "Tower for ssmap")
    ("oldDC", "use old DC bit encoding")
    ("allSectors", "All sectors were loaded to each processor. Used in standalone AUX tests and ModelSim")
    ("inChipOff", value< std::string >()->default_value("0"), "constant offset of map from RoadID to location in Bank TTree. Skimmed pattern banks usually use inChipOff = 1")
    ("inDirName,d", value< std::string >()->default_value("spy_output"), "TF Constants File")
    ("proc,p", value< std::string >()->default_value("0"), "Which proc to run bit level check on. Default is to run on them all")
    ("doSS", "do SSMap bit-check")
    ("noHS", "NOT include HitSort algorithm in SSMap bit-check")
    ("doTX", "do TX bit-check")
    ("doDO", "do DO bit-check")
    ("doTF", "do TF bit-check")
    ("doHW", "do HW bit-check")
    ("doAll", "do all bit-checks")
    ("debug", "dump debuggin info")
    ("bitstream,b", "dump bitstream")
    ("loop", "if running one testvector on a loop")
    ("split", "split pattern banks for TF check if using 128PU banks")
    ;

  positional_options_description p;

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  
  } catch( ... ) { // In case of errors during the parsing process, desc is printed for help 
    std::cerr << desc << endl; 
    return 1;
  }
  
  notify(vm);
  
  if (vm.count("help") ) { // if help is required, then desc is printed to output
    cout << endl <<  desc << endl ; 
    return 0;	
  }

  int proc = 2;
  istringstream( vm["proc"].as< std::string >() ) >> proc;

  bool debug=false;
  if (vm.count("debug") )
    { // if help is required, then desc is printed to output
      debug = true;
      report::dbg_lvl=1;
    }

  bool bitstream=vm.count("bitstream") ? true : false;

  bool doAll = true;

  bool doSS = vm.count("doSS") ? true : false;
  bool doHS = vm.count("noHS") ? false : true;
  bool doTX = vm.count("doTX") ? true : false;
  bool doDO = vm.count("doDO") ? true : false;
  bool doTF = vm.count("doTF") ? true : false;
  bool doHW = vm.count("doHW") ? true : false;
  bool loop = vm.count("loop") ? true : false;
  bool split = vm.count("split") ? true : false;

  if(doSS || doTX || doDO || doTF || doHW) doAll=false;
  
  if(doAll){
    doSS = true;
    doHS = true;
    doTX = false;
    doDO = true;
    doTF = true;
    doHW = true;
  }

  if(doSS == false) doHS=false;

  std::string ambank    = vm["ambank"].as<std::string>();
  std::string ssbase    = vm["ssbase"].as<std::string>();
  std::string tower    = vm["tower"].as<std::string>();
  bool oldDC            = vm.count("oldDC")      ? true : false;
  bool allSectors       = vm.count("allSectors") ? true : false;
  std::string inChipOff = vm["inChipOff"].as<std::string>();

  std::string tf_consts = vm["tf_consts"].as<std::string>();
  std::string inDirName = vm["inDirName"].as<std::string>();

  return  daq::ftk::aux_check_spy_buffers(tf_consts, ambank, oldDC, allSectors, inChipOff, inDirName, tower, ssbase, doSS, doHS, doTX, doDO, doTF, doHW, debug, bitstream, loop, proc, split);

}

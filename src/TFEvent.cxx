#include "aux/TF/TFEvent.h"

struct track_less_than{
  inline bool operator() (const daq::ftk::TFCombination* track1, const daq::ftk::TFCombination* track2){
    return ((*track1) < (*track2));
  }
};

namespace daq {
  namespace ftk {
    TFEvent::TFEvent()
      : id( -1 )
    {}

    TFEvent::~TFEvent() {}

    void TFEvent::setID(int v) { id = v; }

    void TFEvent::addRoad( TFRoad *road ) { vec_roads.push_back( road ); }

    int TFEvent::getID() const { return id; }
    unsigned int TFEvent::getNRoads() const { return int(vec_roads.size()); }

    TFRoad* TFEvent::getRoad(unsigned int i)
    { return vec_roads[i]; }

    std::vector<TFCombination*> TFEvent::getTrackVector()
    {
      std::vector<TFCombination*> vec_tracks;
      for(unsigned int iroad = 0 ; iroad < vec_roads.size() ; iroad++ )
	{
	  for(unsigned int icomb = 0 ; icomb < vec_roads[iroad]->getNCombinations() ; icomb++ )
	    {
	      TFCombination *comb = vec_roads[iroad]->getCombination(icomb);
	      if( comb->getPasses() ) vec_tracks.push_back( comb );
	    }
	}
      std::sort(vec_tracks.begin(), vec_tracks.end(), track_less_than());
      return vec_tracks;
    }

    bool TFEvent::compareTracks(TFEvent *other,unsigned int &matched_tracks, unsigned int &total_tracks, bool split_sectors )
    {
      std::vector<TFCombination*> my_tracks = getTrackVector();
      std::vector<TFCombination*> other_tracks = other->getTrackVector();

      matched_tracks = 0;
      total_tracks   = my_tracks.size();

      for( ; my_tracks.size() > 0 ; my_tracks.pop_back() )
	{
	  TFCombination *my_track = my_tracks.back();

      if(split_sectors) {
        my_track->setSectorID(my_track->getSectorID()%8%4+my_track->getSectorID()/8*4);
      }

	  for(unsigned int iother = 0 ; iother < other_tracks.size() ; iother++ )
	    {
	      TFCombination *other_track = other_tracks[iother];
	      if( (*other_track) == (*my_track) )
		{
		  // Track found!
		  // Iterate number of matched tracks, and remove the match from the list so we don't
		  // match 2 "my_track"s to a single "other_track"
		  matched_tracks++;
		  other_tracks.erase( other_tracks.begin() + iother );
		  break;
		}
	    }

	}

      // print unmatched tracks
      if(other_tracks.size()>0 && report::dbg_lvl>1)
	{
	  report::debug("The following tracks are left over in other event...");
	  for(auto other_track : other_tracks)
	    other_track->print();
	} else if (report::dbg_lvl>1) {
      report::debug("No unmatched tracks!");
    }

      // cleanup & return result
      other_tracks.clear();
      return total_tracks == matched_tracks;
    }

    void TFEvent::printRoads()
    {
      for( std::size_t i = 0 ; i < vec_roads.size() ; i++ ) vec_roads[i]->print();
    }

    void TFEvent::printRoadsAndCombinations()
    {
      for( std::size_t i = 0 ; i < vec_roads.size() ; i++ )
	{
	  vec_roads[i]->print();
	  vec_roads[i]->printCombinations();
	}
    }
  } // namespace ftk
} // namespace daq

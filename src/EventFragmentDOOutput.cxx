#include "aux/SpyBuffer/EventFragmentDOOutput.h"

#include <iostream>

#include "aux/TF/TFGen.h"

namespace daq {
  namespace ftk {

    EventFragmentDOOutput::EventFragmentDOOutput()
      : EventFragment()
    { }

    EventFragmentDOOutput::~EventFragmentDOOutput()
    { }

    std::vector<unsigned int> EventFragmentDOOutput::getData() const
    { return _data; }

    void EventFragmentDOOutput::parseFragment(const std::vector<unsigned int> &data)
    {
      // Read meta-data
      setL1ID(data[1]);

      // Parse the things
      _data=data;

      // Kill the first two words (b0f and L1ID)
      if (_data.size() > 1) {
        _data.erase(_data.begin());
        _data.erase(_data.begin());
      } else {
        std::cout << "You don't have two words in this fragment. Hopefully this means you're on the last fragment and it got cut off" << std::endl;
      }
    }

    std::vector<unsigned int> EventFragmentDOOutput::bitstream() const
    {
      std::vector<unsigned int> result;
      result.push_back(0xb0b00d01);
      result.push_back(getL1ID());
      result.insert(result.end(), _data.begin(), _data.end());
      return result;
    }

    std::vector<EventFragment*> DOOutput_splitFragments(const std::vector<unsigned int>& data)
    {
      std::vector<EventFragment*> events;
  
      std::vector<unsigned int> eventData;

      EventFragment *event=0;
      for(unsigned int i=0;i<data.size();i++)
	{

	  // Check for start of new event
	  if(data[i]==0xb0b00d01)
	    {
	      if(event!=0) // Close last event
		{
		  event->parseFragment(eventData);
		  events.push_back(event);
		}

	      // Create new event
	      event=new EventFragmentDOOutput();
	      eventData.clear();
	    }
      
	  // Add word to data list
	  eventData.push_back(data[i]);
	}

      eventData.pop_back(); // Kill last event since it might be incomplete

      return events;
    }
  } // namespace ftk
} // namespace daq

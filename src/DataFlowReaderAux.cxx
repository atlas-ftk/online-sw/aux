#include "aux/aux_vme_regs.h"

#include "ftkcommon/Utils.h"

#include "aux/DataFlowReaderAux.h"
#include "ftkcommon/ReadSRFromISVectors.h"

//OnlineServices to check if running within a partition
#include <ipc/partition.h>
#include <ipc/core.h>


// ers
#include <ers/ers.h>
#include "ftkcommon/exceptions.h"



using namespace std;
using namespace daq::ftk;

/************************************************************/
DataFlowReaderAux::DataFlowReaderAux(std::shared_ptr<OHRawProvider<>> ohRawProvider, bool forceIS) 
   : DataFlowReader(ohRawProvider, forceIS)
/************************************************************/
{
}



/************************************************************/
void DataFlowReaderAux::init(const string& deviceName, std::vector<uint> clock_freq, std::vector<uint> periods, const string& partitionName, const string& isServerName)
/************************************************************/
{
  DataFlowReader::init(deviceName, partitionName, isServerName);
  DataFlowReader::name_dataflow();
  m_theAUXSummary = std::make_shared<FtkDataFlowSummaryAUXNamed>(m_ipcp,name_dataflow());
  m_theSummary = std::static_pointer_cast<FtkDataFlowSummaryAUXNamed>(m_theAUXSummary);
  m_clock_freqs = clock_freq;
  m_periods = periods;

  /*
  std::unique_ptr<aux_registers> reg_getter(new aux_registers()); // Deleted when function returns
  m_ISRegs = reg_getter->getISRegs(); // Map of all the registers to do in setupCollection

  std::string outstr = "";
  for (auto fpga = m_ISRegs.cbegin(); fpga != m_ISRegs.cend(); fpga++) {
    for (auto it = fpga->second.cbegin(); it != fpga->second.cend(); it++) {
       outstr += fpga->first + " : ";
       outstr += it->first + " : ";
       outstr += it->second + "\n";
    }
  }
  ERS_LOG("m_ISRegs:\n" + outstr);
  */


  ERS_LOG("Initializing AUX DataFlowSummary : " <<name_dataflow() << " : pointer");
}


/************************************************************/
vector<string> DataFlowReaderAux::getISVectorNames()
/************************************************************/
{
  vector<string> srv_names;

  srv_names.push_back("I1_sr_v");
  srv_names.push_back("I2_sr_v");
  srv_names.push_back("P1_sr_v");
  srv_names.push_back("P2_sr_v");
  srv_names.push_back("P3_sr_v");
  srv_names.push_back("P4_sr_v");

  srv_names.push_back("I1_sb_v");
  srv_names.push_back("I2_sb_v");
  srv_names.push_back("P1_sb_v");
  srv_names.push_back("P2_sb_v");
  srv_names.push_back("P3_sb_v");
  srv_names.push_back("P4_sb_v");
  
  //cluster count
  srv_names.push_back("I1_cc_v");
  srv_names.push_back("I2_cc_v");
    
  //fit limit
  srv_names.push_back("P1_fl_v");
  srv_names.push_back("P2_fl_v");
  srv_names.push_back("P3_fl_v");
  srv_names.push_back("P4_fl_v");

  //combiner overflow
  srv_names.push_back("P1_co_v");
  srv_names.push_back("P2_co_v");
  srv_names.push_back("P3_co_v");
  srv_names.push_back("P4_co_v");
  
  return srv_names;
}

/************************************************************/
void DataFlowReaderAux::publishExtraHistos(uint32_t option)
/************************************************************/
{
  std::vector<int64_t> srv;
  
  //General 
  srv.clear();
  getAuxFreezeStatus(srv);
  publishSingleTH1F(srv, "Aux Freeze Status");
  m_theAUXSummary->AuxFreezeStatus = srv;
  
  srv.clear(); 
  getAuxFifoOverflow(srv);  
  publishSingleTH1F(srv,"Aux input/output FIFO overflow");
  m_theAUXSummary->AuxFifoOverflow = srv;

  srv.clear(); 
  getAuxNEventsInOut(srv);  
  publishSingleTH1F(srv,"Aux Number of Events In/Out on each channel");
  m_theAUXSummary->AuxNEventsInOut = srv;


  //Sums of counters
  srv.clear(); 
  getAuxOutputTracks(srv);  
  publishSingleTH1F(srv,"NumOutputTracks");
  m_theAUXSummary->AuxOutputTracks = srv;

  srv.clear(); 
  getAuxTFFitCount(srv);  
  publishSingleTH1F(srv,"TF fit count (sum of 7 fitters)");
  m_theAUXSummary->AuxTFFitCount = srv;
  
  srv.clear(); 
  getAuxTFFitRate(srv);  
  publishSingleTH1F(srv,"TF fit rate (sum of 7 fitters)");
  m_theAUXSummary->AuxTFFitRate = srv;
  
  srv.clear(); 
  getAuxTFTrackCount(srv);  
  publishSingleTH1F(srv,"TF track count (sum of 7 fitters)");
  m_theAUXSummary->AuxTFTrackCount = srv;
  
  srv.clear(); 
  getAuxTFTrackRate(srv);  
  publishSingleTH1F(srv,"TF track rate (sum of 7 fitters)");
  m_theAUXSummary->AuxTFTrackRate = srv;
  
  srv.clear(); 
  getAuxNumFitsByType(srv);  
  publishSingleTH1F(srv,"Total number of fits of each type.");
  m_theAUXSummary->AuxNumFitsByType = srv;

  srv.clear(); 
  getAuxNumTracksByType(srv);  
  publishSingleTH1F(srv,"Total number of tracks of each type.");
  m_theAUXSummary->AuxNumTracksByType = srv;

  // Per FPGA Counters
  srv.clear(); 
  getAuxTFFitCountOneFPGA(srv,3);  
  publishSingleTH1F(srv,"TF fit count in Processor 1");
  m_theAUXSummary->AuxTFFitCount1 = srv;
  
  srv.clear(); 
  getAuxTFFitCountOneFPGA(srv,4);  
  publishSingleTH1F(srv,"TF fit count in Processor 2");
  m_theAUXSummary->AuxTFFitCount2 = srv;
  
  srv.clear(); 
  getAuxTFFitCountOneFPGA(srv,5);  
  publishSingleTH1F(srv,"TF fit count in Processor 3");
  m_theAUXSummary->AuxTFFitCount3 = srv;
 
  srv.clear(); 
  getAuxTFFitCountOneFPGA(srv,6);  
  publishSingleTH1F(srv,"TF fit count in Processor 4");
  m_theAUXSummary->AuxTFFitCount4 = srv;
  
  srv.clear(); 
  getAuxTFTrackCountOneFPGA(srv,3);  
  publishSingleTH1F(srv,"TF track count in Processor 1");
  m_theAUXSummary->AuxTFTrackCount1 = srv;

  srv.clear(); 
  getAuxTFTrackCountOneFPGA(srv,4);  
  publishSingleTH1F(srv,"TF track count in Processor 2");
  m_theAUXSummary->AuxTFTrackCount2 = srv;

  srv.clear(); 
  getAuxTFTrackCountOneFPGA(srv,5);  
  publishSingleTH1F(srv,"TF track count in Processor 3");
  m_theAUXSummary->AuxTFTrackCount3 = srv;

  srv.clear(); 
  getAuxTFTrackCountOneFPGA(srv,6);  
  publishSingleTH1F(srv,"TF track count in Processor 4");
  m_theAUXSummary->AuxTFTrackCount4 = srv;

  srv.clear();
  getAuxRoadCount(srv);  
  publishSingleTH1F(srv,"Number of roads in each processor");
  m_theAUXSummary->AuxRoadCount = srv;

  srv.clear();
  getAuxClusterCount(srv);
  publishSingleTH1F(srv, "Aux Number of Clusters per Layer");
  m_theAUXSummary->AuxClusterCount = srv;

  srv.clear();
  getAuxModuleCount(srv);
  publishSingleTH1F(srv, "Aux Number of Modules per Layer");
  m_theAUXSummary->AuxModuleCount = srv;

  srv.clear();
  getAuxSSIDCount(srv);
  publishSingleTH1F(srv, "Aux Number of SSIDs per Layer");
  m_theAUXSummary->AuxSSIDCount = srv;
  
  srv.clear();
  getAuxErrorCountInput1(srv);
  publishSingleTH1F(srv, "Aux Input 1 errors");
  m_theAUXSummary->AuxErrorCountInput1 = srv;
  
  srv.clear();
  getAuxErrorCountInput2(srv);
  publishSingleTH1F(srv, "Aux Input 2 errors");
  m_theAUXSummary->AuxErrorCountInput2 = srv;
  
  srv.clear();
  getAuxErrorCountProcessor(srv, 3);
  publishSingleTH1F(srv, "Aux Processor 1 errors");
  m_theAUXSummary->AuxErrorCountProcessor1 = srv;
  
  srv.clear();
  getAuxErrorCountProcessor(srv, 4);
  publishSingleTH1F(srv, "Aux Processor 2 errors");
  m_theAUXSummary->AuxErrorCountProcessor2 = srv;
  
  srv.clear();
  getAuxErrorCountProcessor(srv, 5);
  publishSingleTH1F(srv, "Aux Processor 3 errors");
  m_theAUXSummary->AuxErrorCountProcessor3 = srv;
  
  srv.clear();
  getAuxErrorCountProcessor(srv, 6);
  publishSingleTH1F(srv, "Aux Processor 4 errors");
  m_theAUXSummary->AuxErrorCountProcessor4 = srv;

  srv.clear();
  getAuxInHoldCount(srv);
  publishSingleTH1F(srv, "Aux Input Hold Count");
  m_theAUXSummary->AuxInHoldCount= srv;
  
  srv.clear();
  getAuxOutHoldCount(srv);
  publishSingleTH1F(srv, "Aux Output Hold Count");
  m_theAUXSummary->AuxOutHoldCount= srv;
  
  srv.clear();
  getAuxInternalHoldCount(srv);
  publishSingleTH1F(srv, "Aux Internal Hold Count");
  m_theAUXSummary->AuxInternalHoldCount= srv;

  
  //Per FPGA Rates
  srv.clear(); 
  getAuxTFFitRateOneFPGA(srv,3);  
  publishSingleTH1F(srv,"TF fit rate in Processor 1");
  m_theAUXSummary->AuxTFFitRate1 = srv;
 
  srv.clear(); 
  getAuxTFFitRateOneFPGA(srv,4);  
  publishSingleTH1F(srv,"TF fit rate in Processor 2");
  m_theAUXSummary->AuxTFFitRate2 = srv;

  srv.clear(); 
  getAuxTFFitRateOneFPGA(srv,5);  
  publishSingleTH1F(srv,"TF fit rate in Processor 3");
  m_theAUXSummary->AuxTFFitRate3 = srv;

  srv.clear(); 
  getAuxTFFitRateOneFPGA(srv,6);  
  publishSingleTH1F(srv,"TF fit rate in Processor 4");
  m_theAUXSummary->AuxTFFitRate4 = srv;

  srv.clear(); 
  getAuxTFTrackRateOneFPGA(srv,3);  
  publishSingleTH1F(srv,"TF track rate in Processor 1");
  m_theAUXSummary->AuxTFTrackRate1 = srv;

  srv.clear(); 
  getAuxTFTrackRateOneFPGA(srv,4);  
  publishSingleTH1F(srv,"TF track rate in Processor 2");
  m_theAUXSummary->AuxTFTrackRate2 = srv;

  srv.clear(); 
  getAuxTFTrackRateOneFPGA(srv,5);  
  publishSingleTH1F(srv,"TF track rate in Processor 3");
  m_theAUXSummary->AuxTFTrackRate3 = srv;

  srv.clear(); 
  getAuxTFTrackRateOneFPGA(srv,6);  
  publishSingleTH1F(srv,"TF track rate in Processor 4");
  m_theAUXSummary->AuxTFTrackRate4 = srv;
  srv.clear(); 
  
  
  
  //Max Stuff 
  srv.clear();
  getAuxMaxFits(srv, 3);
  publishSingleTH1F(srv, "Aux Max Fits 1");
  m_theAUXSummary->AuxMaxFits1 = srv;
  
  srv.clear();
  getAuxMaxFits(srv,4);
  publishSingleTH1F(srv, "Aux Max Fits 2");
  m_theAUXSummary->AuxMaxFits2 = srv;
  
  srv.clear();
  getAuxMaxFits(srv,5);
  publishSingleTH1F(srv, "Aux Max Fits 3");
  m_theAUXSummary->AuxMaxFits3 = srv;
  
  srv.clear();
  getAuxMaxFits(srv,6);
  publishSingleTH1F(srv, "Aux Max Fits 4");
  m_theAUXSummary->AuxMaxFits4 = srv;
  
  srv.clear();
  getAuxMaxTracks(srv, 3);
  publishSingleTH1F(srv, "Aux Max Tracks 1");
  m_theAUXSummary->AuxMaxTracks1 = srv;
  
  srv.clear();
  getAuxMaxTracks(srv,4);
  publishSingleTH1F(srv, "Aux Max Tracks 2");
  m_theAUXSummary->AuxMaxTracks2 = srv;
  
  srv.clear();
  getAuxMaxTracks(srv,5);
  publishSingleTH1F(srv, "Aux Max Tracks 3");
  m_theAUXSummary->AuxMaxTracks3 = srv;
  
  srv.clear();
  getAuxMaxTracks(srv,6);
  publishSingleTH1F(srv, "Aux Max Tracks 4");
  m_theAUXSummary->AuxMaxTracks4 = srv;
  
  srv.clear();
  getAuxMaxRoads(srv);
  publishSingleTH1F(srv, "Aux Max Roads");
  m_theAUXSummary->AuxMaxRoads = srv;
  
  
  srv.clear();
  getAuxMaxClusters(srv);
  publishSingleTH1F(srv, "Aux Max Clusters");
  m_theAUXSummary->AuxMaxClusters = srv;
  
  
  srv.clear();
  getAuxMaxModules(srv);
  publishSingleTH1F(srv, "Aux Max Modules");
  m_theAUXSummary->AuxMaxModules = srv;
  
  srv.clear();
  getAuxMaxSSIDs(srv);
  publishSingleTH1F(srv, "Aux Max SSIDs");
  m_theAUXSummary->AuxMaxSSIDs = srv;
  
  srv.clear();
  getAuxMaxTracksOut(srv);
  publishSingleTH1F(srv, "Aux Max Tracks Out");
  m_theAUXSummary->AuxMaxTracksOut = srv;
 
  srv.clear();
  getAuxMaxProcessingTime(srv);
  publishSingleTH1F(srv, "Aux Max Processing Time");
  m_theAUXSummary->AuxMaxProcessingTime = srv;

  srv.clear();
  getAuxMinProcessingTime(srv);
  publishSingleTH1F(srv, "Aux Min Processing Time");
  m_theAUXSummary->AuxMinProcessingTime = srv;

  //Data Truncation
  srv.clear(); 
  getAuxFitLimitReached(srv,3);  
  publishSingleTH1F(srv,"TF fit limit reached in Processor 1");
  m_theAUXSummary->AuxFitLimitReached1 = srv;

  srv.clear(); 
  getAuxFitLimitReached(srv,4);  
  publishSingleTH1F(srv,"TF fit limit reached in Processor 2");
  m_theAUXSummary->AuxFitLimitReached2 = srv;

  srv.clear(); 
  getAuxFitLimitReached(srv,5);  
  publishSingleTH1F(srv,"TF fit limit reached in Processor 3");
  m_theAUXSummary->AuxFitLimitReached3 = srv;

  srv.clear(); 
  getAuxFitLimitReached(srv,6);  
  publishSingleTH1F(srv,"TF fit limit reached in Processor 4");
  m_theAUXSummary->AuxFitLimitReached4 = srv;

  getAuxCombinerOverflow(srv,3);  
  publishSingleTH1F(srv,"TF combiner overflow in Processor 1");
  m_theAUXSummary->AuxCombinerOverflow1 = srv;

  srv.clear(); 
  getAuxCombinerOverflow(srv,4);  
  publishSingleTH1F(srv,"TF combiner overflow in Processor 2");
  m_theAUXSummary->AuxCombinerOverflow2 = srv;

  srv.clear(); 
  getAuxCombinerOverflow(srv,5);  
  publishSingleTH1F(srv,"TF combiner overflow in Processor 3");
  m_theAUXSummary->AuxCombinerOverflow3 = srv;

  srv.clear(); 
  getAuxCombinerOverflow(srv,6);  
  publishSingleTH1F(srv,"TF combiner overflow in Processor 4");
  m_theAUXSummary->AuxCombinerOverflow4 = srv;

  srv.clear();
  getAuxHWOverlapLimitReached(srv);
  publishSingleTH1F(srv, "Aux more than 16 tracks in a road");
  m_theAUXSummary->AuxHWOverlapLimitReached = srv;
  
  srv.clear();
  getAuxMaxHitsPerSSID(srv);
  publishSingleTH1F(srv, "Aux more than 8 clusters in an SSID");
  m_theAUXSummary->AuxMaxHitsPerSSID = srv;
  
  srv.clear();
  getAuxRoadLimitReached(srv);
  publishSingleTH1F(srv, "More roads than limit set in OKS");
  m_theAUXSummary->AuxRoadLimitReached = srv;

}


/************************************************************/
void DataFlowReaderAux::getFpgaTemperature(vector<int64_t>& srv)
/************************************************************/
{
  srv.push_back(127 - 0xFF + getBitRange(readRegister(TEMPERATURE,1),8,15));
  srv.push_back(127 - 0xFF + getBitRange(readRegister(TEMPERATURE,2),8,15));
  srv.push_back(127 - 0xFF + getBitRange(readRegister(TEMPERATURE,3),8,15));
  srv.push_back(127 - 0xFF + getBitRange(readRegister(TEMPERATURE,4),8,15));
  srv.push_back(127 - 0xFF + getBitRange(readRegister(TEMPERATURE,5),8,15));
  srv.push_back(127 - 0xFF + getBitRange(readRegister(TEMPERATURE,6),8,15));
}

/************************************************************/
void DataFlowReaderAux::getAuxRoadCount(vector<int64_t>& srv)
/************************************************************/
{
  srv.push_back(readRegister(TOTAL_ROADS,3));
  srv.push_back(readRegister(TOTAL_ROADS,4));
  srv.push_back(readRegister(TOTAL_ROADS,5));
  srv.push_back(readRegister(TOTAL_ROADS,6));
}

/************************************************************/
void DataFlowReaderAux::getAuxRoadLimitReached(vector<int64_t>& srv)
/************************************************************/
{
  srv.push_back(readRegister(ROAD_LIMIT_REACHED,3));
  srv.push_back(readRegister(ROAD_LIMIT_REACHED,4));
  srv.push_back(readRegister(ROAD_LIMIT_REACHED,5));
  srv.push_back(readRegister(ROAD_LIMIT_REACHED,6));
}

/************************************************************/
void DataFlowReaderAux::getAuxMaxRoads(vector<int64_t>& srv)
/************************************************************/
{
  srv.push_back(readRegister(MAX_ROADS,3));
  srv.push_back(readRegister(MAX_ROADS,4));
  srv.push_back(readRegister(MAX_ROADS,5));
  srv.push_back(readRegister(MAX_ROADS,6));
}


/************************************************************/
void DataFlowReaderAux::getAuxOutputTracks(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
  srv.push_back(readRegister(OUTPUT_TRACKS, 2)); // readRegister(register address, # of FPGA)
}

/************************************************************/
void DataFlowReaderAux::getAuxFreezeStatus(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
  srv.push_back(readRegister(FREEZE_STATUS, 1)); // readRegister(register address, # of FPGA)
  srv.push_back(readRegister(FREEZE_STATUS, 2)); // readRegister(register address, # of FPGA)
  srv.push_back(readRegister(FREEZE_STATUS, 3)); // readRegister(register address, # of FPGA)
  srv.push_back(readRegister(FREEZE_STATUS, 4)); // readRegister(register address, # of FPGA)
  srv.push_back(readRegister(FREEZE_STATUS, 5)); // readRegister(register address, # of FPGA)
  srv.push_back(readRegister(FREEZE_STATUS, 6)); // readRegister(register address, # of FPGA)
  
  srv.push_back(readRegister(HW_FREEZE_STATUS, 2)); // readRegister(register address, # of FPGA)
}



/************************************************************/
void DataFlowReaderAux::getAuxTFFitCount(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
  for(unsigned int fpgaidx=3;fpgaidx<=6;fpgaidx++){
    u_int total_fits = 0;
    for(unsigned int fitteridx=0;fitteridx<=6;fitteridx++){
      //srv.push_back(readRegister((0x4d<<16)+(fitteridx<<8), fpgaidx)); // readRegister(register address, # of FPGA)
      total_fits += readRegister((0x4d<<16)+(fitteridx<<8), fpgaidx);
    }
    srv.push_back(total_fits);
  }  
}

/************************************************************/
void DataFlowReaderAux::getAuxTFFitCountOneFPGA(vector<int64_t>& srv, unsigned int fpgaidx)  // Ruo
/************************************************************/
{
    u_int total_fits = 0;
    for(unsigned int fitteridx=0;fitteridx<=6;fitteridx++){
      //srv.push_back(readRegister((0x4d<<16)+(fitteridx<<8), fpgaidx)); // readRegister(register address, # of FPGA)
      total_fits = readRegister((0x4d<<16)+(fitteridx<<8), fpgaidx);
      srv.push_back(total_fits);
    }
}

/************************************************************/
void DataFlowReaderAux::getAuxTFFitRate(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
  for(unsigned int fpgaidx=3;fpgaidx<=6;fpgaidx++){
    u_int total_fits_rate = 0;
    for(unsigned int fitteridx=0;fitteridx<=6;fitteridx++){
      //srv.push_back(readRegister((0x4e<<16)+(fitteridx<<8), fpgaidx)); // readRegister(register address, # of FPGA)
      total_fits_rate += 10*readRegister((0x4e<<16)+(fitteridx<<8), fpgaidx);
    }
    srv.push_back(total_fits_rate);
  }  
}

/************************************************************/
void DataFlowReaderAux::getAuxTFFitRateOneFPGA(vector<int64_t>& srv, unsigned int fpgaidx)  // Ruo
/************************************************************/
{
    u_int total_fits_rate = 0;
    for(unsigned int fitteridx=0;fitteridx<=6;fitteridx++){
      //srv.push_back(readRegister((0x4e<<16)+(fitteridx<<8), fpgaidx)); // readRegister(register address, # of FPGA)
      total_fits_rate = 10*readRegister((0x4e<<16)+(fitteridx<<8), fpgaidx);
      srv.push_back(total_fits_rate);
    }
}

/************************************************************/
void DataFlowReaderAux::getAuxMaxFits(vector<int64_t>& srv, unsigned int fpgaidx)  // Ruo
/************************************************************/
{
    u_int max = 0;
    for(unsigned int fitteridx=0;fitteridx<=6;fitteridx++){
      //srv.push_back(readRegister((0x4e<<16)+(fitteridx<<8), fpgaidx)); // readRegister(register address, # of FPGA)
      max = readRegister((0x4d<<16)+(7<<8) + (fitteridx<<4), fpgaidx);
      srv.push_back(max);
    }
}


/************************************************************/
void DataFlowReaderAux::getAuxMaxSSIDs(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
    u_int ssid_count = 0;
    u_int nLayers = 11;
    for( u_int iLayer=0; iLayer < nLayers; iLayer++){
        if(iLayer < 7){
            ssid_count = readRegister((SSID_COUNT<<16)+(7<<8)+(iLayer<<4), 1);
            srv.push_back(ssid_count);

        }
        else{
            ssid_count = readRegister((SSID_COUNT<<16)+(7<<8)+((iLayer-7)<<4), 2);
            srv.push_back(ssid_count);
        }
    }
}

/************************************************************/
void DataFlowReaderAux::getAuxMaxModules(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
    u_int cluster_count = 0;
    u_int nLayers = 8;
    for( u_int iLayer=0; iLayer < nLayers; iLayer++){
        if(iLayer < 4){
            cluster_count = readRegister((MODULE_COUNT<<16)+(7<<8)+(iLayer<<4), 1);
            srv.push_back(cluster_count);

        }
        else{
            cluster_count = readRegister((MODULE_COUNT<<16)+(7<<8)+((iLayer-4)<<4), 2);
            srv.push_back(cluster_count);
        }
    }
}

/************************************************************/
void DataFlowReaderAux::getAuxMaxClusters(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
    u_int cluster_count = 0;
    u_int nLayers = 8;
    for( u_int iLayer=0; iLayer < nLayers; iLayer++){
        if(iLayer < 4){
            cluster_count = readRegister((CLUSTER_COUNT<<16)+(7<<8)+(iLayer<<4), 1);
            srv.push_back(cluster_count);

        }
        else{
            cluster_count = readRegister((CLUSTER_COUNT<<16)+(7<<8)+((iLayer-4)<<4), 2);
            srv.push_back(cluster_count);
        }
    }
}


/************************************************************/
void DataFlowReaderAux::getAuxMaxTracksOut(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
  srv.push_back(readRegister(0x450, 2)); // readRegister(register address, # of FPGA)
}

/************************************************************/
void DataFlowReaderAux::getAuxHWOverlapLimitReached(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
  srv.push_back(-1);
}

/************************************************************/
void DataFlowReaderAux::getAuxMaxHitsPerSSID(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
  srv.push_back(-1);
}


/************************************************************/
void DataFlowReaderAux::getAuxMaxTracks(vector<int64_t>& srv, unsigned int fpgaidx)  // Ruo
/************************************************************/
{
    u_int max = 0;
    for(unsigned int fitteridx=0;fitteridx<=6;fitteridx++){
      //srv.push_back(readRegister((0x4e<<16)+(fitteridx<<8), fpgaidx)); // readRegister(register address, # of FPGA)
      max = readRegister((0x4f<<16)+(7<<8) + (fitteridx<<4), fpgaidx);
      srv.push_back(max);
    }
}


/************************************************************/
void DataFlowReaderAux::getAuxTFTrackCount(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
  for(unsigned int fpgaidx=3;fpgaidx<=6;fpgaidx++){
    u_int total_track = 0;
    for(unsigned int fitteridx=0;fitteridx<=6;fitteridx++){
      //srv.push_back(readRegister((0x4f<<16)+(fitteridx<<8), fpgaidx)); // readRegister(register address, # of FPGA)
      total_track += readRegister((0x4f<<16)+(fitteridx<<8), fpgaidx);
    }
    srv.push_back(total_track);
  }  
}

/************************************************************/
void DataFlowReaderAux::getAuxTFTrackCountOneFPGA(vector<int64_t>& srv, unsigned int fpgaidx)  // Ruo
/************************************************************/
{
    u_int total_track = 0;
    for(unsigned int fitteridx=0;fitteridx<=6;fitteridx++){
      //srv.push_back(readRegister((0x4f<<16)+(fitteridx<<8), fpgaidx)); // readRegister(register address, # of FPGA)
      total_track = readRegister((0x4f<<16)+(fitteridx<<8), fpgaidx);
      srv.push_back(total_track);
    }
}

/************************************************************/
void DataFlowReaderAux::getAuxTFTrackRate(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
  for(unsigned int fpgaidx=3;fpgaidx<=6;fpgaidx++){
    u_int total_track_rate = 0;
    for(unsigned int fitteridx=0;fitteridx<=6;fitteridx++){
      //srv.push_back(readRegister((0x50<<16)+(fitteridx<<8), fpgaidx)); // readRegister(register address, # of FPGA)
      total_track_rate += 10*readRegister((0x50<<16)+(fitteridx<<8), fpgaidx);
    }
    srv.push_back(total_track_rate);
  }  
}

/************************************************************/
void DataFlowReaderAux::getAuxTFTrackRateOneFPGA(vector<int64_t>& srv, unsigned int fpgaidx)  // Ruo
/************************************************************/
{
    u_int total_track_rate = 0;
    for(unsigned int fitteridx=0;fitteridx<=6;fitteridx++){
      //srv.push_back(readRegister((0x50<<16)+(fitteridx<<8), fpgaidx)); // readRegister(register address, # of FPGA)
      total_track_rate = 10*readRegister((0x50<<16)+(fitteridx<<8), fpgaidx);
      srv.push_back(total_track_rate);
    }
}

/************************************************************/
void DataFlowReaderAux::getAuxFitLimitReached(vector<int64_t>& srv, unsigned int fpgaidx)  // Ruo
/************************************************************/
{   
    u_int limit_count = 0;
    for(unsigned int fitteridx=0;fitteridx<=6;fitteridx++){
      //srv.push_back(readRegister((0x50<<16)+(fitteridx<<8), fpgaidx)); // readRegister(register address, # of FPGA)
      limit_count = readRegister((FIT_LIMIT_REACHED<<16)+(fitteridx<<8), fpgaidx);
      srv.push_back(limit_count);
    }
}

/************************************************************/
void DataFlowReaderAux::getAuxCombinerOverflow(vector<int64_t>& srv, unsigned int fpgaidx)  // Ruo
/************************************************************/
{   
    u_int overflow_count = 0;
    for(unsigned int fitteridx=0;fitteridx<=6;fitteridx++){
      //srv.push_back(readRegister((0x50<<16)+(fitteridx<<8), fpgaidx)); // readRegister(register address, # of FPGA)
      overflow_count = readRegister((COMBINER_OVERFLOW<<16)+(fitteridx<<8), fpgaidx);
      srv.push_back(overflow_count);
    }
}



/************************************************************/
void DataFlowReaderAux::getAuxNumFitsByType(vector<int64_t>& srv) 
/************************************************************/
{
    u_int nom_fits = 0;
    u_int pix_fits = 0;
    u_int sct_fits = 0;
    for (int fpgaidx = 3; fpgaidx <= 6; fpgaidx++) {
      nom_fits += readRegister((0x4d<<16)+(0<<8), fpgaidx);

      for (int fittypeidx = 1; fittypeidx <= 4; fittypeidx++) {
          pix_fits += readRegister((0x4d<<16)+(fittypeidx<<8),fpgaidx);
      }

      for (int fittypeidx = 5; fittypeidx <= 6; fittypeidx++) {
          sct_fits += readRegister((0x4d<<16)+(fittypeidx<<8),fpgaidx);
      }
    }
    srv.push_back(nom_fits);
    srv.push_back(pix_fits);
    srv.push_back(sct_fits);
}

/************************************************************/
void DataFlowReaderAux::getAuxNumTracksByType(vector<int64_t>& srv) 
/************************************************************/
{
    u_int nom_tracks = 0;
    u_int pix_tracks = 0;
    u_int sct_tracks = 0;
    for (int fpgaidx = 3; fpgaidx <= 6; fpgaidx++) {
      nom_tracks += readRegister((0x4f<<16)+(0<<8), fpgaidx);

      for (int fittypeidx = 1; fittypeidx <= 4; fittypeidx++) {
          pix_tracks += readRegister((0x4f<<16)+(fittypeidx<<8),fpgaidx);
      }

      for (int fittypeidx = 5; fittypeidx <= 6; fittypeidx++) {
          sct_tracks += readRegister((0x4f<<16)+(fittypeidx<<8),fpgaidx);
      }
    }
    srv.push_back(nom_tracks);
    srv.push_back(pix_tracks);
    srv.push_back(sct_tracks);
}


/************************************************************/
void DataFlowReaderAux::getFifoInBusy(std::vector<int64_t>& srv)
/************************************************************/
{
  getAuxInLinkInfo(srv, SB_FIFO_USAGE, true); 
  // Busy status is bit 15
  for (auto& entry : srv) {
  	entry = ((entry >> 15) & 0x1);	
  }
}

/************************************************************/
void DataFlowReaderAux::getFifoOutBusy(std::vector<int64_t>& srv)
/************************************************************/
{
  getAuxOutLinkInfo(srv, SB_FIFO_USAGE, true); 
  // Busy status is bit 15
  for (auto& entry : srv) {
  	entry = ((entry >> 15) & 0x1);
  }
}

/************************************************************/
void DataFlowReaderAux::getFifoInEmpty(std::vector<int64_t>& srv)
/************************************************************/
{
  getAuxInLinkInfo(srv, SB_FIFO_USAGE, true);
  // Occupancy of FIFO is bits 0 to 12 in 32 bit word.
  for (auto& entry : srv) {
	entry &= 0x1fff;
  }
}

/************************************************************/
void DataFlowReaderAux::getFifoOutEmpty(std::vector<int64_t>& srv)
/************************************************************/
{
  getAuxOutLinkInfo(srv, SB_FIFO_USAGE, true);
  // Occupancy of FIFO is bits 0 to 12 in 32 bit word.
  for (auto& entry : srv) {
  	entry &= 0x1fff;
  }
}


//this is the slink fifo overflow
/************************************************************/
void DataFlowReaderAux::getAuxFifoOverflow(std::vector<int64_t>& srv)
/************************************************************/
{
  getAuxInLinkInfo(srv, SB_FIFO_USAGE, false);
  getAuxOutLinkInfo(srv, SB_FIFO_USAGE, false);
  for (auto& entry : srv) {
  	entry &= 0x2000; // bit 13 = overflow
  }
}



/************************************************************/
void DataFlowReaderAux::getEventRate(vector<int64_t>& srv)
/************************************************************/
{
    getAuxInLinkInfo(srv, SB_EVENT_RATE, false);
    getAuxOutLinkInfo(srv, SB_EVENT_RATE, false);
    for (auto& entry : srv) {
        entry = entry/10000; // bit 13 = overflow
    }
}

/************************************************************/
void DataFlowReaderAux::getL1id(std::vector<int64_t>& srv)
/************************************************************/
{
    getAuxInLinkInfo(srv, SB_L1ID, false);
}
/************************************************************/
void DataFlowReaderAux::getAuxInHoldCount(std::vector<int64_t>& srv)
/************************************************************/
{
    getAuxInLinkInfo(srv, SB_HOLD_COUNT, false);
}

/************************************************************/
void DataFlowReaderAux::getAuxOutHoldCount(std::vector<int64_t>& srv)
/************************************************************/
{
    getAuxOutLinkInfo(srv, SB_HOLD_COUNT, false);
}

/************************************************************/
void DataFlowReaderAux::getAuxInternalHoldCount(std::vector<int64_t>& srv)
/************************************************************/
{
    getAuxInternalLinkInfo(srv, SB_HOLD_COUNT, false);
}

/************************************************************/
void DataFlowReaderAux::getFifoInBusyFraction(std::vector<int64_t>& srv, uint32_t option)
/************************************************************/
{
    getAuxInLinkInfo(srv, SB_HOLD_FRAC, true);
}

/************************************************************/
void DataFlowReaderAux::getFifoOutBusyFraction(std::vector<int64_t>& srv, uint32_t option)
/************************************************************/
{
    getAuxOutLinkInfo(srv, SB_HOLD_FRAC, true);
}

// For BusyFraction, need to divide register by (ClockFreq[MHz] * 1e5)
/************************************************************/
void DataFlowReaderAux::getBusyFraction(std::vector<int64_t>& srv, uint32_t option)
/************************************************************/
{
    getAuxInternalLinkInfo(srv, SB_HOLD_FRAC, true);
}


/************************************************************/
void DataFlowReaderAux::getDataErrors(std::vector<int64_t>& srv)
/************************************************************/
{
  getAuxInLinkInfo(srv, SB_ERROR_BITS, false);
  getAuxOutLinkInfo(srv, SB_ERROR_BITS, false);
}

/************************************************************/
void DataFlowReaderAux::getLinkInStatus(std::vector<int64_t>& srv)
/************************************************************/
{
  u_int reg;

  // Input QSFP's from DF
  for (int fpga = 1; fpga <= 2; fpga++) {
	  reg = readRegister(SLINK_STATUS, fpga);
	  for (int ch = 0; ch < 4; ch++) {
		  srv.push_back((reg >> ch) & 1);
	  }
  }

  // Processor Roads
  for (int i = 3; i <= 6; i++) {
	  reg = readRegister(LINK_STATUS, i);
	  // Magic numbers where the processor road status is stored
	  srv.push_back((reg >> 24) & 1);
	  srv.push_back((reg >> 25) & 1);
	  srv.push_back((reg >> 26) & 1);
	  srv.push_back((reg >> 27) & 1);
  } 
}

/************************************************************/
void DataFlowReaderAux::getLinkOutStatus(std::vector<int64_t>& srv)
/************************************************************/
{
  u_int reg;
  
  // I1 SSData
  reg = readRegister(LINK_STATUS, 1);
  for (int ch = 0; ch < 8; ch++) {
	  srv.push_back((reg >> ch) & 1);
  }

  // I2 SSData
  reg = readRegister(LINK_STATUS, 2);
  for (int ch = 0; ch < 4; ch++) {
	  srv.push_back((reg >> ch) & 1);
  }

  // I2 SFP
  // 5th bit in SLINK_STATUS status register (1st bits = QSFP)
  srv.push_back((readRegister(SLINK_STATUS, 2) >> 4) & 1);
}




/************************************************************/
void DataFlowReaderAux::getAuxNEventsInOut(std::vector<int64_t>& srv)
/************************************************************/
{
  getAuxInLinkInfo(srv, SB_NUM_PACKETS, false);
  getAuxOutLinkInfo(srv, SB_NUM_PACKETS, false); 
}

/************************************************************/
void DataFlowReaderAux::getNEventsProcessed(std::vector<int64_t>& srv)
/************************************************************/
{
  srv.push_back(readRegister(HW_IH_NEventsProcessed,2)); 
}

/***************************************************************/
void DataFlowReaderAux::getNPacketsDiscarded(vector<int64_t>& srv)
/***************************************************************/
{
  srv.push_back(readRegister(HW_IH_NPacketsDiscarded_P1,2));
  srv.push_back(readRegister(HW_IH_NPacketsDiscarded_P2,2));
  srv.push_back(readRegister(HW_IH_NPacketsDiscarded_P3,2));
  srv.push_back(readRegister(HW_IH_NPacketsDiscarded_P4,2));
}

/***************************************************************/
void DataFlowReaderAux::getNPacketsHeldSync(vector<int64_t>& srv)
/***************************************************************/
{
  srv.push_back(readRegister(HW_IH_NPacketsHeldSync_P1,2));
  srv.push_back(readRegister(HW_IH_NPacketsHeldSync_P2,2));
  srv.push_back(readRegister(HW_IH_NPacketsHeldSync_P3,2));
  srv.push_back(readRegister(HW_IH_NPacketsHeldSync_P4,2));
}
/***************************************************************/
void DataFlowReaderAux::getNPacketsDiscardedSync(vector<int64_t>& srv)
/***************************************************************/
{
  srv.push_back(readRegister(HW_IH_NPacketsDiscardedSync_P1,2));
  srv.push_back(readRegister(HW_IH_NPacketsDiscardedSync_P2,2));
  srv.push_back(readRegister(HW_IH_NPacketsDiscardedSync_P3,2));
  srv.push_back(readRegister(HW_IH_NPacketsDiscardedSync_P4,2));
}
/***************************************************************/
void DataFlowReaderAux::getNPacketsTimeoutSync(vector<int64_t>& srv)
    /***************************************************************/
{
    srv.push_back(readRegister(HW_IH_NTimeoutsSync_P1,2));
    srv.push_back(readRegister(HW_IH_NTimeoutsSync_P2,2));
    srv.push_back(readRegister(HW_IH_NTimeoutsSync_P3,2));
    srv.push_back(readRegister(HW_IH_NTimeoutsSync_P4,2));
}
// Should be histogram of skew times.
/***************************************************************/
void DataFlowReaderAux::getInputSkewSync(vector<int64_t>& srv)
    /***************************************************************/
{
    srv.push_back(readRegister(HW_IH_SkewCounter,2));
}


/************************************************************/
void DataFlowReaderAux::getAuxClusterCount(vector<int64_t>& srv)  // Ruo
    /************************************************************/
{
    u_int cluster_count = 0;
    u_int nLayers = 8;
    for( u_int iLayer=0; iLayer < nLayers; iLayer++){
        if(iLayer < 4){
            cluster_count = readRegister((CLUSTER_COUNT<<16)+(iLayer<<8), 1);
            srv.push_back(cluster_count);

        }
        else{
            cluster_count = readRegister((CLUSTER_COUNT<<16)+((iLayer-4)<<8), 2);
            srv.push_back(cluster_count);
        }
    }
}

/************************************************************/
void DataFlowReaderAux::getAuxModuleCount(vector<int64_t>& srv)  // Ruo
    /************************************************************/
{
    u_int module_count = 0;
    u_int nLayers = 8;
    for( u_int iLayer=0; iLayer < nLayers; iLayer++){
        if(iLayer < 4){
            module_count = readRegister((MODULE_COUNT<<16)+(iLayer<<8), 1);
            srv.push_back(module_count);

        }
        else{
            module_count = readRegister((MODULE_COUNT<<16)+((iLayer-4)<<8), 2);
            srv.push_back(module_count);
        }
    }
}

/************************************************************/
void DataFlowReaderAux::getAuxSSIDCount(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
    u_int ssid_count = 0;
    u_int nLayers = 11;
    for( u_int iLayer=0; iLayer < nLayers; iLayer++){
        if(iLayer < 7){
            ssid_count = readRegister((SSID_COUNT<<16)+(iLayer<<8), 1);
            srv.push_back(ssid_count);

        }
        else{
            ssid_count = readRegister((SSID_COUNT<<16)+((iLayer-7)<<8), 2);
            srv.push_back(ssid_count);
        }
    }
}


/************************************************************/
void DataFlowReaderAux::getAuxMaxProcessingTime(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
    srv.push_back( readRegister(MAXTIME_SSMAP, 1));
    srv.push_back( readRegister(MAXTIME_SSMAP, 2));
    srv.push_back( readRegister(MAXTIME_PROC,  3));
    srv.push_back( readRegister(MAXTIME_PROC,  4));
    srv.push_back( readRegister(MAXTIME_PROC,  5));
    srv.push_back( readRegister(MAXTIME_PROC,  6));
    srv.push_back( readRegister(MAXTIME_HW,    2));
    srv.push_back( readRegister(MAXTIME_PU,    2));
}

/************************************************************/
void DataFlowReaderAux::getAuxMinProcessingTime(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
    srv.push_back( readRegister(MINTIME_SSMAP, 1));
    srv.push_back( readRegister(MINTIME_SSMAP, 2));
    srv.push_back( readRegister(MINTIME_PROC,  3));
    srv.push_back( readRegister(MINTIME_PROC,  4));
    srv.push_back( readRegister(MINTIME_PROC,  5));
    srv.push_back( readRegister(MINTIME_PROC,  6));
    srv.push_back( readRegister(MINTIME_HW,    2));
    srv.push_back( readRegister(MINTIME_PU,    2));

}

/************************************************************/
void DataFlowReaderAux::getAuxErrorCountInput1(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
    srv.push_back(-1);
}

/************************************************************/
void DataFlowReaderAux::getAuxErrorCountInput2(vector<int64_t>& srv)  // Ruo
/************************************************************/
{
    srv.push_back(-1);
}

/************************************************************/
void DataFlowReaderAux::getAuxErrorCountProcessor(vector<int64_t>& srv,  unsigned int fpgaidx)  // Ruo
/************************************************************/
{
    srv.push_back(-1);
}


/************************************************************/
void DataFlowReaderAux::getAuxInLinkInfo(std::vector<int64_t>& srv, uint sb_entry, bool isRate)
/************************************************************/
{

    // I1 QSFP: Input from DF
    srv.push_back(readRegister(I1_QSFP	+ CHANNEL0 +	sb_entry, 1)/( isRate ? (float) m_periods[0]: 1 ));
    srv.push_back(readRegister(I1_QSFP	+ CHANNEL1 + 	sb_entry, 1)/( isRate ? (float) m_periods[0] : 1 ));
    srv.push_back(readRegister(I1_QSFP	+ CHANNEL2 +	sb_entry, 1)/( isRate ? (float) m_periods[0] : 1 ));
    srv.push_back(readRegister(I1_QSFP	+ CHANNEL3 + 	sb_entry, 1)/( isRate ? (float) m_periods[0] : 1 ));

    // I2 QSFP: Input from DF
    srv.push_back(readRegister(I2_QSFP	+ CHANNEL0 +	sb_entry, 2)/(  isRate ? (float) m_periods[1] : 1 ));
    srv.push_back(readRegister(I2_QSFP	+ CHANNEL1 + 	sb_entry, 2)/(  isRate ? (float) m_periods[1] : 1 ));
    srv.push_back(readRegister(I2_QSFP	+ CHANNEL2 +	sb_entry, 2)/(  isRate ? (float) m_periods[1] : 1 ));
    srv.push_back(readRegister(I2_QSFP	+ CHANNEL3 + 	sb_entry, 2)/(  isRate ? (float) m_periods[1] : 1 ));

    for (int fpga_i = 3; fpga_i <= 6; fpga_i++) {
        srv.push_back(readRegister(P_ROADS	+ CHANNEL0 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
        srv.push_back(readRegister(P_ROADS	+ CHANNEL1 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
        srv.push_back(readRegister(P_ROADS	+ CHANNEL2 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
        srv.push_back(readRegister(P_ROADS	+ CHANNEL3 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
    }


}

/************************************************************/
void DataFlowReaderAux::getAuxOutLinkInfo(std::vector<int64_t>& srv, uint sb_entry, bool isRate)
/************************************************************/
{

  // I1 SSDATA: Output to AMB
  srv.push_back(readRegister(I1_SSDATA	+ CHANNEL0 +	sb_entry, 1)/(  isRate ? (float) m_periods[0] : 1 ));
  srv.push_back(readRegister(I1_SSDATA	+ CHANNEL1 + 	sb_entry, 1)/(  isRate ? (float) m_periods[0] : 1 ));
  srv.push_back(readRegister(I1_SSDATA	+ CHANNEL2 +	sb_entry, 1)/(  isRate ? (float) m_periods[0] : 1 ));
  srv.push_back(readRegister(I1_SSDATA	+ CHANNEL3 + 	sb_entry, 1)/(  isRate ? (float) m_periods[0] : 1 ));
  srv.push_back(readRegister(I1_SSDATA	+ CHANNEL4 +	sb_entry, 1)/(  isRate ? (float) m_periods[0] : 1 ));
  srv.push_back(readRegister(I1_SSDATA	+ CHANNEL5 + 	sb_entry, 1)/(  isRate ? (float) m_periods[0] : 1 ));
  srv.push_back(readRegister(I1_SSDATA	+ CHANNEL6 +	sb_entry, 1)/(  isRate ? (float) m_periods[0] : 1 ));
  srv.push_back(readRegister(I1_SSDATA	+ CHANNEL7 + 	sb_entry, 1)/(  isRate ? (float) m_periods[0] : 1 ));

  // I2 SSDATA: Output to AMB
  srv.push_back(readRegister(I2_SSDATA	+ CHANNEL0 +	sb_entry, 2)/(  isRate ? (float) m_periods[1] : 1 ));
  srv.push_back(readRegister(I2_SSDATA	+ CHANNEL1 + 	sb_entry, 2)/(  isRate ? (float) m_periods[1] : 1 ));
  srv.push_back(readRegister(I2_SSDATA	+ CHANNEL2 +	sb_entry, 2)/(  isRate ? (float) m_periods[1] : 1 ));
  srv.push_back(readRegister(I2_SSDATA	+ CHANNEL3 + 	sb_entry, 2)/(  isRate ? (float) m_periods[1] : 1 ));

  // I2 SFP: Output to SSB
  srv.push_back(readRegister(I2_SFP 	+ CHANNEL0 + 	sb_entry, 2)/(  isRate ? (float) m_periods[1] : 1 ));

}

// For BusyFraction, need to divide register by (ClockFreq[MHz] * m_period)
/************************************************************/
void DataFlowReaderAux::getAuxInternalLinkInfo(std::vector<int64_t>& srv, uint sb_entry, bool isRate)
/************************************************************/
{
    //Internal busy fractions
  // I1 to proc
  srv.push_back(readRegister(I1_HITS	+ CHANNEL0 +	sb_entry, 1)/(  isRate ? (float) m_periods[0] : 1 ));
  srv.push_back(readRegister(I1_HITS	+ CHANNEL1 + 	sb_entry, 1)/(  isRate ? (float) m_periods[0] : 1 ));
  srv.push_back(readRegister(I1_HITS	+ CHANNEL2 +	sb_entry, 1)/(  isRate ? (float) m_periods[0] : 1 ));
  srv.push_back(readRegister(I1_HITS	+ CHANNEL3 + 	sb_entry, 1)/(  isRate ? (float) m_periods[0] : 1 ));
  srv.push_back(readRegister(I1_HITS	+ CHANNEL4 +	sb_entry, 1)/(  isRate ? (float) m_periods[0] : 1 ));
  srv.push_back(readRegister(I1_HITS	+ CHANNEL5 + 	sb_entry, 1)/(  isRate ? (float) m_periods[0] : 1 ));
  srv.push_back(readRegister(I1_HITS	+ CHANNEL6 +	sb_entry, 1)/(  isRate ? (float) m_periods[0] : 1 ));

  // I2 to proc
  srv.push_back(readRegister(I2_HITS	+ CHANNEL0 +	sb_entry, 1)/(  isRate ? (float) m_periods[1] : 1 ));
  srv.push_back(readRegister(I2_HITS	+ CHANNEL1 + 	sb_entry, 1)/(  isRate ? (float) m_periods[1] : 1 ));
  srv.push_back(readRegister(I2_HITS	+ CHANNEL2 +	sb_entry, 1)/(  isRate ? (float) m_periods[1] : 1 ));
  srv.push_back(readRegister(I2_HITS	+ CHANNEL3 + 	sb_entry, 1)/(  isRate ? (float) m_periods[1] : 1 ));
 
  //I2 from proc
  srv.push_back(readRegister(I2_GENDATA	+ CHANNEL0 +	sb_entry, 1)/(  isRate ? (float) m_periods[1] : 1 ));
  srv.push_back(readRegister(I2_GENDATA	+ CHANNEL1 + 	sb_entry, 1)/(  isRate ? (float) m_periods[1] : 1 ));
  srv.push_back(readRegister(I2_GENDATA	+ CHANNEL2 +	sb_entry, 1)/(  isRate ? (float) m_periods[1] : 1 ));
  srv.push_back(readRegister(I2_GENDATA	+ CHANNEL3 + 	sb_entry, 1)/(  isRate ? (float) m_periods[1] : 1 ));

  //Proc from Inputs 
  
  for (int fpga_i = 3; fpga_i <= 6; fpga_i++) {
      srv.push_back(readRegister(P_HITS	+ CHANNEL0 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
      srv.push_back(readRegister(P_HITS	+ CHANNEL1 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
      srv.push_back(readRegister(P_HITS	+ CHANNEL2 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
      srv.push_back(readRegister(P_HITS	+ CHANNEL3 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
      srv.push_back(readRegister(P_HITS	+ CHANNEL4 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
      srv.push_back(readRegister(P_HITS	+ CHANNEL5 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
      srv.push_back(readRegister(P_HITS	+ CHANNEL6 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
      srv.push_back(readRegister(P_HITS	+ CHANNEL7 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
      srv.push_back(readRegister(P_HITS	+ CHANNEL8 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
      srv.push_back(readRegister(P_HITS	+ CHANNEL9 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
      srv.push_back(readRegister(P_HITS	+ CHANNEL10 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
     
      srv.push_back(readRegister(P_TRACKS+ CHANNEL0 + 	sb_entry, fpga_i)/(  isRate ? (float) m_periods[fpga_i-1] : 1 ));
  }


}

//SSMap
/************************************************************/
void DataFlowReaderAux::getFifoInOverflowFlag(std::vector<int64_t>& srv)
/************************************************************/
{
    //Input 1
    for (int pix_ch = 0 ; pix_ch < 3 ; pix_ch++  ){
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_INPUT           + (pix_ch<<8),     1 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_HS_INPUT        + (pix_ch<<8),     1 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_PIX_INTERLEAVER + (pix_ch<<8),     1 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_PIX_INTERLEAVER + ((pix_ch+1)<<8), 1 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_PM_FRAME        + (pix_ch<<8),     1 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_PM_FRAME        + ((pix_ch+1)<<8), 1 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_PM_DATA         + (pix_ch<<8),     1 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_PM_DATA         + ((pix_ch+1)<<8), 1 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_AM_L1ID         + (pix_ch<<8),     1 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_AM_L1ID         + ((pix_ch+1)<<8), 1 )   & 0x2000 );
        for (int hs_layer = 0; hs_layer < 4; hs_layer++) {
            for (int hs_fifo = 0; hs_fifo < 4; hs_fifo++) {
                srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_HS_LAYERS         + ( (16*pix_ch+4 + 4*hs_layer+hs_fifo)<<8), 1 ) & 0x2000 );
            }
        }
    }
    
    for (int sct_ch = 3; sct_ch < 4 ; sct_ch++  ){
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_INPUT           + (sct_ch<<8), 1 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_SCT_SERIALIZER  + (sct_ch<<8), 1 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_HS_INPUT        + (sct_ch<<8), 1 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_AM_L1ID         + (sct_ch<<8), 1 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_PM_FRAME        + (sct_ch<<8), 1 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_PM_DATA         + (sct_ch<<8), 1 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_AM_L1ID         + (sct_ch<<8), 1 )   & 0x2000 );
        for (int hs_layer = 0; hs_layer < 4; hs_layer++) {
            for (int hs_fifo = 0; hs_fifo < 4; hs_fifo++) {
                srv.push_back(   readRegister( FIFO_SSMAP+I1_SSMAP_HS_LAYERS         + ( (16*sct_ch+4 + 4*hs_layer+hs_fifo)<<8), 1) & 0x2000 );
            }
        }
    }

    //Input2
    for (int sct_ch = 0; sct_ch < 4 ; sct_ch++  ){
        srv.push_back(   readRegister( FIFO_SSMAP+I2_SSMAP_INPUT           + (sct_ch<<8), 2 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I2_SSMAP_SCT_SERIALIZER  + (sct_ch<<8), 2 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I2_SSMAP_HS_INPUT        + (sct_ch<<8), 2 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I2_SSMAP_AM_L1ID         + (sct_ch<<8), 2 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I2_SSMAP_PM_FRAME        + (sct_ch<<8), 2 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I2_SSMAP_PM_DATA         + (sct_ch<<8), 2 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_SSMAP+I2_SSMAP_AM_L1ID         + (sct_ch<<8), 2 )   & 0x2000 );
        for (int hs_layer = 0; hs_layer < 4; hs_layer++) {
            for (int hs_fifo = 0; hs_fifo < 4; hs_fifo++) {
                srv.push_back(   readRegister( FIFO_SSMAP+I2_SSMAP_HS_LAYERS         + ( (16*sct_ch+4 + 4*hs_layer+hs_fifo)<<8), 2 ) & 0x2000 );
            }
        }
    }

}

//HW and FTK2ROS
/************************************************************/
void DataFlowReaderAux::getFifoOutOverflowFlag(std::vector<int64_t>& srv)
/************************************************************/
{
    //FTK2ROS
    srv.push_back(   readRegister( FIFO_FTK2ROS, 2 )     & 0x2000 );

    //Hitwarrior
    for ( int ch = 0; ch < 4 ; ch++ ){
        srv.push_back(   readRegister( FIFO_HW_INPUT         + (ch<<8), 2 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_HW_COMPARATOR    + (ch<<8), 2 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_HW_DATA_BUFFER   + (ch<<8), 2 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_HW_GATED_OUTPUT  + (ch<<8), 2 )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_HW_PM_DATA       + (ch<<8), 2 )   & 0x2000 );
    }

    srv.push_back(   readRegister( FIFO_HW_PM_FRAME, 2 )   & 0x2000 );

}

//Processors
/************************************************************/
void DataFlowReaderAux::getInternalOverflowFlag(std::vector<int64_t>& srv)
/************************************************************/
{
    for( int proc = 3; proc < 7; proc++){
        srv.push_back(   readRegister( FIFO_DFSYNC_HEADER,   proc )   & 0x2000 );
        srv.push_back(   readRegister( FIFO_DFSYNC_TRAILER,  proc )   & 0x2000 );
        for( int ch = 0; ch < 6; ch++){
            srv.push_back(   readRegister( FIFO_DFSYNC_PIX_HIT + (ch << 8),  proc )   & 0x2000 );
            srv.push_back(   readRegister( FIFO_DFSYNC_PIX_SS  + (ch << 8),  proc )   & 0x2000 );
    }
        for( int ch = 0; ch < 5; ch++){
            srv.push_back(   readRegister( FIFO_DFSYNC_SCT     + (ch << 8),  proc )   & 0x2000 );
        }
        for( int ch = 0; ch < 4; ch++){
            srv.push_back(   readRegister( FIFO_DFSYNC_ROAD    + (ch << 8),  proc )   & 0x2000 );
        }
    }
}

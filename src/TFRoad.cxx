#include "aux/TF/TFRoad.h"

#include "aux/AuxUtils.h"

namespace daq {
  namespace ftk {
    TFRoad::TFRoad()
      : id(-1), sectorid(0), layermap(0), guessmap(0)
    {}

    TFRoad::~TFRoad()
    {}

    void TFRoad::addHit(unsigned int ilayer, Int_tf v)
    { Hits[ilayer].push_back(v); }

    void TFRoad::setID(int v)
    { id = v; }

    void TFRoad::setSectorID(unsigned int v)
    { sectorid = v; }

    void TFRoad::setLayermap(unsigned int v)
    { layermap = v; }

    void TFRoad::setGuessmap(unsigned int v)
    { guessmap = v; }
    
    void TFRoad::addCombination(TFCombination *comb)
    { vec_combinations.push_back( comb ); }

    int TFRoad::getID() const
    { return id; }

    unsigned int TFRoad::getSectorID() const
    { return sectorid; }

    unsigned int TFRoad::getLayermap() const
    { return layermap; }

    unsigned int TFRoad::getGuessmap() const
    { return guessmap; }
    
    unsigned int TFRoad::getNCombinations() const
    { return vec_combinations.size(); }

    TFCombination* TFRoad::getCombination(unsigned int i)
    { return vec_combinations[i]; }

    bool TFRoad::generateCombinations(bool debug)
    {
      if(debug) std::cout << "initialize hit_ptr, max_ptr\n";
      std::size_t hit_ptr[tf::NLayers];
      std::size_t max_ptr[tf::NLayers];
      for(unsigned int ilayer = 0; ilayer < tf::NLayers; ilayer++ )
	{
	  hit_ptr[ilayer] = 0;
	  max_ptr[ilayer] = Hits[ilayer].size();
	}
    
      int icomb = 0;
      bool last_combination_done = false;
      if(debug) std::cout << "Process combinations\n";
      while(!last_combination_done)
	{
	  // Initialize combination
	  TFCombination *tmp_comb = new TFCombination();
	  if(debug) std::cout << "Initilize Combination: id " << id << " sectorid " << sectorid << " layermap " << layermap << " guessmap " << guessmap << std::endl;
	  tmp_comb->setID      ( ++icomb  );
	  tmp_comb->setRoadID  ( id       );
	  tmp_comb->setSectorID( sectorid );
	  tmp_comb->setLayermap( layermap );
	  tmp_comb->setGuessmap( guessmap );

	  // Fill the hits
	  if(debug) std::cout << "Fill the hits\n";
	  bool found_hit = false;
	  for(unsigned int ilayer = 0; ilayer < tf::NLayers; ilayer++)
	    {
	      // Skip layers with no hits unless they are wildcards
	      // If wildcard then insert a dummy hit equal to 0
	      if(max_ptr[ilayer]==0)
		{
		  if( aux::getBit(guessmap,ilayer)==0 )
		    {
		      tmp_comb->setHit( ilayer , 0 );
		    }
		  continue;
		}
	      found_hit = true;
	      tmp_comb->setHit(ilayer, Hits[ilayer][hit_ptr[ilayer]]);
	    }
	  if(!found_hit) {
	    std::cout << "In TFRoad::generateCombinations. Did not find hit associated with road. ERROR\n";
	    return true;//found error
	  }
	  tmp_comb->extractCoordinates();
	  vec_combinations.push_back(tmp_comb);

	  // determine the last layer that has hits
	  unsigned int last_layer;
	  for(last_layer = tf::NLayers-1; max_ptr[last_layer]==0; last_layer--);

	  // Now iterate the hit_ptrs to the next combination
	  if(debug) std::cout << "iterate hit_ptrs\n";
	  for(unsigned int ilayer = 0 ; ilayer <= last_layer ; ilayer++)
	    {
	      // Skip missing layers
	      if( max_ptr[ilayer]==0 ) continue;

	      // Iterate hit_ptr
	      // If hit_ptr is above max, then set it to zero and move on to
	      // iterate hit_ptr in the next layer. Otherwise break.
	      hit_ptr[ilayer]++;
	      if(hit_ptr[ilayer]==max_ptr[ilayer])
		{
		  hit_ptr[ilayer] = 0;
		  if(ilayer == last_layer) last_combination_done = true;
		}
	      else
		break;
	    }
	}
      return false;//no error
    }

    void TFRoad::print()
    {
      //
      // for debugging
      //
      report::blank( "" );

      report::info( "Printing road %i..." , id );
      report::blank( "SectorID: %i" , sectorid );
      report::blank( "Layermap: %s" , tf::getLayermapStr(layermap).c_str() );
      report::blank( "Guessmap: %s" , tf::getLayermapStr(guessmap).c_str() );
      report::blank( "Hits:" );
      for( std::size_t i = 0 ; i < tf::getMaxPerLayer(Hits) ; i++ )
	{
	  std::string line = "";
	  for(unsigned int ilayer = 0; ilayer<tf::NLayers; ilayer++)
	    {
	      std::string hitstr = i < Hits[ilayer].size() ? tf::getHitStr(Hits[ilayer][i]) : "";
	      line = auxstring::Format( "%s %14s" , line.c_str() , hitstr.c_str() );
	    }
	  report::blank( line );
	}
      report::blank( "Combinations: %i", vec_combinations.size() );    
    }

    void TFRoad::printCombinations()
    {
      for( std::size_t i = 0 ; i < vec_combinations.size() ; i++ ) vec_combinations[i]->print();
    }
  } // namespace ftk
} // namespace daq

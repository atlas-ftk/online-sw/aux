#include "aux/aux_registers.h"

namespace daq {
namespace ftk{

aux_registers::aux_registers() { 
}

aux_registers::~aux_registers() { 
}

// Takes a string of the quantity to compute all the link status register addresses for
// Returns a map of maps. First key gives the fpga, second key gives the link, final key gives the address
// String options for LinkInfoString: check m_LinkInfoKey in the header file
// Fills the I1, I2, and Proc entries of the map LinkRegs
void aux_registers::getLinkRegs(std::string LinkInfoString, std::map<std::string,std::map<std::string,uint> > &LinkRegs) {
    // Look up the address that corresponds to the requested information
    uint LinkInfoAddr = m_LinkInfoKey.at(LinkInfoString);
    
    /////////////
    // Input 1 //
    /////////////
    // QSFP links from DF
    LinkRegs.at("I1").insert(std::make_pair("QSFP0_"+LinkInfoString,   (0x30<<16) + (0x0<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("QSFP1_"+LinkInfoString,   (0x30<<16) + (0x1<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("QSFP2_"+LinkInfoString,   (0x30<<16) + (0x2<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("QSFP3_"+LinkInfoString,   (0x30<<16) + (0x3<<8) + LinkInfoAddr));
 
    // Hit links to the processors
    LinkRegs.at("I1").insert(std::make_pair("Hit0_"+LinkInfoString,    (0x31<<16) + (0x0<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("Hit1_"+LinkInfoString,    (0x31<<16) + (0x1<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("Hit2_"+LinkInfoString,    (0x31<<16) + (0x2<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("Hit3_"+LinkInfoString,    (0x31<<16) + (0x3<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("Hit4_"+LinkInfoString,    (0x31<<16) + (0x4<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("Hit5_"+LinkInfoString,    (0x31<<16) + (0x5<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("Hit6_"+LinkInfoString,    (0x31<<16) + (0x6<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("Hit7_"+LinkInfoString,    (0x31<<16) + (0x7<<8) + LinkInfoAddr));
 
    // SSData links to AMB
    LinkRegs.at("I1").insert(std::make_pair("SSData0_"+LinkInfoString, (0x32<<16) + (0x0<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("SSData1_"+LinkInfoString, (0x32<<16) + (0x1<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("SSData2_"+LinkInfoString, (0x32<<16) + (0x2<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("SSData3_"+LinkInfoString, (0x32<<16) + (0x3<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("SSData4_"+LinkInfoString, (0x32<<16) + (0x4<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("SSData5_"+LinkInfoString, (0x32<<16) + (0x5<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("SSData6_"+LinkInfoString, (0x32<<16) + (0x6<<8) + LinkInfoAddr));
    LinkRegs.at("I1").insert(std::make_pair("SSData7_"+LinkInfoString, (0x32<<16) + (0x7<<8) + LinkInfoAddr));
 
    /////////////
    // Input 2 //
    /////////////
    // QSFP links from DF
    LinkRegs.at("I2").insert(std::make_pair("QSFP0_"+LinkInfoString,   (0x30<<16) + (0x0<<8) + LinkInfoAddr));
    LinkRegs.at("I2").insert(std::make_pair("QSFP1_"+LinkInfoString,   (0x30<<16) + (0x1<<8) + LinkInfoAddr));
    LinkRegs.at("I2").insert(std::make_pair("QSFP2_"+LinkInfoString,   (0x30<<16) + (0x2<<8) + LinkInfoAddr));
    LinkRegs.at("I2").insert(std::make_pair("QSFP3_"+LinkInfoString,   (0x30<<16) + (0x3<<8) + LinkInfoAddr));
 
    // Hit links to the processors
    LinkRegs.at("I2").insert(std::make_pair("Hit0_"+LinkInfoString,    (0x31<<16) + (0x0<<8) + LinkInfoAddr));
    LinkRegs.at("I2").insert(std::make_pair("Hit1_"+LinkInfoString,    (0x31<<16) + (0x1<<8) + LinkInfoAddr));
    LinkRegs.at("I2").insert(std::make_pair("Hit2_"+LinkInfoString,    (0x31<<16) + (0x2<<8) + LinkInfoAddr));
    LinkRegs.at("I2").insert(std::make_pair("Hit3_"+LinkInfoString,    (0x31<<16) + (0x3<<8) + LinkInfoAddr));

    // SSData links to AMB
    LinkRegs.at("I2").insert(std::make_pair("SSData0_"+LinkInfoString, (0x32<<16) + (0x0<<8) + LinkInfoAddr));
    LinkRegs.at("I2").insert(std::make_pair("SSData1_"+LinkInfoString, (0x32<<16) + (0x1<<8) + LinkInfoAddr));
    LinkRegs.at("I2").insert(std::make_pair("SSData2_"+LinkInfoString, (0x32<<16) + (0x2<<8) + LinkInfoAddr));
    LinkRegs.at("I2").insert(std::make_pair("SSData3_"+LinkInfoString, (0x32<<16) + (0x3<<8) + LinkInfoAddr));

    // Gen (track) links from the processors
    LinkRegs.at("I2").insert(std::make_pair("Track0_"+LinkInfoString,  (0x34<<16) + (0x0<<8) + LinkInfoAddr));
    LinkRegs.at("I2").insert(std::make_pair("Track1_"+LinkInfoString,  (0x34<<16) + (0x1<<8) + LinkInfoAddr));
    LinkRegs.at("I2").insert(std::make_pair("Track2_"+LinkInfoString,  (0x34<<16) + (0x2<<8) + LinkInfoAddr));
    LinkRegs.at("I2").insert(std::make_pair("Track3_"+LinkInfoString,  (0x34<<16) + (0x3<<8) + LinkInfoAddr));

    // SFP link to SSB
    LinkRegs.at("I2").insert(std::make_pair("SFP_"+LinkInfoString,     (0x35<<16) + (0x0<<0) + LinkInfoAddr));

    ///////////////
    // Processor //
    ///////////////
    // Input hit links
    LinkRegs.at("Proc").insert(std::make_pair("Hit0_"+LinkInfoString,  (0x56<<16) + (0x0<<8) + LinkInfoAddr));
    LinkRegs.at("Proc").insert(std::make_pair("Hit1_"+LinkInfoString,  (0x56<<16) + (0x1<<8) + LinkInfoAddr));
    LinkRegs.at("Proc").insert(std::make_pair("Hit2_"+LinkInfoString,  (0x56<<16) + (0x2<<8) + LinkInfoAddr));
    LinkRegs.at("Proc").insert(std::make_pair("Hit3_"+LinkInfoString,  (0x56<<16) + (0x3<<8) + LinkInfoAddr));
    LinkRegs.at("Proc").insert(std::make_pair("Hit4_"+LinkInfoString,  (0x56<<16) + (0x4<<8) + LinkInfoAddr));
    LinkRegs.at("Proc").insert(std::make_pair("Hit5_"+LinkInfoString,  (0x56<<16) + (0x5<<8) + LinkInfoAddr));
    LinkRegs.at("Proc").insert(std::make_pair("Hit6_"+LinkInfoString,  (0x56<<16) + (0x6<<8) + LinkInfoAddr));
    LinkRegs.at("Proc").insert(std::make_pair("Hit7_"+LinkInfoString,  (0x56<<16) + (0x7<<8) + LinkInfoAddr));
    LinkRegs.at("Proc").insert(std::make_pair("Hit8_"+LinkInfoString,  (0x56<<16) + (0x8<<8) + LinkInfoAddr));
    LinkRegs.at("Proc").insert(std::make_pair("Hit9_"+LinkInfoString,  (0x56<<16) + (0x9<<8) + LinkInfoAddr));
    LinkRegs.at("Proc").insert(std::make_pair("Hit10_"+LinkInfoString, (0x56<<16) + (0xA<<8) + LinkInfoAddr));
 
    // Road links from AMB
    LinkRegs.at("Proc").insert(std::make_pair("Road0_"+LinkInfoString, (0x59<<16) + (0x0<<8) + LinkInfoAddr));
    LinkRegs.at("Proc").insert(std::make_pair("Road1_"+LinkInfoString, (0x59<<16) + (0x1<<8) + LinkInfoAddr));
    LinkRegs.at("Proc").insert(std::make_pair("Road2_"+LinkInfoString, (0x59<<16) + (0x2<<8) + LinkInfoAddr));
    LinkRegs.at("Proc").insert(std::make_pair("Road3_"+LinkInfoString, (0x59<<16) + (0x3<<8) + LinkInfoAddr));

    // Track link to Input 2
    LinkRegs.at("Proc").insert(std::make_pair("Track_"+LinkInfoString, (0x57<<16) + (0x0<<8) + LinkInfoAddr));
}

// Need to split up output into map of maps so each fpga is separated. Makes easier to loop in StatusRegisterAuxFactory
std::map<std::string, std::map<std::string,uint> > aux_registers::getISRegs() {

    std::map<std::string, std::map<std::string,uint> > ISRegs; // The map to return

    // Fill ISRegs with empty maps for each fpga
    std::map<std::string,uint> I1Regs;
    std::map<std::string,uint> I2Regs;
    std::map<std::string,uint> ProcRegs;
    ISRegs.insert(std::make_pair("I1", I1Regs));
    ISRegs.insert(std::make_pair("I2", I2Regs));
    ISRegs.insert(std::make_pair("Proc", ProcRegs));

    // All linkstatus register addresses
    getLinkRegs("NumPackets",    ISRegs);
    getLinkRegs("EventRate",     ISRegs);
    getLinkRegs("L1ID",          ISRegs);
    getLinkRegs("Errors",        ISRegs);
    getLinkRegs("FifoNumHolds",  ISRegs);
    getLinkRegs("FifoUsageAv",   ISRegs);
    getLinkRegs("FifoUsage",     ISRegs);

    // Other IS monitoring registers.
    ISRegs.at("I1").insert(  std::make_pair("Temperature", TEMPERATURE)); // TEMPERATURE defined in aux_vme_regs.h
    ISRegs.at("I2").insert(  std::make_pair("Temperature", TEMPERATURE));
    ISRegs.at("Proc").insert(std::make_pair("Temperature", TEMPERATURE));

    ISRegs.at("I2").insert(std::make_pair("OutputTracks", OUTPUT_TRACKS)); // OUTPUT_TRACKS defined in aux_vme_regs.h

    // TF fit count in each fitter
    ISRegs.at("Proc").insert(std::make_pair("FitCountNom",    (0x4d<<16)+(0x0<<8)));
    ISRegs.at("Proc").insert(std::make_pair("FitCountPix0",   (0x4d<<16)+(0x1<<8)));
    ISRegs.at("Proc").insert(std::make_pair("FitCountPix1",   (0x4d<<16)+(0x2<<8)));
    ISRegs.at("Proc").insert(std::make_pair("FitCountPix2",   (0x4d<<16)+(0x3<<8)));
    ISRegs.at("Proc").insert(std::make_pair("FitCountPix3",   (0x4d<<16)+(0x4<<8)));
    ISRegs.at("Proc").insert(std::make_pair("FitCountSct0",   (0x4d<<16)+(0x5<<8)));
    ISRegs.at("Proc").insert(std::make_pair("FitCountSct1",   (0x4d<<16)+(0x6<<8)));

    // TF fit rate in each fitter per sample period
    ISRegs.at("Proc").insert(std::make_pair("FitRateNom",     (0x4e<<16)+(0x0<<8)));
    ISRegs.at("Proc").insert(std::make_pair("FitRatePix0",    (0x4e<<16)+(0x1<<8)));
    ISRegs.at("Proc").insert(std::make_pair("FitRatePix1",    (0x4e<<16)+(0x2<<8)));
    ISRegs.at("Proc").insert(std::make_pair("FitRatePix2",    (0x4e<<16)+(0x3<<8)));
    ISRegs.at("Proc").insert(std::make_pair("FitRatePix3",    (0x4e<<16)+(0x4<<8)));
    ISRegs.at("Proc").insert(std::make_pair("FitRateSct0",    (0x4e<<16)+(0x5<<8)));
    ISRegs.at("Proc").insert(std::make_pair("FitRateSct1",    (0x4e<<16)+(0x6<<8)));

    // TF track count
    ISRegs.at("Proc").insert(std::make_pair("TrackCountNom",  (0x4f<<16)+(0x0<<8)));
    ISRegs.at("Proc").insert(std::make_pair("TrackCountPix0", (0x4f<<16)+(0x1<<8)));
    ISRegs.at("Proc").insert(std::make_pair("TrackCountPix1", (0x4f<<16)+(0x2<<8)));
    ISRegs.at("Proc").insert(std::make_pair("TrackCountPix2", (0x4f<<16)+(0x3<<8)));
    ISRegs.at("Proc").insert(std::make_pair("TrackCountPix3", (0x4f<<16)+(0x4<<8)));
    ISRegs.at("Proc").insert(std::make_pair("TrackCountSct0", (0x4f<<16)+(0x5<<8)));
    ISRegs.at("Proc").insert(std::make_pair("TrackCountSct1", (0x4f<<16)+(0x6<<8)));

    // TF track rate
    ISRegs.at("Proc").insert(std::make_pair("TrackRateNom",   (0x50<<16)+(0x0<<8)));
    ISRegs.at("Proc").insert(std::make_pair("TrackRatePix0",  (0x50<<16)+(0x1<<8)));
    ISRegs.at("Proc").insert(std::make_pair("TrackRatePix1",  (0x50<<16)+(0x2<<8)));
    ISRegs.at("Proc").insert(std::make_pair("TrackRatePix2",  (0x50<<16)+(0x3<<8)));
    ISRegs.at("Proc").insert(std::make_pair("TrackRatePix3",  (0x50<<16)+(0x4<<8)));
    ISRegs.at("Proc").insert(std::make_pair("TrackRateSct0",  (0x50<<16)+(0x5<<8)));
    ISRegs.at("Proc").insert(std::make_pair("TrackRateSct1",  (0x50<<16)+(0x6<<8)));

    return ISRegs;
}

}
}


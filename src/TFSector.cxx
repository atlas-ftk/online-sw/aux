#include "aux/TF/TFSector.h"

#include "aux/AuxString.h"
#include "aux/Report.h"
#include "aux/TF/TFGen.h"

TFSector::TFSector() 
  : id( -1 )
{
  // zero out all of the constants by default
  
  for(unsigned int i = 0 ; i < tf::NChiComponents ; i++ )
    {
      HVector[i] = 0;
      for(unsigned int j = 0 ; j < tf::NCoords ; j++ )
	{
	  SMatrix[i][j] = 0;
	}
    }
  
  for(unsigned int i = 0 ; i < tf::NPixLayers ; i++ )
    {
      for(unsigned int j = 0 ; j < 3 ; j++ )
	{
	  PixGuessFrac[i][j] = 0;
	  PixGuessExp[i][j] = 0;
	}
    }
    
  for(unsigned int i = 0 ; i < tf::NSctLayers ; i++ )
    {
      SctGuessFrac[i] = 0;
      SctGuessExp[i] = 0;
    }
}

TFSector::~TFSector()
{}

void TFSector::setID(int _id)
{ id = _id; }

void TFSector::setS( int ichi , int icoord , Int_tf val )
{ SMatrix[ichi][icoord] = val; }

void TFSector::setH( int ichi , Int_tf val )
{ HVector[ichi] = val; }

void TFSector::setPixGuessFrac(unsigned int ilayer , int iconst , Int_tf val)
{ PixGuessFrac[ilayer][iconst] = val; }

void TFSector::setPixGuessExp(unsigned int ilayer , int iconst , Int_tf val)
{ PixGuessExp[ilayer][iconst] = val; }

void TFSector::setSctGuessFrac(unsigned int ilayer , Int_tf val)
{ SctGuessFrac[ilayer] = val; }

void TFSector::setSctGuessExp(unsigned int ilayer , Int_tf val )
{ SctGuessExp[ilayer] = val; }

int TFSector::getID() const
{ return id; }

Int_tf TFSector::getS( int ichi , int ihit ) const
{ return SMatrix[ichi][ihit]; }

Int_tf TFSector::getH( int ichi ) const
{ return HVector[ichi]; }

Int_tf TFSector::getPixGuessFrac(unsigned int ilayer , int iconst ) const
{ return PixGuessFrac[ilayer][iconst]; }

Int_tf TFSector::getPixGuessExp( int ilayer , int iconst ) const
{ return PixGuessExp[ilayer][iconst]; }
  
Int_tf TFSector::getSctGuessFrac(unsigned int ilayer ) const
{ return SctGuessFrac[ilayer]; }

Int_tf TFSector::getSctGuessExp(unsigned int ilayer ) const { return SctGuessExp[ilayer]; }

void TFSector::print() 
{
  //
  report::blank( "" );
  report::info( "Dumping Sector ID %i..." , id );
  
  report::blank( "SMatrix:" );
  for( int i = 0 ; i < tf::NChiComponents ; i++ )
    {
      std::string line ( "" );
      for( int j = 0 ; j < tf::NCoords ; j++ )
	{
	  line += auxstring::Format( "%10lli " , SMatrix[i][j] );
	}
      report::blank( "%s" , line.c_str() );
    }
  
  report::blank( "HVector:" );
  for( int i = 0 ; i < tf::NChiComponents ; i++ )
    {
      report::blank( "%10lli" , HVector[i] );
    }
  
  report::blank( "PixGuess:" );
  for( int i = 0 ; i < tf::NPixLayers ; i++ )
    {
      std::string line( "" );
      for( int j = 0 ; j < 3 ; j++ )
	{
	  std::string entry = auxstring::Format( "%lli*2^%lli" , PixGuessFrac[i][j] , PixGuessExp[i][j] );
	  line += auxstring::Format( "%21s " , entry.c_str() );
	}
      report::blank( "%s" , line.c_str() );
    }
  
  report::blank( "SctGuess:" );
  for( int i = 0 ; i < tf::NSctLayers ; i++ )
    {
      std::string entry = auxstring::Format( "%lli*2^%lli" , SctGuessFrac[i] , SctGuessExp[i] );
      report::blank( "%21s" , entry.c_str() );
    }
  
  report::blank( "" );
}


#include "aux/TF/TFEventList.h"

#include "aux/AuxUtils.h"

#include "aux/TF/TFRoadBuilder.h"

#include "aux/SpyBuffer/EventFragmentDOOutput.h"
#include "aux/SpyBuffer/EventFragmentTrackFTKPacket.h"

#include <bitset>

using namespace daq::ftk;

TFEventList::TFEventList(const std::string& _name)
  : name( _name )
{}

TFEventList::~TFEventList()
{}

unsigned int TFEventList::getNEvents() const
{ return vec_events.size(); }

std::string TFEventList::getName() const
{ return name; }

TFEvent* TFEventList::getEvent(unsigned int ievent) const
{ return vec_events[ievent]; }


bool TFEventList::compareAllEvents(TFEventList *other, bool split_sectors)
{
  report::printassert(getNEvents()==other->getNEvents(),"TFEventLists being compared must have equal number of events! %d vs %d",getNEvents(),other->getNEvents());
  
  // print heading
  report::debug("Starting track comparison between %s and %s.",getName().c_str(),other->getName().c_str());

  bool isGood=true;
  for(unsigned iev = 0 ; iev < vec_events.size() ; iev++ )
    {

      std::string evname = auxstring::Format( "%s.0x%08x" , name.c_str() , vec_events[iev]->getID() );
      report::debug("Testing event %s",evname.c_str());

      TFEvent *event1=vec_events[iev];
      TFEvent *event2=other->getEvent(iev);
      
      report::warnif(event1->getID()!=event2->getID(),"Events at #%d have L1ID 0x%08x!=0x%08x",iev,event1->getID(),event2->getID());

      unsigned int total_tracks , matched_tracks;

      bool matches2 = event2->compareTracks(event1, matched_tracks, total_tracks, split_sectors);
      report::debug("Emulation %s's event only has %d/%d of %s's tracks.",getName().c_str(),matched_tracks,total_tracks,other->getName().c_str());
      isGood&=matches2;
 
      bool matches1 = event1->compareTracks(event2, matched_tracks, total_tracks, false);
      report::debug("%s event only has %d/%d of emulation %s's tracks.",other->getName().c_str(),matched_tracks,total_tracks,getName().c_str());
      isGood&=matches1;

    }
  report::debug("Match result: %d",isGood);

  return isGood;
}

bool TFEventList::buildFromDOOutputEventCollections(const std::vector<EventFragmentCollection*>& eventCollections, bool debug, int proc)
{
  if(debug) std::cout << "Start buildFromDOOutputEventCollections \n";
  for(auto eventCollection : eventCollections)
    {
      if(debug) std::cout << "New Event collection\n";
      TFEvent *doevent=new TFEvent();
      doevent->setID(eventCollection->getL1ID());

      // Build up the roads
      TFRoadBuilder roadBuilder;

      // Start by getting road info
      if(debug) std::cout << "Get Road Info\n";
      std::vector<unsigned int> road_hitmaskandsectorid=dynamic_cast<EventFragmentDOOutput*>(eventCollection->getEventFragment(11))->getData();
      std::vector<unsigned int> road_roadid=dynamic_cast<EventFragmentDOOutput*>(eventCollection->getEventFragment(12))->getData();

      // Prepare hit streams
      if(debug) std::cout << "Get Hit Streams\n";
      std::vector<unsigned int> hitData[tf::NStreams];
      for(unsigned int istream=0; istream<tf::NStreams; istream++)
	{
	  EventFragmentDOOutput *streamEventFragment=dynamic_cast<EventFragmentDOOutput*>(eventCollection->getEventFragment(istream));
	  hitData[istream]=streamEventFragment->getData();
	}

      // Loop through the roads
      if(debug) std::cout << "Loop over roads\n";
      for(unsigned int roadIdx=0;roadIdx<road_roadid.size();roadIdx++)
	{
	  //	  std::cout << "Rui: sector: " << aux::getBitRange(road_hitmaskandsectorid[roadIdx],0,tf::SectorWidth-1) << " " << aux::getBitRange(road_hitmaskandsectorid[roadIdx],0,tf::SectorWidth-1) %4 << " proc: " << proc << std::endl;
	  roadBuilder.addRoadID  (aux::getBitRange(road_roadid[roadIdx]            ,0,tf::RoadWidth-1  ));
	  roadBuilder.addSectorID(aux::getBitRange(road_hitmaskandsectorid[roadIdx],0,tf::SectorWidth-1)*4+proc-1);

	  unsigned int hitmap=aux::getBitRange(road_hitmaskandsectorid[roadIdx],13,13+tf::NStreams-1);
	  unsigned int layermap=tf::getLayermapFromStreammap(hitmap);
	  unsigned int guessmap=aux::getBitRange(road_hitmaskandsectorid[roadIdx],24,31);
	  roadBuilder.addLayermap(layermap);
	  roadBuilder.addGuessmap(guessmap);

	  //
	  // Add all of the hits
	  //

	  //
	  // pixels
	  for(unsigned int ilayer=0; ilayer<tf::NPixLayers; ilayer++)
	    {
	      std::vector<unsigned int> hitWords;
	      // Merge two separate hit streams
	      unsigned int streamIdx0 = 2*ilayer;
	      unsigned int streamIdx1 = 2*ilayer+1;

	      unsigned int hitWord=0;
	      bool eor=false;	      
	      
	      // Add even hits
	      if(aux::getBit(hitmap,streamIdx0))
		{
		  do
		    {
		      report::printassert(hitData[streamIdx0].size()>0,"Missing EOR in stream %d for event %08x.",streamIdx0,eventCollection->getL1ID());

		      hitWord=hitData[streamIdx0].front();
		      hitData[streamIdx0].erase(hitData[streamIdx0].begin());

		      eor=aux::getBit(hitWord,31);
		      hitWords.push_back(hitWord);
		    }
		  while(!eor);
		}

	      // Add odd hits
	      if(aux::getBit(hitmap,streamIdx1))
		{
		  do
		    {
		      report::printassert(hitData[streamIdx1].size()>0,"Missing EOR in stream %d for event %08x.",streamIdx1,eventCollection->getL1ID());

		      hitWord=hitData[streamIdx1].front();
		      hitData[streamIdx1].erase(hitData[streamIdx1].begin());

		      eor=aux::getBit(hitWord,31);
		      hitWords.push_back(hitWord);
		    }
		  while(!eor);
		}

	      // Add hits to the road
	      eor=hitWords.empty();
	      while(!eor)
		{
		  hitWord=hitWords.front();
		  hitWords.erase(hitWords.begin());

		  eor=hitWords.empty();
		  roadBuilder.addHitWord(ilayer,aux::getBitRange(hitWord,0,tf::PixLayerWidth-1),eor);
		}
	    }

	  //
	  // sct
	  for(unsigned int sctlayer=0; sctlayer<tf::NSctLayers; sctlayer++)
	    {
	      unsigned int ilayer    = tf::NPixLayers+sctlayer;
	      unsigned int streamIdx = 2*tf::NPixLayers+sctlayer;

	      unsigned int hitWord=0;
	      bool eor=false;	      
	      
	      // Add even hits
	      if(aux::getBit(hitmap,streamIdx))
		{
		  do
		    {
		      report::printassert(hitData[streamIdx].size()>0,"Missing EOR in stream %d for event %08x.",streamIdx,eventCollection->getL1ID());

		      hitWord=hitData[streamIdx].front();
		      hitData[streamIdx].erase(hitData[streamIdx].begin());

		      eor=aux::getBit(hitWord,31);
		      roadBuilder.addHitWord(ilayer,aux::getBitRange(hitWord,0,tf::SctLayerWidth-1),eor);
		    }
		  while(!eor);
		}
	  }
	}

      if(debug) std::cout << "generate roads\n";
      std::vector<TFRoad*> roads=roadBuilder.generateRoads();
      bool combinations_error = false;
      for(auto road : roads)
	{
	  combinations_error = road->generateCombinations(false);//debug
	  if (combinations_error) return true;//exit with error. TF fails bit check
	  doevent->addRoad(road);
	}
      if(debug) std::cout << "finished with event\n";
      vec_events.push_back(doevent);
    }
if(debug) std::cout << "exit build from event collections\n";
return false;//no error
}

void TFEventList::buildFromTrackEventCollections(const std::vector<EventFragmentCollection*>& eventCollections)
{
  for(auto eventCollection : eventCollections)
    {
      TFEvent *tfevent=new TFEvent();
      tfevent->setID(eventCollection->getL1ID());

      EventFragmentTrackFTKPacket *trackEventFragment=dynamic_cast<EventFragmentTrackFTKPacket*>(eventCollection->getEventFragment(0));

      report::debug("Load TF output event %x",eventCollection->getL1ID());

      // a road per packet (for now)
      for(unsigned int trkIdx=0;trkIdx<trackEventFragment->getNTracks();trkIdx++)
	{
	  TFCombination *track=trackEventFragment->getTrack(trkIdx);

	  TFRoad *road=0; // Find road matching this configuration
	  for(unsigned int roadIdx=0;roadIdx<tfevent->getNRoads();roadIdx++)
	    {
	      TFRoad *tmp_road=tfevent->getRoad(roadIdx);

	      if(tmp_road->getID()==track->getRoadID() &&
		 tmp_road->getSectorID()==track->getSectorID() &&
		 tmp_road->getLayermap()==track->getLayermap())
		{
		  road=tmp_road;
		  break;
		}
	    }

	  if(road==0)
	    { // No road matching track exists, create it
	      road=new TFRoad();
	      road->setID(track->getRoadID());
	      road->setSectorID(track->getSectorID());
	      road->setLayermap(track->getLayermap());

	      tfevent->addRoad(road);	      
	    }

	  // Add combination
	  road->addCombination(track);
	}

      // Register the event
      vec_events.push_back(tfevent);
    }
}

void TFEventList::buildFromFile(const std::string& path)
{
  if( path.find("DOTVStream") != std::string::npos || path.find("DOOutTestVectors") != std::string::npos )
    {
      report::info( "Loading events in DOParser format" );
      buildFromDOParserFile( path );
    }
  else if( path.find("DOOutput") != std::string::npos )
    {
      report::info( "Loading events in DOOutput format" );
      buildFromDOOutputFile( path );
    }
  else if( path.find("BLSpy") != std::string::npos )
    {
      report::info( "Loading events in BLSpy format" );
      buildFromBLSpyBufferFile( path );
    }
  else assert( false );
}

void TFEventList::buildFromBLSpyBufferFile(const std::string& path)
{
  std::ifstream f( path.c_str() );
  std::string line;

  TFRoadBuilder roadBuilder;
  //
  // For a proper comparison to the TF output we only want to reconstruct full events.
  // Skip all of the input until we find the first EOE flag. Then begin the nominal
  // event loading loop.
  //
  while( f )
    { // readlines until beginning of event is found
      if( ! safe_getline(f,line) ) break;
      report::debug( "searching for first EOE flag, skipping line: %s" , line.c_str() );
    
      std::vector<std::string> a = auxstring::Tokenize( line , ' ' );
    
      Int_tf layermap_eoe_word = aux::readHex( a[17] );
      int eoe = aux::getBit( layermap_eoe_word , 8 );
    
      if( eoe )
	{
	  report::info( "First end-of-event found, begin writing events" );
	  break;
	}
    }
  
  int ievent = 0;
  while(f)
    { // loop over events
      TFEvent *tmp_event = new TFEvent();
      tmp_event->setID( ievent++ );
    
      bool road_sector_layermap_written = false;
      bool layer_written[tf::NLayers];
      for(unsigned int ilayer = 0 ; ilayer < tf::NLayers ; ilayer++ ) layer_written[ilayer] = false;
    
      bool eoe_found = false;
    
      while( f )
	{
	  if( ! safe_getline(f,line) ) break;
      
	  std::vector<std::string> a = auxstring::Tokenize( line , ' ' );
      
	  Int_tf pix_word_0        = aux::readHex( a[ 1] );
	  Int_tf pix_word_1        = aux::readHex( a[ 3] );
	  Int_tf pix_word_2        = aux::readHex( a[ 5] );
	  Int_tf sct_word_01       = aux::readHex( a[ 7] );
	  Int_tf sct_word_23       = aux::readHex( a[ 9] );
	  Int_tf sct_word_4        = aux::readHex( a[11] );
	  Int_tf sector_word       = aux::readHex( a[13] );
	  Int_tf road_word         = aux::readHex( a[15] );
	  Int_tf layermap_eoe_word = aux::readHex( a[17] );
      
	  int eoe = aux::getBit( layermap_eoe_word , 8 );
	  if( eoe == 1 )
	    {
	      // end-of-event flag found
	      eoe_found = true;
	      break;
	    }
      
	  if( ! road_sector_layermap_written )
	    {
	
	      Int_tf sector   = aux::getBitRange( sector_word , 0 , tf::SectorWidth-1 );
	      roadBuilder.addSectorID(sector);

	      Int_tf road     = aux::getBitRange( road_word , 0 , tf::RoadWidth-1 );
	      roadBuilder.addRoadID(road);

	      Int_tf layermap = aux::getBitRange( layermap_eoe_word , 0 , tf::NLayers-1 );
	      roadBuilder.addLayermap(layermap);
	
	      // turn off any layers not included in the layermap
	      for(unsigned int ilayer = 0 ; ilayer < tf::NLayers ; ilayer++ )
		{
		  if( aux::getBit(layermap,ilayer) == 0 ) layer_written[ilayer] = true;
		}

	      road_sector_layermap_written = true;
	    }
      
	  //
	  // Write all hits in road
	  //

	  if( ! layer_written[0] )
	    {
	      Int_tf layer = aux::getBitRange( pix_word_0 , 0 , tf::PixLayerWidth-1 );
	      bool eor = aux::getBit( pix_word_0 , tf::PixLayerWidth );
	      roadBuilder.addHitWord( 0 , layer , eor );
	      if( eor ) layer_written[0] = true;
	    }
      
	  if( ! layer_written[1] )
	    {
	      Int_tf layer = aux::getBitRange( pix_word_1 , 0 , tf::PixLayerWidth-1 );
	      bool eor = aux::getBit( pix_word_1 , tf::PixLayerWidth );
	      roadBuilder.addHitWord( 1 , layer , eor );
	      if( eor ) layer_written[1] = true;
	    }
      
	  if( ! layer_written[2] )
	    {
	      Int_tf layer = aux::getBitRange( pix_word_2 , 0 , tf::PixLayerWidth-1 );
	      bool eor = aux::getBit( pix_word_2 , tf::PixLayerWidth );
	      roadBuilder.addHitWord( 2 , layer , eor );
	      if( eor ) layer_written[2] = true;
	    }

	  if( ! layer_written[3] )
	    {
	      Int_tf layer = aux::getBitRange( sct_word_01 , 0 , 13 );
	      bool eor = aux::getBit( sct_word_01 , 14 );
	      roadBuilder.addHitWord( 3 , layer , eor );
	      if( eor ) layer_written[3] = true;
	    }
      
	  if( ! layer_written[4] )
	    {
	      Int_tf layer = aux::getBitRange( sct_word_01 , 16 , 29 );
	      bool eor = aux::getBit( sct_word_01 , 30 );
	      roadBuilder.addHitWord( 4 , layer , eor );
	      if( eor ) layer_written[4] = true;
	    }
      
	  if( ! layer_written[5] )
	    {
	      Int_tf layer = aux::getBitRange( sct_word_23 , 0 , 13 );
	      bool eor = aux::getBit( sct_word_23 , 14 );
	      roadBuilder.addHitWord( 5 , layer , eor );
	      if( eor ) layer_written[5] = true;
	    }
      
	  if( ! layer_written[6] )
	    {
	      Int_tf layer = aux::getBitRange( sct_word_23 , 16 , 29 );
	      bool eor = aux::getBit( sct_word_23 , 30 );
	      roadBuilder.addHitWord( 6 , layer , eor );
	      if( eor ) layer_written[6] = true;
	    }
      
	  if( ! layer_written[7] )
	    {
	      Int_tf layer = aux::getBitRange( sct_word_4 , 0 , 13 );
	      bool eor = aux::getBit( sct_word_4 , 14 );
	      roadBuilder.addHitWord( 7 , layer , eor );
	      if( eor ) layer_written[7] = true;
	    }


	  // check if the EOR flags are all high. If so, then reset them to low so the next
	  // road will be written.
	  if( layer_written[0] &&
	      layer_written[1] &&
	      layer_written[2] &&
	      layer_written[3] &&
	      layer_written[4] &&
	      layer_written[5] &&
	      layer_written[6] &&
	      layer_written[7] )
	    {
	      road_sector_layermap_written = false;
	      for(unsigned int ilayer = 0 ; ilayer < tf::NLayers ; ilayer++) layer_written[ilayer] = false;
	    }

	}
    
      // only keep the event if the eoe flag was found
      if( eoe_found )
	{
	  std::vector<TFRoad*> roads=roadBuilder.generateRoads();
	  for(auto road : roads)
	    tmp_event->addRoad(road);

	  if( tmp_event->getNRoads() > 0 )
	    vec_events.push_back( tmp_event );
	  else
	    delete tmp_event;
	}

    }
  
  report::info( "Done reading events file" );  
}

void TFEventList::buildFromDOParserFile(const std::string& path)
{
  std::ifstream f( path.c_str() );
  std::string line;

  int ievent = 0;

  TFRoadBuilder roadBuilder;
  
  while( f )
    { // loop over events
    
      TFEvent *tmp_event = new TFEvent();
      tmp_event->setID( ievent++ );
    
      while( f )
	{ // loop over DO output in a single event
      
	  if( ! safe_getline(f,line) ) break;
	  if( line.find("New Event") != std::string::npos || line.find("EventNum") != std::string::npos ) break;
      
	  // Retrieve the valid raw event data
	  std::vector<std::string> a = auxstring::Tokenize(line,'\t');
      
	  if( false )
	    {
	      report::debug( line );
	      for( std::size_t i = 0 ; i < a.size() ; i++ )
		{
		  report::debug( "%2i : %lli" , i , aux::read(a[i]) );
		}
	    }
      
	  //report::debug( "validmask = %s" , a[doparse::VALID].c_str() );
	  int validmask = aux::read( a[doparse::VALID] );
      
	  // check if sector ID is valid
	  if( aux::getBit( validmask , doparse::getValidBit(doparse::SECTORID) ) )
	    roadBuilder.addSectorID( aux::read( a[doparse::SECTORID] ) );
      
	  // check if road ID is valid
	  if( aux::getBit( validmask , doparse::getValidBit(doparse::ROADID) ) )
	    roadBuilder.addRoadID( aux::read( a[doparse::ROADID] ) );

	  // check if streammap is valid, if so convert to layermap
	  if( aux::getBit( validmask , doparse::getValidBit(doparse::STREAMMAP) ) )
	    {
	      int tmp_streammap = aux::readBin( a[doparse::STREAMMAP] );
	      roadBuilder.addLayermap( tf::getLayermapFromStreammap(tmp_streammap) );
	    }
      
	  int eormask = aux::read( a[doparse::EOR] );
	  for(unsigned int istream = 0 ; istream < tf::NStreams ; istream++ )
	    {
	      if( aux::getBit( validmask , doparse::getValidBit(istream+doparse::FIRSTPIXSTREAM) ) )
		{
		  int layer  = tf::getLayerFromStream( istream );
		  Int_tf hit = aux::read( a[istream+doparse::FIRSTPIXSTREAM] );
		  bool eor   = aux::getBit( eormask , tf::NStreams - istream - 1 ) == 1;
		  roadBuilder.addHitWord( layer , hit , eor );
		}
	    }
	  
	}
    
      if( tmp_event->getID() > 0 )
	{
	  std::vector<TFRoad*> roads=roadBuilder.generateRoads();
	  for(auto road : roads)
	    tmp_event->addRoad(road);

	  if( tmp_event->getNRoads() > 0 )
	    vec_events.push_back( tmp_event );
	  else
	    delete tmp_event;
	}

    }

}

void TFEventList::buildFromDOOutputFile(const std::string& path)
{
  std::ifstream f( path.c_str() );
  std::string line;

  TFRoadBuilder roadBuilder;
  int ievent = -1;

  while( f )
    { // loop over events

      TFEvent *tmp_event = new TFEvent();
      tmp_event->setID( ievent );
    
      while( f )
	{ // loop over roads each event

	  if( ! safe_getline(f,line) ) break;

	  std::vector<std::string> a = auxstring::Tokenize( line , ' ' );
      
	  if( line.find("EventNum:") != std::string::npos )
	    {
	
	      ievent = aux::read( a[4] ); // event ID for the following event
	      break;

	    }
	  else if( line.find("Road:") != std::string::npos )
	    {

	      // Set road info
	      roadBuilder.addRoadID  (aux::read(a[1]));
	      roadBuilder.addSectorID(aux::read(a[3]));
	      int tmp_streammap = aux::readBin(a[5]);
	      roadBuilder.addLayermap( tf::getLayermapFromStreammap(tmp_streammap) );
	
	      // Load hits from each stream
	      for(unsigned int istream = 0 ; istream < tf::NStreams ; istream++ )
		{

		  if( ! safe_getline(f,line) ) break;

		  int layer = tf::getLayerFromStream( istream );

		  a.clear();
		  a = auxstring::Tokenize( line , ' ' );
		  a = auxstring::Tokenize( a , '[' );
		  a = auxstring::Tokenize( a , ']' );
		  a = auxstring::Tokenize( a , ',' );
	  
		  for(unsigned int ihit = 1 ; ihit < a.size() ; ihit++ )
		    {
		      bool eor   = (ihit == a.size()-1);
		      Int_tf hit = aux::readHex( a[ihit] );
		      roadBuilder.addHitWord( layer , hit , eor );
		    }

		}
	      
	    } else assert( false );
      
	}
    
      if( tmp_event->getID() > 0 )
	{
	  std::vector<TFRoad*> roads=roadBuilder.generateRoads();
	  for(auto road : roads)
	    tmp_event->addRoad(road);

	  if(report::dbg_lvl>=1) tmp_event->printRoads();
	  if( tmp_event->getNRoads() > 0 )
	    {
	      vec_events.push_back( tmp_event );
	    }
	  else
	    delete tmp_event;
	}
    }
}



void TFEventList::buildFromTFVector(std::vector<u_int> &content)
{
  u_int x(0), ievent(0);
  std::vector<TFCombination*> vec_combinations;
  
  // copy the header from the first stream.
  while (x < content.size()) {
    
    assert((content[x] & 0xffff) == 0xb0f0);
    
    TFEvent *tmp_event = new TFEvent();
    tmp_event->setID( ievent++ );
    x += 7;
    
    while (((content[x] >> 24) == 0xa8) || ((content[x] >> 24) == 0x96) || ((content[x] >> 24) == 0x86)){
      
      TFCombination *tmp_comb = new TFCombination();
      tmp_comb->setSectorID( aux::getBitRange(content[x +  0],  0, 11) );
      tmp_comb->setLayermap( aux::getBitRange(content[x +  0], 16, 23) );
      tmp_comb->setRoadID(   aux::getBitRange(content[x +  1],  0, 29) );
      
      // load the hits.
      for(unsigned int ilayer=0 ; ilayer < tf::NLayers ; ilayer++ )
	{
	  tmp_comb->setHit( ilayer, content[x + 2 + ilayer] );
	}
      
      tmp_comb->setChiSquare((content[x+10] << 28) + content[x+11]);
      tmp_comb->extractCoordinates();
      tmp_comb->setPasses( true );
      if(report::dbg_lvl>=1) tmp_comb->print();
      vec_combinations.push_back( tmp_comb );
      
      x += 12; // next track
    }
    
    // look for the end...
    while (0xe0f0 != (content[x] & 0xffff)) x++;
    x++;
    
    // re. Jordan, N.B.: ALL tracks in same road!!!
    TFRoad *tmp_road = new TFRoad();
    for (auto c : vec_combinations) tmp_road->addCombination(c);
    tmp_event->addRoad( tmp_road );
    vec_events.push_back( tmp_event );
    
  }
}


void TFEventList::buildFromBLMap(std::map<u_int, std::vector<u_int> > &content)
{
  u_int x(0), ievent(0);

  // Used for building up the roads
  TFRoadBuilder roadBuilder;
  
  // copy the header from the first stream.
  while (x < content[0x2c].size())
    { // new events.
      report::debug("BL NEW EVENT");
      TFEvent *tmp_event = new TFEvent();
      tmp_event->setID( ievent++ );

      bool new_road = true;
      bool layer_done[tf::NLayers];
      while (!(content[0x2c][x] & 0x100))
	{ // break if it's a new event.
      
	  if (new_road)
	    {
	      // write the basic information of the road, 
	      // and record whether or not each layer has hits -- 
	      // if it doesn't then don't record any!!
	      roadBuilder.addSectorID(aux::getBitRange(content[0x2a][x], 0, tf::SectorWidth-1));
	      roadBuilder.addRoadID(  aux::getBitRange(content[0x2b][x], 0, tf::RoadWidth-1  ));
	
	      u_int layermap = aux::getBitRange(content[0x2c][x], 0, tf::NLayers-1);
	      roadBuilder.addLayermap(layermap);
	
	      for (int i = 0; i < 8; i++) layer_done[i] = !((layermap >> i) & 1);

	      new_road = false;
	    }
      
	  // pixels are all the same
	  // and start from the lowest position.
	  for (unsigned int pix = 0; pix < 3; pix++)
	    {
	
	      if (!layer_done[pix])
		{
		  Int_tf hit = aux::getBitRange(content[0x24+pix][x], 0, tf::PixLayerWidth-1);
		  bool   eor = aux::getBit     (content[0x24+pix][x],    tf::PixLayerWidth);
	  
		  roadBuilder.addHitWord(pix, hit, eor);
		  if(eor) layer_done[pix] = true;
		}
	    }
      
	  // sct hits flip back and forth for even/odd.
	  for (u_int sct = 0; sct < 5; sct++)
	    {
	      if (!layer_done[3+sct])
		{ 
		  int sctW   = sct % 2; int sct2 = sct/2;
		  Int_tf hit = aux::getBitRange(content[0x27+sct2][x], sctW*16, 13 + sctW*16);
		  bool   eor = aux::getBit     (content[0x27+sct2][x], 14 + sctW*16);

		  roadBuilder.addHitWord(3+sct, hit, eor);
		  if(eor) layer_done[3+sct] = true;
		}
	    }

	  // if the next line is  a new word, reset these counters.
	  // this is safe, because even at the end of event, 
	  // there should be a new line for the 0x100 signal.
	  // use OR since roads in different events could have the same ID.
	  if (content[0x2b][x] != content[0x2b][x+1] ||
	      (content[0x2c][x+1] & 0x100))
	    {
	      new_road = true;
	    }

	  x++;
	}
      
      std::vector<TFRoad*> roads=roadBuilder.generateRoads();
      for(auto road : roads)
	{
	  tmp_event->addRoad(road);
	}

      if( tmp_event->getNRoads() > 0 )
	{
	  report::debug("NROADS = %d and NCOMB = %d in event ... printing:::: ",tmp_event->getNRoads(),tmp_event->getRoad(0)->getNCombinations());
	  if(report::dbg_lvl>=1) tmp_event->printRoads();
	  vec_events.push_back( tmp_event );
	}
      else
	delete tmp_event;

      x++; // now jump over the end of event word.
      new_road = true;

    }
  
  report::debug("Finished with %d events.",vec_events.size());
}

std::vector<EventFragment*> TFEventList::dumpToEventFragments() const
{
  std::vector<EventFragment*> eventFragments;
  for(auto event : vec_events)
    {
      EventFragmentTrackFTKPacket *eventFragment=new EventFragmentTrackFTKPacket();
      eventFragments.push_back(eventFragment);

      eventFragment->setL1ID(event->getID());
      eventFragment->setDumpChi2(true);

      for(auto track : event->getTrackVector())
	{
	  eventFragment->addTrack(track);
	}
    }
  return eventFragments;
}

bool TFEventList::safe_getline( std::ifstream &f , std::string &line )
{
  if( !f ) return false;
  getline( f , line );
  if( line.length()==0 ) return false;
  return true;
}

/*****************************************************************/
/*                                                               */
/*                                                               */
/*****************************************************************/

//#include <iostream>
#include <string>

// OKS, RC and IS
#include "aux/ReadoutModuleAux.h"

#include "aux/dal/ReadoutModule_Aux.h"
#include "aux/dal/firmware.h"
#include "aux/dal/AuxConstants.h"
//#include "ftkcommon/dal/PatternBankConf.h"

#include "config/Configuration.h"
#include "DFdal/RCD.h"
#include "DFdal/ReadoutConfiguration.h"
#include "RunControl/Common/OnlineServices.h"
#include "is/info.h"

// common tools
#include "ftkcommon/Utils.h"

// VME
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"

using namespace daq::ftk;
using namespace std;

/**********************/
ReadoutModule_Aux::ReadoutModule_Aux()
/**********************/
{ 
  #include "cmt/version.ixx"
  ERS_LOG("ReadoutModule_Aux::constructor: Entered"       << std::endl
    << "Build timestamp: " << __DATE__ << " " << __TIME__ << std::endl
    << cmtversion );

  m_configuration = 0;
  m_dryRun        = false;

  ERS_LOG("ReadoutModule_Aux::constructor: Done");
}

/***********************/
ReadoutModule_Aux::~ReadoutModule_Aux() noexcept
/***********************/
{
  ERS_LOG("ReadoutModule_Aux::destructor: Done");
}

/************************************************************/
void ReadoutModule_Aux::setup(DFCountedPointer<ROS::Config> configuration)
/************************************************************/
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), "Setup");

  std::cout << "TESTING THE CONFIG COUT" << std::endl;

  if (daq::ftk::isLoadMode())
    { ERS_LOG("Loading mode"); }
  
  // Get online objects from daq::rc::OnlineServices
  m_ipcpartition      = daq::rc::OnlineServices::instance().getIPCPartition();
  std::string appName = daq::rc::OnlineServices::instance().applicationName();
  auto dal_rcdAppl    = daq::rc::OnlineServices::instance().getApplication().cast<daq::df::RCD>();
  Configuration *conf = configuration->getPointer<Configuration>("configurationDB");
  // OR: Configuration &conf   = daq::rc::OnlineServices::instance().getConfiguration();
  auto readOutConfig  = conf->cast<daq::df::ReadoutConfiguration>( dal_rcdAppl->get_Configuration() );
 
  // Read some info from the DB
  const dal::ReadoutModule_Aux * module_dal = conf->get<dal::ReadoutModule_Aux>(configuration->getString("UID"));
  m_name	       = module_dal->get_Name();
  m_slot	       = module_dal->get_Slot();
  m_isServerName       = readOutConfig->get_ISServerName(); 
  m_dryRun             = module_dal->get_DryRun();
  m_ispublish          = false;//keep false until after prepareForRun
  if(m_dryRun)
    { /*FTK_WARNING("ReadoutModule_Aux.DryRun option set in DB, VME calls with be skipped!" ) */};


  //
  // Configure the Aux object
  //
  m_aux=std::make_unique<daq::ftk::aux>(m_slot, m_name);
  m_aux->setup(module_dal);

  //
  // Creating the ISInfo object
  //
  std::string isProvider = m_isServerName + "." + m_name;
  ERS_LOG("IS: publishing in " << isProvider);
  try 
  { 
     //FIXME: remove this hack when migration to cmake is complete
    m_auxNamed=std::make_unique<auxNamed>( m_ipcpartition, isProvider.c_str() ); 
  }
  catch( ers::Issue &ex)
  {
    daq::ftk::ISException e(ERS_HERE, "Error while creating auxNamed object", ex);  // NB: append the original exception (ex) to the new one 
    ers::warning(e);  //or throw e;  
  }
  
  // Creating the Histogramming provider
  std::string OHServer("Histogramming"); // TODO: make it configurable
  std::string OHName("FTK_" + m_name);   // Name of the pubblication directory
  try { 
    m_ohProvider=std::make_unique<OHRootProvider> ( m_ipcpartition , OHServer, OHName ); 
    m_ohRawProvider=std::make_unique<OHRawProvider<> >( m_ipcpartition , OHServer, getDFOHNameString(m_name, "AUX", -1, m_slot) ); 
    ERS_LOG("OH: publishing in " << OHServer << "." << m_name << ", from application " << appName);
  }
  catch ( daq::oh::Exception & ex)
  { // Raise a warning or throw an exception. 
    daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
    ers::warning(e);  //or throw e;  
  }
  m_dfReaderAux=std::make_unique<DataFlowReaderAux>(m_ohRawProvider, true);
  m_dfReaderAux->init(m_name);

  
  // Construct the histograms
  m_Histogram=std::make_unique<TH1F>("newHistogram","newRegHistogram",10,-50.5,50.5);

  //Monitoring stuff
  m_statusRegisterAuxFactory=std::make_unique<StatusRegisterAuxFactory>(m_name, m_slot, "123456", m_isServerName, m_ipcpartition);

  tLog.end(ERS_HERE);  // End log conter
}

unsigned int countBits(unsigned int data) {
  int count=0;
  for (unsigned int i=0; i<sizeof(unsigned int)*8; i++) 
    if ( (data>>i)&0x1 ) count++;
  return count;
}

/********************************/
void ReadoutModule_Aux::configure(const daq::rc::TransitionCmd& cmd)
/********************************/
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  if(m_dryRun)
    {
      ERS_LOG(cmd.toString() << " Dryrun Done");
      return;
    }

  m_aux->configure();

  tLog.end(ERS_HERE);  // End log conter
}

/********************************/
void ReadoutModule_Aux::connect(const daq::rc::TransitionCmd& cmd)
/********************************/
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  if(m_dryRun)
    {
      ERS_LOG(cmd.toString() << " Dryrun Done");
      return;
    }

  m_aux->connect();

  tLog.end(ERS_HERE);  // End log conter
}

/**********************************/    
void ReadoutModule_Aux::prepareForRun(const daq::rc::TransitionCmd& cmd)
/**********************************/    
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  m_aux->prepareForRun();
  m_ispublish = true;
  tLog.end(ERS_HERE);  // End log conter
}

/**************************/
void ReadoutModule_Aux::stopRecording(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  m_ispublish = false;
  m_aux->stop();
  tLog.end(ERS_HERE);  // End log conter
}

/**************************/
void ReadoutModule_Aux::publish()
/**************************/
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(),"Publish");

  //if(m_interrupt) return;
  if(!m_ispublish){
    ERS_LOG("PrepareForRun is not finished. Exit Publish.");
    return;
  }
  

  if(m_dryRun)
    {
  	  // Left blank deliberately. Coded this way for consistency with the rest of the code.
    } 
  else 
    {
      m_statusRegisterAuxFactory->readout();
      m_dfReaderAux->readIS();
      m_dfReaderAux->publishAllHistos();
    }

  // Call a method of the libaux.so; just for testing purpose. TODO: remove it
  //daq::ftk::dummy();

  // Collect the relevant information from VME
  // TODO: fill

  // Publish in IS with schema, i.e. fill ISInfo object
  // Example:
  //m_auxNamed->ExampleString = "Test";
  //m_auxNamed->ExampleU16    = 77;
  //m_auxNamed->ExampleS32    = rand()%100;
  //try { m_auxNamed->checkin();}
  //catch ( daq::oh::Exception & ex)
  //{ // Raise a warning or throw an exception. 
  //  daq::ftk::ISException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
  //  ers::warning(e);  //or throw e;  
  //}

  // Fill and publish histograms
  // Example:
  //m_Histogram->Fill( rand()%100 - 50 );
  //try { m_ohProvider->publish( *m_Histogram, "newRegHistogram", true ); }
  //catch ( daq::oh::Exception & ex)
  //{ // Raise a warning or throw an exception. 
  //  daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
  //  ers::warning(e);  //or throw e;  
  //}
  tLog.end(ERS_HERE);  // End log conter
}

/**************************/
void ReadoutModule_Aux::clearInfo() { }
/**************************/

//FOR THE PLUGIN FACTORY
extern "C"
{
  extern ROS::ReadoutModule* createReadoutModule_Aux();
}

ROS::ReadoutModule* createReadoutModule_Aux()
{
  return (new ReadoutModule_Aux());
}


// /**************************/
// void ReadoutModule_Aux::user(const daq::rc::UserCmd& cmd)
// /**************************/
// {
//   const std::string& cmdName = cmd.commandName();

//   ERS_LOG("Received user command \"" + cmdName + "\" while in state \"" + cmd.currentFSMState() + "\"");

//   const auto& params = cmd.commandParameters();
//   string slot = static_cast<ostringstream*>( &(ostringstream() << m_slot) )->str();

//   ERS_LOG("Received user command \"" + cmdName + "\" with argument \"" + params[0] + "\"in RM with slot " + slot);

//   if (cmdName == "resync"){
//     if ( params[0] == slot){
//       //Taking proper actions                                                                                                                                                       
//       ERS_LOG("Correct slot, Try to reset SSID links to AMB");
//       m_aux->reset_links_to_amb();
//     }
//     else{
//       ERS_LOG("Slot does not match, user command ignored.");
//     }
//   }
// }

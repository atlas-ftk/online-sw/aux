#include "aux/AuxSpyBuffer.h"
#include <cmath>

namespace daq {
  namespace ftk {
  
    //constructor
    AuxSpyBuffer::AuxSpyBuffer(unsigned int fpga, unsigned int channel, unsigned int channelID, unsigned int slot, unsigned int crate){
      
      m_buffer.clear();
      m_spyStatus = 0;
      m_sourceID = 0;
      m_slot = slot;
      m_fpga = fpga;
      m_overflow = false;
      m_lastAddress = -1;
      m_channel = channel;
      m_channelID = channelID;
      m_errorFreeze = false;
      m_nextBoardFreeze = false;
      m_internalFreeze = false;
      m_endOfEventFreeze = false;
      m_boardNumber = getVMEBoardNumber(crate, slot);
      m_position = getPosition();

      readSpyBuffer();
    }

    
    //taken from aux.cxx/aux_read_buffer
    int AuxSpyBuffer::readSpyBuffer(){

      m_buffer.clear();

      VMEInterface *vme=VMEManager::global().aux(m_slot,m_fpga);
      
      //set tmode rx stratix
      vme->write_word(TMODE,0x1);

      // Figure out the last addresss
      unsigned int buffinfo=vme->read_word(m_channel<<16);

      m_spyPointer = buffinfo&0x3fff;
      m_overflow = (buffinfo>>14)&1;

      unsigned int dim_buffer=(buffinfo>>16)&0x3fff;
      
      //read the content of the memory of the selected link
      m_buffer=vme->read_block(m_channel<<16,(1<<dim_buffer)+1);

      
      //ERS_LOG("Size of buffer is " << m_buffer.size());

      //set tmode rx stratix
      vme->write_word(TMODE, 0x0);



      //generate SourceIDSpyBuffer word
      m_sourceID = encode_SourceIDSpyBuffer( daq::ftk::BoardType::AUX, m_boardNumber, m_channelID , m_position);
      
      unwrapBuffer();
      return true;
    }

    //aux standard
    uint32_t AuxSpyBuffer::getSpyFreeze(){ //is 32 bits enough for us to have a real mask in aux? 
      
      VMEInterface *input1=VMEManager::global().aux(m_slot,1);
      VMEInterface *input2=VMEManager::global().aux(m_slot,2);
      VMEInterface *proc1 =VMEManager::global().aux(m_slot,3);
      VMEInterface *proc2 =VMEManager::global().aux(m_slot,4);
      VMEInterface *proc3 =VMEManager::global().aux(m_slot,5);
      VMEInterface *proc4 =VMEManager::global().aux(m_slot,6);
      VMEInterface *fpgas[6]={input1,input2,proc1,proc2,proc3,proc4};

      unsigned int register_content;
      uint32_t freeze_high = 0;
      
      for(unsigned int fpgaidx=0;fpgaidx<6;fpgaidx++)
	{
	  register_content=fpgas[fpgaidx]->read_word(FREEZE_STATUS);
	  if(register_content != 0){
            freeze_high = 1;
            m_internalFreeze = 1;
          }
	}

      register_content=fpgas[1]->read_word(HW_FREEZE_STATUS);
      if(register_content != 0){
        freeze_high = 1;
        m_nextBoardFreeze = 1;
      }

      return freeze_high;

    }


  daq::ftk::Position AuxSpyBuffer::getPosition(){
    

    //board input channels on fgpa 1, 2 
    if( m_channel == 0x01 || m_channel ==  0x02 || m_channel == 0x03 || m_channel == 0x04 )
      return daq::ftk::Position::IN;

    //board output m_channel on fpga 2
    if( m_channel == 0x09 && m_fpga == 2 )
      return daq::ftk::Position::OUT;

    return daq::ftk::Position::OTHER_POSITION;

  }

 void AuxSpyBuffer::encodeSpyStatus(){
    
   
   uint32_t spyStatus_bit=0;
   uint32_t size = std::log2(m_buffer.size());
   spyStatus_bit |= (0XF) & size;            //4 bits for spy buffer size
   
   spyStatus_bit <<= 3;
   spyStatus_bit |= (0x7) & 0;                                     //3 bits of nothing
   
   spyStatus_bit <<= 1;
   spyStatus_bit |= (0x1) & m_overflow;                           //1 bit of overflow

   spyStatus_bit <<= 4;
   spyStatus_bit |= (0x7) & 0;                                   //4 bits of nothing for now, no idea what 1st freeze is

   spyStatus_bit <<= 1;
   spyStatus_bit |= (0x1) & m_errorFreeze;                       //whatever this is
   spyStatus_bit <<= 1;
   spyStatus_bit |= (0x1) & m_nextBoardFreeze;                       //freeze from SSB
   spyStatus_bit <<= 1;
   spyStatus_bit |= (0x1) & m_internalFreeze;                       //any other freeze on aux
   spyStatus_bit <<= 1;
   spyStatus_bit |= (0x1) & m_endOfEventFreeze;                       //whatever this is
 
   spyStatus_bit <<= 16;
   spyStatus_bit |= m_spyPointer; 




   m_spyStatus = spyStatus_bit;

   
 
  }

  }
}












     
 



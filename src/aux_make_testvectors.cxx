#include <aux/aux_spybuffer_lib.h>

#include <ftkcommon/EventFragment.h>
#include <ftkcommon/EventFragmentFTKPacket.h>
#include <ftkcommon/EventFragmentDummyFTKPacket.h>
#include <ftkcommon/EventFragmentRoadPacket.h>
#include <ftkcommon/EventFragmentDummyRoadPacket.h>

#include "aux/aux.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>

#include <TFile.h>
#include <TTree.h>

namespace daq {
  namespace ftk {
    void aux_map_unique_roads(std::vector<EventFragment*>& events, std::map<int,int>& reducedRoadIDs, std::map<int,int>& fullRoadIDs)
    {
      for(auto event: events){
	for(auto road: event->bitstream()){
	  int road_nobitmask = road & 0x3fffff;
	  if(road>>16==0xE0DA) continue;
	  if(reducedRoadIDs.find(road_nobitmask) != reducedRoadIDs.end()){
	    continue;//This HW road ID has been found already. 
	  }
	  reducedRoadIDs[road_nobitmask] = (fullRoadIDs.end()->first&0x3fffff)+1; // Add new reduced road ID to reduced map with full ID as key
	  fullRoadIDs[reducedRoadIDs[road_nobitmask]] = road_nobitmask; // Add new full road ID to full map with reduced ID as key
	}
      }
    }

    void aux_reduce_roadIDs(std::vector<EventFragment*>& events, std::map<int,int>& reducedRoadIDs)
    {
      for(auto event: events){
	std::vector<unsigned int> newData;
	std::vector<unsigned int> data = event->bitstream();	
	for(auto road: data){
	  int road_nobitmask = road & 0x3fffff;
	  if(road>>16==0xE0DA){
	    newData.push_back(road);
	  }
	  else{
	    newData.push_back((road&0xff000000)|reducedRoadIDs[road_nobitmask]);
	  }
	}
	event->parseFragment(newData);
      }
    }

    void aux_make_skimmed_bank(u_int proc, std::map<int,int>& reducedRoadIDs, std::map<int,int>& fullRoadIDs, std::string ammappath)
    {
      std::cout << "Open: " << ammappath << std::endl;
      TFile *bankFile = TFile::Open(ammappath.c_str(),"READ");
      TTree *bank  = dynamic_cast<TTree *>(bankFile->Get("Bank"));
      TTree *ssmap = dynamic_cast<TTree *>(bankFile->Get("SSMap"));

      std::string skimmedBankPath = "testvectors/spy_to_modelSim/proc"+std::to_string(proc)+"SkimmedBank.root";
      std::cout << "Make: " << skimmedBankPath << std::endl;
      TFile *skimmedBankFile = TFile::Open(skimmedBankPath.c_str(),"RECREATE");
      TTree *skimmedBank = bank->CloneTree(0);
      TTree *ssmapCopy = ssmap->CopyTree("");

      int nPatternsPerChip = 131072;
      int lamb = proc - 1;
      int fpga_offset = lamb*nPatternsPerChip*16;
      
      int fillCount = 0;
      for(unsigned int i = 0; i < fullRoadIDs.size(); i++){
	int fullRoadID = fullRoadIDs[i+1];
	int ID = (( (fullRoadID>>17)*nPatternsPerChip ) | ( (fullRoadID)&0x1ffff )) + fpga_offset;
	//std::cout << std::hex << "Get Road: " <<fullRoadID<< " from entry " << ID << " filling " << fillCount << " for reduced ID " << reducedRoadIDs[fullRoadID]<<std::endl;
	bank->GetEntry(ID);
	skimmedBank->Fill();
	fillCount += 1;
      }
      int npatterns = skimmedBank->GetEntries();
      std::cout << "Write skimmed bank" << std::endl;
      skimmedBank->Write();
      ssmapCopy->Write();
      skimmedBankFile->Close();
      bankFile->Close();

      std:: cout << "Make hex skimmed bank" << std::endl;
      daq::ftk::aux_ammap_convert(skimmedBankPath, npatterns, false, 1, false);
    }


    void aux_dump_testvector( const std::string& file , const std::vector<EventFragment*>& fragments, std::vector<int> L1IDs )
    {
      for(unsigned int i = 0; i < L1IDs.size(); i++) L1IDs[i] = L1IDs[i]&0xffff;

      std::ofstream fh;
      fh.open(file);
      bool isRoadFile = (file.find("roads") != std::string::npos) || (file.find("Roads") != std::string::npos);
      std::vector<int> L1IDsFound;
      std::vector<int> L1IDsAvailable;
      for(auto fragment : fragments)
	{
	  if(L1IDs.size()){ // If using L1ID list, only include fragment in TVs if L1ID is in requested list of L1IDs
	    L1IDsAvailable.push_back(fragment->getL1ID());
	    if(std::find(L1IDs.begin(), L1IDs.end(), (fragment->getL1ID()&0xffff) ) == L1IDs.end()) continue;
	    L1IDsFound.push_back(fragment->getL1ID());
	  }

	  std::vector<uint> data=fragment->bitstream();
	  for(auto word : data)
	    {
	      bool isCtrl=(word==0xb0f0b0f0)?true:false;
	      isCtrl=(word==0xe0dae0da)?true:isCtrl;
	      isCtrl=(word==0xe0f0e0f0)?true:isCtrl;
	      if(isRoadFile && word>>16==0xE0DA){
		isCtrl=true;
		word = 0xf700<<16|(fragment->getL1ID()&0xffff);
	      }
	      
	      fh << isCtrl << "\t0x" << std::hex << std::setw(8) << std::setfill('0') << word << std::dec <<std::endl;
	    }

	}

      fh.close();

      if(L1IDs.size() != L1IDsFound.size()){
	std::cout << "WARNING: Did not find all requested L1IDs for " << file << std::endl;
	std::cout              << "Requested: " << std::hex;
	for(auto L1ID: L1IDs         ) std::cout << L1ID << " ";
	std::cout << std::endl << "    Found: ";
	for(auto L1ID: L1IDsFound    ) std::cout << L1ID << " ";
	std::cout << std::endl << "Available: ";
	for(auto L1ID: L1IDsAvailable) std::cout << L1ID << " ";
	std::cout << std::endl << std::endl;
      }
    }

    bool aux_make_testvectors ( const std::string& inDirName, std::vector<int> L1IDs, std::string ammappath, bool debug )
    {
      /// fpga, buffer, data
      std::map<u_int, std::map<u_int, std::vector<u_int> > > data;

      aux_buffers_build_data_structure(data);     // build the map     
      aux_buffers_download_data(data, inDirName, debug); // call download the buffers

      // Aux input
      std::vector<EventFragment*> plane0 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[1][0x01],16);
      std::vector<EventFragment*> plane1 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[1][0x02],16);
      std::vector<EventFragment*> plane2 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[1][0x03],16);
      std::vector<EventFragment*> plane3 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[1][0x04],16);
      std::vector<EventFragment*> plane4 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[2][0x01],16);
      std::vector<EventFragment*> plane5 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[2][0x02],16);
      std::vector<EventFragment*> plane6 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[2][0x03],16);
      std::vector<EventFragment*> plane7 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[2][0x04],16);

      aux_dump_testvector("testvectors/spy_to_modelSim/data_plane_0.txt", plane0, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_plane_1.txt", plane1, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_plane_2.txt", plane2, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_plane_3.txt", plane3, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_plane_4.txt", plane4, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_plane_5.txt", plane5, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_plane_6.txt", plane6, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_plane_7.txt", plane7, L1IDs);

      //Post SSMap TVs
      //# File       | Physical Channel |       DataOrganizer Channel |            | Spy Buffer
      //# TXInput  0 : SCT 1                                                       |     Input2 col 0 (0x0e)
      //# TXInput  1 : SCT 2                                                       |     Input2 col 1 (0x0f)
      //# TXInput  2 : SCT 3                                                       |     Input2 col 2 (0x10)
      //# TXInput  3 : SCT 4                                                       |     Input2 col 3 (0x11)
      //# TXInput  4 : SCT 0                                                       |     Input1 col 0 (0x11)
      //# TXInput  5 : PIX 0 <-> SSID[3] == 0 => pix_stream_data( 0/ 1) SSID/HIT   |     Input1 col 1 (0x12)
      //# TXInput  6 : PIX 0 <-> SSID[3] == 1 => pix_stream_data( 2/ 3) SSID/HIT   |     Input1 col 2 (0x13)
      //# TXInput  7 : PIX 1 <-> SSID[3] == 0 => pix_stream_data( 4/ 5) SSID/HIT   |     Input1 col 3 (0x14)
      //# TXInput  8 : PIX 1 <-> SSID[3] == 1 => pix_stream_data( 6/ 7) SSID/HIT   |     Input1 col 4 (0x15)
      //# TXInput  9 : PIX 2 <-> SSID[3] == 0 => pix_stream_data( 8/ 9) SSID/HIT   |     Input1 col 5 (0x16)
      //# TXInput 10 : PIX 2 <-> SSID[3] == 1 => pix_stream_data(10/11) SSID/HIT   |     Input1 col 6 (0x17)
      //# TXInput 11 : contains empty packets as this layer is unused.             |     Input1 col 7 (0x18)
      std::vector<EventFragment*> TXInput0 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[2][0x0e],0);
      std::vector<EventFragment*> TXInput1 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[2][0x0f],0);
      std::vector<EventFragment*> TXInput2 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[2][0x10],0);
      std::vector<EventFragment*> TXInput3 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[2][0x11],0);
      std::vector<EventFragment*> TXInput4 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[1][0x11],0);
      std::vector<EventFragment*> TXInput5 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[1][0x12],0);
      std::vector<EventFragment*> TXInput6 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[1][0x13],0);
      std::vector<EventFragment*> TXInput7 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[1][0x14],0);
      std::vector<EventFragment*> TXInput8 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[1][0x15],0);
      std::vector<EventFragment*> TXInput9 =FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[1][0x16],0);
      std::vector<EventFragment*> TXInput10=FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[1][0x17],0);
      std::vector<EventFragment*> TXInput11=FTKPacket_splitFragments<EventFragmentDummyFTKPacket>(data[1][0x18],0);

      aux_dump_testvector("testvectors/spy_to_modelSim/data_TXInput_0.txt", TXInput0, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_TXInput_1.txt", TXInput1, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_TXInput_2.txt", TXInput2, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_TXInput_3.txt", TXInput3, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_TXInput_4.txt", TXInput4, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_TXInput_5.txt", TXInput5, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_TXInput_6.txt", TXInput6, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_TXInput_7.txt", TXInput7, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_TXInput_8.txt", TXInput8, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_TXInput_9.txt", TXInput9, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_TXInput_10.txt",TXInput10, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/data_TXInput_11.txt",TXInput11, L1IDs);

      //Road TVs 0x44, 0x45, 0x46, 0x47
      std::vector<EventFragment*> Proc1TXRoads0 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[3][0x44],16);
      std::vector<EventFragment*> Proc1TXRoads1 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[3][0x45],16);
      std::vector<EventFragment*> Proc1TXRoads2 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[3][0x46],16);
      std::vector<EventFragment*> Proc1TXRoads3 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[3][0x47],16);

      std::vector<EventFragment*> Proc2TXRoads0 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[4][0x44],16);
      std::vector<EventFragment*> Proc2TXRoads1 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[4][0x45],16);
      std::vector<EventFragment*> Proc2TXRoads2 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[4][0x46],16);
      std::vector<EventFragment*> Proc2TXRoads3 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[4][0x47],16);

      std::vector<EventFragment*> Proc3TXRoads0 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[5][0x44],16);
      std::vector<EventFragment*> Proc3TXRoads1 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[5][0x45],16);
      std::vector<EventFragment*> Proc3TXRoads2 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[5][0x46],16);
      std::vector<EventFragment*> Proc3TXRoads3 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[5][0x47],16);

      std::vector<EventFragment*> Proc4TXRoads0 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[6][0x44],16);
      std::vector<EventFragment*> Proc4TXRoads1 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[6][0x45],16);
      std::vector<EventFragment*> Proc4TXRoads2 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[6][0x46],16);
      std::vector<EventFragment*> Proc4TXRoads3 =RoadPacket_splitFragments<EventFragmentDummyRoadPacket>(data[6][0x47],16);

      //Dump road TVs as seen on chip
      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc1_TXInput_0.txt", Proc1TXRoads0, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc1_TXInput_1.txt", Proc1TXRoads1, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc1_TXInput_2.txt", Proc1TXRoads2, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc1_TXInput_3.txt", Proc1TXRoads3, L1IDs);

      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc2_TXInput_0.txt", Proc2TXRoads0, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc2_TXInput_1.txt", Proc2TXRoads1, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc2_TXInput_2.txt", Proc2TXRoads2, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc2_TXInput_3.txt", Proc2TXRoads3, L1IDs);

      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc3_TXInput_0.txt", Proc3TXRoads0, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc3_TXInput_1.txt", Proc3TXRoads1, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc3_TXInput_2.txt", Proc3TXRoads2, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc3_TXInput_3.txt", Proc3TXRoads3, L1IDs);

      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc4_TXInput_0.txt", Proc4TXRoads0, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc4_TXInput_1.txt", Proc4TXRoads1, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc4_TXInput_2.txt", Proc4TXRoads2, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/roads_proc4_TXInput_3.txt", Proc4TXRoads3, L1IDs);

      //count unique roads for skimmed pattern bank
      std::map<int,int> proc1ReducedRoadIDs;
      std::map<int,int> proc1FullRoadIDs;
      aux_map_unique_roads(Proc1TXRoads0, proc1ReducedRoadIDs, proc1FullRoadIDs);
      aux_map_unique_roads(Proc1TXRoads1, proc1ReducedRoadIDs, proc1FullRoadIDs);
      aux_map_unique_roads(Proc1TXRoads2, proc1ReducedRoadIDs, proc1FullRoadIDs);
      aux_map_unique_roads(Proc1TXRoads3, proc1ReducedRoadIDs, proc1FullRoadIDs);

      std::map<int,int> proc2ReducedRoadIDs;
      std::map<int,int> proc2FullRoadIDs;
      aux_map_unique_roads(Proc2TXRoads0, proc2ReducedRoadIDs, proc2FullRoadIDs);
      aux_map_unique_roads(Proc2TXRoads1, proc2ReducedRoadIDs, proc2FullRoadIDs);
      aux_map_unique_roads(Proc2TXRoads2, proc2ReducedRoadIDs, proc2FullRoadIDs);
      aux_map_unique_roads(Proc2TXRoads3, proc2ReducedRoadIDs, proc2FullRoadIDs);

      std::map<int,int> proc3ReducedRoadIDs;
      std::map<int,int> proc3FullRoadIDs;
      aux_map_unique_roads(Proc3TXRoads0, proc3ReducedRoadIDs, proc3FullRoadIDs);
      aux_map_unique_roads(Proc3TXRoads1, proc3ReducedRoadIDs, proc3FullRoadIDs);
      aux_map_unique_roads(Proc3TXRoads2, proc3ReducedRoadIDs, proc3FullRoadIDs);
      aux_map_unique_roads(Proc3TXRoads3, proc3ReducedRoadIDs, proc3FullRoadIDs);

      std::map<int,int> proc4ReducedRoadIDs;
      std::map<int,int> proc4FullRoadIDs;
      aux_map_unique_roads(Proc4TXRoads0, proc4ReducedRoadIDs, proc4FullRoadIDs);
      aux_map_unique_roads(Proc4TXRoads1, proc4ReducedRoadIDs, proc4FullRoadIDs);
      aux_map_unique_roads(Proc4TXRoads2, proc4ReducedRoadIDs, proc4FullRoadIDs);
      aux_map_unique_roads(Proc4TXRoads3, proc4ReducedRoadIDs, proc4FullRoadIDs);

      //Make Skimmed pattern bank
      if(ammappath != ""){
	aux_make_skimmed_bank(1,proc1ReducedRoadIDs,proc1FullRoadIDs,ammappath);
	aux_make_skimmed_bank(2,proc2ReducedRoadIDs,proc2FullRoadIDs,ammappath);
	aux_make_skimmed_bank(3,proc3ReducedRoadIDs,proc3FullRoadIDs,ammappath);
	aux_make_skimmed_bank(4,proc4ReducedRoadIDs,proc4FullRoadIDs,ammappath);
      }

      //replace HW roadID with reduced roadID
      aux_reduce_roadIDs(Proc1TXRoads0, proc1ReducedRoadIDs);
      aux_reduce_roadIDs(Proc1TXRoads1, proc1ReducedRoadIDs);
      aux_reduce_roadIDs(Proc1TXRoads2, proc1ReducedRoadIDs);
      aux_reduce_roadIDs(Proc1TXRoads3, proc1ReducedRoadIDs);

      aux_reduce_roadIDs(Proc2TXRoads0, proc2ReducedRoadIDs);
      aux_reduce_roadIDs(Proc2TXRoads1, proc2ReducedRoadIDs);
      aux_reduce_roadIDs(Proc2TXRoads2, proc2ReducedRoadIDs);
      aux_reduce_roadIDs(Proc2TXRoads3, proc2ReducedRoadIDs);

      aux_reduce_roadIDs(Proc3TXRoads0, proc3ReducedRoadIDs);
      aux_reduce_roadIDs(Proc3TXRoads1, proc3ReducedRoadIDs);
      aux_reduce_roadIDs(Proc3TXRoads2, proc3ReducedRoadIDs);
      aux_reduce_roadIDs(Proc3TXRoads3, proc3ReducedRoadIDs);

      aux_reduce_roadIDs(Proc4TXRoads0, proc4ReducedRoadIDs);
      aux_reduce_roadIDs(Proc4TXRoads1, proc4ReducedRoadIDs);
      aux_reduce_roadIDs(Proc4TXRoads2, proc4ReducedRoadIDs);
      aux_reduce_roadIDs(Proc4TXRoads3, proc4ReducedRoadIDs);

      //dump skimmed roads TVs for use with skimmed pattern bank
      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc1_TXInput_0.txt", Proc1TXRoads0, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc1_TXInput_1.txt", Proc1TXRoads1, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc1_TXInput_2.txt", Proc1TXRoads2, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc1_TXInput_3.txt", Proc1TXRoads3, L1IDs);

      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc2_TXInput_0.txt", Proc2TXRoads0, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc2_TXInput_1.txt", Proc2TXRoads1, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc2_TXInput_2.txt", Proc2TXRoads2, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc2_TXInput_3.txt", Proc2TXRoads3, L1IDs);

      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc3_TXInput_0.txt", Proc3TXRoads0, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc3_TXInput_1.txt", Proc3TXRoads1, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc3_TXInput_2.txt", Proc3TXRoads2, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc3_TXInput_3.txt", Proc3TXRoads3, L1IDs);

      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc4_TXInput_0.txt", Proc4TXRoads0, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc4_TXInput_1.txt", Proc4TXRoads1, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc4_TXInput_2.txt", Proc4TXRoads2, L1IDs);
      aux_dump_testvector("testvectors/spy_to_modelSim/skimmedRoads_proc4_TXInput_3.txt", Proc4TXRoads3, L1IDs);
      std::cout << "Done" << std::endl;
      return true;
    }
  } //namespcae daq
} //namespcae ftk



#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


/** Parsing input parmaeters and calling the am_init function
 *
 */
int main(int argc, char **argv)
{
 
  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  std::vector<int> L1IDs;
  options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ("inDirName,d", value< std::string >()->default_value("spy_output"), "Directory containing spy buffer dump.")
    ("L1IDs,l", value< std::vector<int> >(&L1IDs)->multitoken(), "L1IDs to include. Default is all (does not ensure overlap!)")
    ("ammappath,a", value< std::string >()->default_value(""), "Location of input AMMAP.")
    ("debug", "dump debuggin info")
    ;

  positional_options_description p;

  variables_map vm;
  try
    {
      store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    }
  catch( ... ) 
    { // In case of errors during the parsing process, desc is printed for help 
      std::cerr << desc << std::endl;
      return 1;
    }
  
  notify(vm);
  
  if (vm.count("help") )
    { // if help is required, then desc is printed to output
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;	
    }

  bool debug=false;
  if (vm.count("debug")) debug = true;

  std::string inDirName = vm["inDirName"].as<std::string>();
  std::string ammappath = vm["ammappath"].as<std::string>();

  return  daq::ftk::aux_make_testvectors(inDirName,L1IDs,ammappath,debug) ;

}

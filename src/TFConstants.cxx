#include "aux/TF/TFConstants.h"

#include "aux/AuxString.h"
#include "aux/AuxUtils.h"
#include "aux/Report.h"
#include "aux/TF/TFGen.h"

TFConstants::TFConstants()
{
  empty_sector = new TFSector();
}

TFConstants::~TFConstants()
{
  // delete all of the sector objects
  delete empty_sector;
  for(unsigned int isec = 0 ; isec < vec_sectors.size() ; isec++ ) delete vec_sectors[isec];
}

//returns integer and its exponent
std::pair<int, int> TFConstants::getFloating(double input)
{
	//floating point ranges (assuming 13 bits for significand and 5 bits for exponent)
	const int minsignificand = -4096;
	const int maxsignificand = 4095;
	const int maxexponent = 31;

	std::pair<int,int> output;

	//if signficand is greater than 13 bits, return max value
 	if(round(input) > maxsignificand){
 		output.first = maxsignificand;
 		output.second = 0;
 		return output;
 	}
 	if(round(input) < minsignificand){
 		output.first = maxsignificand;
 		output.second = 0;
 		return output;
 	}

 	double significand = input; 
 	int exponent = 0;
 	while(round(significand*2.0) <= maxsignificand &&
 		round(significand*2.0) >= minsignificand &&
 			exponent <= maxexponent){
 			significand *= 2.0;
 			exponent++;
 	}
  
        int sig = round(significand);

	output.first = sig;
	output.second = exponent;

	return output;
 	
}

//set offsets
int TFConstants::getSigned(double d, int offset)
{

	int minint = -131072;
	int maxint = 131071;

	//	int r = round(pow(2.0,offset) * d);
	int r = trunc(pow(2.0,offset) * d);
	if(r > maxint) r = maxint;
	if(r < minint) r = minint;

	return r;
}

unsigned int TFConstants::getNSectors()
{ return vec_sectors.size(); }

TFSector* TFConstants::getSector(unsigned int sectorid)
{
  // simple iterative search through all the sectors
  for(unsigned int isec = 0 ; isec < vec_sectors.size() ; isec++ )
    if( vec_sectors[isec]->getID() == sectorid ) return vec_sectors[isec];

  // sector not found, return zero constants
  report::warn( "constants not loaded for sector %5i" , sectorid );
  return empty_sector;
}

void TFConstants::buildFromGCON(const std::string& path )
{
   std::ifstream input (path.c_str());
   std::string line;
   int sec;

   const int NCHI = tf::NChiComponents;
   const int NCOORDS = tf::NCoords;
   const int KERNELOFFSET = 12;
   //   const int KERNELOFFSET = 13;
   const int KAVERAGEOFFSET = 4;
   //   const int KAVERAGEOFFSET = 6;
   const int NSCT = tf::NSctLayers;
   const int NPIX = tf::NPixLayers;
   
   //temporary data structs for reading in constants
   double kaverages[NCHI];
   double kernel[NCHI][NCOORDS];
   double majkk[NCOORDS][NCOORDS];
   std::vector<double> majinvkk;
   
   int maxsec = 16385;

   //read in the file
   while(input >> line){
		   
      //look for "sector" keyword
      if(line == "sector"){
	   input >> sec; 
	   
	   if (sec < 16384){
	   //new sector object
	      TFSector *tmp_sector = new TFSector();
	      tmp_sector->setID(sec);
	      majinvkk.clear();
              
	      //Find kaverages and set H vector
	      while(input >> line)  if(line == "kaverages")  break;
	      for(int i=0; i<NCHI; i++){
		      input >> kaverages[i];
 		     
		      //set vector
		      tmp_sector->setH(i, getSigned(kaverages[i],KAVERAGEOFFSET));
	      }
	      
	      //Find kernel and set S matrix 
	      while (input >> line)  if(line == "kernel")  break;
	      for(int i=0; i<NCHI; i++){
		      for(int j=0; j<NCOORDS; j++){

			      input >> kernel[i][j];

			      kernel[i][j] *= const_scale_map[j];
			      //set matrix
			      tmp_sector->setS(i,j, getSigned(kernel[i][j],KERNELOFFSET)); 
		      }
	      }

	      //Build matrix to calculate guess constants
              for (int i = 0; i < NCOORDS; i++) {
		      for (int j = 0; j < NCOORDS; j++) {
			      majkk[i][j] = 0.;

          		      // we actually just need the block diagonal bit,
          		      // but make the whole matrix, regardless.
          		      for (int r = 0; r < NCHI; r++) 
				      majkk[i][j] += kernel[r][i] * kernel[r][j];
		      }
	      }



	      // calculate the matrix inverses.
	      for (int pi = 0; pi < 2*NPIX; pi += 2) {

		      //get determinant.
		      double det = majkk[pi][pi] * majkk[pi+1][pi+1] - majkk[pi+1][pi] * majkk[pi][pi+1];

		      // negatives to help with t-vector calculation
		      majinvkk.push_back( - majkk[pi+1][pi+1]/det);
		      majinvkk.push_back(   majkk[pi+1][pi  ]/det);
		      majinvkk.push_back( - majkk[pi  ][pi  ]/det);
	      }

	      for (int si = NPIX*2; si < NPIX*2 + NSCT; si++) {
		      majinvkk.push_back( - 1./majkk[si][si]);
	      }

	      //Now set pix guess and sct guess info
	      //first 9 elements are pix guesses, last 3 are sct guesses
	      int ind = 0;
	      for(int i=0; i<NPIX; i++){
		      for(int j=0; j<3; j++){
			      std::pair<int,int> temp = getFloating(majinvkk[ind]);
			      tmp_sector->setPixGuessFrac(i,j,temp.first);
			      tmp_sector->setPixGuessExp(i,j,temp.second);
			      ind++;
		      }
	      }

	      for(int i=0; i< NSCT; i++){
		      std::pair<int,int> temp = getFloating(majinvkk[i+ (3*NPIX)]);
		      tmp_sector->setSctGuessFrac(i,temp.first);
		      tmp_sector->setSctGuessExp(i,temp.second);
	      }
              

	      //add this sector to the list of sectors
	      vec_sectors.push_back( tmp_sector );
	   }
      }
   }
   input.close();
}




void TFConstants::buildFromFile(const std::string& path )
{
  std::ifstream f( path.c_str() );
  std::string line;
  
  while( true )
    {
      if( !f ) break;
      
      getline( f , line );
      
      if(line.length()==0) continue;
      
      if( line.find("SECTOR")==0 )
	{
	  
	  TFSector *tmp_sector = new TFSector();
	  auxstring::ReplaceAll( line , "SECTOR" , "" );
	  tmp_sector->setID( std::stoi(line) );
	  report::debug( "Adding sector %i", tmp_sector->getID());
	  
	  for(unsigned int i = 0 ; i < tf::NChiComponents ; i++ )
	    {
	      for(unsigned int j = 0 ; j < tf::NCoords ; j++ )
		{
		  getline( f , line );
		  tmp_sector->setS( i , j , aux::read(line) );
		}
	      getline( f , line );
	      tmp_sector->setH(i, aux::read(line));
	    }
	  
	  for(unsigned int i = 0 ; i < tf::NPixLayers ; i++ )
	    {
	      for(unsigned int j = 0 ; j < 3 ; j++ )
		{
		  getline( f , line );
		  std::vector<std::string> a = auxstring::Tokenize(line,' ');
		  std::string s_frac = a[0];
		  std::string s_exp  = a[1];
		  tmp_sector->setPixGuessFrac(i, j, aux::read(s_frac));
		  tmp_sector->setPixGuessExp (i, j, aux::read(s_exp) );
		}
	    }
	  
	  for(unsigned int i = 0 ; i < tf::NSctLayers ; i++ )
	    {
	      getline( f , line );
	      std::vector<std::string> a = auxstring::Tokenize(line,' ');
	      std::string s_frac = a[0];
	      std::string s_exp  = a[1];
	      tmp_sector->setSctGuessFrac(i, aux::read(s_frac));
	      tmp_sector->setSctGuessExp (i, aux::read(s_exp) );
	    }
	  
	  tmp_sector->print();
	  vec_sectors.push_back( tmp_sector );
	}
    }
  
  f.close();  
}


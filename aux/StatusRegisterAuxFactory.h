#ifndef STATUSREGISTERAUXFACTORY_H
#define STATUSREGISTERAUXFACTORY_H

#include "ftkvme/StatusRegisterVMEFactory.h"

// okx schema generated class
#include "aux/dal/StatusRegisterAuxStatusNamed.h"
#include "aux/aux_registers.h"

#include <string>

// Namespaces
namespace daq {
namespace ftk {

/*! \brief Reads out Registers from the AUX using StatusRegisterVMECollection objects
 * Will publish them to the IS once fully implemented.
 */
class StatusRegisterAuxFactory 
: public StatusRegisterVMEFactory
{
public:
		/*! \brief Class constructor, opens vme connection
	 *
	 * \param auxName The name of the AUX being read from
	 * \param auxSlot The VME slot the AUX is situated in
	 * \param registersToRead tells the factory which register collections to read out:
	 *   T = Random registers used for testing
	 *
	 * \param isServerName The name of the IS server the register values will be sent to
	 * \param IPCPartition The IPCPartition to be used for IS publishing
	 */
	StatusRegisterAuxFactory(
		std::string auxName,
		u_int auxSlot,
		std::string registersToRead,
		std::string isServerName,
		IPCPartition ipcPartition
	);

	~StatusRegisterAuxFactory();

private:
	// Member Functions

    // Unused. Need something like this when calling each register individually
    void setupAuxCollections(std::unique_ptr<StatusRegisterAuxStatusNamed> &m_srAuxSNamed);

private:
	// Member Variables
};

} // namespace ftk
} // namespace daq

#endif /* STATUSREGISTERAUXFACTORY_H */

//
// Header file for AMBank for DO bit level check
// Created by Michael Hank on July 23, 2019.
// Largely based on Patrick's python code, also using work done by Lesya.
//

#include <vector>
#include <map>
#include <string>
#include <utility>
#include <aux/DataOrganizer.h>
#include <TFile.h>
#include <TTree.h>

#ifndef BITLVL_AMBANK_H
#define BITLVL_AMBANK_H

namespace daq
{
    namespace ftk
    {
        class AMBank
        {
            public:

                string path;

                bool oldDC;
                bool allSectors;
                int inChipOff;
                bool rootBank; //Assuming root bank for now
                int m_bankdcbits[8];
                static const unsigned int nPatternsPerChip = 131072; //AM06 max patterns per chip. Used when loading the full pattern bank
                int lamb;
                int nRoadAddrs;
                bool getHWroadIDMode;

                std::vector<int> firedRoads;

                TFile * ambankfile;
                TTree * BankTree;
                TTree * SSMapTree;

                //initialize
                AMBank(string inpath, bool in_oldDC, bool in_allSectors, int in_inChipOff)
                {
                    path = inpath;

                    oldDC = in_oldDC;
                    allSectors = in_allSectors;
                    inChipOff = in_inChipOff;
                    rootBank = true;

                    ambankfile = TFile::Open(inpath.c_str(),"READ");
                    BankTree  = dynamic_cast<TTree *>(ambankfile->Get("Bank"));
                    SSMapTree = dynamic_cast<TTree *>(ambankfile->Get("SSMap"));
                    
                    for (u_int i=0; i<8; i++){                
                        m_bankdcbits[i] = 2; 
                    }
                    lamb = 0;
                    nRoadAddrs = 0;

                    getHWroadIDMode = true;
                }

                //Get ID for the bank tree
                int getBankTTreeID(int roadID)
                {
                    int ID;
                    int fpga_offset = lamb*nPatternsPerChip*16;
                    if (getHWroadIDMode){
                        ID = ( ((roadID)>>17)*nPatternsPerChip ) | ( (roadID)&0x1ffff ) - inChipOff + fpga_offset;
                    } else {
                        ID = roadID;
                    }
                    if(std::find(firedRoads.begin(), firedRoads.end(), ID) == firedRoads.end()) {//firedRoads does not contain ID
                        firedRoads.push_back(ID);
                    }
                    return ID;
                }


        };
    


    }//namespace ftk
}//namespace daq

#endif

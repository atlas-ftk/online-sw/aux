#ifndef AUXUTILS_H_
#define AUXUTILS_H_

#include "aux/AuxMath.h"
#include <string>

/*
 * useful utilities for bit manipulation
 */

namespace aux
{
  /*! \brief Convert binary string to integer
   *
   * \param n string in format 0bXXXXXX
   *
   * \return integer value of n
   */
  Int_tf readBin(std::string n);

  /*! \brief Convert hex string to integer
   *
   * \param n string in format 0xXXXXXX
   *
   * \return integer value of n
   */
  Int_tf readHex(std::string n);

  /*! \brief Convert string to integer
   *
   * Automatically determines base on number in string and converts to
   * integer. The available bases are 0xXXXXXX (hex), 0bXXXXXX (bin)
   * and XXXX (dec).
   *
   * \param n string with number
   *
   * \return integer value of n
   */  
  Int_tf read(const std::string& n);

  /*! \brief Get bit value
   *
   * \param word Word to inspect
   * \param ibit The bit to return
   *
   * \return Value of bit ibit in word
   */
  int getBit(unsigned int word, unsigned int ibit);

  /*! \brief Determine sign of a number
   *
   * 0 is defined to be positive
   *
   * \param v the integer
   * 
   * \return v/|v|
   */
  Int_tf sgn(Int_tf v);

  /*! \brief create bitmask for several LSBs
   *
   * \param nbits number of bits to select
   *
   * \return the bitmask value
   */
  Int_tf getMask(unsigned int nbits);

  /*! \brief Perform a right bit-shift with overflow check
   *
   * Overflow is defined as the maxwidth mask not resulting in the
   * same number as before the mask.
   *
   * \param val reference to integer to right shift
   * \param nbits number of bits to shift
   * \param maxwidth maximum expected size of number
   * \param reference where to set overflow status
   */
  void shiftRight(Int_tf &val, unsigned int nbits, unsigned int maxwidth, bool &overflow);

  /*! \brief get bits in a specific range
   *
   * \param word integer to inspect
   * \param being first bit in range (inclusive)
   * \param end last bit in range (inclusive)
   *
   * \param bits in the requested range
   */
  Int_tf getBitRange(Int_tf word, unsigned int begin,unsigned int end);

  /*! \brief Pad string with 0's
   *
   * \param s reference to string to pad
   * \param ndigits total number of requested characters in string
   */
  void zfill(std::string &s, unsigned int ndigits);

  /*! \brief Convert hex character into a binary string
   *
   * \param h hex character
   *
   * \return binary representation
   */
  std::string getBinFromHexDigit(char h);

  /*! \brief Convert hex string into a binary string
   *
   * \param h hex string
   *
   * \return binary string
   */
  std::string getBinFromHex(const std::string& h);

  /*! \brief Convert a base-10 number to a binary string with trunctuation
   *
   * 1. convert the base 10 string to a hex string (with built-in functions)
   * 2. convert the hex string to binary string
   * 3. truncate or extend string based on nbits
   *
   * \param v input number string
   * \param nbits requested width of output
   * 
   * \return string of width nbits containing bits corresponding to number v
   */
  std::string getSignedBin( const std::string &v , unsigned int nbits );

  /*! \brief Convert a base-10 number to a hex string string with trunctuation
   *
   * \param v input number string
   * \param nbits requested width of binary representation of number
   * 
   * \return hex string corresponding to the input base-10 number string
   */
  std::string getSignedHex(const std::string &v , unsigned int nbits);

  /*! \brief Convert a based-10 number to a hex string of fixed width
   *
   * \param v input number string
   * \param ndigits requested width of output
   *
   * \return hex string of width ndigits corresponding to number v
   */
  std::string getHexStr(Int_tf v, unsigned int ndigits);

  /*! \brief Convert a based-10 number to a bin string of fixed width
   *
   * \param v input number string
   * \param ndigits requested width of output
   *
   * \return binary string of width ndigits corresponding to number v
   */  
  std::string getBinStr(Int_tf v, unsigned int ndigits);
}

#endif // AUXUTILS_H_

//////////////////////////////////////////////////////
// Header file containing the declaration of the 
// functions used to access the aux board
///////////////////////////////////////////////////////

#ifndef FTK_AUX_H
#define FTK_AUX_H

#include <vector>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include <bitset>
#include "stdio.h"
#include "stdlib.h"
#include <unistd.h>
#include <math.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "ftkcommon/patternbank_lib.h"
#include "ftkvme/VMEInterface.h"
#include "ftkvme/VMEManager.h"
#include "aux/aux_vme_regs.h"

#define PERIOD       120000
#define FREQUENCY 150000000

using std::cout;  using std::endl; using std::setw; using std::cerr; using std::bitset; 
using std::vector;

namespace daq 
{
  namespace ftk
  {
    // Constants for titles
    extern const std::string FPGATitles[6];

    /*! \brief Routine to load an Tx FIFO.
     *
     * Loads a Tx FIFO with data using the contents of a file.
     *
     * \param slot The slot that the card is plugged into.
     * \param fpga The FPGA to connect to
     * \param buffer The address of the Tx FIFO
     * \param file Text file with data to load.
     *
     * \return True on success, otherwise false (ie: VME error).
     */
    

    bool aux_load_tx_fifo( u_int slot, u_int fpga, u_int buffer, const std::string& file);

    void aux_read( u_int slot, int fpga, int regaddr);

    /*! \brief Load SSMap RAM
     *
     * \param slot The slot that the card is plugged into.
     * \param fpga The FPGA to connect to
     * \param address Address of SSMap RAM
     * \param ssmappath Path to input ssmap contents
     * \param isModule True when loading the module ssmap
     *
     * \return True on success, false otherwise
     */
    bool aux_ssmap_load( u_int slot, u_int fpga, u_int address, const std::string& ssmappath, bool isModule);

    /*! \brief Load TF constants RAM
     *
     * \param slot The slot that the card is plugged into.
     * \param fpga The FPGA to connect to
     * \param tfconstpath Path to file listing all sector files
     *
     * \return True on success, false otherwise
     */
    bool aux_tfconst_load( u_int slot, u_int fpga, const std::string& tfconstpath, int sector, bool checksum, bool dump, bool loadAllSectors); 

    /*! \brief Barrel shifting constants with bitwise operations.
     *
     * \param input The input value.
     * \param shift Number of bits to shift the input.
     * \param width The number of bits the constant should be.
     * \param sector Sector number (-1 if using old file format).
     *
     * \return Shifted and truncated output
     */
    signed long long aux_asr(signed long long input , int shift, int width);

    bool aux_ammap_load( u_int slot, u_int fpga, const std::string& ammappath, u_int npatterns, 
			 unsigned int nPatternsPerChip, unsigned int banktype, unsigned int SSOffset,unsigned int InChipOff, bool loadAllSectors, bool debug,
			 FTKPatternBank* b, uint bankChecksum, bool checksum = false);
    bool aux_ammap_load_hex( u_int slot, u_int fpga, const std::string& ammappath, bool debug);
    bool aux_ammap_convert(std::string ammappath, u_int npatterns, bool debug, u_int InChipOff = 1, bool loadAllSectors = false);

    /*! \brief Read AUX spybuffers and dump them to text.
     *
     * Reads a set of AUX spybuffers from one FPGA and prints them to the 
     * std_out. The last word written into the spybuffer is always shown at the bottom.
     *
     * \param slot The slot that the card is plugged into.
     * \param fpga The FPGA to connect to
     * \param dim_buffer The number of words to read from the buffer. Set to 0 to read everything.
     * \param buffer A list of buffer addresses (ie: 0x1, 0x2...) to read.
     * \param root ?
     * \param output Path to textfile to dump spy buffer into. Set to empty for no file.
     *
     * \return True on success, otherwise false (ie: VME error).
     */ 
    bool aux_read_buffer( int slot, int fpga, u_int dim_buffer, std::vector<u_int> buffer, const std::string& output, const std::string& source);

    // Remote update of an FPGA on the AUX card 
    bool aux_read_epcq( int slot, int fpga, uint epcqaddress, const std::string& file);
   
    // Reconfigure the AUX card using firmware at a specific address in the EPCQ.
    bool aux_reconfigure( int slot, int fpga, uint epcqaddress);

    // Reset all of the registers and clocks in the AUX 
    bool aux_reset(int slot, int fpga, bool doMain, bool doXcvr, bool doSync, bool doReg, bool doErr, bool doTxFIFO, bool doSpybuffers, bool doFreeze, bool doErrLatch);
    // Insert here library functions
    bool aux_reset(int slot, int fpga, bool doMain, bool doXcvr, bool doSync, bool doReg, bool doErr, bool doTxFIFO, bool doSpybuffers, bool doFreeze, bool doErrLatch);

    bool aux_reset_amb(int slot);

    bool aux_read_buffer( int slot, int fpga, u_int dim_buffer, std::vector<u_int> buffer, const std::string& output = "", const std::string& source = "vme");
    bool aux_load_tx_fifo(u_int slot, u_int fpga, u_int buffer, const std::string& file);
    bool aux_ssmap_load( u_int slot, u_int fpga, u_int address, const std::string& ssmappath, bool isModule);
    bool aux_ammap_convert_old(std::string ammappath, u_int npatterns, bool debug, bool loadAllSectors);
    bool aux_ammap_convert(std::string ammappath, u_int npatterns, bool debug, u_int InChipOff, bool loadAllSectors);
    bool aux_ammap_load_hex( u_int slot, u_int fpga, const std::string& ammappath, bool debug);
    bool aux_ammap_load( u_int slot, u_int fpga, const std::string& ammappath, u_int npatterns,
                         unsigned int nPatternsPerChip, unsigned int banktype, unsigned int SSOffset, unsigned int InChipOff, bool loadAllSectors, bool debug,
                         FTKPatternBank* b, uint bankChecksum, bool checksum);
    bool aux_ammap_load_rc( u_int slot, u_int fpga, const std::string& ammappath, u_int npatterns,
                         unsigned int nPatternsPerChip, unsigned int banktype, unsigned int SSOffset, unsigned int InChipOff, bool loadAllSectors, bool debug,
                         FTKPatternBank* b, uint bankChecksum);
    signed long long aux_asr(signed long long input , int shift, int width);
    bool aux_tfconst_load(u_int slot, u_int fpga, const std::string& gcon, int SECTOR, bool checksum, bool dump, bool loadAllSectors);
    bool aux_tfconst_check( u_int slot, u_int fpga, const std::string& tfconstpath );
    bool aux_vmeblocktest( u_int slot, u_int fpga, u_int address, u_int nWords );

    // Performe remote update of an FPGA on the AUX card
    int aux_upgrade_wait_for_done(int handle);
    int aux_upgrade_clear_regs(int handle);
    bool aux_upgrade( int slot, int fpga, uint epcqaddress, std::string file, uint startaddress, uint endaddress);

    // ??
    bool aux_vmetest( int slot, int fpga, u_int addr, u_int value);

    // ??
    bool vme7700write( int slot, int fpga, int regaddr, int value);

    // Information about a FIFO
    struct aux_fifostatus_t
    {
      u_int size;
      u_int usedw;
      float usedw_avg;
      bool  hold;
      bool  empty;
      bool  ovrflw;
      float holdRate;
    };
    aux_fifostatus_t aux_fifostatus(VMEInterface *vme, u_int addr, u_int period);

    struct aux_linkstatus_t
    {
      aux_fifostatus_t fifo;
      aux_fifostatus_t l1idsync_fifo;
      bool             up;
      u_int            nPackets;
      u_int rising_hold_count;
      //u_int            ratePackets;
      float            ratePackets;
      u_int            l1id;
      u_int            errors;
      float            start_rate;
    };
    aux_linkstatus_t aux_load_linkstatus(VMEInterface *vme, u_int addr, u_int ch, u_int upaddr, u_int upbit);
    
    // Just for testing purpose. TODO: remove it
    void dummy();

      

  }//namespace ftk
}//namespace daq

#endif

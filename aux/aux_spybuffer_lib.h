//////////////////////////////////////////////////////
// Header file containing the declaration of the 
// functions used to parse dumped aux spybuffers
///////////////////////////////////////////////////////

#ifndef FTK_AUX_SPYBUFFER_LIB_H
#define FTK_AUX_SPYBUFFER_LIB_H

#include <vector>
#include <map>
#include <string>

namespace daq 
{
  namespace ftk
  {
    /*! \brief Load spybuffers stored in one file
     *
     * \param data Map containing the vectors that will be filled with the spybuffer contents
     * \param buffs List of spybuffer addresses in thefile
     * \param file_name Path to the file to be parsed
     */
    void aux_get_from_file(std::map<u_int, std::vector<u_int> > &data, std::vector<u_int> buffs, const std::string& file_name, bool debug);

    /*! \brief Initialize the structure for all Aux spybuffers
     *
     * \param data An empty map that will be filled with spybuffer vectors.
     */
    void aux_buffers_build_data_structure(std::map<u_int, std::map<u_int, std::vector<u_int> > > &data);

    /*! \brief Load all Aux spy buffers contents
     *
     * \param data Map with empty spybuffer vectors
     * \param inDirName Directory containing an Aux spybuffer dump
     */
    void aux_buffers_download_data(std::map<u_int, std::map<u_int, std::vector<u_int> > > &data, const std::string& inDirName, bool debug);

  }//namespace ftk
}//namespace daq

#endif

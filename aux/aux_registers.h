#ifndef AUX_REGISTERS
#define AUX_REGISTERS

#include <map>
#include "aux/aux_card.h"
#include "aux/aux_vme_regs.h"

namespace daq {
namespace ftk {

class aux_registers {

    public:
        aux_registers();
        ~aux_registers();

        // Get all register addresses published to and read from IS
        std::map<std::string, std::map<std::string,uint> > getISRegs();

    private:
        // Get the register addresses of all link information. The LinkInfoString is one of the strings below in the m_LinkInfoKey
        void getLinkRegs(std::string LinkInfoString, std::map<std::string,std::map<std::string,uint> > &LinkRegs);

        // The addressing of the quantities inside the link status blocks
        std::map<std::string, uint> m_LinkInfoKey = {{"NumPackets",       0x0<<4}, // Total number of packets on this link
                                                     {"EventRate",        0x1<<4}, // Number of events in the last sampling period
                                                     {"L1ID",             0x2<<4}, // Most recent L1ID
                                                     {"Errors",           0x3<<4}, // Link error bits
                                                     {"FifoNumHolds",     0x4<<4}, // Number of clock cycles with hold high in the last sampling period
                                                     {"FifoUsageAv",      0x5<<4}, // Sum of fifo usage each clock cycle in last sampling period
                                                     {"FifoUsage",        0x6<<4}, // Current FIFO usage. 31..16: FIFO Size; 15: Current hold; 14 downto 0: Current occupancy
                                                     };
};

}
}

#endif

#ifndef AUXMATH_H_
#define AUXMATH_H_

#include <assert.h>
#include <algorithm>
#include <limits>
#include <cmath>

/*
 *
 * Some useful mathematical functions based on TMath.h from ROOT
 *
 */

// Define integer used in TF math
typedef int64_t Int_tf;

namespace auxmath
{
  /*! \brief Round to nearest integer.
   *
   * Rounds half integers to the nearest even integer.
   *
   * \param x input number
   *
   * \return rounded number
   */
  template<typename T>
    inline int Nint( T x )
    {
      int i;
      if (x >= 0)
	{
	  i = int(x + 0.5);
	  if ( i & 1 && x + 0.5 == T(i) ) i--;
	}
      else
	{
	  i = int(x - 0.5);
	  if ( i & 1 && x - 0.5 == T(i) ) i++;
	}
      return i;
    }

  /*! \brief Round down a number
   *
   * \param x input number
   *
   * \return floored number
   */
  inline int FloorNint(double x)
  { return Nint( floor(x) ); }

  /*! \brief Take 2 to some power
   * 
   * \param n power to take 2 to
   *
   * \return 2^n
   */
  inline unsigned int Powerof2(unsigned int n)
  {
    return (1<<n);
  }
}

#endif // AUXMATH_H_

#ifndef AUX_CARD_H
#define AUX_CARD_H


#include "aux/aux_proc.h"
#include "aux/aux_inputs.h"
#include "aux/aux_vme_regs.h"
#include "aux/AuxSpyBuffer.h"

#include <config/ConfigObject.h>
#include <config/Configuration.h>

#include <dal/OnlineSegment.h>
#include <dal/ResourceSet.h>
#include <dal/Segment.h>
#include <dal/Partition.h>

#include <dal/seg-config.h>
#include <dal/util.h>

#include "DFdal/ReadoutConfiguration.h"
#include "RunControl/Common/OnlineServices.h"
#include "ftkcommon/dal/PatternBankConf.h"

//#include "aux/dal/ReadoutModule_Aux.h"
#include "ProcessingUnit/dal/ReadoutModule_PU.h"
#include "aux/dal/firmware.h"
#include "aux/dal/AuxConstants.h"
#include "aux/dal/AuxTestParams.h"

#include "ftkcommon/FtkEMonDataOut.h"
#include "ftkcommon/SourceIDSpyBuffer.h"
namespace daq
{
  namespace ftk
  {

    class aux
    {

    public:
      aux(unsigned int slot, unsigned int crate);
      virtual ~aux() noexcept; // Destructor

      /*! \brief return object for processor x
       *
       * If an valid processor ID is specified (outside of range 1 to 4), then
       * the AuxInvalidProcID exception is thrown.
       *
       * \param Processor index (range 1 to 4)
       *
       * \return Processor object
       */
      std::shared_ptr<proc> p(unsigned int x);

      //// Run Control FSMs ////

      /*! \brief setup transition
       *
       * Retrieve and store the configuration parameters from OKS.
       */
      void setup(const dal::ReadoutModule_PU*);


      /*! \brief configure transition
       *
       * 1. Check board access:
       *  – read a checkword via VME/IPBus
       * 2. Check board & fw versions
       *  – wrt OKS configuration
       * 3. Disable communication to downstream
       *  – prevent data for being sent and ignore HOLD
       * 4. Disable communication from upstream
       *  – prevent HOLD to upstream and ignore data
       * 5. Setup high speed links
       *  – so that all TX links are sending IDLE
       */
      void configure();

      void checkConfig();


      /*! \brief connect transition
       *
       * 1.Check that all RX links are receiving IDLE and are
       *   aligned (via register)
       *  - if an RX is not aligned, reset it and
       *    repeat RX check until
       *   - transition timeout expiration (via oks)
       *   - or 3 attempts
       *  - if failed, raise a warning or error
       * NB: if the method does not return before timeout
       * expiration, the RC put the component in error state
       * and prevent the transition completion
       *
       * 2. Configure all ?configuration registers? and 
       *    configuration RAMs LUTs
       */
      void connect();

      //user command to reset links to AMB if AMB has alignment issues
      //void reset_links_to_amb();

      /*! \brief prepareForRun transition
       *
       * 1. Reset
       *  – all FIFOs and FSMs
       *  – all spy buffers and error registers
       *  – all IS variables and histograms
       * 2. Enable communication from upstream
       *  – enable sending HOLD upstream
       *  – start accepting data from upstream
       * 3. Enable communication to downstream
       *  – comply with HOLD from downstream
       *  – allow data to be sent downstream
       */
      void prepareForRun();
      void stop();
      void publishFullStats();

      void loadAndSendEvent();

      //// Useful functions ////
      void reset_all();
      void reset_main();
      void reset_sync();
      void reset_spybuffers();
      void reset_tf();

      /*! \brief Load all RAMs on the FPGA
       */
      void set_checksumWarning(bool warningOnly);
      //bool check_link_status();
      void load_rams();
      void enable_hitsort(bool enable);

      void load_tvs();
      void send_tvs();

      void enable_rslb();

      void check_links();

      void set_hold_ignore_proc(bool ignore);
      void set_force_hold_proc_out(bool force);
      void enable_proc_to_input_freeze(bool enable);
      void set_hold_ignore_amb(bool ignore);
      void set_hold_ignore_hw(bool ignore);
      void set_hold_ignore_ssb(bool ignore);
      void set_ignore_freeze(bool ignore);
      void set_ignore_SSB_freeze(bool ignore);
      void set_freeze_mask(u_int ProcMask, u_int I1Mask, u_int I2Mask, u_int HWMask);
      void set_MaxFitsPerRoad(u_int MaxFitsPerRoad);
      void set_MaxRoadsPerEvent(u_int MaxRoadsPerEvent);

      void enable_amb_freeze(bool enable);

      //// Settings ////
      void set_loop        (bool b);
      void set_FTKPatternBank();
      void set_am_constants(const string& s);
      void set_NPattPerChip(const unsigned int n);
      void set_banktype(const unsigned int t);
      void set_ssoffset(const bool o);
      void set_InChipOff(const unsigned int c);

      void set_loadAllSectors(int pidx, const bool l);
      void set_tf_constants(int pidx, const string& s, unsigned int cs);

      void set_ss_map_path (int layer, const string& s, unsigned int cs);
      void set_module_path (int layer, const string& s, unsigned int cs);

      void set_road_path   (const std::string& s);
      void set_hit_path    (const std::string& s);
      void set_ss_path     (const std::string& s);

      /*! \brief Set source of hits
       *
       * Possible values: 
       *  df: Input from the QSFPs (aka DF)
       *  ssmap: Input from FIFOs before the SSMap
       *  inptx: TxFIFOs on the lines going from the processors
       *
       * \param s String describing the source of hits
       */
      void set_source_hits (std::string s);

      /*! \brief Set source of roads using a mask
       *
       * Bit X set to 1 means use AMB input.
       *
       * \param mask Mask determining AMB input
       */
      void set_source_roads(u_int mask);

      std::vector<u_int> get_clock_freqs();
      std::vector<u_int> get_periods();

      //name for ERS log
      std::string name_ftk() { return m_name; }

    private:

      // Settings
      uint m_slot;
      uint m_crate;
      
      bool m_loop;
      std::string m_hit_path, m_ss_path, m_road_path;
      std::string m_source_hits;
      u_int m_source_roads;

      // Objects
      std::shared_ptr<input1>     m_i1;
      std::shared_ptr<input2>     m_i2;
      std::vector<std::shared_ptr<proc>>   m_procs;

      std::vector<std::shared_ptr<aux_interface>> m_fpgas;
      std::vector<std::shared_ptr<aux_interface>> m_fpgas_procsFirst;
      std::vector<std::shared_ptr<aux_interface>> m_aux_interface_inputs;
      std::vector<std::shared_ptr<aux_interface>> m_aux_interface_procs;


      std::unique_ptr<FTKPatternBank> m_FTKPatternBank;

      // Hold configurations
      bool m_holdIgnoreProc;
      bool m_forceHoldProcOut;
      bool m_holdIgnoreAMB;
      bool m_holdIgnoreHW;
      bool m_holdIgnoreSSB;
      bool m_allowFreezeFromProcsToInputs;
      bool m_ignore_freeze;
      bool m_ignore_SSB_freeze;

      bool m_enableAMBFreeze;

      bool m_enable_hitsort;

      std::string     m_name;
      u_int m_ProcFreezeMask;
      u_int m_I1FreezeMask;
      u_int m_I2FreezeMask;
      u_int m_HWFreezeMask;

      u_int m_MaxFitsPerRoad;
      u_int m_MaxRoadsPerEvent;


      uint m_towerID;

      daq::ftk::FtkEMonDataOut                *m_ftkemonDataOut;
    };

  }
}


#endif

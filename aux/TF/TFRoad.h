#ifndef TFROAD_H_
#define TFROAD_H_

#include <iostream>
#include <vector>
#include <map>


#include "aux/TF/TFGen.h"
#include "aux/TF/TFCombination.h"
#include "aux/Report.h"

namespace daq {
  namespace ftk {
    /*! \brief A road consisting of several combinations/fits
     */
    class TFRoad
    {
    public:
      /*! \brief Create an empty road
       */
      TFRoad();

      /*! \brief Destructor, does nothing
       */
      ~TFRoad();

      /*! \brief add hit to layer
       *
       * \param ilayer layer index
       * \param v hit word
       */
      void addHit(unsigned int ilayer,Int_tf v);

      /*! \brief set road ID
       *
       * \param v new road ID
       */  
      void setID(int v);

      /*! \brief set sector ID
       *
       * \param v new sector ID
       */  
      void setSectorID(unsigned int v);

      /*! \brief set layermap
       *
       * \param v new layermap
       */
      void setLayermap(unsigned int v);

      /*! \brief set guessmap
       *
       * \param v new guessmap
       */
      void setGuessmap(unsigned int v);
      
      /*! \brief Add a combination
       *
       * \param comb combination to add to the road
       */
      void addCombination(TFCombination *comb);

      /*! \brief get road ID
       *
       * \return road ID
       */
      int getID() const;

      /*! \brief get sector ID
       *
       * \return sector ID
       */  
      unsigned int getSectorID() const;

      /*! \brief get layermap
       *
       * \return layermap
       */    
      unsigned int getLayermap() const;

      /*! \brief get guessmap
       *
       * \return guessmap
       */    
      unsigned int getGuessmap() const;
      
      /*! \brief get the number of combinations
       *
       * \return number of combinations in the road
       */
      unsigned int getNCombinations() const;

      /*! \brief get a combination associated with the road
       *
       * \param i index of combination
       *
       * \return combination at index i
       */
      TFCombination* getCombination(unsigned int i);

      /*! \brief generate combinations
       *
       * Generate combinations from the list of hits added.
       */
      bool generateCombinations(bool debug);

      /*! \brief print debugging information
       */
      void print();

      /*! \brief print debugging information of print
       */  
      void printCombinations();

    private:

      int id;
      unsigned int sectorid;
      unsigned int layermap;
      unsigned int guessmap;
      std::vector<Int_tf> Hits[tf::NLayers];

      std::vector<TFCombination*> vec_combinations;
    };
  } // namespace ftk
} // namespace daq
#endif // TFROAD_H_

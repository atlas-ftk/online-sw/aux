#ifndef _TFEVENT_H_
#define _TFEVENT_H_

#include <iostream>
#include <vector>
#include <map>

#include "aux/TF/TFGen.h"
#include "aux/TF/TFRoad.h"
#include "aux/AuxString.h"
#include "aux/Report.h"

namespace daq {
  namespace ftk {
    /*! \brief Event consisting of multiple roads
     */

    class TFEvent
    {
    public:

      /*! \brief Constructor, does nothing
       */
      TFEvent();
      /*! \brief Destructor, does nothing
       */  
      ~TFEvent();

      /*! \brief Set event ID
       *
       * \param v New event ID
       */
      void setID(int v);

      /*! \brief Get the event ID
       *
       * \return the event ID
       */
      int getID() const;


      /*! \brief Add a road to the event
       *
       * \param road road to add
       */
      void addRoad(TFRoad *road);

      /*! \brief Retreive a road from the event
       *
       * \param i index of the road
       *
       * \return The road at index i
       */
      TFRoad* getRoad(unsigned int i);

      /*! \brief Get number of roads.
       *
       * \return Number of roads in the event
       */
      unsigned int getNRoads() const;

      //
      // create and return a vector of the passing tracks
      // (as pointers to TFCombination objects)
      //
      std::vector<TFCombination*> getTrackVector();

      /*! \brief Compare tracks with another event
       *
       * Loop over all the tracks in this event. Check if each track exists in
       * the "other" event. If all the tracks exist in the "other" event then
       * return true.
       *
       * A check is made to ensure that a track in other is not matched
       * to multiple tracks in this.
       *
       * \param matched_tracks set to the number of tracks matched
       * \param total_tracks set to the total number of tracks in this event
       *
       * \return true if all tracks are present in the other event
       */
      bool compareTracks(TFEvent *other, unsigned int &matched_tracks, unsigned int &total_tracks, bool split_sectors );

      /*! \brief Print all roads in the event.
       */
      void printRoads();

      /*! \brief Print all roads in the event and their combinations.
       */  
      void printRoadsAndCombinations();

    private:
  
      int id;

      // Road objects - instantiated using raw info
      std::vector<TFRoad*> vec_roads;  
    };
  } // namespace ftk
} // namespace daq
#endif

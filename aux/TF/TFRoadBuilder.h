#ifndef TFROADBUILDER_H_
#define TFROADBUILDER_H_

#include "aux/TF/TFRoad.h"

#include <vector>

namespace daq {
  namespace ftk {
    /*! \brief Build roads from raw input
     * 
     * A helper class to build roads from raw input which contains unsyncrhonized
     * stream of hits, road IDs, sector IDs and layermap.
     */
    class TFRoadBuilder
    {
    public:
      /*! \brief Constructor, does nothing
       */
      TFRoadBuilder();

      /*! \brief Destructor, does nothing
       */
      ~TFRoadBuilder();

      /*! \brief Add a new sector ID
       *
       * \param sectorID the sector ID of the road
       */
      void addSectorID(unsigned int sectorID);

      /*! \brief Add a new road ID
       *
       * \param roadID the road ID
       */  
      void addRoadID(unsigned int roadID);

      /*! \brief Add a new layermap
       *
       * \param layermap the layermap for the road
       */  
      void addLayermap(unsigned int layermap);

      /*! \brief Add a new guessmap (i.e. LiveLayers from DO)
       *
       * \param guessmap the guessmap for the road
       */  
      void addGuessmap(unsigned int guessmap);
      
      /*! \brief Add hit word
       *
       * \param ilayer the layer of the hit
       * \param hit The hit coordinates
       * \param eor end of road bit (high for last hit in a road
       */
      void addHitWord(unsigned int ilayer, Int_tf hit, bool eor);

      /*! \brief Generate the roads
       * 
       * Generates the roads using the preloaded information. The
       * combinations are also created automaticall. All of the
       * internal data structures are cleared at the end.
       *
       * \return The list of generated roads.
       */
      std::vector<TFRoad*> generateRoads();

      /*! \brief Print contents of builder
       */
      void print();

    private:
      std::vector<unsigned int> _SectorIDs;
      std::vector<unsigned int> _RoadIDs;
      std::vector<unsigned int> _Layermaps;
      std::vector<unsigned int> _Guessmaps;

      std::vector<tf::HitWord> _HitWords[tf::NLayers];

    };
  } // namespace ftk
} // namespace daq
#endif // TFROADBUILDER_H_

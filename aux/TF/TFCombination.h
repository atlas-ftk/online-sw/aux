#ifndef _TFCOMBINATION_H_
#define _TFCOMBINATION_H_

#include <iostream>
#include <vector>
#include <map>

#include "aux/TF/TFGen.h"
#include "aux/AuxMath.h"

namespace daq {
  namespace ftk {
    /*! \brief Single combination consisting of a set of 1 hit per layer.
     *
     * This is the elementary object used in fitting. Each object has its own chi-square value
     */
    class TFCombination
    {
    public:

      /*! \brief Initialize empty combination
       */
      TFCombination();

      /*! \brief Initialize by making a shallow copy of anothre combination
       *
       * \param other source combination
       */
      TFCombination( TFCombination *other );

      /*! \brief Destructor, does nothing
       */
      ~TFCombination();

      /*! \brief Compare with another combination
       *
       * Combinations are defined as equal if they have the same
       *  - road id
       *  - sector id
       *  - layermap
       *  - guessmap
       *  - chi squared
       *  - hits
       *
       * \param other combination to compare against
       *
       * \return True if the combinations are equal
       */
      bool operator==( const TFCombination &other ) const;
      bool operator <( const TFCombination &other ) const;

      /*! \brief Set combation ID
       *
       * \param v new combination ID
       */
      void setID(int v);

      /*! \brief Set road ID
       *
       * \param v new road ID
       */
      void setRoadID(int v);

      /*! \brief Set sector ID
       *
       * \param v new sector ID
       */
      void setSectorID(unsigned int v);
  
      /*! \brief Set layermap
       *
       * \param v new layermap
       */
      void setLayermap(unsigned int v);

      /*! \brief Set guessmap
       *
       * \param v new guessmap
       */
      void setGuessmap(unsigned int v);
      
      /*! \brief Set hit in layer
       *
       * \param ilayer layer number
       * \param val hit value
       */
      void setHit(unsigned int ilayer, Int_tf val );

      /*! \brief set coordinate
       *
       * \param icoord coordinate ID
       * \param val new coordinates
       */
      void setCoord(unsigned int icoord, Int_tf val );
  
      /*! \brief Set chi2
       *
       * \param v chi2
       */
      void setChiSquare(Int_tf val);
  
      /*! \brief Set overflow bit
       *
       * \param v new overflow value
       */
      void setOverflow(bool v);

      /*! \brief set wheter track passes chi2 cut
       *
       * \param v new value
       */
      void setPasses(bool v);

      /*! \brief Set nominal chi2
       *
       * \param val new nominal chi2
       */
      void setChiSquareNominal(Int_tf val);
  
      /*! \brief Set nominal overflow bit
       *
       * \param v new overflow value
       */
      void setOverflowNominal(bool v);

      /*! \brief Set recovery chi2
       *
       * \param ilayer layer number
       * \param val new recovery chi2
       */
      void setChiSquareRecovery(unsigned int ilayer, Int_tf val);

      /*! \brief Set recovery overflow bit
       *
       * \param ilayer layer number
       * \param val new overflow value
       */
      void setOverflowRecovery(unsigned int ilayer, bool v);

      /*! \brief Get combination ID
       *
       * \return combination ID
       */
      int getID() const;

      /*! \brief Get road ID
       *
       * \return road ID
       */
      int getRoadID() const;

      /*! \brief Get sector ID
       *
       * \return sector ID
       */
      unsigned int getSectorID() const;

      /*! \brief Get layermap
       *
       * \return layermap
       */
      unsigned int getLayermap() const;

      /*! \brief Get guessmap
       *
       * \return guessmap
       */
      unsigned int getGuessmap() const;

      /*! \brief Get a corrected layermap with 1's for dead layers
       *
       * \return Layermap that only has zero's for live layers with no hits
       */
      unsigned int getLiveLayermap() const;
      
      /*! \brief Get hit for in a layer
       *
       * \param layer layer of interest
       *
       * \return hit value
       */
      Int_tf getHit(unsigned int ilayer) const;

      /*! \brief Get coordinate
       *
       * \param icoord coordinate index
       *
       * \return coordinate value
       */
      Int_tf getCoord(unsigned int icoord) const;

      /*! \brief Get chi2
       *
       * \return chi2
       */
      Int_tf getChiSquare() const;

      /*! \brief Get overflow value
       *
       * \return overflow value
       */
      bool getOverflow() const;

      /*! \brief Get passes value
       *
       * \return pass value
       */
      bool getPasses() const;

      /*! \brief Get nominal chi2
       *
       * \return nominal chi2
       */
      Int_tf getChiSquareNominal() const;

      /*! \brief Get nominal overflow value
       *
       * \return nominal overflow value
       */
      bool getOverflowNominal() const;

      /*! \brief Get recovery chi2
       *
       * \param ilayer layer of interest
       * 
       * \return recovery chi2
       */
      Int_tf getChiSquareRecovery(unsigned int ilayer) const;

      /*! \brief Get recovery overflow value
       *
       * \param ilayer layer of interest
       *
       * \return recovery overflow value
       */
      bool getOverflowRecovery(unsigned int ilayer) const;

      /*! \brief Determine whether a recovery fit was attempted
       *
       * \return true if a recovery fit was attempted
       */
      bool recoveryAttempted() const;

      /*! \brief Get scaled chi2
       *
       * \return scaled chi2
       */
      double getScaledChiSquare() const;

      /*! \brief Get scaled nominal chi2
       *
       * \return scaled nominal chi2
       */  
      double getScaledChiSquareNominal() const;

      /*! \brief Get scaled recovery chi2
       *
       * \param ilayer layer of interest
       *
       * \return scaled recovery chi2
       */    
      double getScaledChiSquareRecovery(unsigned int ilayer) const;

      /*! \brief Converts hit words into hit coordinates
       */
      void extractCoordinates();

      /*! \brief Set pixel hit word given the guessed coordinate values
       *
       * \param ilayer layer of interest
       * \param coord0 dimension 0 of hit
       * \param coord1 dimension 1 of hit
       */
      void constructPixHit(unsigned int ilayer, Int_tf coord0, Int_tf coord1);

      /*! \brief Set sct hit word given the guessed coordinate values
       *
       * \param ilayer layer of interest
       * \param coord coordinate
       */
      void constructSctHit(unsigned int ilayer, Int_tf coord);

      /*! \brief Print information about this combination
       */
      void print() const;

    private:

      int id;
      int roadid;
      unsigned int sectorid;
      int layermap;
      int guessmap;

      Int_tf Hits[tf::NLayers];
      Int_tf Coords[tf::NCoords];

      bool overflow;
      bool passes;
      Int_tf ChiSquare;

      // extra vars for keeping track of the different ChiSquare values when performing majority recovery
      bool overflow_nominal;
      bool overflow_recovery[tf::NLayers];
      Int_tf ChiSquare_nominal;
      Int_tf ChiSquare_recovery[tf::NLayers];
    };
  } // namespace ftk
} // namespace daq
#endif

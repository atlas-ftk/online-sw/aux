#ifndef TFGEN_H_
#define TFGEN_H_

#include "aux/AuxMath.h"
#include <vector>
#include <string>
/*
 *
 * Define all the general specs for the TF in this file.
 * This should match the specs defined in the firmware in TF/project/TFGen.vhdl
 *
 */

namespace tf
{

  // 
  // Basic configuration
  //
  const unsigned int NLayers = 8;
  const unsigned int NPixLayers = 3;
  const unsigned int NSctLayers = 5;
  const unsigned int NCoords = 11;
  const unsigned int NPixCoords = 6;
  const unsigned int NSctCoords = 5;
  const unsigned int NStreams = 11;
  const unsigned int NPixStreams = 6;
  const unsigned int NSctStreams = 5;
  const unsigned int NChiComponents = 6;
  const unsigned int NHelixParameters = 5;
  const unsigned int NConstantsPerSector = 86;

  //
  // Offsets used on board for constants and hits
  // In powers of 2
  //
  const unsigned int HitOffset = 3;
  const unsigned int SConstOffset = 12;
  const unsigned int HConstOffset = 4;

  //
  // Bit shifts used for calculations on the board
  // In negative powers of 2 (negative because we always downshift)
  //
  const unsigned int ChiShift = 2;
  const unsigned int ChiPartialShift = 2;
  const unsigned int TShift = 16;
  const unsigned int GuessedChiShift = 2;
  const unsigned int MissingHitShift = 22;

  //
  // Max widths for words used in calculation
  //
  const unsigned int ConstWidth = 18;
  const unsigned int GuessFracWidth = 12;
  const unsigned int GuessExpWidth = 5;
  const unsigned int ChiWidth = 27;
  const unsigned int RoadWidth = 22;
  const unsigned int SectorWidth = 12;
  const unsigned int PixLayerWidth = 29;
  const unsigned int SctLayerWidth = 14;

  // 
  // Chi-square cut values
  //
  const Int_tf PixChiSquareCut   = 0b0000001000000000000000000000000000000;
  const Int_tf SctChiSquareCut   = 0b0000001010000000000000000000000000000;
  const Int_tf TightChiSquareCut = 0b0000001100000000000000000000000000000;
  const Int_tf LooseChiSquareCut = 0b1000000000000000000000000000000000000;
  
  

  //
  // Structures
  //
  struct HitWord
  {
    Int_tf hit;
    bool eor;
    HitWord(Int_tf _hit, bool _eor)
      : hit( _hit )
      , eor( _eor )
    {}
    HitWord()
      : hit(0)
      , eor(0)
    {}
  };

  /*! \brief get size of biggest vector in an array
   *
   * Return the size of the largest vector in the array of vectors; one vector per layer
   *
   * \param v input array of vectors
   *
   * \return size of biggest vector in array
   */
  template<typename ObjT> std::size_t getMaxPerLayer(const std::vector<ObjT> v[NLayers] )
    {
      std::size_t result = 0;
      for(unsigned int ilayer = 0 ; ilayer < NLayers ; ilayer++)
	{
	  if( v[ilayer].size() > result ) result = v[ilayer].size();
	}
      return result;
    }

  /*! \brief Determine the first missing layer
   *
   * \param layermap the layermap
   *
   * \return Index of the first missing layer, NLayers if not found
   */
  unsigned int getMissingLayer(unsigned int layermap);

  /*! \brief Determine layer number for a coordinate index
   *
   * \param icoord coordinate index
   *
   * \return layer index corresponding to the coordinate index
   */
  unsigned int getLayerFromCoord(unsigned int icoord);

  /*! \brief Get layer index corresponding to stream index
   *
   * This assumes that the stream order is the same as on board.
   * \param istream stream index
   *
   * \return layer index corresponding to the stream index
   */
  unsigned int getLayerFromStream(unsigned int istream );

  /*! \brief Convert streammap to layermap
   *
   * Merge the duplicate pixel streams in the map, and also reverse
   * the order of the bits.
   *
   * \param streammap streammap to convert
   */
  unsigned int getLayermapFromStreammap(unsigned int streammap);

  /*! \brief Convert layermap into a string
   *
   * \param layermap layermap to convert
   *
   * \return binary string represending the layermap
   */
  std::string getLayermapStr(unsigned int layermap);

  /*! \brief Convert hitword into a string
   *
   * \param hitword hitword to convert
   *
   * \return binary string represending the hitword
   */  
  std::string getHitWordStr(const HitWord &hitword);

  /*! \brief Convert hit into a string
   *
   * \param hitword hit to convert
   *
   * \return binary string represending the hit
   */    
  std::string getHitStr(Int_tf hit);

}


namespace doparse
{

  // 
  // Order of words in DO Output file
  //
  enum DOOutput {
    SECTORID = 0 ,
    ROADID = 1 ,
    STREAMMAP = 2 ,
    FIRSTPIXSTREAM = 3 ,
    FIRSTSCTSTREAM = 9 ,
    EOR = 14 ,
    EOE = 15 ,
    VALID = 16 ,
    END = 17
  };

  /*! \brief Convert DO output word to bit position
   *
   * For a given word in the DO output, return the corresponding bit number in the valid word.
   *
   * \param iword input word
   *
   * \return bit position of valid bit
   */
  unsigned int getValidBit(unsigned int iword);
}


#endif // TFGEN_H_

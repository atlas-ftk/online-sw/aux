#ifndef _TFSECTOR_H_
#define _TFSECTOR_H_

#include <iostream>
#include <vector>
#include <map>

#include "aux/TF/TFGen.h"
#include "aux/AuxMath.h"

/* \brief Constains all constants for a given sector
 */

class TFSector
{

public:
  /*! \brief Creates an empty sector.
   * 
   * All constants are set to 0.
   */
  TFSector();

  /*! \brief Destructor, does nothing
   */
  ~TFSector();

  /*! \brief Set sector ID
   *
   * \param _id new id
   */
  void setID(int _id);

  /*! \brief Set sector S
   *
   * \param ichi
   * \param icoord
   * \param val new value
   */
  void setS(int ichi , int icoord , Int_tf val );


  /*! \brief Set sector H
   *
   * \param ichi
   * \param val new value
   */
  void setH(int ichi , Int_tf val );

  /*! \brief set guess frac for pixel
   *
   * \param ilayer layer index
   * \param iconst
   * \param val new value
   */
  void setPixGuessFrac(unsigned int ilayer , int iconst , Int_tf val);

  /*! \brief set guess exp for pixel
   *
   * \param ilayer layer index
   * \param iconst
   * \param val new value
   */
  void setPixGuessExp(unsigned int ilayer , int iconst , Int_tf val);

  /*! \brief set guess frac for sct
   *
   * \param ilayer layer index
   * \param val new value
   */  
  void setSctGuessFrac(unsigned int ilayer , Int_tf val);

  /*! \brief set guess frac for sct
   *
   * \param ilayer layer index
   * \param val new value
   */    
  void setSctGuessExp(unsigned int ilayer , Int_tf val );

  /*! \brief Get sector ID
   *
   * \return sector ID
   */
  int getID() const;

  /*! \brief Get sector S
   *
   * \param ichi
   * \param ihit
   *
   * \return S
   */
  Int_tf getS( int ichi , int ihit ) const;

  /*! \brief Get sector H
   *
   * \param ichi
   *
   * \return H
   */  
  Int_tf getH( int ichi ) const;

  /*! \brief Get pixel guess frac
   *
   * \param ilayer layer index
   * \param iconst
   *
   * \return pixel guess frac
   */
  Int_tf getPixGuessFrac(unsigned int ilayer , int iconst ) const;

  /*! \brief Get pixel guess exp
   *
   * \param ilayer layer index
   * \param iconst
   *
   * \return pixel guess exp
   */  
  Int_tf getPixGuessExp( int ilayer , int iconst ) const;
  
  /*! \brief Get SCT guess frac
   *
   * \param ilayer layer index
   * \param iconst
   *
   * \return SCT guess frac
   */
  Int_tf getSctGuessFrac(unsigned int ilayer ) const;

  /*! \brief Get SCT guess exp
   *
   * \param ilayer layer index
   * \param iconst
   *
   * \return SCT guess exp
   */
  Int_tf getSctGuessExp(unsigned int ilayer ) const;

  /*! \brief Print debugging information
   *
   * dump constants for debugging
   */
  void print();

private:
  
  int id;

  Int_tf SMatrix[tf::NChiComponents][tf::NCoords]; // S_ij matrix
  Int_tf HVector[tf::NChiComponents]; // H_i additive constants

  // Hit-guessing constants are stored as floating point
  Int_tf PixGuessFrac[tf::NPixLayers][3] , PixGuessExp[tf::NPixLayers][3]; // Inverse C Matrix for guessing pixel hits
  Int_tf SctGuessFrac[tf::NSctLayers] , SctGuessExp[tf::NSctLayers]; // Inverse C Matrix for guessing SCT hits


};

#endif

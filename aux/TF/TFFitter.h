#ifndef TFFITTER_H_
#define TFFITTER_H_

#include <iostream>
#include <vector>
#include <map>

#include "aux/TF/TFConstants.h"
#include "aux/TF/TFEventList.h"
#include "aux/TF/TFSector.h"

namespace daq {
  namespace ftk {
    class TFFitter
    {
    public:
      /*! \breif Initialize a track fitter
       *
       * The track fitter does not take overship of the event list
       * and constants map.
       *
       * \param _constants Reference to constants map
       * \param _events List of events to fit
       */
      TFFitter(TFConstants *_constants, TFEventList *_events);

      /*! \brief Destructor, does nothing
       */
      ~TFFitter();

      /*! \brief Calculate chi2 for all events
       *
       * Loop over all combinations
       * For each combination, check to see if there are any missing layers
       * and do the corresponding chi-square calculation
       *
       */
      void calculateChiSquares(bool allSectors, int procID);

      /*! \brief Calculate the pixel chi2 for a combination
       *
       * First guess the mixing Pixel Hits, then compute the ChiSquare
       */
      void calculatePixChiSquare(TFCombination *comb, TFSector *sector);

      /*! \brief Calculate the sct chi2 for a combination
       *
       * First guess the mixing SCT Hits, then compute the ChiSquare
       */
      void calculateSctChiSquare(TFCombination *comb, TFSector *sector);

      /*! \brief Calculate the chi2 for a combination
       *
       * Compute the nominal ChiSquare
       */  
      void calculateNomChiSquare(TFCombination *comb, TFSector *sector);
 
    private:

      TFConstants *constants;
      TFEventList *events;

    };
  } // namespace ftk
} // namespace daq

#endif // TFFITTER_H_


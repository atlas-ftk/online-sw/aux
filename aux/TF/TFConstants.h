#ifndef TFCONSTANTS_H_
#define TFCONSTANTS_H_

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

#include "aux/TF/TFSector.h"

/*! \brief Constants bank for the TrackFitter
 */
class TFConstants
{
public:
  /*! \brief Constructor, does nothing
   */
  TFConstants();

  /*! \brief Delete all loaded sectors
   */
  ~TFConstants();

  /*! \brief Get number of sectors
   *
   * \return number of sectors
   */
  unsigned int getNSectors();

  const float const_scale_map[11] = {1, 16/16.88, 1, 16/16.88, 1, 16/16.88, 1, 1, 1, 1, 1};
  /*! \brief Get sector
   *
   * \param sectorid sector ID
   *
   * \return pointer to the sector, empty sector (not 0) if not found
   */
  TFSector* getSector(unsigned int sectorid);

  /*! \brief load constants map from a file
   *
   * \param path path to the file containing the constants
   */
  void buildFromFile(const std::string& path );
  
  /* load constants from a gcon file
   *
   * Based off of buildFromFile and aux_tfconst_load.cxx and convert_gcon_for_aux.py
   */
  void buildFromGCON(const std::string& path );
  
  /*get significand and exponent from floating */
  std::pair<int, int> getFloating(double input);
  
  /* sets offsets */
  int getSigned(double d, int offset);


private:
  std::vector<TFSector*> vec_sectors;
  TFSector *empty_sector;
};

#endif

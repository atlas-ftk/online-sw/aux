#ifndef AUX_EPCQUPGRADE_H_
#define AUX_EPCQUPGRADE_H_

#include "aux/AUX_FWMap.h"

#include "ftkvme/VMEInterface.h"

namespace daq 
{
  namespace ftk 
  {
    class AUX_EPCQUpgrade
    {
    public:
      AUX_EPCQUpgrade(const AUX_EPCQUpgrade&);
      AUX_EPCQUpgrade(uint slot, uint fpga, AUX_FWMap map);

      bool isDone() const;

      void wait_for_done();
      void reset_control();
      void clear_buffer();
      void sector_erase();
      void write_init(uint epcqaddress);
      void write_page();
      void write_deinit();

    private:
      VMEInterface *_vme;
      uint _fpga;
      AUX_FWMap _map;

      // upgrade fsm
      std::ifstream _fh;
      bool _firstOp;
      bool _done;
      uint _nextaddr;
      //      uint nPercent;
      uint _bytesread;
      uint _epcqaddress;
    };
  }
}

#endif // AUX_EPCQUPGRADE_H_

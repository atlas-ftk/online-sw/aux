#ifndef AUX_SPYBUFFER_PARSERS_H_
#define AUX_SPYBUFFER_PARSERS_H_

#include "aux/SpyBuffer/EventFragmentDOOutput.h"

#include <vector>

namespace daq {
  namespace ftk {
    std::vector<EventFragmentDOOutput> aux_spybuffer_parse_dooutput(const std::vector<unsigned int>& data);
  } // namespace ftk
} // namespace daq

#endif // AUX_SPYBUFFER_PARSERS_H_

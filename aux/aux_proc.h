#ifndef AUX_PROC_H
#define AUX_PROC_H

#include "aux/aux_interface.h"
#include "ftkcommon/patternbank_lib.h"

namespace daq
{
  namespace ftk
  {

    class proc : public aux_interface
    {

    public:
      proc(uint slot, uint fpga, std::string _name);

      //// Run Control FSMs ////
      /*! \brief configure transition
       *
       * 1. common configure
       * 2. enable RSLB
       */
      void configure();
      bool check_link_status(int i);

      //// Useful functions ////
      void UnlockProcsDriveAUXbus();
      void set_checksumWarning(bool warningOnly);
      void set_towerID(uint towerID);
      void load_rams(bool isLoadMode);

      /*! \breif Controls data-flow from upstream
       *
       * Controls input from AMB
       *
       * \param enable Enable input dataflow.
       */
      void set_source_roads(bool amb);
      virtual void enable_upstream_rx(bool enable);
      virtual void set_upstream_flow(bool enable);
      virtual void set_downstream_flow(bool enable);

      void set_hold_ignore_hw(bool ignore);
      void hold_ignore_hw();
      void set_ignore_freeze(bool ignore);
      void set_freeze_mask(u_int mask);
      void enable_rslb();
      void set_hits_from_tvs(bool v);
      void set_roads_from_tvs(bool v);
      void reset_dfsynch();
      void reset_SimpleSimplexLDC();
      void reset_do();
      void reset_tf();
      virtual void reset_logic();

      void set_MaxFitsPerRoad(u_int MaxFitsPerRoad);
      void set_MaxRoadsPerEvent(u_int MaxRoadsPerEvent);

      void load_tf_const();
      void load_am_const();

      /* \brief Calculate the TF checksum and return it.
       *
       * \return Checksum of the TF constants map
       */
      uint calc_tf_check_sum();

      /* \brief Check the TF checksum
       *
       * Runs the TF checksum calculation and compares it against an
       * expected number. If a difference is seen, an error is thrown.
       */      
      void check_tf_check_sum();

      /* \brief Calculate the AM checksum and return it.
       *
       * If the EMIF is not ready, a checksum of 0x0 is returned.
       *
       * If the checksum is not available after 20 sec, an AuxAMMapChecksumTimeout
       * exception is thrown.
       *
       * \return Checksum of the AM map
       */
      uint calc_am_check_sum();

      
      /* \brief Check the AM checksum
       *
       * Runs the AM checksum calculation and compares it against an
       * expected number. If a difference is seen, an error is thrown.
       */
      uint get_am_checksum();
      void calibrate_emif();
      
          
      void load_road_fifos(string path);
      void load_hit_and_ssid_fifos(string path);

      /*! \brief Empty both the hit and road FIFOs on the processor
       */
      void reset_hit_and_road_fifos();

      /*! \brief Start sending roads from FIFOs
       *
       * \param loop Send in loop mode.
       */
      void send_road_fifos(bool loop);

      /*! \brief Start sending hits from FIFOs
       *
       * \param loop Send in loop mode.
       */
      void send_hit_and_ssid_fifos(bool loop);

      void set_loadAllSectors(const bool l) { loadAllSectors = l; }
      void set_tf_constants(const string& s, uint cs) { tf_constants = s; tf_checksum = cs; }
      //void set_am_constants(const string& s, uint cs) { am_constants = s; am_checksum = cs; }
      void set_FTKPatternBank(FTKPatternBank* b) { m_FTKPatternBank = b; }
      uint m_bankChecksum = 0;
      void set_am_constants(const string& s) { am_constants = s; }
      void set_NPattPerChip(const uint n) { NPattPerChip = n; }
      void set_banktype(const uint t) { banktype = t; }

      void set_ssoffset(const bool o) { ssoffset = o; }
      void set_InChipOff(const uint c) { InChipOff = c; }
    
      //name for ERS log
      std::string name_ftk() { return m_name; }

    private:
      std::string tf_constants, am_constants;
      bool m_roads_from_amb = true;
      bool loadAllSectors = true;
      FTKPatternBank* m_FTKPatternBank;
      uint tf_checksum, am_checksum;
      uint m_npatterns;
      uint NPattPerChip; /**< Number of patterns to loaded in each chip */
      uint banktype;         /**< pattern bank type: 0 for root format, 1 for ascii format */
      bool ssoffset;            /**< superstrip offset to avoid SSID 0 which would always match */
      std::string m_name;
      uint InChipOff;
      bool m_loadAllSectors = true;
      bool m_EMIFready;

      bool m_checksumWarning = true;

      bool m_holdIgnoreHW = false;

    };

  }
}


#endif

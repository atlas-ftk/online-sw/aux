//
// Header file for DOLayer and DataOrganizer classes for DO bit level check
// Created by Michael Hank on July 23, 2019.
// Largely based on Patrick's python code, also using work done by Lesya.
//

#ifndef FTK_AUX_BITLEVEL_DATAORGANIZER_H
#define FTK_AUX_BITLEVEL_DATAORGANIZER_H

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <bitset>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <utility>

#include <aux/AMBank.h>

namespace daq
{
    namespace ftk
    {

        class DOLayer
        {
            public:
                static const unsigned int nDCBits=8;
                static const unsigned int nAMSSID = 2048;//max SSID for arrays

                //vectors and arrays
                std::vector<long> HitData;

                //Hit List Pointer (HLP) stores the address of the first hit with a given SSID
                //- It is an array we have a separate HLP for each of the 8 DC possibilities
                //- For each DC-possibilities the HLP acts like a dictionary which maps AMSSID to address in the Hit List Memory
                int HitListPointer[nDCBits][nAMSSID];

                //Hit Count Memory (HCM) stores the number of hits with a given SSID
                //- It is an array we have a separate HCM for each of the 8 DC possibilities
                //- For each DC-possibilities the HCM acts like a dictionary which maps AMSSID to the number of hits
                unsigned int HitCountMem[nDCBits][nAMSSID];
    
                //Dictionary
                //Hit List Memory (HLM) stores the hits in a local memory to be retrieved later
                //- each hit is given a local address
                //- the addresses are simply incremented from 0
                std::map<int, int> HitListMem;

                int previousSSID;
                int nHits;
                int addr;

                //initialize
                DOLayer()
                {
                    ClearLayer();

                }

                //add a Hit
                bool addHit(int fullSSID, int hit)
                {
                    bool hit_err = false;
            
                    nHits+=1;
                    //the SSID with out the 3LSB
                    int AMSSID_int = (fullSSID >> 3) & 2047; //2047=0b11111111111
                    int DCBits_int =  fullSSID & 7;

                    int newfullSSID_int   =  ((AMSSID_int << 3) | DCBits_int);
           
                    // If new SSID 
                    if(HitCountMem[DCBits_int][AMSSID_int] == 0){
                        HitCountMem[DCBits_int][AMSSID_int]    = 0;
                        HitListPointer[DCBits_int][AMSSID_int] = addr;
                    } else if (newfullSSID_int != previousSSID){
                        //ERROR, hits are arriving in order that is not grouped by SSID
                        cout << "ERROR: New SSID is not equal to previous SSID, but SSID has arrived in the past" << endl;       
                        cout << "newfullSSID_int, previousSSID " << newfullSSID_int << " " << previousSSID << endl;
                        hit_err = true;
                    }

                    previousSSID = newfullSSID_int;

                    // Count SSIDs
                    HitCountMem[DCBits_int][AMSSID_int] += 1;
                    HitListMem[addr] = hit;
                    addr += 1;
                    return hit_err;
                }

                //get matched hits for an AMSSID
                std::vector<long> getHits(int AMSSID, int encodedDC, bool * match_error, bool doDebug=true)
                {
                    std::vector<long> matchedHits;

                    //build DC mask out of encoded DC bits. Uses base three encoding, 0, 1, X where X is DC
                    //                      0          1          X
                    int DCIndexROM[27] = {0b00000001,0b00000010,0b00000011, // 00[]
                                          0b00000100,0b00001000,0b00001100, // 01[]
                                          0b00000101,0b00001010,0b00001111, // 0X[]
                                          0b00010000,0b00100000,0b00110000, // 10[]
                                          0b01000000,0b10000000,0b11000000, // 11[]
                                          0b01010000,0b10100000,0b11110000, // 1X[]
                                          0b00010001,0b00100010,0b00110011, // X0[]
                                          0b01000100,0b10001000,0b11001100, // X1[]
                                          0b01010101,0b10101010,0b11111111};// XX[]

                    if (encodedDC > 26){
                        cout << "ERROR: encoded DC is larger than 3^3" << endl;
                        *match_error = true;
                        return matchedHits;
                    } else if (doDebug){
                        cout << "DCIndexROM["<< encodedDC << "] = " << std::bitset<8>(DCIndexROM[encodedDC]) << endl;
                        cout << std::hex;
                    }

                    for(u_int iDC=0; iDC<8; iDC++){
                        if (not ((DCIndexROM[encodedDC] >> iDC)&0b1)) {
                            continue; //check that this ternary bit combo is used in this pattern
                        }
                        if (HitCountMem[iDC][AMSSID]==0){//AMSSID not in HitCountMem
                            continue; //check that this ternary bit combo has hits
                        }
                        
                        //Number of hits
                        unsigned int hitCount = HitCountMem[iDC][AMSSID];
                        //Memory of first pix hit
                        long hitPointer = HitListPointer[iDC][AMSSID];

                        for(u_int i=0; i<hitCount; i++){
                            matchedHits.push_back(HitListMem[hitPointer+i]);
                        }
                    }

                    return matchedHits;
                }

                void PrintEvent()
                {
                    std::cout << "Hit Count Memory:" << std::endl;
                    for(unsigned int i=0; i<nDCBits; i++){
                        for(unsigned int j=0; j<nAMSSID; j++){
                            std::cout << "\t " << HitCountMem[i][j] << std::endl;
                        }
                    }

                    std::cout << "Hit List Pointer:" << std::endl;
                    for(unsigned int i=0; i<nDCBits; i++){
                        for(unsigned int j=0; j<nAMSSID; j++){
                            std::cout << "\t " << HitListPointer[i][j] << std::endl;
                        }
                    }

                    std::cout << "Hit List Memory:" << std::endl;
                    for(unsigned int i=0; i<HitListMem.size(); i++){
                        std::cout << "\t " << HitListMem[i] << std::endl;
                    }
                }

                void ClearLayer()
                {
                    //Clear the arrays
                    for(unsigned int i=0; i<nDCBits; i++){
                        for(unsigned int j=0; j<nAMSSID; j++){
                            HitCountMem[i][j] = 0;
                            HitListPointer[i][j] = 0;
                        }
                    }

                    HitListMem.clear();

                    //Something we won't encounter naturally
                    previousSSID=-1;

                    nHits=0;

                    addr = 0;

                }
        };

        //This class is similar to aux_DO_spies, except it has the emulated outputs and elimates non-overlapping events
        class DataOrganizer
        {
            
            public:
                
                // Emulated outputs. Each of these is a vector of ints per event, except outHits is a vector of ints per layer per event.
                std::vector<std::vector<std::vector<long>>> outHits;
                std::vector<std::vector<int>> outRoads;
                std::vector<std::vector<int>> outSectors;
                std::vector<std::vector<int>> outLayerMaps;

                // Inputs. Each event has 11 vectors of ints, one for each layer.
                std::vector<std::vector<std::vector<long>>> inHits;
                
                // Board outputs
                std::vector<std::vector<long>> BoardLaySec;
                std::vector<std::vector<std::vector<long>>> BoardHits;

                bool found_error;
                bool verbose;

                // Set Error
                void SetError(bool input_error)
                {
                    found_error = input_error;
                }

                // Set Verbose
                void SetVerbose(bool input_verbose)
                {
                    verbose = input_verbose;
                }

                // Add Input and Board Information
                void addInBoard(std::vector<long> sct0, std::vector<long> sct1, std::vector<long> sct2, std::vector<long> sct3, std::vector<long> sct4, std::vector<long> pix0, std::vector<long> pix1, std::vector<long> pix2, std::vector<long> pix3, std::vector<long> pix4, std::vector<long> pix5, std::vector<long> laysec, std::vector<long> o_sct0, std::vector<long> o_sct1, std::vector<long> o_sct2, std::vector<long> o_sct3, std::vector<long> o_sct4, std::vector<long> o_pix0, std::vector<long> o_pix1, std::vector<long> o_pix2, std::vector<long> o_pix3, std::vector<long> o_pix4, std::vector<long> o_pix5)
                {
                    inHits.push_back({sct0, sct1, sct2, sct3, sct4, pix0, pix1, pix2, pix3, pix4, pix5});
                    BoardLaySec.push_back(laysec);
                    BoardHits.push_back({o_pix0, o_pix1, o_pix2, o_pix3, o_pix4, o_pix5,o_sct0, o_sct1, o_sct2, o_sct3, o_sct4});
                }
                

                // Emulating a single event
                void emulate_event(std::vector<long> sct0, std::vector<long> sct1, std::vector<long> sct2, std::vector<long> sct3, std::vector<long> sct4, std::vector<long> pix0, std::vector<long> pix1, std::vector<long> pix2, std::vector<long> pix3, std::vector<long> pix4, std::vector<long> pix5, std::vector<long> roads, string ambankpath, bool oldDC, bool allSectors, int inChipOff, int proc){

                DOLayer MyLayers[11]; //0 is sct0...4 is sct4, 5 is pix0...11 is pix6

                // ***Write Mode***

                long word;
                int SSID;
                long hit;

                //SCT
                for (u_int i = 7; i < sct0.size()-6; i++){//header is 7 or 8 words. Trailer is 6 words.
                    word=sct0[i];
                    if (word==0){//skip empty words
                        continue;
                    }
                    SSID= (word>>16) & 16383; //SSID is 14 bits
                    hit=word & 65535; //hit is 16 bits
                    found_error |= MyLayers[6].addHit(SSID, hit);
                    MyLayers[6].HitData.push_back(word);
                }

                for (u_int i = 7; i < sct1.size()-6; i++){//header is 7 or 8 words. Trailer is 6 words.
                    word=sct1[i];
                    if (word==0){//skip empty words
                        continue;
                    }
                    SSID= (word>>16) & 16383; //SSID is 14 bits
                    hit=word & 65535; //hit is 16 bits
                    found_error |= MyLayers[7].addHit(SSID, hit);
                    MyLayers[7].HitData.push_back(word);
                }

                for (u_int i = 7; i < sct2.size()-6; i++){//header is 7 or 8 words. Trailer is 6 words.
                    word=sct2[i];
                    if (word==0){//skip empty words
                        continue;
                    }
                    SSID= (word>>16) & 16383; //SSID is 14 bits
                    hit=word & 65535; //hit is 16 bits
                    found_error |= MyLayers[8].addHit(SSID, hit);
                    MyLayers[8].HitData.push_back(word);
                }

                for (u_int i = 7; i < sct3.size()-6; i++){//header is 7 or 8 words. Trailer is 6 words.
                    word=sct3[i];
                    if (word==0){//skip empty words
                        continue;
                    }
                    SSID= (word>>16) & 16383; //SSID is 14 bits
                    hit=word & 65535; //hit is 16 bits
                    found_error |= MyLayers[9].addHit(SSID, hit);
                    MyLayers[9].HitData.push_back(word);
                }

                for (u_int i = 7; i < sct4.size()-6; i++){//header is 7 or 8 words. Trailer is 6 words.
                    word=sct4[i];
                    if (word==0){
                        continue;
                    }
                    SSID= (word>>16) & 16383; //SSID is 14 bits
                    hit=word & 65535; //hit is 16 bits
                    found_error |= MyLayers[10].addHit(SSID, hit);
                    MyLayers[10].HitData.push_back(word);
                }

                //Pixel
                bool gotSSID= false;
                //Pix header is 7 words, trailer 6 words.
                for (u_int i = 7; i < pix0.size()-6; i++){
                    word=pix0[i];
                    if (word==0){//skip empty words
                        continue;
                    }
                    //first comes ssid, then hit
                    if (not gotSSID) {
                        SSID= word;
                        gotSSID=true;
                    } else {
                        hit=word;
                        found_error |= MyLayers[0].addHit(SSID, hit);
                        gotSSID=false;
                    }
                    MyLayers[0].HitData.push_back(word);
                }

                for (u_int i = 7; i < pix1.size()-6; i++){
                    word=pix1[i];
                    if (word==0){//skip empty words
                        continue;
                    }
                    //first comes ssid, then hit
                    if (not gotSSID) {
                        SSID= word;
                        gotSSID=true;
                    } else {
                        hit=word;
                        found_error |= MyLayers[1].addHit(SSID, hit);
                        gotSSID=false;
                    }
                    MyLayers[1].HitData.push_back(word);
                }

                for (u_int i = 7; i < pix2.size()-6; i++){
                    word=pix2[i];
                    if (word==0){//skip empty words
                        continue;
                    }
                    //first comes ssid, then hit
                    if (not gotSSID) {
                        SSID= word;
                        gotSSID=true;
                    } else {
                        hit=word;
                        found_error |= MyLayers[2].addHit(SSID, hit);
                        gotSSID=false;
                    }
                    MyLayers[2].HitData.push_back(word);
                }

                for (u_int i = 7; i < pix3.size()-6; i++){
                    word=pix3[i];
                    if (word==0){//skip empty words
                        continue;
                    }
                    //first comes ssid, then hit
                    if (not gotSSID) {
                        SSID= word; 
                        gotSSID=true;
                    } else {
                        hit=word;
                        found_error |= MyLayers[3].addHit(SSID, hit);
                        gotSSID=false;
                    }
                    MyLayers[3].HitData.push_back(word);
                }

                for (u_int i = 7; i < pix4.size()-6; i++){
                    word=pix4[i];
                    if (word==0){//skip empty words
                        continue;
                    }
                    //first comes ssid, then hit
                    if (not gotSSID) {
                        SSID= word;
                        gotSSID=true;
                    } else {
                        hit=word;
                        found_error |= MyLayers[4].addHit(SSID, hit);
                        gotSSID=false;
                    }
                    MyLayers[4].HitData.push_back(word);
                }

                for (u_int i = 7; i < pix5.size()-6; i++){
                    word=pix5[i];
                    if (word==0){//skip empty words
                        continue;
                    }
                    //first comes ssid, then hit
                    if (not gotSSID) {
                        SSID= word;
                        gotSSID=true;
                    } else {
                        hit=word;
                        found_error |= MyLayers[5].addHit(SSID, hit);
                        gotSSID=false;
                    }
                    MyLayers[5].HitData.push_back(word);
                }

                // ***Load AMBank***
                //TODO put this earlier and pass in as argument so we don't have to load for every event
                AMBank ambank = AMBank(ambankpath, oldDC, allSectors, inChipOff );
                ambank.lamb = proc;

                // ***Read mode***
                vector<int> roadIDs;
                vector<int> sectorIDs;
                vector<int> layerMaps;
                vector<vector<long>> outputStream11L(11,std::vector<long>(0));//11 layers to store hits in, empty for now

                int bitmask;
                int roadID;
                int use_sectorID;
                int sectorID;
                int iRoad=0;
                for (u_int i = 2; i < roads.size(); i++){//Roads have header length 2, no trailer
                    word=roads[i];
                    if (word==0){//Skip empty words
                        continue;
                    }
                    bitmask = word >> 24;
                    roadID = word & 2097151; //2097151=0x1fffff, only using 21 bits
                    roadIDs.push_back(roadID);
                    iRoad+=1;
                    int ID = ambank.getBankTTreeID(roadID);


                    int ssid[8];
                    int hbmask;
                    int dcmask;

                    ambank.BankTree->SetBranchAddress("ssid", &ssid);
                    ambank.BankTree->SetBranchAddress("hbmask", &hbmask);
                    ambank.BankTree->SetBranchAddress("dcmask", &dcmask);

                    ambank.BankTree->SetBranchAddress("sectorID", &sectorID);
                    ambank.BankTree->GetEntry(ID);


                    if (allSectors){
                        use_sectorID = sectorID;
                    } else {
                        use_sectorID = sectorID >> 2;
                    }

                    sectorIDs.push_back(use_sectorID);


                    int layerMap=0;
                    int shift=0;
                    int nDC;
                    int currss;
                    int hb;
                    int dc;
                    int val;

                    int ndcx;
                    int ndcy;

                    ambank.SSMapTree->SetBranchAddress("ndcx", &ndcx);
                    ambank.SSMapTree->SetBranchAddress("ndcy", &ndcy);

                    for (u_int i8L = 0; i8L < 8; i8L++){
                        ambank.SSMapTree->GetEntry(i8L);
                        nDC = ndcx + ndcy;
                        currss = ssid[i8L] << (nDC*2);

                        hb = (hbmask>>(shift)) & int(pow(2,nDC)-1+0.5); //+0.5 to avoid rounding errors
                        dc = (dcmask>>(shift)) & int(pow(2,nDC)-1+0.5); //+0.5 to avoid rounding errors
                        shift += nDC;

                        for(u_int ibit=0; ibit<nDC; ibit++){
                            if(dc&(1<<ibit)){
                                val = 0;
                            } else if (hb&(1<<ibit)) {
                                val = 2;
                            } else {
                                val = 1;
                            }
                            currss |= val << (ibit*2);
                        }

                        int nconvbits = nDC;
                        int oldSS_H = currss>>(nDC*2);
                        int bitsToConvert = currss & int(pow(2,2*nconvbits)-1+0.5); //+0.5 to avoid rounding errors
                        int convertedBits=0;
                        int ternval;

                        for(u_int ibit=0; ibit<bitsToConvert; ibit++){
                            if(ibit<16){//We can't shift more than 31 bits. In this case, ternval should hopefully be 0 so no need to do anything.
                                ternval = (bitsToConvert>>(2*ibit)) & 3;
                                if(ternval == 2){
                                    convertedBits |= 1<<ibit;
                                }
                            }
                        }
                        SSID = (oldSS_H<<(nconvbits)) | (convertedBits);
                        int AMSSID = SSID>>3;
                        int ternaryBaseThree0;
                        int ternaryBaseThree1;
                        int ternaryBaseThree2;

                        if ((dc>>0)&1){
                            ternaryBaseThree0 = 2;
                        } else {
                            ternaryBaseThree0 = (SSID>>0)&1;
                        }

                        if ((dc>>1)&1){
                            ternaryBaseThree1 = 2;
                        } else {
                            ternaryBaseThree1 = (SSID>>1)&1;
                        }

                        if ((dc>>2)&1){
                            ternaryBaseThree2 = 2;
                        } else {
                            ternaryBaseThree2 = (SSID>>2)&1;
                        }

                        int encodedDC = 1*ternaryBaseThree0 + 3*ternaryBaseThree1 + 9*ternaryBaseThree2;//3^0, 3^1, 3^2

                        // Determine which stream we are in

                        u_int i11L;
                        if (i8L < 3) {
                            // Pix Layer
                            i11L = 2*i8L + (AMSSID%2);
                        } else {
                            //subract 3 to get nth SCT Layer
                            int nthSCT = (i8L - 3);
                            //add 6 to get the correct 11-layer number
                            i11L   = nthSCT + 6;
                        }
               
                        //Getting matched hits
                        bool match_err = false;
                        std::vector<long> matchedHits = MyLayers[i11L].getHits(AMSSID, encodedDC, &match_err, verbose);
                        SetError(found_error or match_err); 

                        int enableLayer = (bitmask>>i8L)&1;
                        if (matchedHits.size()>0 && not enableLayer){
                            cout << "WARNING: Bitmask for this road has layer with hits disabled! AMB issue? Road: " << word << endl;
                        }
                        if (not enableLayer){
                            continue;
                        }

                        // Update the layer map
                        if (matchedHits.size()>0){
                            layerMap = layerMap | (0b1 << (10-i11L) );
                        }

                        // Add them to the output
                        for (u_int ihit=0; ihit<matchedHits.size(); ihit++) {
                            outputStream11L[i11L].push_back(matchedHits[ihit]);
                        }
                    }
                    layerMaps.push_back(layerMap);

                }
                //Finished read mode

                //Store expected output
                storeExpectedOutput(outputStream11L,roadIDs,sectorIDs,layerMaps);

                //roadIDs, sectorIDs, layermaps, and layers should be cleared automatically as out of scope

                return;
                }
                


                void storeExpectedOutput(std::vector<std::vector<long>> outputStream11L, std::vector<int> roadIDs, std::vector<int> sectorIDs, std::vector<int> layerMaps)
                {
                    //Adds in an event of simulated output

                    outHits.push_back(outputStream11L);
                    outRoads.push_back(roadIDs);
                    outSectors.push_back(sectorIDs);
                    outLayerMaps.push_back(layerMaps);
    
                }   

                void compare(bool is_loop)
                {
                    //Formatting
                    cout << std::hex;                   
 
                    //By construction, all vectors should be same size (number of events)
                    int num_events = inHits.size();

                    vector<vector<int>> boardLiveLayers;
                    vector<vector<int>> boardLayerMaps;
                    vector<vector<int>> boardSectors;

                    cout << "Number of Events " << num_events << endl; 
                    for (u_int event=0; event<num_events; event++){
                        vector<int> boardLLperEv;
                        vector<int> boardLMperEv;
                        vector<int> boardSecperEv;

                        for (u_int i=2; i<BoardLaySec[event].size(); i++){//start at 2 to skip headers

                            //8 highest bits are live layers in reverse order
                            std::string to_rev = std::bitset< 8 >( BoardLaySec[event][i]>>24 ).to_string(); // string conversion

                            reverse(to_rev.begin(), to_rev.end());

                            boardLLperEv.push_back(std::stoi(to_rev, nullptr, 2));

                            //Next 11 bits are layermap in reverse order
                            to_rev = std::bitset< 11 >((BoardLaySec[event][i]>>13)&0b11111111111).to_string();

                            reverse(to_rev.begin(), to_rev.end());

                            boardLMperEv.push_back(std::stoi(to_rev, nullptr, 2));

                            //13 lowest bits are sectorID
                            boardSecperEv.push_back(BoardLaySec[event][i]&0b1111111111111);
                        }

                        //Remove headers from board hits
                        for (u_int iL=0; iL<11; iL++){
                            BoardHits[event][iL].erase(BoardHits[event][iL].begin(), BoardHits[event][iL].begin() + 2);
                        }

                        boardLiveLayers.push_back(boardLLperEv);
                        boardLayerMaps.push_back(boardLMperEv);
                        boardSectors.push_back(boardSecperEv);

                        //We don't check roads as we read them in from the output (input doesn't have full L1IDs)
                        //TODO: If loop mode, we could read in from do_roads_p*.dat


                        //TODO: need to add LiveLayers with corresponding logic to complete the bit-checks
                        //These weren't in python either, so lower priority

                    }

                    //Compare

                    bool found_err = false;

                    //Loop over events
                    for (u_int event=0; event<num_events; event++){
                        cout << "Event " << event << endl;
                    
                        //Check sectors
                        if (boardSectors[event]!=outSectors[event] ){
                            cout << "V########################V" << endl;
                            cout << "V Error in Sector stream V" << endl;
                            cout << "V########################V" << endl;

                            if (boardSectors[event].size()==outSectors[event].size()){
                                cout << "Equal Number of Sectors" << endl;
                                for (u_int i=0; i<boardSectors[event].size(); i++){
                                    if (boardSectors[event][i]!=outSectors[event][i] ){
                                        cout << "Sector mismatch in road " << std::setfill('0') << setw(8) << outRoads[event][i] << endl;
                                        cout << "Sim Sector: " << std::setfill('0') << setw(8) << outSectors[event][i] << endl;
                                        cout << "Board Sector: " << std::setfill('0') << setw(8) << boardSectors[event][i] << endl;
                                        found_err = true;
                                    }
                                }
                            } else if (boardSectors[event].size()>outSectors[event].size() ) {
                                cout << "More Sectors in Board output than Sim" << endl;
                                cout << "Board has " << boardSectors[event].size() << " sectors." << endl;
                                cout << "Sim has " << outSectors[event].size() << " sectors." << endl;

                                found_err = true;

                                cout << endl;
                                cout << "Board Sector :: Sim Sector" << endl;
                                for (u_int i=0; i<boardSectors[event].size(); i++){
                                    cout << std::setfill('0') << setw(8) << boardSectors[event][i];
                                    if (i<outSectors[event].size()){
                                        cout << " :: " << std::setfill('0') << setw(8) << outSectors[event][i];
                                        if(outSectors[event][i]!=boardSectors[event][i]){
                                            cout << "   <<<";
                                        }
                                    }                                    
                                    cout << endl;
                                }
                                cout << endl;
                            } else if (boardSectors[event].size()<outSectors[event].size() ) {
                                cout << "More Sectors in Sim output than Board" << endl;
                                cout << "Board has " << boardSectors[event].size() << " sectors." << endl;
                                cout << "Sim has " << outSectors[event].size() << " sectors." << endl;
                                found_err = true;
                                cout << endl;
                                cout << "Board Sector :: Sim Sector" << endl;
                                for (u_int i=0; i<outSectors[event].size(); i++){
                                    if (i<boardSectors[event].size()){
                                        cout << std::setfill('0') << setw(8) << boardSectors[event][i];
                                    } else {
                                        cout << "        ";
                                    }
                                    cout << " :: " << std::setfill('0') << setw(8) << outSectors[event][i] << endl;
                                    if (i<boardSectors[event].size()){
                                        if(outSectors[event][i]!=boardSectors[event][i]){
                                            cout << "   <<<";
                                        }
                                    }
                                }
                            }
                            
                        }

                        //Check layermaps
                        if (boardLayerMaps[event]!=outLayerMaps[event] ){
                            cout << "V########################V" << endl;
                            cout << "V Error in LayerMaps stream V" << endl;
                            cout << "V########################V" << endl;

                            if (boardLayerMaps[event].size()==outLayerMaps[event].size()){
                                cout << "Equal Number of LayerMaps" << endl;
                                for (u_int i=0; i<boardLayerMaps[event].size(); i++){
                                    if (boardLayerMaps[event][i]!=outLayerMaps[event][i] ){
                                        cout << "LayerMaps mismatch in road " << std::setfill('0') << setw(8) << outRoads[event][i] << endl;
                                        cout << "Sim LayerMaps: " << std::setfill('0') << setw(8) << outLayerMaps[event][i] << endl;
                                        cout << "Board LayerMaps: " << std::setfill('0') << setw(8) << boardLayerMaps[event][i] << endl;
                                        found_err = true;
                                    }
                                }
                            } else if (boardLayerMaps[event].size()>outLayerMaps[event].size() ) {
                                cout << "More LayerMaps in Board output than Sim" << endl;
                                cout << "Board has " << boardLayerMaps[event].size() << " LayerMaps." << endl;
                                cout << "Sim has " << outLayerMaps[event].size() << " LayerMaps." << endl;

                                found_err = true;

                                cout << endl;
                                cout << "Board LayerMaps :: Sim LayerMaps" << endl;
                                for (u_int i=0; i<boardLayerMaps[event].size(); i++){
                                    cout << std::setfill('0') << setw(8) << boardLayerMaps[event][i];
                                    if (i<outLayerMaps[event].size()){
                                        cout << " :: " << std::setfill('0') << setw(8) << outLayerMaps[event][i];
                                        if(outLayerMaps[event][i]!=boardLayerMaps[event][i]){
                                            cout << "   <<<";
                                        }
                                    }
                                    cout << endl;
                                }
                                cout << endl;
                            } else if (boardLayerMaps[event].size()<outLayerMaps[event].size() ) {
                                cout << "More LayerMaps in Sim output than Board" << endl;
                                cout << "Board has " << boardLayerMaps[event].size() << " LayerMaps." << endl;
                                cout << "Sim has " << outLayerMaps[event].size() << " LayerMaps." << endl;
                                found_err = true;
                                cout << endl;
                                cout << "Board LayerMaps :: Sim LayerMaps" << endl;
                                for (u_int i=0; i<outLayerMaps[event].size(); i++){
                                    if (i<boardLayerMaps[event].size()){
                                        cout << std::setfill('0') << setw(8) << boardLayerMaps[event][i];
                                    } else {
                                        cout << "        ";
                                    }
                                    cout << " :: " << std::setfill('0') << setw(8) << outLayerMaps[event][i] << endl;
                                    if (i<boardLayerMaps[event].size()){
                                        if(outLayerMaps[event][i]!=boardLayerMaps[event][i]){
                                            cout << "   <<<";
                                        }
                                    }
                                }
                            }

                        }

                        //Check hits

                        //Sorting as hit order not fully deterministic
                        for (u_int iL=0; iL<11; iL++){
                            sort(BoardHits[event][iL].begin(),BoardHits[event][iL].end());
                            sort(outHits[event][iL].begin(),outHits[event][iL].end());
                        }

                        if(BoardHits[event]!=outHits[event]){
                            found_err = true;
                            cout << "V########################V" << endl;
                            cout << "V Error in Hit stream V" << endl;
                            cout << "V########################V" << endl;
                            for (u_int iL=0; iL<11; iL++){
                                if(BoardHits[event][iL]!=outHits[event][iL]){   
                                    if(iL<6){
                                        cout << "PIX "<< iL <<" Does NOT Match:" << endl;
                                    } else {
                                        cout << "SCT "<< iL-6 <<" Does NOT Match:" << endl;
                                    }
                                    cout << "Board Hits :: Sim Hits" << endl;
                                    for (u_int i=0; i<max(BoardHits[event][iL].size(),outHits[event][iL].size()); i++){
                                        if (i<BoardHits[event][iL].size()){
                                            cout << std::setfill('0') << setw(8) << BoardHits[event][iL][i];
                                        } else {
                                            cout << "        ";
                                        }
                                        if (i<outHits[event][iL].size()){
                                            cout << " :: " << std::setfill('0') << setw(8) << outHits[event][iL][i];
                                            if (i<BoardHits[event][iL].size()){
                                                if(outHits[event][iL][i]!=BoardHits[event][iL][i]){
                                                    cout << "   <<<";
                                                }
                                            }
                                        }
                                        cout << endl;
                                    }
                                }
                            }
                        }                     
                    }         

                    cout << "Processed " << num_events << " events." << endl;
                    if(found_err or found_error){
                        cout << "ERROR: Sim and board do NOT agree for this processor" << endl;
                    } else {
                        cout << "Sim and board agree for this processor. Good job!" << endl;
                    }
                }
        };


    }//namespace ftk
}//namespace daq

#endif

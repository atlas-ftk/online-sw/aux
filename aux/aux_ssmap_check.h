#include <vector>
#include <map>
#include <string>
#include <utility>

#ifndef FTK_AUX_BITLEVEL_SS_LIB_H
#define FTK_AUX_BITLEVEL_SS_LIB_H

namespace daq
{
    namespace ftk
    {

        void ssmap_toplevel(bool verbose, const std::string& inDirName, string tower, string ssbase, bool doHS, bool is_loop);

        vector<vector<string>> ssmap_read_file(string mode, string file, int col);

        std::pair<vector<vector<string>>, vector<vector<string>>> ssmap_overlap(vector<vector<string>> in_packets,vector<vector<string>> out_packets,bool is_loop);

        vector<vector<string>> ssmap_emulation(vector<vector<string>> in_packets, bool doHS, string mode, string plane, string tower, string ssbase);

        bool ssmap_compare(vector<vector<string>> in_packets,vector<vector<string>> out_packets,bool is_loop, bool is_sort, bool verbose, string mode, string plane);

        template<typename Out>
        void split(const std::string &s, char delim, Out result);

        std::vector<std::string> split(const std::string &s, char delim);

        bool ends_with(std::string const &fullString, std::string const &end);
        
        class ssmap_buffer
        {
            public:

            string mode;
            string plane;
            string infile;
            int incol;
            string outfile;
            int outcol;

            ssmap_buffer(string mode_in, string plane_in, string infile_in, int incol_in, string outfile_in, int outcol_in)
            {
                mode=mode_in;
                plane=plane_in;
                infile=infile_in;
                incol=incol_in;
                outfile=outfile_in;
                outcol=outcol_in;
            }

        };
        
        class SSMap
        {
            public:
            
            map<int,int> ssmap;

            int get(int key)
            {
                if(ssmap.find(key) == ssmap.end()) { //If key not present
                    return 0;
                } else {
                    return ssmap[key];
                }
            }


            SSMap(string hexfile)
            {
                cout << "Reading file " << hexfile << endl;
                std::ifstream infile(hexfile);
                int addrbase=0;
                for( std::string line; getline( infile, line ); ){
                    
                    if (line.empty()){
                        continue;
                    }
                    int nb = std::stoi(line.substr(1,2), nullptr, 16);
                    int addr = std::stoi(line.substr(3,4), nullptr, 16);
                    string cmd = line.substr(7,2);
                    int data;
                    if(nb>0){
                        data=std::stoi(line.substr(9,nb*2), nullptr, 16);
                    }
                    if (cmd=="00"){ // DATA
                        addr=addr+addrbase;
                        ssmap.insert(pair<int, int>(addr, data));
                    } else if (cmd=="01") {
                        break; // DONE
                    } else if (cmd=="04") { // Extended linear address
                        addrbase=data<<16;
                    } else {
                        cout << "non-data command:" << cmd << endl; 
                    }
                }


            }

        };

    }//namespace ftk
}//namespace daq
 
#endif   

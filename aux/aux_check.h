//////////////////////////////////////////////////////
// Header file containing the declaration of the 
// functions used to check the aux board
///////////////////////////////////////////////////////

#ifndef FTK_AUX_CHECK_H
#define FTK_AUX_CHECK_H


#include <vector>
#include <sstream>

#include "ftkcommon/core.h"
#include "ftkcommon/exceptions.h"
#include "ers/ers.h"

namespace daq 
{
  namespace ftk
  {
    /*! \brief Routine to check Track Fitter.
     *
     * If the dumpBitstream option is specified, the results of the emulation and the
     * firmware output are dumped to a file. The files are tracks_emu.txt and tracks_out.txt
     * correspondingly.
     *
     * \param tf_consts Path to the constants file
     * \param tf_in Contents of the TF input spy buffer (DO output)
     * \param tf_out Contents of the TF output spy buffer
     * \param dumpBitstream Write the bitstream from emulation/firmware
     *
     * \return True if TF output matches.
     */
    bool aux_tf_compare(const std::string& tf_consts, std::map<u_int, std::vector<u_int> >& tf_in, const std::vector<u_int>& tf_out, bool dumpBitstream, std::string proc, bool debug, bool allSectors, bool is_loop=false, bool split_sectors=false);

    /*! \brief Initialize the data structure for all spy buffers.
     *
     * The data structure is a map containing an entry for each FPGA. Keys
     * are the FPGA IDs. The entry is a map containing an entry for each 
     * spy buffer, with key being the spy buffer number. The entry is a list
     * containing the spy buffer contents.
     *
     * The keys and entries (empty spy buffers) are initialized by this function. The
     * list of spy buffer number is defined in aux_spy_regs.h.
     *
     * \param data The data structure to initialize
     */
    void aux_buffers_build_data_structure(std::map<u_int, std::map<u_int, std::vector<u_int> > > &data);

    /*! \brief Load data from the output of aux_dump_buffers.
     *
     * \param data Reference to the structure to will with the data
     * \param inDirName Path to the directory containing the spybuffer outputs
     */
    void aux_buffers_download_data(std::map<u_int, std::map<u_int, std::vector<u_int> > > &data, const std::string& inDirName);
    
  }//namespace ftk
}//namespace daq

#endif

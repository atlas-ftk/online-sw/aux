#ifndef EVENTFRAGMENTTRACKFTKPACKET_H_
#define EVENTFRAGMENTTRACKFTKPACKET_H_

#include "ftkcommon/EventFragmentFTKPacket.h"

#include "aux/TF/TFCombination.h"

namespace daq {
  namespace ftk {
    /*! \brief Track FTK packet
     *
     * Output of the AUX/input into the SSB consisting of tracks fit by the
     * AUX. Each FTK track packet contains a set of track packets. See the
     * AUX spec for their format.
     */
    class EventFragmentTrackFTKPacket : public EventFragmentFTKPacket
    {
    public:
      /*! \brief Contructor, does nothing
       */  
      EventFragmentTrackFTKPacket();
      
      /*! \brief Deletes all of associated tracks
       */    
      virtual ~EventFragmentTrackFTKPacket();
      
      /*! \brief Get number of tracks in packet
       *
       * \return number of tracks
       */
      unsigned int getNTracks() const;
      
      /*! \brief Get track
       *
       * \param idx index of track of interest
       * 
       * \return track at index idx
       */  
      TFCombination* getTrack(unsigned int idx) const;

      /*! \brief Add track
       *
       * \param track New track to add
       */  
      void addTrack(TFCombination *track);

      /*! \brief Get dump chi2 property
       *
       * \return True when dump chi2 will be output by bitstream function
       */
      bool getDumpChi2() const;

      /*! \brief Set dump chi2 property
       *
       * Automatically set by the parsePayload() function upon detection
       * of chi2 in the track packets.
       *
       * \param dumpChi2 New value of dump chi2
       */
      void setDumpChi2(bool dumpChi2);

    protected:
      /*! \brief Extact track payload
       *
       * \param data Track packet payload
       */
      virtual void parsePayload(const std::vector<unsigned int>& data);

      /*! \brief Output the bitstream of the track packets
       *
       * The chi2 words are only included if the dumpChi2 property is 
       * true.
       *
       * \return The raw data of the tracks in the spy buffer
       */
      virtual std::vector<unsigned int> bitstreamPayload() const;

    private:
      std::vector<unsigned int> _data;

      std::vector<TFCombination*> _tracks;

      bool _dumpChi2;
    };

  } // namespace ftk
} // namespace daq

#endif // EVENTFRAGMENTTRACKFTKPACKET_H_

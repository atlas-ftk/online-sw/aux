#ifndef EVENTFRAGMENTDOOUTPUT_H_
#define EVENTFRAGMENTDOOUTPUT_H_

#include "ftkcommon/EventFragment.h"

namespace daq {
  namespace ftk {
    /*! \brief DO output spybuffer
     *
     * Other than stripping the trailer (EE word, L1ID), no 
     * additional parsing is done by this class.
     */
    class EventFragmentDOOutput : public EventFragment
    {
    public:
      /*! \brief Constructor
       */
      EventFragmentDOOutput();
      
      /*! \brief Destructor
       */
      virtual ~EventFragmentDOOutput();

      /*! \brief Get data for the event, not including EE word or L1ID
       *
       * \return event data
       */
      std::vector<unsigned int> getData() const;

      /*! \brief Load the event fragment
       *
       * Extacts L1ID and removed trailer. Use getData() to obtain the
       * data from the event frament.
       *
       * \param data event fragment data
       */
      virtual void parseFragment(const std::vector<unsigned int>& data);

      /*! \brief Output the bitstream for the event
       *
       * \return The raw data of the spy buffer
       */
      virtual std::vector<unsigned int> bitstream() const;

    private:
      std::vector<unsigned int> _data;
    };

    /*! \brief Parse raw DO output spybuffers event-by-event
     *
     * The contents of the last event are ignored, to ensure
     * complete events. Also any data before the first event
     * header is ignored.
     *
     * \param data raw DO output spybuffer data
     *
     * \return list of FTKDOOutputEventFragment corresponding 
     */
    std::vector<EventFragment*> DOOutput_splitFragments(const std::vector<unsigned int>& data);
  } // namespace ftk
} //namespace daq

#endif // EVENTFRAGMENTDOOUTPUT_H_

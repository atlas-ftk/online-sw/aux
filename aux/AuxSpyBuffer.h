#include "ftkcommon/SpyBuffer.h"
#include <string>
#include "ftkvme/VMEInterface.h"
#include "ftkcommon/core.h"
#include "ftkvme/VMEManager.h"
#include "aux/aux_vme_regs.h"
#include "ftkcommon/SourceIDSpyBuffer.h"

// Implementation of common spybuffer readout to emon
// 
// Lesya Horyn, 3/8/18
//


namespace daq {
  namespace ftk{

    class AuxSpyBuffer: public daq::ftk::SpyBuffer{

      public:
        
        AuxSpyBuffer(unsigned int fpga, unsigned int channel, unsigned int channelID, unsigned int slot, unsigned int crate);

        int readSpyBuffer();

        std::vector< uint32_t > getBuffer() {  return m_buffer; }; 
        uint32_t getSpyStatus() { return m_spyStatus; };
        uint32_t getSourceID() {return m_sourceID; }; 
        uint32_t getSpyFreeze();
        bool getSpyOverflow() { return m_overflow; };
        unsigned int getSpyPointer() { return m_spyPointer; };
        unsigned int getSpyDimension() { return m_buffer.size(); };

        int readSpyStatusRegister(uint32_t &spyStatus) { spyStatus = m_spyStatus; return 0;};



      private:


        unsigned int m_slot;
        unsigned int m_fpga;
        unsigned int m_channelID;
        unsigned int m_boardNumber;
        daq::ftk::Position m_position; 
        unsigned int m_channel; 
        unsigned int m_lastAddress;

        bool m_internalFreeze;
        bool m_endOfEventFreeze;
        bool m_nextBoardFreeze;
        bool m_errorFreeze; 
        
        uint32_t m_sourceID; //SourceIDSpyBuffer

        bool m_overflow;

        uint32_t m_spyPointer; //I think this tells us where the last written word is ("Offset to the start of the spybuffer data")
        

        unsigned int getBoardNumber(std::string name);
        void encodeSpyStatus();
        daq::ftk::Position getPosition();
      
   
   
    };
  }
}





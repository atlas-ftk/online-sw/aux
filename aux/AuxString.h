#ifndef AUXSTRING_H_
#define AUXSTRING_H_

#include <assert.h>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <stdarg.h>
#include <stdlib.h>

#include "aux/AuxMath.h"

/*! \brief Some useful string functions based on TString.h from ROOT
 */
namespace auxstring
{
  /*! \brief Create a string using printf-line formatting.
   *
   * \param fmt format of the string
   * \param ... format modifiers
   *
   * \return formatted string
   */
  std::string Format(const std::string& fmt, ...);

  /*! \brief In-place string replacement
   *
   * \param str string on which the replacement will be performed
   * \param from search for string
   * \param to replace with string
   */
  void ReplaceAll(std::string& str, const std::string& from, const std::string& to);

  /*! \brief Split a string on character
   *
   * \param string source string
   * \param token character on which to use as separator
   *
   * \return list of columns that were separated by token
   */
  std::vector<std::string> Tokenize(const std::string& str, char token);

  /*! \brief Split a series of string
   *
   * Tokenize each string in the vector, return a vector with size >= a.
   *
   * \param a list of source strings
   * \param token character on which to use as separator
   *
   * \return list of string that were separated by token
   */
  std::vector<std::string> Tokenize(const std::vector<std::string> &a , char token);

  /*! \brief Convert string into long long integer
   *
   * \param str string representing the number
   * \param base base of the number in the string
   *
   * \return str as a long long
   */
  Int_tf BaseConvertLL(const std::string& str,unsigned int base);

}


#endif // AUXSTRING_H_

#ifndef AUX_INPUTS_H
#define AUX_INPUTS_H

#include "aux/aux_interface.h"

using namespace std;

namespace daq
{
  namespace ftk
  {

    class input1 : public aux_interface
    {

    public:
      input1(unsigned int slot, std::string name);
      virtual void check_freeze();

      //// Run Control FSMs ////
      bool check_link_status(int i);
      //// Useful functions ////
      void load_rams(bool isLoadMode);
      void set_ignore_freeze(bool ignore);
      void set_freeze_mask(u_int mask);

      /*! \breif Controls data-flow from upstream
       *
       * Controls input from QSFP.
       *
       * \param enable Enable input dataflow.
       */
      virtual void enable_upstream_rx(bool enable);
      virtual void set_upstream_flow(bool enable);

      /*! \brief Controls data-flow to downstream
       *
       * Controls output to AMB.
       *
       * \param enable Enable dataflow.
       */
      virtual void set_downstream_flow(bool enable);

      void set_altera_to_altera();

      void load_hit_fifos(const std::string& path);
      void load_hit_and_ssid_fifos(const std::string& path);
      void load_ssid_fifos(const std::string& path);

      void reset_hit_fifos();
      void send_hit_fifos_pseudo_df(bool loop);
      void send_hit_fifos(bool loop);
      void send_hit_and_ssid_fifos(bool loop);
      void send_pseudodf_hit_fifos(bool loop); 
      void stop_hit_fifos(); 
      void enable_hitsort(bool enable);
      void reset_ssmap();
      void reset_slink();
      void reset_slink_fifos();
      virtual void reset_logic();
      void enable_proc_to_input_freeze(bool enable);
      /*! \brief Set HOLD ignore from AMB
       *
       * \param ignore True if AMB HOLD should be ignored.
       */
      void set_hold_ignore_amb(bool ignore);
      void hold_ignore_amb();

      /*! \brief Set HOLD ignore from Processors
       *
       * \param ignore True if Processor HOLD should be ignored.
       */
      void set_hold_ignore_proc(bool ignore);
      void hold_ignore_proc();


      void load_local_ss_maps(uint layer);
      void load_module_maps(uint layer);
      void set_checksumWarning(bool warningOnly);
      void run_ss_check_sums();
      void check_ss_checksum(uint layer);
      void check_module_checksum(uint layer);

      void set_ss_map_path(uint layer, const std::string& s, uint cs);
      void set_module_path(uint layer, const std::string& s, uint cs);
      
      //name for ERS log
      std::string name_ftk() { return m_name; }
      
      void set_source_hits(const std::string& s) { m_source_hits = s;};

    private:
      
      std::string m_source_hits;

      std::string m_ss_paths[4];
      std::string m_module_paths[4];


      unsigned int m_ss_cs[4];
      unsigned int m_module_cs[4];
    

      bool m_checksumWarning = true;
      bool m_holdIgnoreAMB = false;
      bool m_holdIgnoreProc = false;
    };

    class input2 : public aux_interface
    {

    public:
      input2(unsigned int slot, std::string name);

      //// Run Control FSMs ////
      virtual void configure();
      virtual void check_freeze();
      bool check_link_status(int i);
      bool check_sfp_status(uint i);
      //// Useful functions ////
      bool CheckAUXbusLockCode();
      void load_rams(bool isLoadMode);
      void set_ignore_freeze(bool ignore);
      void set_freeze_mask(u_int mask, u_int HWmask);
      void set_ignore_SSB_freeze(bool ignore);


      /*! \breif Controls data-flow from upstream
       *
       * Controls input from QSFP.
       *
       * \param enable Enable input dataflow.
       */
      virtual void enable_upstream_rx(bool enable);
      virtual void set_upstream_flow(bool enable);

      /*! \brief Controls data-flow to downstream
       *
       * Controls output to AMB and SFP.
       *
       * \param enable Enable dataflow.
       */
      virtual void set_downstream_flow(bool enable);

      void set_altera_to_altera();
      void reset_ssmap();
      void reset_hit_warrior();
      void reset_slink();
      void reset_slink_fifos();
      virtual void reset_logic();

      void enable_proc_to_input_freeze(bool enable);

      /*! \brief Set HOLD ignore from AMB
       *
       * \param ignore True if AMB HOLD should be ignored.
       */
      void set_hold_ignore_amb(bool ignore);
      void hold_ignore_amb();

      /*! \brief Set HOLD ignore from Processors
       *
       * \param ignore True if Processor HOLD should be ignored.
       */
      void set_hold_ignore_proc(bool ignore);
      void hold_ignore_proc();
      void set_force_hold_proc_out(bool force);
      void force_hold_proc_out();

      /*! \brief Set HOLD ignore from SSB
       *
       * \param ignore True if SSB HOLD should be ignored.
       */
      void set_hold_ignore_ssb(bool ignore);
      void hold_ignore_ssb();

      void load_hit_fifos(const std::string& path);
      void load_hit_and_ssid_fifos(const std::string& path);
      void load_ssid_fifos(const std::string& path);

      void reset_hit_fifos();
      void send_hit_fifos_pseudo_df(bool loop);
      void load_local_ss_maps(uint layer);
      void load_module_maps(uint layer);
      void send_hit_fifos(bool loop); 
      void send_hit_and_ssid_fifos(bool loop); 
      void stop_hit_fifos(); 
      void set_checksumWarning(bool warningOnly);
      void run_ss_check_sums();
      void check_ss_checksum(uint layer);
      void check_module_checksum(uint layer);

      void set_ss_map_path(uint layer, const std::string& s, uint cs);
      void set_module_path(uint layer, const std::string& s, uint cs);
      
      //name for ERS log
      std::string name_ftk() { return m_name; }

      /*! \brief Set source of SFP
       *
       * Possible values: 
       *  tracks: Tracks from the HW
       *  qsfp#: Pass through QSFP input in channel #
       *
       * \param s String describing the source for SFP output
       */
      void set_source_sfp(const std::string& s);
      

      void set_source_hits(std::string s) {m_source_hits = s;};
      
    private:
      std::string m_source_sfp;
      std::string m_source_hits;

      std::string m_ss_paths[4];
      std::string m_module_paths[4];

      unsigned int m_ss_cs[4];
      unsigned int m_module_cs[4];
      

      bool m_checksumWarning = true;
      bool m_holdIgnoreAMB  = false;
      bool m_holdIgnoreProc = false;
      bool m_holdIgnoreSSB  = false;
      bool m_forceHoldProcOut = false;
    };

  }
}

#endif


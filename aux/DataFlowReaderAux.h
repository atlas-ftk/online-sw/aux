#ifndef DATAFLOWREADERAUX_H
#define DATAFLOWREADERAUX_H

#include "ftkcommon/DataFlowReader.h"
#include "aux/dal/FtkDataFlowSummaryAUXNamed.h"

#include "aux/aux_vme_regs.h"
#include "aux/aux_status.h" 
#include "aux/aux_registers.h"

#include <string>
#include <vector>
#include <memory>

// Namespaces
namespace daq {
namespace ftk {

/*! \brief AUX implementation of DataFlowReader
*/
class DataFlowReaderAux : public DataFlowReader
{
public:
    /*! \brief Constructor: sets the input scheme (IS inputs or vectors)
     *  \param ohRawProvider Pointer to initialized  OHRawProvider object
     *  \param forceIS Force the code to use IS as the source of information (default is 0)
     */
  DataFlowReaderAux(std::shared_ptr<OHRawProvider<>> ohRawProvider, 
		bool forceIS = false);

  void init(const string& deviceName, std::vector<uint> clock_freq, std::vector<uint> periods, const std::string& partitionName = "FTK", const std::string& isServerName = "DF");

    // The function to overwrite for publishing extra histos.
    /*! \brief Publish extra distributions to IS as histograms
     *  \param option To be defined (default is 0)
     */
    void publishExtraHistos(uint32_t option = 0);


    //Redeclaration of common stuff
    void getFpgaTemperature(std::vector<int64_t>& srv);
    void getEventRate(std::vector<int64_t>& srv);
    void getL1id(std::vector<int64_t>& srv);
    void getDataErrors(std::vector<int64_t>& srv);
    
    void getFifoInBusy(std::vector<int64_t>& srv);
    void getFifoOutBusy(std::vector<int64_t>& srv);
    void getFifoInEmpty(std::vector<int64_t>& srv);
    void getFifoOutEmpty(std::vector<int64_t>& srv);
    void getFifoInBusyFraction(std::vector<int64_t>& srv, uint32_t option = 0);
    void getFifoOutBusyFraction(std::vector<int64_t>& srv, uint32_t option = 0);
    void getFifoInOverflowFlag(std::vector<int64_t>& srv); //SSMap
    void getFifoOutOverflowFlag(std::vector<int64_t>& srv); //HW and FTK2ROS
    void getInternalOverflowFlag(std::vector<int64_t>& srv); //Processors

    void getBusyFraction(std::vector<int64_t>& srv, uint32_t option = 0);
    void getLinkInStatus(std::vector<int64_t>& srv);
    void getLinkOutStatus(std::vector<int64_t>& srv);
    
    void getNEventsProcessed(std::vector<int64_t>& srv);
    void getNPacketsDiscarded(std::vector<int64_t>& srv); 
    void getNPacketsHeldSync(std::vector<int64_t>& srv);
    void getNPacketsDiscardedSync(std::vector<int64_t>& srv);
    void getNPacketsTimeoutSync(std::vector<int64_t>& srv);
    void getInputSkewSync(std::vector<int64_t>& srv);

    //Counts 
    void getAuxClusterCount(std::vector<int64_t>& srv);
    void getAuxModuleCount(std::vector<int64_t>& srv);
    void getAuxSSIDCount(std::vector<int64_t>& srv);
    void getAuxRoadCount(vector<int64_t>& srv);
    void getAuxOutputTracks(std::vector<int64_t>& srv);
    void getAuxTFFitCount(std::vector<int64_t>& srv);
    void getAuxTFTrackCount(std::vector<int64_t>& srv);
    void getAuxTFFitCountOneFPGA(std::vector<int64_t>& srv, unsigned int fpgaidx);
    void getAuxTFTrackCountOneFPGA(std::vector<int64_t>& srv, unsigned int fpgaidx);
    void getAuxNumFitsByType(vector<int64_t>& srv);
    void getAuxNumTracksByType(vector<int64_t>& srv);
    void getAuxNEventsInOut(std::vector<int64_t>& srv);
    void getAuxFifoOverflow(std::vector<int64_t>& srv);
    
    void getAuxErrorCountInput1(std::vector<int64_t>& srv);
    void getAuxErrorCountInput2(std::vector<int64_t>& srv);
    void getAuxErrorCountProcessor(std::vector<int64_t>& srv,  unsigned int fpgaidx);
    
    void getAuxInHoldCount(std::vector<int64_t>& srv);
    void getAuxOutHoldCount(std::vector<int64_t>& srv);
    void getAuxInternalHoldCount(std::vector<int64_t>& srv);
    void getAuxFreezeStatus(std::vector<int64_t>& srv);
    
    void getAuxFitLimitReached(std::vector<int64_t>& srv,unsigned int fpgaidx);
    void getAuxRoadLimitReached(std::vector<int64_t>& srv);
    void getAuxCombinerOverflow(std::vector<int64_t>& srv,unsigned int fpgaidx);
    
    //Rates
    void getAuxTFFitRateOneFPGA(std::vector<int64_t>& srv, unsigned int fpgaidx);
    void getAuxTFFitRate(std::vector<int64_t>& srv);
    void getAuxTFTrackRate(std::vector<int64_t>& srv);
    void getAuxTFTrackRateOneFPGA(std::vector<int64_t>& srv, unsigned int fpgaidx);


    //Max Stuff
    void getAuxMaxClusters(std::vector<int64_t>& srv);
    void getAuxMaxModules(std::vector<int64_t>& srv);
    void getAuxMaxSSIDs(std::vector<int64_t>& srv);
    void getAuxMaxFits(std::vector<int64_t>& srv, unsigned int fpgaidx);
    void getAuxMaxTracks(std::vector<int64_t>& srv, unsigned int fpgaidx);
    void getAuxMaxRoads(std::vector<int64_t>& srv);
    void getAuxMaxTracksOut(std::vector<int64_t>& srv);
    void getAuxHWOverlapLimitReached(std::vector<int64_t>& srv);
    void getAuxMaxHitsPerSSID(std::vector<int64_t>& srv);
    void getAuxMaxProcessingTime(std::vector<int64_t>& srv);
    void getAuxMinProcessingTime(std::vector<int64_t>& srv);


private:
    std::vector<u_int> m_clock_freqs;
    std::vector<u_int> m_periods;
    std::map<std::string, std::map<std::string,uint> > m_ISRegs;

    std::shared_ptr<FtkDataFlowSummaryAUXNamed> m_theAUXSummary;
    std::vector<std::string> getISVectorNames();
    void getAuxInLinkInfo( std::vector<int64_t> &srv, u_int sb_entry, bool isRate);
    void getAuxOutLinkInfo(std::vector<int64_t> &srv, u_int sb_entry, bool isRate);
    void getAuxInternalLinkInfo(std::vector<int64_t> &srv, u_int sb_entry, bool isRate);
    

    // ???????? Move all these defs to aux/aux_spy_regs.h?
    // Which spy buffer for each FPGA (ie what link to monitor)
    u_int I1_QSFP	= (0x30 << 16);
    u_int I1_HITS   = (0x31 << 16);
    u_int I1_SSDATA	= (0x32 << 16);

    u_int I2_QSFP 	= (0x30 << 16);
    u_int I2_HITS   = (0x31 << 16);
    u_int I2_SSDATA	= (0x32 << 16);
    u_int I2_GENDATA= (0x34 << 16);
    u_int I2_SFP 	= (0x35 << 16);

    u_int P_HITS	= (0x56 << 16);
    u_int P_TRACKS	= (0x57 << 16);
    u_int P_ROADS	= (0x59 << 16);
    //u_int LINK_STATUS	= (0x140); // Defined in aux/aux_vme_regs.h
    //u_int SLINK_STATUS 	= (0x150); // Defined in aux/aux_vme_regs.h

    // Entry in a given spy buffer block register
    u_int SB_NUM_PACKETS= (0x00 << 4);
    u_int SB_EVENT_RATE = (0x01 << 4);
    u_int SB_L1ID	    = (0x02 << 4);
    u_int SB_ERROR_BITS = (0x03 << 4);
    u_int SB_HOLD_FRAC	= (0x04 << 4);
    u_int SB_FIFO_USAGE	= (0x06 << 4);
    u_int SB_HOLD_COUNT = (12 << 4);

    // Channel locations
    u_int CHANNEL0 	= (0x00 << 8);
    u_int CHANNEL1 	= (0x01 << 8);
    u_int CHANNEL2 	= (0x02 << 8);
    u_int CHANNEL3 	= (0x03 << 8);
    u_int CHANNEL4 	= (0x04 << 8);
    u_int CHANNEL5 	= (0x05 << 8);
    u_int CHANNEL6 	= (0x06 << 8);
    u_int CHANNEL7 	= (0x07 << 8);
    u_int CHANNEL8 	= (0x08 << 8);
    u_int CHANNEL9 	= (0x09 << 8);
    u_int CHANNEL10 = (0x0a << 8);
};

} // namespace ftk
} // namespace daq

#endif /* DATAFLOWREADERAUX_H */

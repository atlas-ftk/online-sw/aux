#include <vector>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include "stdio.h"
#include "stdlib.h"
#include <unistd.h>
#include <math.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "ftkcommon/patternbank_lib.h"
#include "ftkvme/VMEInterface.h"
#include "ftkvme/VMEManager.h"
#include "aux/aux_vme_regs.h"
#include "aux/aux.h"

using std::cout;  using std::endl; using std::setw; using std::cerr; using std::bitset; 
using std::vector;

namespace daq 
{
  namespace ftk 
  {
    bool aux_status( int slot );

    bool aux_proc_status( int slot, bool do_details, bool rate_details);
    
    bool aux_input_status( int slot );

    bool aux_error_status( int slot, bool do_details );

    bool aux_proc_fifo_status( int slot );

    bool aux_i2_fifo_status( int slot );

    bool aux_i2_fifo_status_IH( int slot );

    bool aux_fifo_status( int slot, bool showP1, bool showP2, bool showP3, bool showP4 ); // Was aux_fifostatus_main

    bool aux_linkstatus( int slot, bool showI1, bool showI2, bool showP1, bool showP2, bool showP3, bool showP4, bool showUnused );

    bool aux_linkerrors( int slot, bool showI1, bool showI2, bool showP1, bool showP2, bool showP3, bool showP4, bool showUnused );

    bool aux_error_count( int slot, std::vector<int> FPGA_Nums, bool DO_errors, bool TF_errors, bool TFComb_errors, bool Input_freezes, bool Proc_freezes, bool Truncations);

    bool aux_aux2ros_status( int slot );
  }
}

#ifndef AUX_OKS_H
#define AUX_OKS_H 

#include <string>
#include <vector>

// #include "ROSCore/ReadoutModule.h"

#include <config/ConfigObject.h>
#include <config/Configuration.h>

#include <dal/OnlineSegment.h>
#include <dal/ResourceSet.h>
#include <dal/Segment.h>
#include <dal/Partition.h>

#include <dal/seg-config.h>
#include <dal/util.h>

#include "DFdal/ReadoutConfiguration.h"
#include "RunControl/Common/OnlineServices.h"

//#include "aux/dal/ReadoutModule_Aux.h"
#include "ProcessingUnit/dal/ReadoutModule_PU.h"
#include "aux/dal/auxNamed.h"

// #include "ipc/partition.h"
// #include "ipc/core.h"
// #include "is/info.h"

#include <iostream>
#include <string>

namespace daq {
  namespace ftk {

    class aux_oks {

      public: 
        aux_oks();

        void print_segment(const daq::core::SegConfig& seg, unsigned int recursion_level);
        void print_resource(const daq::core::ResourceBase * res, unsigned int recursion_level);

    };

  } // namespace ftk
} // namespace daq
#endif // AUX_OKS_H
 


#include <config/ConfigObject.h>
#include <config/Configuration.h>

#include <dal/OnlineSegment.h>
#include <dal/ResourceSet.h>
#include <dal/Segment.h>
#include <dal/Partition.h>

#include <dal/seg-config.h>
#include <dal/util.h>

#include "DFdal/ReadoutConfiguration.h"
#include "RunControl/Common/OnlineServices.h"

#include "aux/dal/ReadoutModule_Aux.h"
//#include "ProcessingUnit/dal/ReadoutModule_PU.h"
#include "aux/dal/firmware.h"
#include "aux/dal/AuxConstants.h"


#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#define DEBUG std::cerr << __FILE__ << "::" << __LINE__ << std::endl



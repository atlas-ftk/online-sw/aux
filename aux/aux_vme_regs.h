#ifndef AUX_VME_REGS_H
#define AUX_VME_REGS_H

#define CHECK_ERROR(x) if (x != VME_SUCCESS) \
    { \
      VME_ErrorPrint(x); \
      return (x); \
    }

#define CHECK_ERROR_STATUS(x,status) if (x != VME_SUCCESS) \
    { \
      VME_ErrorPrint(x); \
      status.vmestatus = x; \
      return (status); \
    }

//++++++++++++++++++++++++++++++++++++++++++++
//++++++++NUMBER OF EACH FITTER TYPE +++++++++
//++++++++++++++++++++++++++++++++++++++++++++
#define NNOMFITTERS 1
#define NPIXFITTERS 4
#define NSCTFITTERS 2

//++++++++++++++++++++++++++++++++++++++++++++
//++++++++++ DIFFERENT FPGA ADDRESSES ++++++++
//++++++++++++++++++++++++++++++++++++++++++++
#define INPUT1                0x01000000
#define INPUT2                0x02000000
#define PROCESSOR1            0x03000000
#define PROCESSOR2            0x04000000
#define PROCESSOR3            0x05000000
#define PROCESSOR4            0x06000000

#define PROTOAUXTX            0x01000000
#define PROTOAUXRX            0x04000000

//++++++++++++++++++++++++++++++++++++++++++++
//++++++++++ VME INTERFACE REGISTER ++++++++++
//++++++++++++++++++++++++++++++++++++++++++++
#define GIT_FPGA              0x00000570
#define GIT_AUXCOMMON         0x00000580
#define CLOCK_FREQUENCY       0x00000590

#define INFO                  0x00000000
#define TMODE                 0x0000003c
#define RESET                 0x00000044
#define RESET_XCVR            0x00000048
#define RESETS                0x0000004c
#define RESET_SYNC            0x00000050

#define TX_FIFOWRITE          0x00000030
#define FREEZE_REQ            0x00000030
#define FREEZE_ACK            0x00000040

#define AUX_DISABLE_HOLDS     0x000000B0

#define TEMPERATURE           0x00000110
#define INTERNAL_LINES        0x00000110
#define FREEZE_LINES          0x00000110
#define HOLD_STATUS           0x00000130
#define SYNC_STATUS           0x00000140
#define LINK_STATUS           0x00000140
#define SLINK_STATUS          0x00000150
#define HITHOLD1_STATUS       0x00000160
#define HITHOLD2_STATUS       0x00000130
#define DFSYNCH_STATUS        0x000001a0
#define DO_STATUS             0x000001b0
#define DO_FREEZE             0x00000350
#define DO_WRITING_FRACTION           0x00000390
#define DO_READING_FRACTION           0x000003a0
#define DO_WRITINGNOTREADING_FRACTION 0x000003b0
#define DO_READINGNOTWRITING_FRACTION 0x000003c0
#define DO_WAITING_FRACTION           0x000003d0
#define TF_STATUS             0x000001c0
#define TFBL_HOLD             0x00000380
#define TFBL_HOLDRATE         0x005D0000
 
#define FREEZE_STATUS         0x000001F0
#define HW_FREEZE_STATUS      0x00000220
#define HW_STATUS             0x000003C0
#define PM_STATUS             0x000003E0
#define CHIPID1               0x00000200
#define CHIPID2               0x00000210
#define PROC_GEN_ERROR_STATUS 0x00000300
#define ROAD_ID_USEDW         0x004C0000
#define AMCINPUT_ID_USEDW     0x004C0100
#define AMCOUTPUT_ID_USEDW    0x004C0200
#define AMLOAD_ID_USEDW       0x004C0300
#define DO_SS_FIFO_1          0x00000230
#define DO_SS_FIFO_2          0x00000240
#define DO_SS_FIFO_3          0x00000250
#define TOTAL_WR_EVENTS       0x00000260
#define RATE_WR_EVENTS        0x00000270
#define TOTAL_RD_EVENTS       0x00000280
#define RATE_RD_EVENTS        0x00000290
#define TOTAL_ROADS           0x000002A0
#define RATE_ROADS            0x000002B0
#define MAX_ROADS             0x000002C0
#define ROAD_LIMIT_REACHED    0x000002D0

#define MAX_FITS_PER_ROAD     0x00000020

#define OUTPUT_TRACKS         0x3f0

#define MAXTIME_SSMAP         0x460
#define MINTIME_SSMAP         0x470
#define MAXTIME_HW            0x480
#define MINTIME_HW            0x490
#define MAXTIME_PU            0x4a0
#define MINTIME_PU            0x4b0
#define MAXTIME_PROC          0x650 
#define MINTIME_PROC          0x660

#define FREEZE_MASK_REG       0x00000100
#define FREEZE_MASK_HW_REG    0x000000F0

#define AMBRR_MONITOR         0x00300000

#define FIFO_SSMAP               0x00450000



#define I1_SSMAP_INPUT           0x00000000
#define I1_SSMAP_SCT_SERIALIZER  0x00000400
#define I1_SSMAP_PIX_INTERLEAVER 0x00000500
#define I1_SSMAP_HS_INPUT        0x00000B00
#define I1_SSMAP_HS_LAYERS       0x00002400
#define I1_SSMAP_PM_FRAME        0x00000f00
#define I1_SSMAP_PM_DATA         0x00001600
#define I1_SSMAP_AM_L1ID         0x00001D00

#define I2_SSMAP_INPUT           0x00000000
#define I2_SSMAP_SCT_SERIALIZER  0x00000400
#define I2_SSMAP_HS_INPUT        0x00000800
#define I2_SSMAP_HS_LAYERS       0x00001800
#define I2_SSMAP_PM_FRAME        0x00000C00
#define I2_SSMAP_PM_DATA         0x00001000
#define I2_SSMAP_AM_L1ID         0x00001400


#define FIFO_LATENCY          0x00390000
#define FIFO_HW_NO_IH_INPUT         0x003c0000
#define FIFO_HW_NO_IH_COMPARATOR    0x003d0000
#define FIFO_HW_NO_IH_OUTPUT        0x003e0000
#define FIFO_HW_NO_IH_PKT_HEADER    0x003f0000
#define FIFO_HW_NO_IH_PKT_DATA      0x00400000
#define FIFO_HW_NO_IH_PKT_TRAILER   0x00410000
#define FIFO_HW_NO_IH_PKT_MERGE     0x00430000
#define FIFO_HW_INPUT         0x003c0000
#define FIFO_HW_COMPARATOR    0x003d0000
#define FIFO_HW_DATA_BUFFER   0x003e0000
#define FIFO_HW_GATED_OUTPUT  0x003f0000
#define FIFO_HW_PM_FRAME      0x00400000
#define FIFO_HW_PM_DATA       0x00410000

#define FIFO_TX               0x00000240
#define FIFO_RX               0x00000300
#define FIFO_ROAD             0x000003C0
#define FIFO_MERGETX          0x00000400
#define FIFO_MERGERX          0x00000410

#define FIFO_FTK2ROS          0x003A0000

#define FIFO_DFSYNC_HEADER    0x004B0400
#define FIFO_DFSYNC_TRAILER   0x004B0500
#define FIFO_DFSYNC_PIX_HIT   0x00480000
#define FIFO_DFSYNC_PIX_SS    0x00490000
#define FIFO_DFSYNC_SCT       0x004A0000
#define FIFO_DFSYNC_ROAD      0x004B0000

#define HOLD_CONTROL          0x000000B0
#define HIT_HOLD_CONTROL      0x000000D0
#define DISABLE_DATA          0x000000C0
#define HIT_DISABLE_DATA      0x000000D0

#define MONITORING_PERIOD     0x640


//++++++++++++++++++++++++++++++++++++++++++++
//++++++++++ BITS ++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++
#define BIT_FREEZE_REQ        7
#define BIT_FREEZE_ACK        4


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++ BITS, WORDS, AND REGISTERS FOR OPERATIONS  ++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define SET_ALTERA_REG         0x00000050
#define SET_ALTERA_WORD        0x00000020

#define PROC_FROM_FIFO_REG 0x80
#define PROC_HITS_FROM_FIFO_BIT 28
#define PROC_ROAD_FROM_FIFO_BIT 31

#define PROC_ENABLE_RSLB_REG    0x40

#define PROC_DO_VMECONTROL       0x70
#define BIT_AMMAP_CHECKSUM_RESET 16
#define BIT_AMMAP_CHECKSUM_START 14
#define BIT_AMMAP_MUX_CHECKSUM   15

#define PROC_DO_RESET_REG       0x70
#define PROC_DO_RESET_BIT       21

#define DO_ERROR_COUNT             0x5F0
#define DO_ERROR_COUNT_CTRL_ADDR   0x70 
#define NUM_DO_ERR_TYPES           13 
#define DO_ERROR_COUNT_CTRL_BITS   22 

#define TFBL_ERROR_COUNTER                   0x600
#define TFBL_ERROR_COUNTER_SELECTOR          0x80 
#define TFBL_ERROR_COUNTER_SELECTOR_BITSHIFT 2

#define TFCOMB_ERROR_COUNTER                   0x630
#define TFCOMB_ERROR_COUNTER_SELECTOR          0x80 
#define TFCOMB_ERROR_COUNTER_SELECTOR_BITSHIFT 7

#define FREEZE_COUNTER                     0x610
#define FREEZE_COUNTER_SELECTOR            0x10
#define FREEZE_COUNTER_SELECTOR_BITSHIFT   17

#define PROC_TF_RESET_REG       0x30
#define PROC_TF_RESET_BIT       9

#define PROC_TF_RUN_CHECKSUM_REG    0x70
#define PROC_TF_RUN_CHECKSUM_BIT    2
#define PROC_TF_RUN_CHECKSUM_WORD   0x4
#define PROC_TF_READ_CHECKSUM_WORD  0x1e0

#define PROC_TF_TOWERID_REG 0x80
#define PROC_TF_TOWERID_BIT 20

#define AUX_ID_REG 0x80
#define AUX_ID_BIT 18
#define AUX_128PU_REG 0x80
#define AUX_128PU_BIT 19

#define PROC_AM_READ_CHECKSUM_WORD  0x1d0
#define BIT_AMMAP_CHECKSUM_DONE   25

#define PROC_ROAD_FIFO_REG0  0x0d
#define PROC_ROAD_FIFO_REG1  0x0e
#define PROC_ROAD_FIFO_REG2  0x0f
#define PROC_ROAD_FIFO_REG3  0x10

#define PROC_HIT_SSID_FIFO_REG0  0x01
#define PROC_HIT_SSID_FIFO_REG1  0x02
#define PROC_HIT_SSID_FIFO_REG2  0x03
#define PROC_HIT_SSID_FIFO_REG3  0x04
#define PROC_HIT_SSID_FIFO_REG4  0x05
#define PROC_HIT_SSID_FIFO_REG5  0x06
#define PROC_HIT_SSID_FIFO_REG6  0x07
#define PROC_HIT_SSID_FIFO_REG7  0x08
#define PROC_HIT_SSID_FIFO_REG8  0x09
#define PROC_HIT_SSID_FIFO_REG9  0x0a
#define PROC_HIT_SSID_FIFO_REG10 0x0b

#define PROC_RESET_HIT_ROAD_FIFO_REG 0x30

#define PROC_SEND_FIFO_DATA_REG 0x30
#define PROC_SEND_FIFO_ROAD_BIT 10
#define PROC_SEND_FIFO_HITS_BIT 8
#define PROC_SEND_FIFO_LOOP_BIT 13

#define BIT_PROC_ENABLE_RSLB 0

#define BIT_PROC_RESET_DFSYNCH 18

#define BIT_PROC_HIT_FORCE_HOLD 16
#define BIT_PROC_HIT_DISABLE_DATA 0

#define BIT_PROC_ROAD_FORCE_HOLD 16
#define BIT_PROC_ROAD_DISABLE_DATA 0

#define BIT_PROC_HW_IGNORE_HOLD 4

#define INPUT_RESET_SSMAP_REG  0x10

#define INPUT_DISABLE_HOLDS_REG 0x80

#define BIT_INPUT_SS_RESET 4
#define BIT_HW_RESET 17

#define BIT_INPUT_SS_CHECKSUM_MUX 0
#define BIT_INPUT_SS_CHECKSUM_START 1
#define BIT_INPUT_SS_CHECKSUM_RESET 2

#define BIT_I1_QSFP_FORCE_HOLD 16
#define BIT_I1_AMB_IGNORE_HOLD 4
#define BIT_I1_HIT_IGNORE_HOLD 0

#define BIT_I1_QSFP_DISABLE_DATA 0
#define BIT_I1_AMB_DISABLE_DATA 4
#define BIT_I1_HIT_DISABLE_DATA 0

#define BIT_I2_QSFP_FORCE_HOLD 16
#define BIT_I2_AMB_IGNORE_HOLD 4
#define BIT_I2_HIT_IGNORE_HOLD 0
#define BIT_I2_TRACKS_FORCE_HOLD 24
#define BIT_I2_SFP_IGNORE_HOLD 24

#define BIT_I2_SSB_IGNORE_FREEZE 8

#define BIT_I2_QSFP_DISABLE_DATA 0
#define BIT_I2_AMB_DISABLE_DATA 4
#define BIT_I2_HIT_DISABLE_DATA 16
#define BIT_I2_TRACKS_DISABLE_DATA 8
#define BIT_I2_SFP_DISABLE_DATA 26

#define INPUT_RESET_HIT_FIFO_REG 0x30
#define INPUT_RESET_HIT_FIFO_BIT 4
#define INPUT_SEND_PSEUDO_DF_FIFO_DATA_REG 0x30
#define INPUT_SEND_PSEUDO_DF_FIFO_DATA_BIT 1
#define INPUT_SEND_PSEUDO_DF_FIFO_LOOP_BIT 5

#define INPUT_THROTTLE_REG 0xa0
#define INPUT_SEND_FIFO_REG  0x30
#define INPUT_SEND_HITS_AUX_LOGIC_BIT  14
#define INPUT_SEND_HITS_USER_LOGIC_BIT 13
#define INPUT_SEND_HITS_FIFO_LOOP_BIT  5
#define INPUT_SEND_HITS_AND_SSIDS_AUX_LOGIC_BIT   2
#define INPUT_SEND_HITS_AND_SSIDS_USER_LOGIC_BIT  1
#define INPUT_SEND_HITS_AND_SSIDS_FIFO_LOOP_BIT   5 

#define INPUT_RUN_SS_CHECKSUM_REG 0x40

#define I1_HIT_FIFO_REG0 0x1
#define I1_HIT_FIFO_REG1 0x2
#define I1_HIT_FIFO_REG2 0x3
#define I1_HIT_FIFO_REG3 0x4

#define I1_HIT_SS_FIFO_REG4  0x11
#define I1_HIT_SS_FIFO_REG5  0x12
#define I1_HIT_SS_FIFO_REG6  0x13
#define I1_HIT_SS_FIFO_REG7  0x14
#define I1_HIT_SS_FIFO_REG8  0x15
#define I1_HIT_SS_FIFO_REG9  0x16
#define I1_HIT_SS_FIFO_REG10 0x17
#define I1_HIT_SS_FIFO_REG11 0x18

#define I1_SS_FIFO_REG4  0x09
#define I1_SS_FIFO_REG5  0x0a
#define I1_SS_FIFO_REG6  0x0b
#define I1_SS_FIFO_REG7  0x0c
#define I1_SS_FIFO_REG8  0x0d
#define I1_SS_FIFO_REG9  0x0e
#define I1_SS_FIFO_REG10 0x0f
#define I1_SS_FIFO_REG11 0x10

#define I1_SS_MAP_FIFO_REG3  0x1f
#define I1_SS_MAP_FIFO_REG0  0x20
#define I1_SS_MAP_FIFO_REG1  0x21
#define I1_SS_MAP_FIFO_REG2  0x22

#define I1_SSID_CHECKSUM3_REG   0x340
#define I1_MODULE_CHECKSUM3_REG 0x350

#define I1_SSID_CHECKSUM0_REG   0x360
#define I1_MODULE_CHECKSUM0_REG 0x370

#define I1_SSID_CHECKSUM1_REG   0x380
#define I1_MODULE_CHECKSUM1_REG 0x390

#define I1_SSID_CHECKSUM2_REG   0x3A0
#define I1_MODULE_CHECKSUM2_REG 0x3B0

#define I2_HIT_FIFO_REG4 0x01
#define I2_HIT_FIFO_REG5 0x02
#define I2_HIT_FIFO_REG6 0x03
#define I2_HIT_FIFO_REG7 0x04

#define I2_HIT_SS_FIFO_REG0 0x0e
#define I2_HIT_SS_FIFO_REG1 0x0f
#define I2_HIT_SS_FIFO_REG2 0x10
#define I2_HIT_SS_FIFO_REG3 0x11

#define I2_SS_FIFO_REG0 0x0a
#define I2_SS_FIFO_REG1 0x0b
#define I2_SS_FIFO_REG2 0x0c
#define I2_SS_FIFO_REG3 0x0d


#define I2_SS_MAP_FIFO_REG4  0x1f
#define I2_SS_MAP_FIFO_REG5  0x20
#define I2_SS_MAP_FIFO_REG6  0x21
#define I2_SS_MAP_FIFO_REG7  0x22

#define I2_RUN_SS_CHECKSUM_REG 0x40

#define I2_SSID_CHECKSUM4_REG   0x340
#define I2_MODULE_CHECKSUM4_REG 0x350

#define I2_SSID_CHECKSUM5_REG   0x360
#define I2_MODULE_CHECKSUM5_REG 0x370

#define I2_SSID_CHECKSUM6_REG   0x380
#define I2_MODULE_CHECKSUM6_REG 0x390

#define I2_SSID_CHECKSUM7_REG   0x3A0
#define I2_MODULE_CHECKSUM7_REG 0x3B0

#define HW_IH_NEventsProcessed         0x230
#define HW_IH_NPacketsDiscarded_P1     0x240
#define HW_IH_NPacketsDiscarded_P2     0x250
#define HW_IH_NPacketsDiscarded_P3     0x260
#define HW_IH_NPacketsDiscarded_P4     0x270
#define HW_IH_NPacketsDiscardedSync_P1 0x280
#define HW_IH_NPacketsDiscardedSync_P2 0x290
#define HW_IH_NPacketsDiscardedSync_P3 0x2A0
#define HW_IH_NPacketsDiscardedSync_P4 0x2B0
#define HW_IH_NTimeoutsSync_P1         0x2C0
#define HW_IH_NTimeoutsSync_P2         0x2D0
#define HW_IH_NTimeoutsSync_P3         0x2E0
#define HW_IH_NTimeoutsSync_P4         0x2F0
#define HW_IH_NPacketsHeldSync_P1      0x300
#define HW_IH_NPacketsHeldSync_P2      0x310
#define HW_IH_NPacketsHeldSync_P3      0x320
#define HW_IH_NPacketsHeldSync_P4      0x330
#define HW_IH_SkewCounter              0x420


#define CLUSTER_COUNT                  0x05
#define MODULE_COUNT                   0x06
#define SSID_COUNT                     0x07

#define FIT_LIMIT_REACHED              0x58 
#define COMBINER_OVERFLOW              0x5C 

//++++++++++++++++++++++++++++++++++++++++++++
//++++++++++ REMOTE UPGRADE REGISTERS ++++++++
//++++++++++++++++++++++++++++++++++++++++++++
#define RU_ENABLE             0x00000038
#define RU_RECON              0x00000060
#define RU_EPCQ               0x00000060
#define RU_ADDR               0x00000070
#define RU_DATA               0x00420000
#define RU_RECONDATA          0x00000090
#define RU_STATUS             0x00000110
#define EPCQ_CONTENTS         0x00420000

//++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++ WORD +++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++
#define FILL_WORD             0xFFFFFFFF
#define BLANK_WORD            0x00000000

#endif

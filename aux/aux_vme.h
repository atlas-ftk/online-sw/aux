#ifndef AUX_VME_H_
#define AUX_VME_H_

#include "ftkcommon/core.h"

namespace daq 
{
  namespace ftk 
  {
    int aux_vme_write_bit(int handle, u_int address, u_int bit, u_int value);
  }
}
#endif // AUX_VME_H_

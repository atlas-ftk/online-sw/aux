#ifndef AUX_FWMAP_H_
#define AUX_FWMAP_H_

#include <string>
#include <vector>
#include <fstream>

namespace daq 
{
  namespace ftk 
  {
    struct aux_fwmap_block_t
    {
      std::string name;
      uint start_address;
      uint end_address;
    };

    class AUX_FWMap
    {
    public:
      AUX_FWMap();
      AUX_FWMap(const std::string& RPDPath, const std::string& MAPPath);

      std::string RPDPath() const;

      aux_fwmap_block_t block(uint idx) const;

    private:
      std::string _RPDPath;
      std::vector<aux_fwmap_block_t> _blocks;

    };
  }
}

#endif // AUX_FWMAP_H_

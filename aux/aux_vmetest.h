#ifndef AUX_UPGRADE_H_
#define AUX_UPGRADE_H_

#include <string>
#include <iostream>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ftkcommon/core.h"
#include "aux/aux_vme_regs.h"

namespace daq 
{
  namespace ftk 
  {
    bool aux_vmetest(int slot, int fpga, u_int addr, u_int value);
  }
}
#endif // AUX_UPGRADE_H_

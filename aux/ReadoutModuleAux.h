/********************************************************/
/*                                                      */
/********************************************************/

#ifndef READOUT_MODULE_AUX_H
#define READOUT_MODULE_AUX_H 

#include <string>
#include <vector>


#include "ipc/partition.h"
#include "ipc/core.h"
#include "ROSCore/ReadoutModule.h"
#include "is/info.h"
#include "oh/OHRootProvider.h"
#include "oh/OHRawProvider.h"
#include "aux/aux_card.h"
#include "aux/dal/auxNamed.h"

#include "aux/DataFlowReaderAux.h"
#include "aux/StatusRegisterAuxFactory.h"

namespace daq {
namespace ftk {

  class ReadoutModule_Aux : public ROS::ReadoutModule
  {
  public:

    // overloaded methods inherited from ReadoutModule
    virtual void setup(DFCountedPointer<ROS::Config> configuration) override;
    virtual void configure(const daq::rc::TransitionCmd& cmd) override;
    virtual void connect(const daq::rc::TransitionCmd& cmd) override;
    virtual void prepareForRun(const daq::rc::TransitionCmd& cmd) override;
    virtual void stopRecording(const daq::rc::TransitionCmd& cmd) override;
    virtual void publish(void) override;
    virtual void clearInfo() override;
    //virtual void user(const daq::rc::UserCmd& cmd) override;

    //virtual DFCountedPointer < Config > getInfo();
    virtual const std::vector<ROS::DataChannel *> *channels() override; 

    //void SlinkClean();

    // Constructor and destructor
    ReadoutModule_Aux();
    virtual ~ReadoutModule_Aux() noexcept;

    //name for ERS log
    std::string name_ftk() { return m_name; }

//    std::string Name;     

  private:

    std::unique_ptr<auxNamed>        m_auxNamed;      ///< Access IS via schema


    std::vector<ROS::DataChannel *>  m_dataChannels;   
    DFCountedPointer<ROS::Config>    m_configuration; /**< Configuration Object, Map Wrapper */
    std::string 					           m_isServerName;	/**< IS Server	*/
    IPCPartition 					           m_ipcpartition;	/**< Partition	*/
    uint32_t    					           m_runNumber;			/**< Run Number	*/
    bool                             m_ispublish;     ///< Used to prevent publishing until end of prepare for run.
    bool                             m_dryRun;        ///< Bypass VME calls
    std::unique_ptr<OHRootProvider>  m_ohProvider;    ///< Histogram provider
     std::shared_ptr<OHRawProvider<>> m_ohRawProvider;    ///< Raw histogram provider
     std::unique_ptr<DataFlowReaderAux> m_dfReaderAux;   ///< DataFlowReaderAMB

    
    // AUX BOARD Specific Parameters
    std::string     m_name;				/**< Card name      */
    uint32_t        m_slot;				/**< Slot	    */
     std::unique_ptr<daq::ftk::aux>  m_aux;                              /**< Implementation */

    // Histograms
     std::unique_ptr<TH1F>           m_Histogram;

    unique_ptr<StatusRegisterAuxFactory> m_statusRegisterAuxFactory;

  };

inline const std::vector<ROS::DataChannel *> *ReadoutModule_Aux::channels()
{
  return &m_dataChannels;
}

} // namespace ftk
} // namespace daq
#endif // READOUT_MODULE_AUX_H
 

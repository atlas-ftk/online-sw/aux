#ifndef AUX_UPGRADE_H_
#define AUX_UPGRADE_H_
namespace daq 
{
  namespace ftk 
  {
    int aux_upgrade_wait_for_done(int handle);
    int aux_upgrade_clear_regs(int handle);
  }
}
#endif // AUX_UPGRADE_H_

//
// Main eader file for DO bit level check
// Created by Michael Hank on July 23, 2019.
// Largely based on Patrick's python code, also using work done by Lesya.
//
//
#include <vector>
#include <map>
#include <string>
#include <utility>
#include <aux/DataOrganizer.h>
#include <aux/AMBank.h>

#ifndef FTK_AUX_BITLEVEL_DO_LIB_H
#define FTK_AUX_BITLEVEL_DO_LIB_H

namespace daq
{
    namespace ftk
    {

        void do_toplevel(bool verbose, const std::string& inDirName, int proc, string ambank, bool oldDC, bool allSectors, int inChipOff, bool is_loop);

        //Store information from the spybuffers
        class aux_DO_spies
        {

        public:
            std::vector< std::vector< std::vector< long > > > m_SCTInput;//(5); //have to use long as ints don't go high enough for words like "FF1234FF"
            std::vector< std::vector< std::vector< long > > > m_PIXInput;//(6); //one vector of longs per event per plane
            std::vector< bool > m_ProcOverflow;//(4);
            std::vector< std::vector< std::vector< std::vector< long > > > > m_SCTOutput;//(4); //one vector of ints per event per plane per processor
            std::vector< std::vector< std::vector< std::vector< long > > > > m_PIXOutput;//(4);
            std::vector< std::vector< std::vector< long > > > m_RoadsInput;//(4); //one vector of ints per event per processor
            std::vector< std::vector< std::vector< long > > > m_RoadsOutput;//(4); //one vector of ints per event per processor
            std::vector< std::vector< std::vector< long > > > m_LaySecOutput;//(4);
            static const int NPROCS = 4; 

            aux_DO_spies(const std::string& inDirName, int proc, bool is_loop) : 
            m_SCTInput(5, std::vector<std::vector<long>>(0)) , 
            m_PIXInput(6, std::vector<std::vector<long>>(0)), 
            m_ProcOverflow(4),

            m_RoadsInput(4, std::vector<std::vector<long>>(0)),

            m_SCTOutput(4, std::vector<std::vector<std::vector<long>>>(5, std::vector<std::vector<long>>(0))), 
            m_PIXOutput(4, std::vector<std::vector<std::vector<long>>>(6, std::vector<std::vector<long>>(0))), 
            m_RoadsOutput(4, std::vector<std::vector<long>>(0)), 
            m_LaySecOutput(4, std::vector<std::vector<long>>(0)) 
            {
                //Label input files
                string in1 = inDirName+"/do_input_i1.dat";
                string in2 = inDirName+"/do_input_i2.dat";

                //Read input
                readInput(in1,in2);

                //Read outputs
                if(proc==0) {
                    for(unsigned int iProc=1; iProc<NPROCS+1; iProc++){
                        readProcs(inDirName + "/do_output_p" + std::to_string(iProc)+".dat", iProc);
                        if(is_loop){
                            readRoads(inDirName + "/do_roads_p" + std::to_string(iProc)+".dat", iProc);
                        }
                    }
                } else {
                    readProcs(inDirName + "/do_output_p" + std::to_string(proc)+".dat", proc);
                    if(is_loop){
                        readRoads(inDirName + "/do_roads_p" + std::to_string(proc)+".dat", proc);
                    }
                }
                
            }
            ~aux_DO_spies() {}

            void readInput( std::string file1, std::string file2)
            {
                //read i1 file
                std::ifstream input( file1 );
                std::string line;


                bool seen_b0f0 [7] = { false }; //Have we seen the beginning of event word? 0 is SCT0, 1-6 is PIX 0-5

                std::vector<long> sct_event;//contains sct 0 events
                std::vector<std::vector<long>> pix_event(6,std::vector<long>(0)) ;//contains pix 0-5 events. pix_event[0] is pix0 event
                for( std::string line; getline( input, line ); ){

                    //Read in first line
                    std::vector<std::string> inLine = auxstring::Tokenize(line,' ');

                    //tells us if output has overflow info, don't need to store yes/no
                    if(inLine[0] == "[ovfl]:" || inLine[0] == "[last]:") {
                        continue;
                    }

                    //loop over line to extract info
                    for(unsigned int iCol = 0; iCol < inLine.size(); iCol++){

                        //Columns 1, 4, 7... have data
                        if((iCol+2) % 3 != 0)  continue;

                        if(inLine[iCol]=="0000b0f0"){
                            if(iCol==1) {
                                if(seen_b0f0[(iCol-1)/3]){
                                    m_SCTInput[0].push_back(sct_event);
                                }
                                sct_event.clear();
                            } else if (iCol < 20) {
                                if(seen_b0f0[(iCol-1)/3]){
                                    m_PIXInput[ (iCol-4)/3 ].push_back(pix_event[(iCol-4)/3]);
                                }
                                pix_event[(iCol-4)/3].clear();
                            }
                            seen_b0f0[(iCol-1)/3] = true;
                        }

                        //group by data type, 1-7 are pixel, 0 and the 4 from input2 are SCT, 7 we neglect (all 0s)
                        if(iCol == 1) {
                            if(seen_b0f0[0]){
                                sct_event.push_back(std::stol(inLine[iCol], nullptr, 16));
                            }
                        } else if(iCol == 22) {
                            continue;
                        } else {
                            if(seen_b0f0[(iCol-1)/3]){
                                pix_event[(iCol-4)/3].push_back(std::stol(inLine[iCol], nullptr, 16));
                            }
                        }
                    }
                }
                pix_event.clear();
                sct_event.clear();

                //read i2 file
                std::ifstream input2( file2 );

                bool seen_b0f0_2 [4] = { false }; //Have we seen the beginning of event word? 0 is SCT1, 1 is SCT 2...
                std::vector<std::vector<long>> sct_event_2(4,std::vector<long>(0));//contains sct 1-4 events
 
                for( std::string line; getline( input2, line ); ){

                    //Read in first line
                    std::vector<std::string> inLine = auxstring::Tokenize(line,' ');

                    //tells us if output has overflow info, don't need to store yes/no
                    if(inLine[0] == "[ovfl]:" || inLine[0] == "[last]:") {
                        continue;
                    }

                    //loop over line to extract info
                    for(unsigned int iCol = 0; iCol < inLine.size(); iCol++){

                        //Columns 1, 4, 7... have data
                        if((iCol+2) % 3 != 0)  continue;

                        if(inLine[iCol]=="0000b0f0"){
                            if(seen_b0f0_2[(iCol-1)/3]){
                                m_SCTInput[(iCol+2)/3].push_back(sct_event_2[(iCol-1)/3]);
                            }
                            sct_event_2[(iCol-1)/3].clear();
                            seen_b0f0_2[(iCol-1)/3] = true;
                        }

                        if(seen_b0f0_2[(iCol-1)/3]){
                            sct_event_2[ (iCol-1)/3 ].push_back(std::stol(inLine[iCol], nullptr, 16));
                        }
                    }
                }
                sct_event_2.clear();
                return;
            }

            void readRoads( std::string file, int proc)
            {

                bool seen_e0da [4] = { false };
                std::vector<long> event;                    

                int eventmax = 0;
                int submax = 99999;
                //Columns 10, 13, 16, 19 have roads
                for(unsigned int iCol = 10; iCol < 20; iCol+=3){
                    int eventnum = -1;
                    if(submax < eventmax){
                        eventmax = submax;
                    }
                    std::ifstream input( file );
                    int linenum = 0;
                    for( std::string line; getline( input, line ); ){
                        std::vector<std::string> inLine = auxstring::Tokenize(line,' ');

                        linenum++;
                        if(linenum > 512){
                            //After 512 lines, spybuffer is repeated
                            //Remove last event as may be incomplete
                            if(!m_RoadsInput[proc-1].empty()){
                                if(iCol == 10){
                                    eventmax = eventmax-1;
                                } else {
                                    submax = submax-1;
                                }
                                break;
                            }
                        }

                        //TODO treat overflows
                        if(inLine[0] == "[last]:" || inLine[0] == "[ovfl]:"){
                            continue;
                        }

                        if (iCol == 10){
                            if(inLine[iCol].substr(0,4)=="e0da"){
                                m_RoadsInput[proc-1].push_back(event);
                                eventnum += 1;
                                eventmax = eventnum;
                                seen_e0da[(iCol-10)/3] = true;     
                            } else if ((seen_e0da[(iCol-10)/3]) && (inLine[iCol]!="00000000")){
                                m_RoadsInput[proc-1][eventnum].push_back(std::stol(inLine[iCol], nullptr, 16)&0b11111111011111111111111111111111);//bit 9 is just end of event stuff
                            }
                        } else {
                            if(inLine[iCol].substr(0,4)=="e0da"){
                                eventnum += 1;
                                submax = eventnum;
                                seen_e0da[(iCol-10)/3] = true;
                            } else if ((seen_e0da[(iCol-10)/3]) && (eventnum <= eventmax) && (inLine[iCol]!="00000000")){
                                m_RoadsInput[proc-1][eventnum].push_back(std::stol(inLine[iCol], nullptr, 16)&0b11111111011111111111111111111111);
                            }

                        }
                    }
                }
                if(submax < eventmax){
                    eventmax = submax;
                }

                m_RoadsInput[proc-1].resize(eventmax);

            }
    
            void readProcs( std::string file, int proc)
            {
                
                std::ifstream input( file );
                std::string line;


                bool seen_b0b0 [13] = { false };
                bool L1ID [13] = { false };
                std::vector<std::vector<long>> out_event(13,std::vector<long>(0)) ; //first 6 are pix out, next 5 sct out, next outlaysec, next outputroads
                for( std::string line; getline( input, line ); ){
                    std::vector<std::string> inLine = auxstring::Tokenize(line,' ');

                    //TODO do I need to treat overflow?

                    if(inLine[6] == "YES" ){//due to whitespace, this is where "YES" occurs
                        m_ProcOverflow[proc-1] = true;
                        continue;
                    } else if (inLine[7] == "NO" ){//TODO I think this is where "NO" will occur
                        m_ProcOverflow[proc-1] = false;
                        continue;
                    } else if (inLine[0] == "[last]:") {
                        continue;
                    } else {
                        for(unsigned int iCol = 0; iCol < inLine.size(); iCol++){
                            //Columns 1, 4, 7... have data
                            if((iCol+2) % 3 != 0)  continue;

                            if(inLine[iCol]=="b0b00d01"){
                                if(seen_b0b0[(iCol-1)/3]){
                                    if(iCol < 17){
                                        m_PIXOutput[proc-1][(iCol-1)/3].push_back(out_event[(iCol-1)/3]);
                                    } else if(iCol < 32){
                                        m_SCTOutput[proc-1][(iCol-19)/3].push_back(out_event[(iCol-1)/3]);
                                    } else if(iCol == 37){
                                        m_RoadsOutput[proc-1].push_back(out_event[(iCol-1)/3]);
                                    } else if (iCol == 34){
                                        m_LaySecOutput[proc-1].push_back(out_event[(iCol-1)/3]);
                                    }
                                }
                                out_event[(iCol-1)/3].clear();
                                seen_b0b0[(iCol-1)/3] = true;
                            }


                            //data columns 0-5 are pix output, 6-10 are sct output, 11 is outlaysec, 12 is output roads
                            //Corresponds to 1-16 pix, 19-31 sct, 37 outlaysec, 34 output roads

                            if(inLine[iCol]!="b0b00d01" and not L1ID[(iCol-1)/3]){//get rid of MSB on hits to match to input format
                                if(iCol < 17){
                                    out_event[(iCol-1)/3].push_back(std::stol(inLine[iCol], nullptr, 16)&0x7fffffff);
                                } else if(iCol < 32){
                                    out_event[(iCol-1)/3].push_back(std::stol(inLine[iCol], nullptr, 16)&0x7fffffff);
                                } else if(iCol == 34){
                                    out_event[(iCol-1)/3].push_back(std::stol(inLine[iCol], nullptr, 16));
                                } else if (iCol == 37){
                                    out_event[(iCol-1)/3].push_back(std::stol(inLine[iCol], nullptr, 16)&0b11111111011111111111111111111111);//bit 9 is just end of event stuff
                                }
                            } else {//leave MSB as is for b0b00d01, L1ID
                                if(iCol < 17){
                                    out_event[(iCol-1)/3].push_back(std::stol(inLine[iCol], nullptr, 16));
                                } else if(iCol < 32){
                                    out_event[(iCol-1)/3].push_back(std::stol(inLine[iCol], nullptr, 16));
                                } else if(iCol == 34){
                                    out_event[(iCol-1)/3].push_back(std::stol(inLine[iCol], nullptr, 16));
                                } else if (iCol == 37){
                                    out_event[(iCol-1)/3].push_back(std::stol(inLine[iCol], nullptr, 16));
                                } 

                            }

                            //If beginning of event word, next word in column is L1ID
                            if(inLine[iCol]=="b0b00d01"){
                                L1ID[(iCol-1)/3]=true;
                            } else {
                                L1ID[(iCol-1)/3]=false;
                            }
                        }
                    }

                }
                
            }

            //Check for overlap when in loop mode (one event on a loop). This just means checking we have a whole event.
            int overlapping_loop(int iProc, bool * found_error)
            {
                cout << "Running on a loop, so ignoring L1IDs" << endl;
                int num_events = std::min({m_SCTInput[0].size(), m_SCTInput[1].size(), m_SCTInput[2].size() , m_SCTInput[3].size(), m_SCTInput[4].size(), m_PIXInput[0].size(),m_PIXInput[1].size(),m_PIXInput[2].size(),m_PIXInput[3].size(),m_PIXInput[4].size(),m_PIXInput[5].size(), m_SCTOutput[iProc][0].size(), m_SCTOutput[iProc][1].size(), m_SCTOutput[iProc][2].size() , m_SCTOutput[iProc][3].size(), m_SCTOutput[iProc][4].size(), m_PIXOutput[iProc][0].size(),m_PIXOutput[iProc][1].size(),m_PIXOutput[iProc][2].size(),m_PIXOutput[iProc][3].size(),m_PIXOutput[iProc][4].size(),m_PIXOutput[iProc][5].size(),m_RoadsOutput[iProc].size(),m_LaySecOutput[iProc].size()});
                if(num_events==0){
                    cout << "ERROR: NO EVENTS IN AT LEAST ONE STREAM OF PROC " << iProc+1 << endl;
                    *found_error = true;
                }
                return num_events;
            }

            //Check for overlap when not in loop mode. Check L1IDs of each stream. Returns first and last L1ID.
            std::pair<int,int> overlapping(int iProc, bool *found_error)
            {
                int first = 0;
                int last = 2147483647;//highest int can go

                for (u_int i = 0; i < m_PIXInput.size(); i++) {//loop over plane
                    for (u_int j = 0; j < m_PIXInput[i].size(); j++) {//loop over event
                        if (m_PIXInput[i][j][3]>first){//L1ID is 4th word
                            first=m_PIXInput[i][j][3];
                        }
                        if (m_PIXInput[i][j][3]<last){
                            last=m_PIXInput[i][j][3];
                        }
                    }
                }
                for (u_int i = 0; i < m_SCTInput.size(); i++) {//loop over plane
                    for (u_int j = 0; j < m_SCTInput[i].size(); j++) {//loop over event
                        if (m_SCTInput[i][j][3]>first){//L1ID is 4th word
                            first=m_SCTInput[i][j][3];
                        }
                        if (m_SCTInput[i][j][3]<last){
                            last=m_SCTInput[i][j][3];
                        }
                    }
                }
                for (u_int i = 0; i < m_SCTOutput[iProc].size(); i++) {//loop over plane
                    for (u_int j = 0; j < m_SCTOutput[iProc][i].size(); j++) {//loop over event
                        if (m_SCTOutput[iProc][i][j][1]>first){//L1ID is 2nd word
                            first=m_SCTOutput[iProc][i][j][1];
                        }
                        if (m_SCTOutput[iProc][i][j][1]<last){
                            last=m_SCTOutput[iProc][i][j][1];
                        }
                    }
                }
                for (u_int i = 0; i < m_PIXOutput[iProc].size(); i++) {//loop over plane
                    for (u_int j = 0; j < m_PIXOutput[iProc][i].size(); j++) {//loop over event
                        if (m_PIXOutput[iProc][i][j][1]>first){//L1ID is 2nd word
                            first=m_PIXOutput[iProc][i][j][1];
                        }
                        if (m_PIXOutput[iProc][i][j][1]<last){
                            last=m_PIXOutput[iProc][i][j][1];
                        }
                    }
                }
                for (u_int i = 0; i < m_RoadsOutput[iProc].size(); i++) {//loop over event
                    if (m_RoadsOutput[iProc][i][1]>first){//L1ID is 2nd word
                        first=m_RoadsOutput[iProc][i][1];
                    }
                    if (m_RoadsOutput[iProc][i][1]<last){
                        last=m_RoadsOutput[iProc][i][1];
                    }
                }
                for (u_int i = 0; i < m_LaySecOutput[iProc].size(); i++) {//loop over event
                    if (m_LaySecOutput[iProc][i][1]>first){//L1ID is 2nd word
                        first=m_LaySecOutput[iProc][i][1];
                    }
                    if (m_LaySecOutput[iProc][i][1]<last){
                        last=m_LaySecOutput[iProc][i][1];
                    }
                }

                if(first>last){
                    cout << "ERROR, NO OVERLAP ON PROC " << iProc+1 << endl;
                    return std::make_pair(-1,-1);
                    *found_error = true;
                }
                return std::make_pair(first, last);
            }

            //Emulate when in loop mode (one event on a loop). Just take the first event from each stream, then the second, etc.
            DataOrganizer emulate_loop(int num_events, string ambank, int proc, bool oldDC, bool allSectors, int inChipOff, bool verbose, bool found_error){
                DataOrganizer myDO;
                myDO.SetError(found_error);
                myDO.SetVerbose(verbose);

                for (u_int i =0; i < num_events; i++){

                    //Add input and board information
                    myDO.addInBoard(m_SCTInput[0][i], m_SCTInput[1][i], m_SCTInput[2][i], m_SCTInput[3][i], m_SCTInput[4][i], m_PIXInput[0][i], m_PIXInput[1][i], m_PIXInput[2][i], m_PIXInput[3][i], m_PIXInput[4][i], m_PIXInput[5][i], m_LaySecOutput[proc][i], m_SCTOutput[proc][0][i], m_SCTOutput[proc][1][i], m_SCTOutput[proc][2][i],m_SCTOutput[proc][3][i],m_SCTOutput[proc][4][i],m_PIXOutput[proc][0][i], m_PIXOutput[proc][1][i], m_PIXOutput[proc][2][i], m_PIXOutput[proc][3][i], m_PIXOutput[proc][4][i], m_PIXOutput[proc][5][i]);

                    //Emulate a single event
                    myDO.emulate_event(m_SCTInput[0][i], m_SCTInput[1][i], m_SCTInput[2][i], m_SCTInput[3][i], m_SCTInput[4][i], m_PIXInput[0][i], m_PIXInput[1][i], m_PIXInput[2][i], m_PIXInput[3][i], m_PIXInput[4][i], m_PIXInput[5][i], m_RoadsOutput[proc][i], ambank, oldDC, allSectors, inChipOff, proc);
                }
                return myDO;
            }
            
            //Emulate when not in loop mode. TODO may need more testing as current spybuffers don't have L1ID overlap.
            DataOrganizer emulate(std::pair<int,int> first_last, string ambank, int proc, bool oldDC, bool allSectors, int inChipOff, bool verbose, bool found_error){

                DataOrganizer myDO;
                myDO.SetError(found_error);
                myDO.SetVerbose(verbose);

                //Find where first L1ID is
                int indices [ 24 ] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
                int event = 0;
            
                //Search until we've found where to start for each stream
                while ( indices[0] == -1 || indices[1] == -1 || indices[2] == -1 || indices[3] == -1 || indices[4] == -1 || indices[5] == -1 ||
                     indices[6] == -1 || indices[7] == -1 || indices[8] == -1 || indices[9] == -1 || indices[10] == -1 || indices[11] == -1 || 
                    indices[12] == -1 || indices[13] == -1 || indices[14] == -1 || indices[15] == -1 || indices[16] == -1 || indices[17] == -1 ||
                     indices[18] == -1 || indices[19] == -1 || indices[20] == -1 || indices[21] == -1 || indices[22] == -1 || indices[23] == -1) {

                    for (u_int j=0; j<m_SCTInput.size();j++){//loop over planes
                        if (indices[j]==-1){
                            if (m_SCTInput[j][event][3]==first_last.first){
                                indices[j]=event;
                            }
                        } 
                    }
                    for (u_int j=0; j<m_PIXInput.size();j++){//loop over planes
                        if (indices[j+5]==-1){
                            if (m_PIXInput[j][event][3]==first_last.first){
                                indices[j+5]=event;
                            }
                        }
                    }

                    if (indices[23]==-1){
                        if(m_RoadsOutput[proc][event][1]==first_last.first){
                            indices[23]=event;
                        }
                    }

                    if (indices[11]==-1){
                        if(m_LaySecOutput[proc][event][1]==first_last.first){
                            indices[11]=event;
                        }
                    }

                    for (u_int j=0; j<m_SCTOutput[proc].size();j++){//loop over planes
                        if (indices[j+12]==-1){
                            if (m_SCTOutput[proc][j][event][1]==first_last.first){
                                indices[j+12]=event;
                            }
                        }
                    }
                    for (u_int j=0; j<m_PIXOutput[proc].size();j++){//loop over planes
                        if (indices[j+17]==-1){
                            if (m_PIXOutput[proc][j][event][1]==first_last.first){
                                indices[j+17]=event;
                            }
                        }
                    }
                    
                    event++;
                }
                int num_events=first_last.second-first_last.first+1;
                for (u_int i =0; i < num_events; i++){
                    //Add input and board information
                    myDO.addInBoard(m_SCTInput[0][indices[0]+i], m_SCTInput[1][indices[1]+i], m_SCTInput[2][indices[2]+i], m_SCTInput[3][indices[3]+i], m_SCTInput[4][indices[4]+i], m_PIXInput[0][indices[5]+i], m_PIXInput[1][indices[6]+i], m_PIXInput[2][indices[7]+i], m_PIXInput[3][indices[8]+i], m_PIXInput[4][indices[9]+i], m_PIXInput[5][indices[10]+i], m_LaySecOutput[proc][indices[11]+i], m_SCTOutput[proc][0][indices[12]+i], m_SCTOutput[proc][1][indices[13]+i], m_SCTOutput[proc][2][indices[14]+i],m_SCTOutput[proc][3][indices[15]+i],m_SCTOutput[proc][4][indices[16]+i],m_PIXOutput[proc][0][indices[17]+i], m_PIXOutput[proc][1][indices[18]+i], m_PIXOutput[proc][2][indices[19]+i], m_PIXOutput[proc][3][indices[20]+i], m_PIXOutput[proc][4][indices[21]+i], m_PIXOutput[proc][5][indices[22]+i]);

                    //Emulate a single event
                    myDO.emulate_event(m_SCTInput[0][indices[0]+i], m_SCTInput[1][indices[1]+i], m_SCTInput[2][indices[2]+i], m_SCTInput[3][indices[3]+i], m_SCTInput[4][indices[4]+i], m_PIXInput[0][indices[5]+i], m_PIXInput[1][indices[6]+i], m_PIXInput[2][indices[7]+i], m_PIXInput[3][indices[8]+i], m_PIXInput[4][indices[9]+i], m_PIXInput[5][indices[10]+i], m_RoadsOutput[proc][indices[23]+i], ambank, oldDC, allSectors, inChipOff, proc);
                }
                return myDO;
            }

            void compare_roads(int iProc)
            {
                int num_road_evs = std::min({m_RoadsOutput[iProc].size(),m_RoadsInput[iProc].size()});
                if(num_road_evs==0){
                    cout << "ERROR: NO ROADS IN AT LEAST ONE STREAM OF PROC " << iProc+1 << endl;
                    //*found_error = true;
                }
                cout << "Checking roads" << endl;
                //Loop over events
                for (u_int event=0; event<num_road_evs; event++){
                    //Remove headers
                    
                    //Sorting
                    sort(m_RoadsOutput[iProc][event].begin()+2,m_RoadsOutput[iProc][event].end());
                    sort(m_RoadsInput[iProc][event].begin(),m_RoadsInput[iProc][event].end());
                    
                    cout << "Event " << event << endl;
                    if (m_RoadsOutput[iProc][event].size()-2!=m_RoadsInput[iProc][event].size() || !std::equal(m_RoadsInput[iProc][event].begin(),m_RoadsInput[iProc][event].end(),m_RoadsOutput[iProc][event].begin()+2)){
                        cout << "V########################V" << endl;
                        cout << "V Error in Roads Stream V" << endl;
                        cout << "V########################V" << endl;
                    
                        cout << "Input Roads :: Output Roads" << endl;
                        cout << std::hex;
                        for (u_int i=0; i<max(m_RoadsInput[iProc][event].size(),m_RoadsOutput[iProc][event].size()-2); i++){
                            if (i<m_RoadsInput[iProc][event].size()){
                                cout << std::setfill('0') << setw(8) << m_RoadsInput[iProc][event][i];//&0b11111111011111111111111111111111
                            } else {
                                cout << "        ";
                            }
                            if (i<m_RoadsOutput[iProc][event].size()-2){
                                cout << " :: " << std::setfill('0') << setw(8) << m_RoadsOutput[iProc][event][i+2];
                                if (i<m_RoadsInput[iProc][event].size()){
                                    if(m_RoadsOutput[iProc][event][i+2]!=m_RoadsInput[iProc][event][i]){
                                        cout << "   <<<";
                                    }
                                }
                            }    
                            cout << endl;                        
                        }
                    }
                }
            }

        };

    }//namespace ftk
}//namespace daq

#endif

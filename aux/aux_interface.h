#ifndef AUX_INTERFACE_H
#define AUX_INTERFACE_H

#include <iostream>
#include <fstream>
#include <vector>

#include "ftkvme/VMEManager.h"

#include "aux/aux_vme_regs.h"
#include "aux/aux.h"

using namespace std;

namespace daq
{
  namespace ftk
  {

    class aux_interface
    {

    public:

      aux_interface(uint _slot, uint _fpga, std::string _name);
      virtual ~aux_interface() noexcept;

      uint get_fpga_idx();

      //// Run Control FSMs ////
      /*! \brief configure transition
       *
       * 1. Read firmware version
       * 2. Reset everything
       */
      virtual void configure();


      /*! \brief connect transition
       *
       * 1. Redo transceiver sync
       * 2. Load all RAMs
       */
      virtual void connect();

      /*! \brief prepareForRun transition
       *
       * 1. Reset error counters
       * 2. Reset freeze
       * 3. Reset spybuffers
       */
      virtual void prepareForRun();

      //// Useful functions ////
      void reset_all();

      void reset_main();
      void reset_spybuffers();
      void reset_error_counters();
      void reset_error_latches();
      void reset_freeze();
      void reset_xcvr();
      void reset_sync();
      void reset_tx_fifos();
      void reset_regs();

      virtual void reset_logic();

      virtual void check_freeze();
      uint read_version();
      
      void check_fw_versions(); 
      virtual void set_requireMatch(bool requireMatch);
      virtual void set_firmware(uint firmware);

      virtual void set_checksumWarning(bool warningOnly);
      virtual void load_rams(bool isLoadMode);
      //virtual bool check_link_status();

      /*! \brief Controls data-flow from upstream
       *
       * Should be inherited by the different FPGA subclasses 
       * to set the correct bits.
       *
       * \param enable Enable input dataflow.
       */
      virtual void enable_upstream_rx(bool enable);
      virtual void set_upstream_flow(bool enable);

      /*! \brief Controls data-flow to downstream
       *
       * Should be inherited by the different FPGA subclasses 
       * to set the correct bits.
       *
       * \param enable Enable dataflow.
       */
      virtual void set_downstream_flow(bool enable);

      /*! \brief Set the event rate for TxFIFOs
       *        
       * \param event_rate New event rate in kHz
       */
      void set_event_rate(uint event_rate);


      /*! \brief Disable block writes to VME.
       */
      void disable_block_writes(bool v = true);


      /*! \brief Set register to enable freeze signal from AMB
       *
       * \param enable New value for control register
       */
      void enable_amb_freeze(bool ignore);
      
      //name for ERS log
      std::string name_ftk() { return m_name; }
      
      //only load rams when isLoadMode = true
      void set_isLoadMode(bool loadmode);

      VMEInterface *m_vme;

    protected:

      uint m_slot;
      uint m_fpga;
      uint m_event_rate;
      uint m_firmware;
      bool m_isLoadMode;
      std::string m_name;
      bool m_requireMatch;

    };

  }
}

#endif

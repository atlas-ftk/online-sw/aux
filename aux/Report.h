#ifndef REPORT_H_
#define REPORT_H_

#include <iostream>
#include <string>
#include <stdarg.h>
#include <stdlib.h>

#include "aux/AuxString.h"

namespace report
{
  const bool suppress_output = false;
  extern unsigned int dbg_lvl;

  /*! \brief Print a message for a tag
   *
   * \param tag the tag to display
   * \param fmt message to display
   * \param ap printf modifiers to fmt
   * \param incl_space end with end-of-line
   */
  void vreport(const std::string& tag , const char *fmt , va_list ap , bool incl_space = true );

  /*! \brief Print a message for a tag
   *
   * \param tag the tag to display
   * \param str message to display
   * \param incl_space end with end-of-line
   */  
  void vreport(const std::string& tag , const std::string& str );

  /*! \brief Print message for a tag in color 
   *
   * \param color bash-style color description
   * \param tag the tag to display
   * \param fmt message to display
   * \param ap printf modifiers to fmt
   * \param incl_space end with end-of-line
   */
  void vreport_color(const std::string& color , const std::string& tag , const char *fmt , va_list ap , bool incl_space = true );

  /*! \brief Print message for a tag in color 
   *
   * \param color bash-style color description
   * \param tag the tag to display
   * \param str message to display
   * \param incl_space end with end-of-line
   */
  void vreport_color(const std::string& color , const std::string& tag , const std::string& str );

  /*! \brief Print message using INFO tag
   *
   * \param fmt message
   * \param ... printf modifiers to fmt
   */
  void info(const char *fmt, ... );

  /*! \brief Print message using INFO tag
   *
   * \param str message to display
   */  
  void info(const std::string& str);

  /*! \brief Print message using EXAM tag
   *
   * \param fmt message
   * \param ... printf modifiers to fmt
   */
  void exam(const char *fmt, ... );

  /*! \brief Print message using EXAM tag
   *
   * \param str message to display
   */  
  void exam(const std::string& str);

  /*! \brief Print message using WARN tag
   *
   * \param fmt message
   * \param ... printf modifiers to fmt
   */
  void warn(const char *fmt, ... );

  /*! \brief Print message using WARN tag
   *
   * \param str message to display
   */  
  void warn(const std::string& str);

  /*! \brief Print message using ERROR tag
   *
   * \param fmt message
   * \param ... printf modifiers to fmt
   */
  void error(const char *fmt, ... );

  /*! \brief Print message using ERROR tag
   *
   * \param str message to display
   */  
  void error(const std::string& str);

  /*! \brief Print message using without a tag
   *
   * \param fmt message
   * \param ... printf modifiers to fmt
   */
  void blank(const char *fmt, ... );

  /*! \brief Print message using without a tag
   *
   * \param str message to display
   */  
  void blank(const std::string& str);

  /*! \brief Print message using DEBUG tag
   *
   * Debug level is not checked.
   *
   * \param fmt message
   * \param ... printf modifiers to fmt
   */
  void debug(const char *fmt, ... );

  /*! \brief Print message using DEBUG tag
   *
   * Debug level is not checked.
   *
   * \param str message to display
   */  
  void debug(const std::string& str);

  /*! \brief Print end-of-line
   */
  void line();

  /*! \brief Print message with a blank text
   *
   * \param fmt message
   * \param ... printf modifiers to fmt
   */  
  void spaceprint(const char *fmt , ... );

  /*! \brief Print message using DEBUG tag
   *
   * The message is printed only if lvl is bigger than the dbg_lvl
   * define at compilation time.
   *
   * \param lvl Debug 
   * \param fmt message
   * \param ... printf modifiers to fmt
   */    
  void debug(int lvl, const char *fmt, ... );

  /*! \brief Print warning only if a confition is met.
   *
   * Message is displayed with the WARN tag
   *
   * \param cond Condition that must be satisfied to print the warning
   * \param fmt message
   * \param ... printf modifiers to fmt
   */
  void warnif(bool cond, const char *fmt, ... );

  /*! \brief Print a message if a condition fails and throw an assert
   *
   * \param cond Condition being checked.
   * \param fmt message
   * \param ... printf modifiers to fmt
   */
  void printassert(bool cond, const char* fmt, ... );

  /*! \brief Print a title header
   *
   * Title header constists of an INFO message and EXAM message.
   *
   * \param _info info message
   * \param _exam exam message
   */
  void title(const char *_info, const char *_exam );

  /*
   * Function for keeping track of memory usage
   * Doesn't work without dump_mem.sh script
   *
  void memory( std::string jobname ) {
    std::string script = tfstring::Format( ". scripts/dump_mem.sh %s" , jobname.c_str() );
    system( script.c_str() );
    std::cout << std::endl;
  }
  */

  
}

#endif // REPORT_H_

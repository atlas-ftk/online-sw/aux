#!/usr/bin/env python

import os, sys, time
import commands
from optparse import OptionParser

fw = {}

fw["factory"] = ["/tbed/firmware/factory/Input1/Rx",
                 "/tbed/firmware/factory/Input2/Rx",
                 "/tbed/firmware/factory/Processor/Rx"]

fw["04.2"] = ["/eos/atlas/atlascerngroupdisk/det-ftk/FW_repo/FW-04.2/AUX/Input1/Rx","/afs/cern.ch/work/t/tseiss/public/fw/fw04.1/Input2_SliceA/Rx","/eos/atlas/atlascerngroupdisk/det-ftk/FW_repo/FW-04.2/AUX/Processor/Rx"]

fw["05"] = ["/eos/atlas/atlascerngroupdisk/det-ftk/FW_repo/FW-05/AUX/Input1/Rx","/eos/atlas/atlascerngroupdisk/det-ftk/FW_repo/FW-05/AUX/Input2_SliceA/Rx","/eos/atlas/atlascerngroupdisk/det-ftk/FW_repo/FW-05/AUX/Processor/Rx"]

def run(line):
  print line
  os.system(line)


def upgrade(slot, procs, tag, fwpath, dir, doFactory):

  from reconfigureBoardAux import reconfigure_board
  #### to upgrade the factory firmare two lines must be switched. DONT DO THIS LIGHTLY
  #### print "DANGER, DANGER, NOT RECONFIGURING!!!"
  reconfigure_board(slot, [str(x) for x in procs], True)
  #reconfigure_board(slot, [str(x) for x in procs], False)
  
  fwpaths = ""
  if tag is not None:
    if tag not in fw:
      print "Tag", tag, "not in available firmwares."
      sys.exit(0)
    else: fwpaths = fw[tag]

  elif fwpath is not None:
    fwpaths = fwpath


  base = (slot << 3)

  for v in fwpaths: 
    status, output = commands.getstatusoutput("dos2unix %s.map" %(dir+v))#run("dos2unix %s.map" % v)
    print "dos2unix %s.map" %(dir+v)
    print output
   # if "problems converting file" in output: exit()
  for x in procs: run("vme_poke 0x%x000038 0x1" % (base + x))

  with open("upgrade", "w") as out:

    if 1 in procs: out.write("1 %s\n" % (dir + fwpaths[0]))
    if 2 in procs: out.write("2 %s\n" % (dir + fwpaths[1]))
    if 3 in procs: out.write("3 %s\n" % (dir + fwpaths[2]))
    if 4 in procs: out.write("4 %s\n" % (dir + fwpaths[2]))
    if 5 in procs: out.write("5 %s\n" % (dir + fwpaths[2]))
    if 6 in procs: out.write("6 %s\n" % (dir + fwpaths[2]))

  time.sleep(1)

  if doFactory:
    run("aux_epcq_upgrade_board_main --slot %d --epcqaddress 0x0 --file upgrade" % slot)
  else:
    run("aux_epcq_upgrade_board_main --slot %d --epcqaddress 0x1000000 --file upgrade" % slot)


if __name__ == "__main__":

  #  Configuration
  parser = OptionParser()
  parser.add_option('-s', '--slot',  type = "int",    default = 11, help="slot")
  parser.add_option('-f', '--fw',    type = "string")
  parser.add_option('-t', '--fwtag',    type = "string")
  parser.add_option('-p', '--procs', type = "string", default = "1,2,3,4,5,6")
  parser.add_option('-d', '--dir',   type = "string", default = "")
  parser.add_option('--doFactory',   action="store_true", default = False)
  o, a = parser.parse_args()

  procs = []
  for letter in o.procs:
    if letter in "123456":
      procs.append(int(letter))
  
  if o.fw is None and o.fwtag is None:
      print "You must pass in a tag! Possible tags include: "
      for k in fw.keys():
          print " \t",k
      print "or your own in the following format: /path/to/input1/Rx, /path/to/input2/Rx, /path/to/processor/Rx" 
      sys.exit(0)
  if o.fw is not None and o.fwtag is not None:
      print "Either give a predefined firmware tag OR a custom tag"
      sys.exit(0)


  #fwtags = []
  fw= ''
  if o.fwtag is not None: fwtags = o.fwtag.split(',')
  else: fwtags = []

  if o.fw is not None: fw = o.fw
  
  sys.exit(upgrade(o.slot, procs, fw, fwtags, o.dir, o.doFactory))



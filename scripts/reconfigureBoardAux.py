#!/usr/bin/env python

import optparse
import os
import sys
from time import sleep

# 
#  Configuration
#
def reconfigure_board(slot, fpga_list, doFactory):

  print  "Reconfig fpgas", fpga_list
  
  for fpga in fpga_list:
      addr = (int(slot) << 27) + (int(fpga) << 24) + 0x38
      os.system("vme_poke "+hex(addr)+" 0x1")
      if doFactory:
          print "aux_reconfigure_main --fpga "+fpga+" --slot "+str(slot)+" --epcqaddress 0x0 --dontWait"
          os.system("aux_reconfigure_main --fpga "+fpga+" --slot "+str(slot)+" --epcqaddress 0x0 --dontWait")
      else:
          print "aux_reconfigure_main --fpga "+fpga+" --slot "+str(slot)+" --epcqaddress 0x1000000 --dontWait"
          os.system("aux_reconfigure_main --fpga "+fpga+" --slot "+str(slot)+" --epcqaddress 0x1000000 --dontWait")
  
  print  "Waiting on firmware...."
  print  "Finished:"
  
  doneList = []
  attempts = 0
  titles=['Input 1    ','Input 2    ','Processor 1','Processor 2','Processor 3','Processor 4']
  nfpga = len(fpga_list)
  while (len(doneList) < nfpga and attempts < 200):

    for fpga in fpga_list:
      addr = (int(slot) << 27) + (int(fpga) << 24)      
      output=os.popen("vme_peek "+hex(addr)).readlines()
      version=output[0].strip()

      chipName = titles[int(fpga)-1]
      if version[-2:]=='a%s'%fpga:
        doneList.append(chipName)
        fpga_list.remove(fpga)
        print  "\t ",chipName,version

    attempts += 1
    sleep(0.2)

  if attempts == 200: print "Tried 200 times, did not reconfigure."

  print 
  

if __name__ == "__main__":

  #  Configuration
  parser = optparse.OptionParser()
  parser.add_option('-s', '--slot',      default=11, help="slot")
  parser.add_option('-f', '--fpgaList',  default="1,2,3,4,5,6", help="comma separate list of fpgas ")
  parser.add_option('--doFactory',       action="store_true", default=False, help="load the factory ")
  o, a = parser.parse_args()
  
  #fpga_list = o.fpgaList.split(",")
  fpga_list = []
  for letter in o.fpgaList:
    if letter in "123456":
      fpga_list.append(letter)

  sys.exit(reconfigure_board(o.slot, fpga_list, o.doFactory))



#!/bin/bash

TAG=$(date +%Y%m%d_%k%M%S)

mkdir dump_${TAG}
cd dump_${TAG}

aux_status_main --slot 10 > aux_status.log
aux_linkstatus_main --slot 10 > aux_linkstatus.log
aux_linkerrors_main --slot 10 > aux_linkerrors.log
aux_proc_status_main --slot 10 > aux_proc_status.log
aux_fifostatus_main --slot 10 > aux_fifostatus.log

mkdir spy_output
aux_dump_buffers --slot 10 --over_ride_freeze

cd ..
tar -cvjf dump_${TAG}.tar.bz2 dump_${TAG}
echo scp atlasgw:$(pwd)/dump_${TAG}.tar.bz2 .
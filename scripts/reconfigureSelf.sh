#!/bin/bash

if [ "$HOSTNAME" = "sbc-tbed-ftk-02" ]; then
    SLOTS=("15" "19")
    echo "LAB4 SBC02"

else
    RCC=${HOSTNAME##*-} # Gets index of the host

    if [ "$RCC" = "01" ]; then
        SLOTS=("2" "3" "4" "5" "7" "8" "9" "10" "12" "13" "14" "15" "17" "18" "19" "20")
        echo "RCC1"
    else if [ ${RCC} = "02" ]; then
        SLOTS=("2" "3" "4" "5" "7" "8" "9" "10" "12" "13" "14" "15" "17" "18" "19" "20")
        echo "RCC2"
    else if [ ${RCC} = "03" ]; then
        SLOTS=("7")
        echo "RCC3"
    else if [ ${RCC} = "04" ]; then
        SLOTS=("2" "3" "4" "5")
        echo "RCC4"
    else if [ ${RCC} = "05" ]; then
        SLOTS=("2" "4" "7" "9" "12" "14" "17" "19")
        echo "RCC5"
    else if [ ${RCC} = "06" ]; then
        SLOTS=("2" "4" "7" "9" "12" "14" "17" "19")
        echo "RCC6"
    else if [ ${RCC} = "07" ]; then
        SLOTS=("2" "4" "7" "9" "12" "14" "17" "19")
        echo "RCC7"
    else if [ ${RCC} = "08" ]; then
        SLOTS=("2" "4" "7" "9" "12" "14" "17" "19")
        echo "RCC8"
    else
        echo "Invalid Host"
        return
    fi;fi;fi;fi;fi;fi;fi;fi;
fi;


NSLOTS=${#SLOTS[@]}
for(( i=0; i<${NSLOTS}; i++));
do
	echo "RECONFIGURING BOARD IN SLOT " ${SLOTS[$i]}
	reconfigureBoardAux.py --slot ${SLOTS[$i]} 
done

